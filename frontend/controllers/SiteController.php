<?php

namespace frontend\controllers;

use common\models\Log;
use common\models\LoginForm;
use common\models\SendEmailForm;
use common\models\User;
use common\models\Visit;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout', 'logout', 'entrance', 'reset-password', 'send-email', 'auth', 'log'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout', 'auth', 'log'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                   /* [
                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'roles' => ['admin'],
                    ],*/
                    [
                        'allow' => true,
                        'actions' => ['about', 'index', 'logout', 'reset-password', 'send-email', 'send-email'],
                        'roles' => ['user'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//    public function actionIndex()
//    {
//        return $this->render('index');
//    }

    public function beforeAction($action)
    {
        if ($action->id == 'auth' || $action->id == 'log') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * login for SM users
     */
    public function actionLog()
    {
        if ($tocken = Yii::$app->request->post('tocken')) {
            $user = User::findOne(['verification_token' => $tocken]);
            if ($user != null) {
                $model = new LoginForm(['scenario' => 'loginApi']);
                $model->email = $user->email;
                $user->verification_token = null;
                $user->save(false);

                $model->login();
                Log::setLog('Вошел в систему', null);

                return $this->goBack();
            } else {
                return $this->redirect(['not-visits']);
            }
        }
    }

    public function actionAuth()
    {
        $this->enableCsrfValidation = false;
        if ($request = Yii::$app->request->post('RM-SB-Authorization')) {
            if ($request == '7(D^xssi|FMga!UC0o^*@A9-*;>Ol{+MliS(|_Qa,Q_lNt2S[$>T*k,q/Vy%V2t' && !empty($email = Yii::$app->request->post('email'))) {
                $user = User::findByEmail($email);
                if ($user) {
                    $user->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
                    if ($user->save(false)) {
                        return $user->verification_token;
                    }

                }
            }
        } else {
                return $this->redirect(['not-visits']);
//            throw new \yii\web\HttpException(401, 'Unauthorized');
        }
    }

    public function actionNotVisits()
    {
        $this->layout = 'empty';
        return $this->render('notVisits');
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        $loginWithEmail = Yii::$app->params['loginWithEmail'];

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = $loginWithEmail ? new LoginForm(['scenario' => 'loginWithEmail']) : new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Log::setLog('Вошел в систему', null);

            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionWaiting()
    {
        $this->layout = 'login';

        return $this->render('waiting');
    }

    public function actionSendEmail($email = null)
    {
        $this->layout = 'login';

        $model = new SendEmailForm();

        if ($email != null) {
            $model->email = $email;
            if ($model->sendEmail()) {
                return $this->render('waiting');
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('warning', 'Проверьте емейл.');
                return $this->redirect(['/site/waiting']);
//                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Невозможно сбросить пароль.');
            }
        }

        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Log::setLog('Вышел из системы', null);

        Yii::$app->user->logout();
        Yii::$app->session->destroy();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token = null)
    {
        $this->layout = 'login';

        try {
            $model = new \common\models\ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $user = User::findOne(['password_reset_token' => $token]);
                $model->resetPassword();
                Yii::$app->getSession()->setFlash('warning', 'Пароль успешно изменен.');
                $user->is_password_changed = 1;
                $user->save(false);

                return $this->redirect('/site/login');
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    public function actionSetpassword()
    {
        ini_set('max_execution_time', '900');

        $users = User::find()->where(['role' => 'user'])->all();
        foreach ($users as $user) {
            $user->password_hash = Yii::$app->security->generatePasswordHash($user->email);
            $user->is_password_changed = 0;
            $user->save(false);
        }
        die('eof');
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
