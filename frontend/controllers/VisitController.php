<?php

namespace frontend\controllers;

use common\models\Slider;
use Yii;
use common\models\Visit;
use common\models\VisitSearch;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VisitController implements the CRUD actions for Visit model.
 */
class VisitController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }


    // вспливающие модальные окошки с информацией
    public function actionModal($page)
    {
        $times = Yii::$app->request->cookies->getValue($page . '-times') ?? 0;
        if ($times >= 3) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400 * 30,
            ]));
        } else {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400,
            ]));
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-times',
                'value' => $times + 1,
                'expire' => time() + 86400 * 30,
            ]));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Lists all Visit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);

        $dataProvider->sort->defaultOrder = ['visit_date' => SORT_DESC];

//        $this->debug($dataProvider->getTotalCount());

        $sliders = Slider::findAll(['status' => 1]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sliders' => $sliders,
        ]);
    }

    public function actionPayment()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('payment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayed()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->andWhere(['in', 'is_payed', ['Payed', 'Charity']]);

        $dataProvider->sort->defaultOrder = ['visit_date' => SORT_DESC];



//$this->debug(Yii::$app->request->queryParams);


//        $rowsModel = new RowForm();
//
//        $rowsModel->rows = Yii::$app->session->get('payed-rows', 10);
//        Yii::$app->session->set('payed-rows', $rowsModel->rows);
//
//        if ($rowsModel->load(Yii::$app->request->post())) {
//            Yii::$app->session->set('payed-rows', $rowsModel->rows);
//        }


        //входящий GET запрос и забираем дату
        if ($request = Yii::$app->request->get()) {
            if (isset($request['Select']['date'])) {
                list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                Yii::$app->session->set('payed-start-date', date('Y-m-d', strtotime($startDate)));
                Yii::$app->session->set('payed-end-date', date('Y-m-d', strtotime($endDate)));
            }
        }

//        list($startDate, $endDate) = $this->datesForPage('payed');

//        if ($filter != 'Все') {
//            $visit = Visit::find()
//                ->where(['user_id' => $this->userSecurityId])
//                ->andWhere(['is_payed' => $filter])
//                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//                ->orderBy("$sort $order");
//            $countQuery = clone($visit);
//            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => Yii::$app->session->get('payed-rows', 10)]);
//            $visit = $countQuery
//                ->offset($pages->offset)
//                ->limit($pages->limit)
//                ->all();
//        } else {
//            $visit = Visit::find()
//                ->where(['user_id' => $this->userSecurityId])
//                ->andWhere(['in', 'is_payed', ['Payed', 'Charity']])
//                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//                ->orderBy("$sort $order");
//            $countQuery = clone($visit);
//            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
//            $visit = $countQuery
//                ->offset($pages->offset)
//                ->limit($pages->limit)
//                ->all();
//        }

//        $filterModel = new Select();
//        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        $summaryData = array();
        foreach ($dataProvider->getModels() as $item) {
            if ($item->payment_status == 'Charity') {
                @$summaryData['charity'] += $item->total_amount;
            }
            if ($item->is_payed == 'Payed') {
                @$summaryData['payed'] += $item->total_amount;
            }
            $summaryData['bonuses'] = 0; //Visit::find()
        }

        $table_total = 0;
        foreach ($dataProvider->getModels() as $item) {
            @$table_total += $item->total_amount;
        }

        return $this->render('payed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

//            'filter' => $filter,
//            'filterModel' => $filterModel,
//            'rowsModel' => $rowsModel,
            'summaryData' => $summaryData,
            'table_total' => $table_total,
        ]);

    }

    /**
     * Displays a single Visit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Visit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Visit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Visit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Visit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
