<?php

namespace frontend\controllers;

use common\models\UploadForm;
use common\models\User;
use common\models\Visit;
use common\models\VisitSearch;
//use common\models\Wallet;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

//use PHPExcel_Settings;
//use PHPExcel_Writer_PDF;
use mpdf\mPDF;
//use PHPExcel_Settings;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;

/**
 * Site controller
 */
class ExcelController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'import-excel', 'excel', 'export-excel', 'export-pdf'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['*'],
                        'roles' => ['?'],
                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'charity'
//                        ],
//                        'roles' => ['admin'],
//                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'import-excel', 'excel', 'export-excel', 'export-pdf'
                        ],
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionImportExcel($file = null)
    {
        $session = Yii::$app->session;

        //для проверки на дубликаты
        $DBvisits = Visit::find()->all();

        $file = 'uploads/' . $file;

        try {
            $importFileType = \PHPExcel_IOFactory::identify($file);
            $objReader = \PHPExcel_IOFactory::createReader($importFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error');
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        if ($highestColumn !== 'O') {
            return $this->redirect(Yii::$app->request->referrer);
        }

        for ($row = 1; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row . null, true, false);

//            //проверка на дубликаты
            $duplicateFound = false;

            foreach ($DBvisits as $item) {
                if ($item->anketa_id == $rowData[0][1] &&
                    $item->user_id == $rowData[0][2] &&
                    $item->project == $rowData[0][3] &&
                    $item->location == $rowData[0][4]) {
                    $duplicateFound = true;
                }
            }

            if ($row == 1 || $duplicateFound) {
                continue;
            }

            $visit = new Visit();

            $visit->visit_date = date('Y.m.d', strtotime($rowData[0][0]));
            $visit->anketa_id = $rowData[0][1];
            $visit->user_id = $rowData[0][2];
            $visit->project = $rowData[0][3];
            $visit->location = $rowData[0][4];
            $visit->amount = $rowData[0][5];
            $visit->bonuse = $rowData[0][6];
            $visit->penalty = $rowData[0][7];
            $visit->extra_charge = $rowData[0][8];
            $visit->payment_method = (($rowData[0][9]) != 1) ? $rowData[0][9] : '';
            $visit->is_payed = (($rowData[0][10]) != 1) ? $rowData[0][10] : 'Не оплачен';
            $visit->last_modified = (($rowData[0][11] != 1)) ? date('Y.m.d', strtotime($rowData[0][11])) : date('Y.m.d', time());
            $visit->visit_status = (($rowData[0][12] != 1)) ? $rowData[0][12] : 'Проверка';
            $visit->payment_status = (($rowData[0][13] != 1)) ? $rowData[0][13] : '';
            $visit->deposit_term = (($rowData[0][14] != 1)) ? $rowData[0][14] : '45';

            $visit->save(false);

            $session->setFlash('excelAdded', 'Вы успешно добавили данные.');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionExcel()
    {
        $this->debug(Yii::$app->request->get());

        if ($request = Yii::$app->request->get('type')) {

            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];

            $is_payed_values = [
                'Not_payed' => 'Не оплачен',
            ];

            $visit_status_values = [
                'Completed' => 'Проверен',
                'Validation' => 'На проверке',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',

            ];

            $payment_status_values = [
                'Accumulation' => 'Накопление',
                'Withdrawal' => 'Вывод средств',
                'Charity' => 'Благотворительность',
                'Payed' => 'Оплачен',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',

            ];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

            switch ($request) {
                case 'index':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['!=', 'total_amount', 0])
                        ->andWhere(['is_payed' => 'Not_payed'])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Таблица визитов')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Статус визита')
                        ->setCellValue('J1', 'Накопительный бонус');

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
                        $row++;
                    }

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'accumulation':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['payment_status' => ['Accumulation']])
//                        ->andWhere(['visit_status' => ['Completed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = $query
                        ->sum('accumulated_bonuse');
                    $summaryData['total'] = $query
                        ->sum('accumulated_bonuse + total_amount');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Накопительный бонус')
                        ->setCellValue('J1', 'Итого с бонусом')
                        ->setCellValue('K1', 'Последнее изменение')
//                        ->setCellValue('L1', 'Статус оплаты')
                        ->setCellValue('L1', 'Срок депозита');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);


                        $startDate = strtotime($item->last_modified);
                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                        $now_date = time();
                        $datediff = $target_date - $now_date;
                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';


                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');

                        $row++;
                    }

                    $row++;
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $summaryData['bonuse']);

                    break;

                case 'payment':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Дата изменения статуса');
//                        ->setCellValue('K1', 'Статус платежа');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);

                    break;

                case 'payed':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Partial_payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Partial_payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Последнее изменение')
                        ->setCellValue('K1', 'Статус оплаты');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

            }
        } else {
            die('Не корректный запрос');
        }

        $filename = "Visits-" . date("d-m-Y-Hi") . ".xls";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');

        exit;
    }

    public function actionExportExcel()
    {
        if ($request = Yii::$app->request->get('type')) {

            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];

            $is_payed_values = [
                'Not_payed' => 'Не оплачен',
            ];

            $visit_status_values = [
                'Completed' => 'Проверен',
                'Validation' => 'На проверке',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',

            ];

            $payment_status_values = [
                'Accumulation' => 'Накопление',
                'Withdrawal' => 'Вывод средств',
                'Charity' => 'Благотворительность',
                'Payed' => 'Оплачен',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',

            ];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

            switch ($request) {
                case 'index':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['!=', 'total_amount', 0])
                        ->andWhere(['is_payed' => 'Not_payed'])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Таблица визитов')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Статус визита')
                        ->setCellValue('J1', 'Накопительный бонус');

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
                        $row++;
                    }

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'accumulation':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['payment_status' => ['Accumulation']])
//                        ->andWhere(['visit_status' => ['Completed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = $query
                        ->sum('accumulated_bonuse');
                    $summaryData['total'] = $query
                        ->sum('accumulated_bonuse + total_amount');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Накопительный бонус')
                        ->setCellValue('J1', 'Итого с бонусом')
                        ->setCellValue('K1', 'Последнее изменение')
//                        ->setCellValue('L1', 'Статус оплаты')
                        ->setCellValue('L1', 'Срок депозита');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);


                        $startDate = strtotime($item->last_modified);
                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                        $now_date = time();
                        $datediff = $target_date - $now_date;
                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';


                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');

                        $row++;
                    }

                    $row++;
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $summaryData['bonuse']);

                    break;

                case 'payment':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Дата изменения статуса');
//                        ->setCellValue('K1', 'Статус платежа');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);

                    break;

                case 'payed':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Partial_payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Partial_payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Последнее изменение')
                        ->setCellValue('K1', 'Статус оплаты');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

            }
        } else {
            die('Не корректный запрос');
        }

        $filename = "Visits-" . date("d-m-Y-Hi") . ".xls";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');

        exit;
    }

    public function actionExportPdf()
    {
        if (Yii::$app->request->get('type')) {

            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];

            $is_payed_values = [
                'Not_payed' => 'Не оплачен',
            ];

            $visit_status_values = [
                'Completed' => 'Проверен',
                'Validation' => 'На проверке',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
            ];

            $payment_status_values = [
                'Accumulation' => 'Накопление',
                'Withdrawal' => 'Вывод средств',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
                'Charity' => 'Благотворительность',
                'Payed' => 'Оплачен',
            ];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

            switch (Yii::$app->request->get('type')) {
                case 'index':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['!=', 'total_amount', 0])
                        ->andWhere(['is_payed' => 'Not_payed'])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Таблица визитов')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Статус визита')
                        ->setCellValue('J1', 'Накопительный бонус');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':j' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);


//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)
//                        ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'accumulation':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));

//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['payment_status' => ['Accumulation']])
//                        ->andWhere(['visit_status' => ['Completed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = $query
                        ->sum('accumulated_bonuse');
                    $summaryData['total'] = $query
                        ->sum('accumulated_bonuse + total_amount');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
//                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Накопительный бонус')
                        ->setCellValue('J1', 'Итого с бонусом')
                        ->setCellValue('K1', 'Последнее изменение')
//                        ->setCellValue('L1', 'Статус оплаты')
                        ->setCellValue('L1', 'Срок депозита');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $startDate = strtotime($item->last_modified);
                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                        $now_date = time();
                        $datediff = $target_date - $now_date;
                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';


                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');


                        $row++;
                    }

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN
//                            )
//                        )
//                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'payment':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Дата изменения статуса');
//                        ->setCellValue('K1', 'Статус платежа');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);

                    break;

                case 'payed':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Последнее изменение')
                        ->setCellValue('K1', 'Статус оплаты');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;
            }
        } else {
            die('Не корректный запрос');
        }

        $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibraryPath = Yii::getAlias("@vendor") . "/phpoffice/phpexcel/Classes/PHPExcel/Writer/PDF/mPDF.php";

        \PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath);

        $filename = "Visits-" . date("d-m-Y-Hi") . ".pdf";

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        $objWriter->setSheetIndex(0);
        $objWriter->save('php://output');

        exit;
    }
}
