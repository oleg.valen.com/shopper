<?php

namespace frontend\controllers;

use common\models\Charity;
use common\models\Faq;
use common\models\Log;
use common\models\Select;
use common\models\Slider;
use common\models\User;
use common\models\Visit;
use common\models\VisitSearch;
use frontend\models\RowForm;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Cookie;
use frontend\models\UploadImage;

/**
 * Site controller
 */
class NewController extends Controller
{
    public $userId;
    public $userSecurityId;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
//        $this->userId = Yii::$app->user->id;
//        $this->userSecurityId = Yii::$app->user->identity->user_id ?? null;
//        Yii::$app->session->set('user', $this->userSecurityId);

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'payment', 'payed', 'bonus', 'charity', 'pay', 'upload',
                    'delete', 'accumulation', 'update', 'import-excel'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'charity'
                        ],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'payment', 'payed', 'bonus', 'charity', 'pay',
                            'delete', 'accumulation', 'update', 'import-excel', 'upload'
                        ],
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    // вспливающие модальные окошки с информацией
    public function actionModal($page)
    {
        $times = Yii::$app->request->cookies->getValue($page . '-times') ?? 0;
        if ($times >= 3) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400 * 30,
            ]));
        } else {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400,
            ]));
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-times',
                'value' => $times + 1,
                'expire' => time() + 86400 * 30,
            ]));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function datesForPage($page)
    {
        $date[] = Yii::$app->session->get("$page-start-date");
        $date[] = Yii::$app->session->get("$page-end-date");

        return $date;
    }

    public function dateSessionInit($pageName)
    {
        if (!Yii::$app->session->has($pageName . '-start-date')) {
            Yii::$app->session->set($pageName . '-start-date', date('Y-m-d', strtotime(Visit::find()
                    ->where(['user_id' => $this->userSecurityId])
                    ->min('visit_date') ?? date('Y-m-d'))));
            Yii::$app->session->set($pageName . '-end-date', date('Y-m-d', strtotime(Visit::find()
                    ->where(['user_id' => $this->userSecurityId])
                    ->max('visit_date') ?? date('Y-m-d'))));
        }
        return true;
    }

    public function dateDiff($date1, $date2)
    {
        return round((strtotime($date1) - strtotime($date2)) / (60 * 60 * 24), 0);
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    public function actionIndex()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->andFilterWhere(['is_payed' => 'Not_payed']);

        $dataProvider->sort->defaultOrder = ['visit_date' => SORT_DESC];

        // получаем новые статусы и дату начала накопления
        foreach ($dataProvider->getModels() as $item) {
            $nextMonthDate = date('Y-m-d', strtotime($item->visit_date . 'first day of next month'));
            $item->accumulation_start_date = date('Y-m-d', strtotime($nextMonthDate . '+20 days'));
            $approvedToPayStatusDate = date('Y-m-d', strtotime($nextMonthDate . '+10 days'));
            $approvedToWithdrawalStatusDate = date('Y-m-d', strtotime($nextMonthDate . '+20 days'));
            if ($approvedToWithdrawalStatusDate <= date('Y-m-d', time())) {
                $item->visit_status = 'Approved_to_withdrawal';
            } else if ($approvedToPayStatusDate <= date('Y-m-d', time())) {
                $item->visit_status = 'Approved_to_pay';
            } else {
                $item->visit_status = 'Completed';
            }
            $item->save(false);
        }

        $bonuseResult = [];
        foreach ($dataProvider->getModels() as $item) {
            $month = date('n', strtotime($item->visit_date));
            $year = date('y', time());

            // дата начала накоплений 21 число следующего месяца
            $startBonusDate = date('Y-m-d', mktime(0, 0, 0, $month + 1, 20, $year));

            // бонусы на дату
            $firstDate = date('Y-m-d', mktime(0, 0, 0, $month + 4, 21, $year));
            $secondDate = date('Y-m-d', mktime(0, 0, 0, $month + 7, 21, $year));
            $thirdDate = date('Y-m-d', mktime(0, 0, 0, $month + 10, 21, $year));

            // результат в таблицу
            @$bonuseResult[$item->anketa_id][$firstDate] = round($item->total_amount * $this->dateDiff($firstDate, $startBonusDate) * 0.1 / 365, 2);
            @$bonuseResult[$item->anketa_id][$secondDate] = round($item->total_amount * $this->dateDiff($secondDate, $startBonusDate) * 0.11 / 365, 2);
            @$bonuseResult[$item->anketa_id][$thirdDate] = round($item->total_amount * $this->dateDiff($thirdDate, $startBonusDate) * 0.11 / 365, 2);
        }

        // раздел благотворительности
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($this->userSecurityId);
        $charities_amount = Visit::charitiesAmount($this->userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($this->userSecurityId);
        //
        $charities_to_view = [];
        if (!empty($charitiesList)) {
            foreach ($charitiesList as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                }
                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                }
            }
        }

        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }
                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        // изображения слайдера и данные пользователя
        $sliders = Slider::findAll(['status' => 1]);

        Visit::bonuseCalculating($this->userSecurityId);

        $summaryData = [];
        // сумма по странице
        $table_total = 0;
        foreach ($dataProvider->getModels() as $item) {
            @$summaryData['bonuse'] += $item->bonuse;
            @$summaryData['total_amount'] += $item->total_amount;
            @$summaryData['accumulated_bonuse'] += $item->accumulated_bonuse;
            @$table_total += $item->total_amount;
        }

        return $this->render('index', [
            'charitiesList' => $charitiesList,
            'charities_to_view' => $charities_to_view,
            'charitiesDone' => $charitiesDone,
            'sliders' => $sliders,
            'table_total' => $table_total,
            'bonuseResult' => $bonuseResult,

            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
        ]);
    }

    public function actionPayment()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->andFilterWhere(['is_payed' => 'Waiting_pay']);

        $dataProvider->sort->defaultOrder = ['visit_date' => SORT_DESC];

        $summaryData = array();
        foreach ($dataProvider->getModels() as $item) {
            @$summaryData['waiting_pay'] += $item->total_amount;
            @$summaryData['bonuses'] += $item->accumulated_bonuse;
            @$summaryData['charity'] += 0; //$item->accumulated_bonuse;
        }

        $table_total = 0;
        foreach ($dataProvider->getModels() as $item) {
            @$table_total += $item->total_amount;
        }

//        $minDate = Visit::find()
//            ->where(['user_id' => $this->userSecurityId])
//            ->andWhere(['is_payed' => 'Waiting_pay'])
//            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//            ->min('last_modified');
//        $minDate = isset($minDate) ? date('d.m.Y', strtotime($minDate)) : ' --- ';

        return $this->render('payment', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
            'table_total' => $table_total,
        ]);
    }

    public function actionPayed()
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->andFilterWhere(['is_payed' => 'Payed']);

        $summaryData = array();
        foreach ($dataProvider->getModels() as $item) {
            @$summaryData['charity'] += $item->is_payed == 'Charity' ? $item->total_amount : 0;
            @$summaryData['payed'] += $item->is_payed == 'Payed' ? $item->total_amount : 0;
            @$summaryData['bonuses'] += 0; //$item->payment_status == 'Payed' ?  $item->accumulated_bonuse : 0;
            @$summaryData['total_amount'] += $item->payed_amount;
        }

        return $this->render('payed', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
        ]);
    }

    public function actionAccumulation($sort = 'visit_date', $order = 'DESC')
    {
        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->andFilterWhere(['is_payed' => 'Payed']);

        $dataProvider->sort->defaultOrder = ['visit_date' => SORT_DESC];

        $visit = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
            ->andWhere(['not', ['is_payed' => 'Payed']])
            ->andWhere(['in', 'payment_status', [null]])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->orderBy("$sort $order")
            ->all();

        // перебор анкет и подсчет бонуса
        foreach ($dataProvider->getModels() as $item) {
            $item->total_amount = $item->pay_rate + $item->bonuse + $item->penalty;
            $item->accumulated_bonuse = round(((time() - strtotime($item->accumulation_start_date)) / 86400) * 0.1 * $item->total_amount / 365, 2);
            $item->save(false);
        }

        $query = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
            ->andWhere(['in', 'payment_status', [null]])
            ->andWhere(['not', ['is_payed' => 'Payed']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
        $summaryData['bonus'] = $query
            ->sum('accumulated_bonuse');
        $summaryData['total'] = $query
            ->sum('accumulated_bonuse + total_amount');
        $summaryData['accumulation'] = $query
            ->sum('total_amount');

        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();
        $userData->accumulated_bonuse = $summaryData['bonus'];
        $userData->save(false);

        return $this->render('accumulation', [
            'visit' => $visit,
            'order' => $order,
            'filterModel' => $filterModel,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'userData' => $userData,
            'summaryData' => $summaryData,
        ]);
    }

    public function actionCharity()
    {
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($this->userSecurityId);
        $charities_amount = Visit::charitiesAmount($this->userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($this->userSecurityId);

        $charities_to_view = Visit::getCharitiesToView($charitiesList, $charities_count, $charities_amount);

        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }

                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        $charities_report = Visit::find()
            ->where(['payment_status' => 'Charity'])
            ->all();

        foreach ($charities_report as $item) {
            @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['total_amount'] += $item->total_amount;

            if (!isset($charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'] = [];
            }
            if (!in_array($item->user_id, $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'][] += $item->user_id;
            }
        }

        $charityTotal = Visit::charitiesAmount($this->userSecurityId);

        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        return $this->render('charity', compact('charityTotal', 'charitiesDone', 'charitiesList', 'userData'));
    }

    public function actionManagement()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()
                ->where(['id' => $_POST['id']])
                ->one();
            $model->payment_status = $_POST['management'];

            if ($_POST['management'] == 'Accumulation') {
                $model->is_payed = 'Not_payed';
                $model->last_modified = date('Y-m-d H:i:s', time());
            }

            if ($_POST['management'] == 'Charity') {
                $model->payment_status = 'Charity';
                $model->is_payed = 'Wallet';
                $model->last_modified = date('Y-m-d H:i:s', time());
//                $model->sent_charity = date('Y-m-d H:i:s', time());

                Log::setLog('Отправил в кошелек на благотворительность', $model->anketa_id);
            }

            if ($_POST['management'] == 'Withdrawal') {
                $model->payment_status = 'Withdrawal';
                $model->is_payed = 'Wallet';
                $model->last_modified = date('Y-m-d H:i:s', time());
//                $model->sent_wallet = date('Y-m-d H:i:s', time());
                if (!empty($_POST['action'])) {
                    Log::setLog('Нажал на кнопку ОТПРАВИТЬ на вкладке НАКОПЛЕНИЕ', $model->anketa_id);
                }
                Log::setLog('Отправил в кошелек на оплату', $model->anketa_id);
            }

            $model->is_payment_status_changed = 2;
            $model->save(false);
            return true;
        }

        return false;
    }

    public function actionBonus()
    {
        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();
        return $this->render('bonus', ['userData' => $userData]);
    }

    public function actionDeposit()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()
                ->where(['id' => $_POST['id']])
                ->one();
            $model->deposit_term = $_POST['term'];
            $model->is_payed = 'Not_payed';
            $model->payment_status = 'Accumulation';
            $model->last_modified = date('Y-m-d H:i:s', time());
            $model->is_payment_status_changed = 2;
            $model->save(false);
            return true;
        }
        return false;
    }

    public function actionPay()
    {
        $model = Visit::find()
            ->where(['payment_status' => ['Withdrawal', 'Charity']])
            ->andWhere(['is_payed' => 'Wallet'])
            ->all();

        foreach ($model as $item) {
            $item->is_payed = ($item->payment_status == 'Charity') ? 'Charity' : 'Waiting_pay';
            $item->is_payment_status_changed = 2;
            $item->last_modified = date('Y-m-d H:i:s', time());
//            $item->sent_pay = date('Y-m-d H:i:s', time());
            Log::setLog('Отправка анкеты из кошелька на ' . (($item->payment_status == 'Charity') ? ' благотворительность' : ' оплату'), $item->anketa_id);

            $item->save(false);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    // ajax file uploader
    public function actionUpload()
    {
        // получение и запись файла из формы
        $model = new UploadImage();
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->upload_image();
            return true;
        }
        return $this->render('upload', ['model' => $model]);
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()->where(['anketa_id' => $_POST['anketa_id']])->one();
            $model->payment_status = null;
            $model->is_payed = 'Not_payed';
            $model->is_payment_status_changed = null;
            $model->last_modified = date('Y-m-d H:i:s', time());
            //$model->delete_wallet = date('Y-m-d H:i:s', time());

            Log::setLog('Удаление анкеты из кошелька', $_POST['anketa_id']);

            $model->save(false);
            return true;
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionQuestions()
    {
        $model = Faq::findAll(['status' => 1]);

        return $this->render('questions', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = Visit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
