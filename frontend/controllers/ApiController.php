<?php

namespace frontend\controllers;

use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\User;
use common\models\rest\Visit;

//use common\models\Visit;
use yii\data\ActiveDataProvider;
use yii\web\ServerErrorHttpException;

class ApiController extends ActiveController
{
    public $modelClass = 'common\models\rest\Visit';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
//            'update' => ['PUT', 'PATCH'],
//            'upd' => ['PUT', 'PATCH'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                if ($user = User::find()->where(['username' => $username])->one() and $user->validatePassword($password)) {
                    return $user;
                }
                return null;
            }
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index', 'view', 'update', 'update-api', 'changed', 'upd', 'anket-count'],
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

//    public function actionUpd($anketa_id)
//    {
//        $anketa_id = Yii::$app->request->get('anketa_id');
//        $user_id = Yii::$app->request->get('user_id');
//
//        if (is_integer($user_id)) {
//            $model = Visit::find()->where(['anketa_id' => $anketa_id, 'user_id' => $user_id])->one();
//        } else {
//            $userModel = User::find()->where(['username' => $user_id])->one();
//            if ($userModel == null) {
//                return false;
//            }
//            $model = Visit::find()->where(['anketa_id' => $anketa_id, 'user_id' => $userModel->user_id])->with(['user'])->one();
//        }
//
//        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
//        $model->acconter_updated = date('Y-m-d H:i:s');
//        $model->save(false);
//        return $model;
//
////        if ($model->save(false) === false && !$model->hasErrors()) {
////            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
////        }
////
////        return $model;
//    }

    public function actionUpd($anketa_id)
    {
        $anketa_id = Yii::$app->request->get('anketa_id');
        $user_id = Yii::$app->request->get('user_id');

        $request = Yii::$app->getRequest()->getBodyParams();

        $model = Visit::find()->where(['anketa_id' => $anketa_id, 'user.username' => $user_id])
            ->joinWith('user')
            ->one();

        if ($model == null) {
            return false;
        }

        foreach ($model->attributes as $key => $item) {
            if (isset($model->{$key})) {
                $model->{$key} = $request[$key] ?? $model->{$key};
            }
        }

//            $model->client_name = $content['client_name'];
        $model->is_payment_status_changed = $request['is_payment_status_changed'];
        $model->acconter_updated = date('Y-m-d H:i:s');
        if ($model->save(false)) {
            return true;
        }

        return false;

    }


    public function actionUpdateApi()
    {
        $request = Yii::$app->request->getBodyParams();

        foreach ($request as $content) {
            $model = Visit::find()
                ->where(['anketa_id' => $content['anketa_id']])
                ->one();

            if ($model == null) {
                continue;
//                return false;
            }

            $model->is_payment_status_changed = $content['is_payment_status_changed'];
            $model->payed_amount = $content['payed_amount'];
            $model->is_payed = $content['is_payed'];
            $model->last_modified = $content['last_modified'];
            $model->payment_date = $content['payment_date'];

//            $model->client_name = $content['client_name'];
//            $model->pay_rate = $content['pay_rate'];
            $model->acconter_updated = date('Y-m-d H:i:s');
            $model->save(false);
        }

        return true;
    }


    public function actionData()
    {
        $anketa_id = Yii::$app->request->get('anketa_id');
        $user_id = Yii::$app->request->get('user_id');

        $model = Visit::find()->where(['anketa_id' => $anketa_id, 'user_id' => $user_id])->one();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

    public function actionChanged()
    {
        $result = \common\models\rest\Visit::find()
            ->with('username')
            ->where(['in', 'visit.is_payment_status_changed', [1, 2]])
//            ->andWhere(['BETWEEN', 'visit_date', '2021-05-01 00:00:00', '2021-06-22 23:59:59'])
//            ->andWhere(['is_prompt_payment' => 1])
            ->limit(5000)
            ->all();

        return $result;
    }

    public function actionAnketCount()
    {
        $result = \common\models\rest\Visit::find()
            ->with('username')
            ->where(['in', 'visit.is_payment_status_changed', [1, 2]])
//            ->andWhere(['BETWEEN',/ 'visit_date', '2021-05-01 00:00:00', '2021-06-22 23:59:59'])
//            ->andWhere(['is_prompt_payment' => 1])
            ->count();

        return $result;
    }

    public function actionAnketaData($anketa_id)
    {
        if ($result = \common\models\rest\Visit::find()
            ->where(['anketa_id' => $anketa_id])
            ->all()) {
            return $result;
        }
        throw new ServerErrorHttpException('Can\'t find this anketa in database');
    }
}
