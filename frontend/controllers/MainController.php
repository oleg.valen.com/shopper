<?php

namespace frontend\controllers;

use common\models\Bonuse;
use common\models\BonusesToUser;
use common\models\Charity;
use common\models\Faq;
use common\models\IsPayed;
use common\models\Log;
use common\models\Select;
use common\models\Slider;
use common\models\Temp;
use common\models\User;
use common\models\Visit;
use common\models\VisitSearch;
use common\models\VisitStatus;
use frontend\models\RowForm;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Cookie;
use frontend\models\UploadImage;

/**
 * Site controller
 */
class MainController extends Controller
{
    public $userId;
    public $userSecurityId;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        $this->userId = Yii::$app->user->id;
        $this->userSecurityId = Yii::$app->user->identity->user_id ?? null;
        Yii::$app->session->set('user', $this->userSecurityId);

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'payment', 'payed', 'bonus', 'charity', 'pay', 'upload',
                    'delete', 'accumulation', 'accumulation-test', 'update', 'import-excel', 'pay-bonus'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'logout'],
                        'roles' => ['?'],
                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'charity'
//                        ],
//                        'roles' => ['admin'],
//                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'payment', 'payed', 'bonus', 'charity', 'pay',
                            'delete', 'accumulation', 'accumulation-test', 'update', 'import-excel', 'upload', 'pay-bonus'
                        ],
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    // модальные окошки с информацией
    public function actionModal($page)
    {
        $times = Yii::$app->request->cookies->getValue($page . '-times') ?? 0;
        if ($times >= 3) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400 * 30,
            ]));
        } else {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-confirmed',
                'value' => 'confirmed',
                'expire' => time() + 86400,
            ]));
            Yii::$app->response->cookies->add(new Cookie([
                'name' => $page . '-times',
                'value' => $times + 1,
                'expire' => time() + 86400 * 30,
            ]));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function datesForPage($page)
    {
        $date[] = Yii::$app->session->get("$page-start-date");
        $date[] = Yii::$app->session->get("$page-end-date");

        return $date;
    }

    public function dateSessionInit($pageName)
    {
        if (!Yii::$app->session->has($pageName . '-start-date')) {
            Yii::$app->session->set($pageName . '-start-date', date('Y-m-d', strtotime(Visit::find()
                    ->where(['user_id' => $this->userSecurityId])
                    ->min('visit_date') ?? date('Y-m-d'))));
            Yii::$app->session->set($pageName . '-end-date', date('Y-m-d', strtotime(Visit::find()
                    ->where(['user_id' => $this->userSecurityId])
                    ->max('visit_date') ?? date('Y-m-d'))));
        }
        return true;
    }

    public function dateDiff($date1, $date2)
    {
        return round((strtotime($date1) - strtotime($date2)) / (60 * 60 * 24), 0);
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    public function actionIndex()
    {
        $rowsModel = new RowForm();
        $rowsModel->rows = Yii::$app->session->get('index-rows', 10);
        Yii::$app->session->set('index-rows', $rowsModel->rows);
        if ($rowsModel->load(Yii::$app->request->post())) {
            Yii::$app->session->set('index-rows', $rowsModel->rows);
        }

        if ($request = Yii::$app->request->get()) {
            if (isset($request['sort'])) {
                Yii::$app->session->set('index-sort', $request['sort']);
                $sortChangedStatus = 1;
            }
            if (isset($request['order'])) {
                Yii::$app->session->set('index-order', $request['order']);
            }
        } else {
            $sortChangedStatus = null;
        }

        $sort = Yii::$app->session->get('index-sort', 'visit_date');
        $order = Yii::$app->session->get('index-order', 'DESC');

        // date init
        $this->dateSessionInit('index');

        //входящий POST запрос и забираем дату
        if ($request = Yii::$app->request->post()) {
            if (isset($request['Select']['date'])) {
                list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                Yii::$app->session->set('index-start-date', date('Y-m-d', strtotime($startDate)));
                Yii::$app->session->set('index-end-date', date('Y-m-d', strtotime($endDate)));
            }
        }
        list($startDate, $endDate) = $this->datesForPage('index');

        $visit = Visit::find()
//            ->where(['user_id' => $this->userSecurityId])
//            ->andWhere(['in', 'is_payed', ['Not_payed', 'Wallet', 'Rejected']])
//            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//            ->orderBy("$sort $order");
            ->where(['AND',
                ['user_id' => $this->userSecurityId],
//                ['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']],
//                ['in', 'payment_status', [null]],
                ['in', 'is_payed', ['Not_payed', 'Rejected']],
                ['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]
            ])->orderBy("$sort $order");

        $countQuery = clone($visit);
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => Yii::$app->session->get('index-rows', 10)]);

        $visit = $countQuery
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // bonuse column items
        $bonuseResult = [];
        foreach ($visit as $item) {
            $month = date('n', strtotime($item->visit_date));
            $year = date('y', time());

            // дата начала накоплений 21 число следующего месяца
            $startBonusDate = date('Y-m-d', mktime(0, 0, 0, $month + 1, 21, $year));

            // бонусы на дату
            $firstDate = date('Y-m-d', mktime(0, 0, 0, $month + 4, 21, $year));
            $secondDate = date('Y-m-d', mktime(0, 0, 0, $month + 7, 21, $year));
            $thirdDate = date('Y-m-d', mktime(0, 0, 0, $month + 10, 21, $year));

            // результат в таблицу
            @$bonuseResult[$item->anketa_id][$firstDate] = round($item->total_amount * $this->dateDiff($firstDate, $startBonusDate) * 0.09 / 365, 2);
            @$bonuseResult[$item->anketa_id][$secondDate] = round($item->total_amount * $this->dateDiff($secondDate, $startBonusDate) * 0.1 / 365, 2);
            @$bonuseResult[$item->anketa_id][$thirdDate] = round($item->total_amount * $this->dateDiff($thirdDate, $startBonusDate) * 0.11 / 365, 2);
        }

        // раздел благотворительности
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($this->userSecurityId);
        $charities_amount = Visit::charitiesAmount($this->userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($this->userSecurityId);
        //

        $charities_to_view = [];
        if (!empty($charitiesList)) {
            foreach ($charitiesList as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                }
                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                }
            }
        }

        //
        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }
                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        // изображения слайдера и данные пользователя
        $sliders = Slider::findAll(['status' => 1]);
        $filterModel = new Select();

        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        $summaryData['bonuse'] = $countQuery->sum('accumulated_bonuse');
        $summaryData['total_amount'] = $countQuery->sum('total_amount');
        $summaryData['accumulated_bonuse'] = $countQuery->sum('accumulated_bonuse');

        // сумма по странице
        $table_total = 0;
        foreach ($visit as $item) {
            @$table_total += $item->total_amount;
        }

        $showVideo = $userData->show_video;
        if ($showVideo) {
            $userData->show_video = false;
            $userData->save(false);
        }

        $badBonuseUsers = ['vjurkin1', 'achepele6', 'spyatkov2', 'jkazants13', 'ipoletsk',
            'tpoletsk', 'msuslova4', 'dglazkov12', 'akolpako16', 'gkadarov4', 'ekazakov45',
            'vmohov', 'eegorova92', 'aandrei3', 'eshestak17', 'vstruchk', 'ejuraeva1',
            'dsuhoruk1', 'oryazano3', 'nzaharov37', 'asuhanov21', 'aloktase', 'rdenisen6',
            'oryazano3', 'aloktase', 'sgubarev2', 'asuhanov21',
            'smogilev2', 'sgubarev2', 'ofilippo31', 'dsuhoruk1', 'oryazano3'];
        $showModal = in_array($userData->username, $badBonuseUsers) ? true : false;

        return $this->render('index', [
            'charitiesList' => $charitiesList,
            'charities_to_view' => $charities_to_view,
            'charitiesDone' => $charitiesDone,
            'visit' => $visit,
            'order' => $order,
            'pages' => $pages,
            'filterModel' => $filterModel,
            'sliders' => $sliders,
            'userData' => $userData,
            'rowsModel' => $rowsModel,
            'summaryData' => $summaryData,
            'table_total' => $table_total,
            'bonuseResult' => $bonuseResult,
            'showVideo' => $showVideo,
            'showModal' => $showModal,
        ]);
    }

    public function actionPayment($sort = 'visit_date', $order = 'DESC', $filter = ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity', IsPayed::PAYED_NOT_BONUS])
    {
        $rowsModel = new RowForm();

        $rowsModel->rows = Yii::$app->session->get('payment-rows', 10);
        Yii::$app->session->set('payment-rows', $rowsModel->rows);

        if ($rowsModel->load(Yii::$app->request->post())) {
            Yii::$app->session->set('payment-rows', $rowsModel->rows);
        }

        // user data
        $this->userSecurityId = Yii::$app->user->identity->user_id;

        // date session init
        $this->dateSessionInit('payment');

        //входящий GET запрос и забираем дату
        if ($request = Yii::$app->request->post()) {
            if (isset($request['Select']['date'])) {
                list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                Yii::$app->session->set('payment-start-date', date('Y-m-d', strtotime($startDate)));
                Yii::$app->session->set('payment-end-date', date('Y-m-d', strtotime($endDate)));
            }
        }

        list($startDate, $endDate) = $this->datesForPage('payment');

        if ($filter != 'Все') {
            $visit = Visit::find()
                ->where(['user_id' => $this->userSecurityId])
                ->andWhere(['is_payed' => $filter])
                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                ->orderBy("$sort $order");
            $countQuery = clone($visit);

            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => Yii::$app->session->get('payment-rows', 10)]);

            $visit = $countQuery
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        } else {
            $visit = Visit::find()
                ->where(['user_id' => $this->userSecurityId])
                ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity', IsPayed::PAYED_NOT_BONUS]])
                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                ->orderBy("$sort $order");
            $countQuery = clone($visit);

            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

            $visit = $countQuery
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        }

        $filterModel = new Select();
        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        // crutch
        $bonuse = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['in', 'is_payed', ['Waiting_pay']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('accumulated_bonuse');
        $summaryData['waiting_pay'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['in', 'is_payed', ['Waiting_pay']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount'); // + ($bonuse ?? 0);
        $summaryData['charity'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['in', 'payment_status', ['Charity']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');
        $summaryData['payed'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['is_payed' => 'Payed'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');
        $summaryData['payment'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['is_payed' => 'Payed'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');
        $summaryData['bonuses'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['is_payed' => 'Waiting_pay'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('accumulated_bonuse');

//        $this->debug($summaryData);

        $table_total = 0;
        foreach ($visit as $item) {
            @$table_total += $item->total_amount;
        }

        $minDate = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['is_payed' => 'Waiting_pay'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->min('last_modified');
        $minDate = isset($minDate) ? date('d.m.Y', strtotime($minDate)) : ' --- ';

        return $this->render('payment', [
            'visit' => $visit,
            'order' => $order,
            'filter' => $filter,
            'pages' => $pages,
            'filterModel' => $filterModel,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'rowsModel' => $rowsModel,
            'userData' => $userData,
            'summaryData' => $summaryData,
            'table_total' => $table_total,
            'minDate' => $minDate,
        ]);
    }

    public function actionPayed($sort = 'visit_date', $order = 'DESC', $filter = ['Payed', 'advance_payment'])
    {
        $rowsModel = new RowForm();

        $rowsModel->rows = Yii::$app->session->get('payed-rows', 10);
        Yii::$app->session->set('payed-rows', $rowsModel->rows);

        if ($rowsModel->load(Yii::$app->request->post())) {
            Yii::$app->session->set('payed-rows', $rowsModel->rows);
        }

        // date session init
        $this->dateSessionInit('payed');

        //входящий GET запрос и забираем дату
        if ($request = Yii::$app->request->post()) {
            if (isset($request['Select']['date'])) {
                list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                Yii::$app->session->set('payed-start-date', date('Y-m-d', strtotime($startDate)));
                Yii::$app->session->set('payed-end-date', date('Y-m-d', strtotime($endDate)));
            }
        }

        list($startDate, $endDate) = $this->datesForPage('payed');

        if ($filter != 'Все') {
            $visit = Visit::find()
                ->where(['user_id' => $this->userSecurityId])
                ->andWhere(['in', 'is_payed', $filter])
                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                ->orderBy("$sort $order");
        } else {
            $visit = Visit::find()
                ->where(['user_id' => $this->userSecurityId])
                ->andWhere(['in', 'is_payed', ['Payed', 'Partial_payed', 'Charity', 'advance_payment']])
                ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                ->orderBy("$sort $order");
        }

//        $this->debug($visit->all());

        // paggination
        $countQuery = clone($visit);
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => Yii::$app->session->get('payed-rows', 10)]);
        $visit = $countQuery
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $filterModel = new Select();
        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        $summaryData['charity'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['payment_status' => 'Charity'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');
        $summaryData['payed'] = Visit::find()
            ->where(['user_id' => $this->userSecurityId])
            ->andWhere(['is_payed' => ['Payed', 'advance_payment']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');
        $summaryData['bonuses'] = 0; //Visit::find()
//            ->where(['user_id' => $this->userSecurityId])
//            ->andWhere(['is_payed' => 'Payed'])
//            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//            ->sum('accumulated_bonuse');

        $table_total = 0;
        foreach ($visit as $item) {
            @$table_total += $item->is_payed == 'Partial_payed' ? $item->partial_payment : $item->total_amount;
        }

        return $this->render('payed', [
            'visit' => $visit,
            'order' => $order,
            'filter' => $filter,
            'pages' => $pages,
            'filterModel' => $filterModel,
            'rowsModel' => $rowsModel,
            'userData' => $userData,
            'summaryData' => $summaryData,
            'table_total' => $table_total,
        ]);
    }

    public function actionAccumulation($sort = 'visit_date', $order = 'DESC')
    {
        // date session init
        $this->dateSessionInit('accumulation');

        if ($request = Yii::$app->request->post()) {
            if (isset($request['Select']['date'])) {
                list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                Yii::$app->session->set('accumulation-start-date', date('Y-m-d', strtotime($startDate)));
                Yii::$app->session->set('accumulation-end-date', date('Y-m-d', strtotime($endDate)));
            }
        }

        list($startDate, $endDate) = $this->datesForPage('accumulation');

        $query = Visit::find()
            ->where(['user_id' => Yii::$app->user->identity->user_id])
            ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
            ->andWhere(['not in', 'is_payed', ['Payed', 'Waiting_pay', 'Sent_to_pay', IsPayed::RECALC, IsPayed::PAYED_NOT_BONUS]])
            ->andWhere(['or',
                ['<=', 'accumulation_start_date', date('Y-m-d')],
                ['is', 'accumulation_start_date', null]])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->orderBy("$sort $order");

        $countQuery = clone($query);
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => Yii::$app->session->get('index-rows', 10)]);

        $query = $countQuery
            ->offset($pages->offset)
            ->limit($pages->limit);
//            ->all();

        $visit = $query->all();

        @$summaryData['bonus'] = $query->sum('accumulated_bonuse');
        @$summaryData['total'] = $query->sum('accumulated_bonuse + total_amount');
        @$summaryData['accumulation'] = $query->sum('total_amount');

        //todo check and remove it
        $userData = User::findOne(['user_id' => Yii::$app->user->identity->user_id]);
        $userData->accumulated_bonuse = $summaryData['bonus'];
        $userData->save(false);

        $filterModel = new Select();

        return $this->render('accumulation', [
            'visit' => $visit,
            'order' => $order,
            'pages' => $pages,
            'filterModel' => $filterModel,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'userData' => $userData,
            'summaryData' => $summaryData,
        ]);
    }

    public function actionCharity()
    {
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($this->userSecurityId);
        $charities_amount = Visit::charitiesAmount($this->userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($this->userSecurityId);

        $charities_to_view = Visit::getCharitiesToView($charitiesList, $charities_count, $charities_amount);

        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }

                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        $charities_report = Visit::find()
            ->where(['payment_status' => 'Charity'])
            ->all();

        foreach ($charities_report as $item) {
            @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['total_amount'] += $item->total_amount;

            if (!isset($charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'] = [];
            }
            if (!in_array($item->user_id, $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'][] += $item->user_id;
            }
        }

        $charityTotal = Visit::charitiesAmount($this->userSecurityId);

        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        return $this->render('charity', compact('charityTotal', 'charitiesDone', 'charitiesList', 'userData'));
    }

    public function actionBonus()
    {
        $userData = User::find()->where(['user_id' => $this->userSecurityId])->one();

        return $this->render('bonus', ['userData' => $userData]);
    }

    public function actionPayBonus()
    {

        return 'ВНИМАНИЕ! С 1 марта 2022 года временно приостановлен вывод бонусов на оплату до стабилизации курсов валют и экономической ситуации в стране (бонусы будут начисляться, но к выводу будут недоступны). Надеемся на Ваше понимание!';

        $month = date('m');
        if (Yii::$app->request->isAjax
            && ($sum = Yii::$app->request->post('sum'))
            && ($model = Visit::find()
                ->where(['user_id' => $this->userSecurityId,
                    'visit_status' => 'Completed',
                    'is_payed' => 'Not_payed'])
                ->andFilterWhere(['AND',
                    ['between', 'visit_date', date("Y-{$month}-01"), date("Y-{$month}-31")],
                    ['is', 'wait_bonuse', null]])
//                ->andFilterWhere(date('j') < 11
//                    ? ['AND',
//                        ['between', 'visit_date', date('Y-m-01'), date('Y-m-03')],
//                        ['is', 'wait_bonuse', null]]
//                    : [])
                ->one())) {

            $model->wait_bonuse += $sum;
//            $model->total_amount = $model->pay_rate + $model->bonuse + $model->penalty;

            $bonuseToUser = BonusesToUser::findAll(['status' => BonusesToUser::STATUS_WALLET, 'user_id' => Yii::$app->user->identity->user_id]);
            $bonuseIds = ArrayHelper::getColumn($bonuseToUser, 'anketa_id');
            if ($model->save() && Visit::updateAll(['accumulated_bonuse' => 0], ['in', 'anketa_id', $bonuseIds])) {
                $bonuseModel = new Bonuse();
                $bonuseModel->payment_date = date('Y-m-d');
                $bonuseModel->user_id = Yii::$app->user->identity->user_id;
                $bonuseModel->login = Yii::$app->user->identity->username;
                $bonuseModel->amount = $sum;
                $bonuseModel->payment_anketa_id = $model->anketa_id;
                $bonuseModel->deadline_date = date('Y-m-d', strtotime("+7 days", strtotime(date('Y-m-d'))));
                $bonuseModel->anketas_data = '';

                foreach ($bonuseToUser as $item) {
                    $bonuseModel->anketas_data .= $item->anketa_id . '-' . $item->amount . '; ';
                    $item->status = BonusesToUser::STATUS_APPROVED;
                    $item->save(false);
                }

                $bonuseModel->save();

                return 'Ваши бонусы добавлены в анкету №' . $model->anketa_id .
                    ' и будут выплачены в течении 40 рабочих дней с момента подачи этой анкеты на оплату.';
            } else {
                return 'Ошибка сохранения.';
            }
        } else {
            return 'Для вывода бонусов нет подходящих проверенных анкет - совершите новый визит.';
        }
    }

    public function actionManagement()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::findOne(['id' => $_POST['id']]);
            $model->payment_status = $_POST['management'];
            if ($_POST['management'] == 'Accumulation') {
                $model->is_payed = 'Not_payed';
                $model->last_modified = date('Y-m-d H:i:s', time());
            }
            if ($_POST['management'] == 'Charity') {
                $model->payment_status = 'Charity';
                $model->is_payed = 'Wallet';
                $model->last_modified = date('Y-m-d H:i:s', time());
                Log::setLog('Отправил в кошелек на благотворительность', $model->anketa_id);
            }
            if ($_POST['management'] == 'Withdrawal') {
                $model->payment_status = 'Withdrawal';
                $model->is_payed = 'Wallet';
                $model->last_modified = date('Y-m-d H:i:s', time());
                if (!empty($_POST['action'])) {
                    Log::setLog('Нажал на кнопку ОТПРАВИТЬ на вкладке НАКОПЛЕНИЕ', $model->anketa_id);
                }
                Log::setLog('Отправил в кошелек на оплату', $model->anketa_id);
            }

            $model->is_payment_status_changed = 2;
            $model->save(false);

            return true;
        }

        return false;
    }

    public function actionDeposit()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()
                ->where(['id' => $_POST['id']])
                ->one();
            $model->deposit_term = $_POST['term'];
            $model->is_payed = 'Not_payed';
            $model->payment_status = 'Accumulation';
            $model->last_modified = date('Y-m-d H:i:s', time());
            $model->is_payment_status_changed = 2;
            $model->save(false);
            return true;
        }
        return false;
    }

    public function actionCountStamp()
    {
        $dateStamp = Visit::find()->where(['!=', 'sent_to_pay', 'null'])->count();
        echo $dateStamp;
        die;
    }

//    public function actionTemp()
//    {
//        $visits = Visit::find()->where(['!=', 'sent_to_pay', 'null'])->all();
//        foreach ($visits as $oneVisit) {
//            $oneVisit->old_date = $oneVisit->sent_to_pay;
//            $oneVisit->new_date = date('Y-m-d H:i:s', $oneVisit->sent_to_pay);
//            $oneVisit->sent_to_pay = null;
//            $oneVisit->save(false);
//        }
//        return 'yes!';
//    }

//    public function actionTemp2()
//    {
//        $visits = Visit::find()->where(['!=', 'new_date', 'null'])->all();
////        print_r($visits[2]->new_date);die;
//        foreach ($visits as $oneVisit) {
//            $oneVisit->sent_to_pay = $oneVisit->new_date;
//            $oneVisit->save(false);
//        }
//        return 'yes!';
//    }

    public function actionPay()
    {
        $model = Visit::findAll([
            'user_id' => Yii::$app->user->identity->user_id,
//            'payment_status' => ['Withdrawal', 'Charity'],
            'is_payed' => 'Wallet',
        ]);

        foreach ($model as $item) {
            if ($item->visit_status != VisitStatus::WAITING_NOT_BONUS) {
                $item->is_payed = 'Waiting_pay';
            } else {
                $item->is_payed = IsPayed::PAYED_NOT_BONUS;
            }
            $item->is_payment_status_changed = 2;
            $item->last_modified = date('Y-m-d H:i:s', time());
            $item->sent_to_pay = date('Y-m-d H:i:s');

            Log::setLog('Отправка анкеты из кошелька на оплату', $item->anketa_id);

            $item->save(false);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    // ajax file uploader
    public function actionUpload()
    {
        // получение и запись файла из формы
        $model = new UploadImage();
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->upload_image();
            return true;
        }
        return $this->render('upload', ['model' => $model]);
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()->where(['anketa_id' => $_POST['anketa_id']])->one();
            $model->payment_status = null;
            $model->is_payed = 'Not_payed';
            $model->is_payment_status_changed = null;
            $model->last_modified = date('Y-m-d H:i:s', time());

            $model->save(false);

            BonusesToUser::deleteAll(['anketa_id' => $_POST['anketa_id']]);

            Log::setLog('Удаление анкеты из кошелька', $_POST['anketa_id']);

            return true;
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionQuestions()
    {
        $model = Faq::findAll(['status' => 1]);

        return $this->render('questions', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = Visit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested anket does not exist.');
    }
}
