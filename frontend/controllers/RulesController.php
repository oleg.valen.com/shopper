<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * Site controller
 */
class RulesController extends Controller
{

//    public function beforeAction($action)
//    {
//        if (parent::beforeAction($action)) {
//            if (!\Yii::$app->user->can('user')) {
//                throw new \yii\web\ForbiddenHttpException('Доступ закрыт.');
//            }
//            return true;
//        } else {
//            return false;
//        }
//    }

//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => [
//                    'rules', 'rules-index', 'rules-charity', 'rules-bonus', 'rules-payments'
//                ],
//                'rules' => [
//                    [
//                        'actions' => ['login'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'rules-index', 'rules-charity', 'rules-bonus', 'rules-payments'
//                        ],
//                        'roles' => [User::ROLE_USER],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionRulesIndex()
    {
        return $this->render('rules-index');
    }

    public function actionRulesCharity()
    {
        return $this->render('rules-charity');
    }

    public function actionRulesBonus()
    {
        return $this->render('rules-bonus');
    }

    public function actionRulesPayments()
    {
        return $this->render('rules-payments');
    }

    public function actionRules($page = 1)
    {
        return $this->render('rules', compact('titles', 'rule', 'page', 'pages'));
    }

}
