<?php

namespace frontend\controllers;

use common\models\BonusesToUser;
use common\models\IsPayed;
use common\models\WaitingNotBonus;
use Yii;
use common\models\Visit;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\filters\{VerbFilter, AccessControl};
use yii\web\Response;

/**
 * Site controller
 */
class AjaxController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout', 'get-prompt'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout', 'entrance', 'reset-password', 'send-email', 'auth', 'log', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['entrance', 'reset-password', 'auth', 'log', 'get-prompt'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'about', 'logout'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['about', 'index', 'logout', 'login', 'reset-password', 'send-email'],
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionGetPrompt()
    {
        if (Yii::$app->request->isAjax && isset($_POST['userId'])) {
            $usersPromptAnkets = Visit::findAll(['user_id' => $_POST['userId'], 'is_prompt_payment' => true, 'is_payed' => 'Not_payed']);
            foreach ($usersPromptAnkets as $item) {
                $item->payment_status = 'Withdrawal';
                $item->is_payed = 'Wallet';
                $item->save(false);
            }

            echo json_encode(true);
        } else {
            echo json_encode(false);
        }

    }

    public function actionWaitingNotBonus()
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if (Yii::$app->request->isAjax && isset($_POST['id'])) {
            if ($model = Visit::find()
                ->where(['user_id' => Yii::$app->user->identity->user_id, 'id' => $_POST['id']])
                ->one()) {
                try {
                    $waitingNotBonus = WaitingNotBonus::find()
                        ->where(['anketa_id' => $model->anketa_id])
                        ->one();
                    if ($waitingNotBonus == null)
                        $waitingNotBonus = new WaitingNotBonus();
                    $waitingNotBonus->anketa_id = $model->anketa_id;
                    $waitingNotBonus->accumulated_bonuse = $model->accumulated_bonuse;
                    $waitingNotBonus->save(false);

                    $model->is_payed = IsPayed::WALLET;
                    $model->accumulation_start_date = null;
                    $model->accumulated_bonuse = 0;
                    $model->save(false);

                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                }
            }
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public function actionGetToPay()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax && isset($_POST['userId']) && isset($_POST['anketaId']) && isset($_POST['method'])) {

            if ($_POST['method'] === 'one') {
                if ($visit = Visit::find()
                    ->where([
                        'user_id' => $_POST['userId'],
                        'anketa_id' => $_POST['anketaId'],
                        'visit_status' => 'Approved_to_withdrawal',
                        'is_payed' => 'Not_payed'
                    ])->one()) {

                    $visit->payment_status = 'Withdrawal';
                    $visit->is_payed = 'Wallet';
                    $visit->save(false);

                    $bonuseToUser = new BonusesToUser();
                    $bonuseToUser->user_id = $_POST['userId'];
                    $bonuseToUser->anketa_id = $_POST['anketaId'];
                    $bonuseToUser->amount = $visit->accumulated_bonuse ?? 0;
                    $bonuseToUser->status = BonusesToUser::STATUS_WALLET;
                    $bonuseToUser->save(false);
                    return true;
                }
            } else {
                if ($usersAnkets = Visit::findAll([
                    'user_id' => $_POST['userId'],
                    'visit_status' => 'Approved_to_withdrawal',
                    'is_payed' => 'Not_payed'
                ])) {
                    foreach ($usersAnkets as $item) {
                        $item->payment_status = 'Withdrawal';
                        $item->is_payed = 'Wallet';
                        $item->save(false);

                        $bonuseToUser = new BonusesToUser();
                        $bonuseToUser->user_id = Yii::$app->user->identity->user_id;
                        $bonuseToUser->anketa_id = $item->anketa_id;
                        $bonuseToUser->amount = $item->accumulated_bonuse ?? 0;
                        $bonuseToUser->status = BonusesToUser::STATUS_WALLET;
                        $bonuseToUser->save(false);
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
