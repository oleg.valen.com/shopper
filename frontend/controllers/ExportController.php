<?php

namespace frontend\controllers;

use common\models\UploadForm;
use common\models\User;
use common\models\Visit;
use common\models\VisitSearch;

//use common\models\Wallet;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

//use PHPExcel_Settings;
//use PHPExcel_Writer_PDF;
use mpdf\mPDF;

//use PHPExcel_Settings;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use XLSXWriter;

/**
 * Site controller
 */
class ExportController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'import-excel', 'excel', 'export-excel', 'export-pdf'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['*'],
                        'roles' => ['?'],
                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'charity'
//                        ],
//                        'roles' => ['admin'],
//                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'import-excel', 'excel', 'export-excel', 'export-pdf'
                        ],
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionExcel()
    {
        $paymentStatus = [
            'Rejected' => 'Отклонен',
            'Payed' => 'Оплачен',
            'Not_payed' => 'Не оплачен',
            'Waiting_pay' => 'Ожидает выплату',
            'Sent_to_pay' => 'Отправлен на оплату',
            'Charity' => 'Благотворительность'
        ];

        ini_set("memory_limit", "512M");
        ini_set('max_execution_time', 300);

        include_once("xlsxwriter.class.php");

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => Yii::$app->user->identity->user_id]);
        $dataProvider->query->select(['id', 'user_id', 'visit_date',
            'anketa_id', 'project', 'location_city', 'location_address', 'pay_rate', 'bonuse', 'penalty', 'accumulated_bonuse',
            'total_amount', 'last_modified', 'is_payed']);


//        $this->debug(Yii::$app->request->getQueryParams());
        $this->debug(Yii::$app->request->queryParams);
        $this->debug($dataProvider->getModels());

        if(Yii::$app->request->get('params') != null) {
            $params = Yii::$app->request->get('params');
            $date = $params['VisitSearch'] ?? null;
            $this->debug($date);
        }

        $this->debug(Yii::$app->request->get('params'));
        switch (Yii::$app->request->get('type')) {
            case 'index':
                $dataProvider->query->andFilterWhere(['is_payed' => 'Not_payed']);

                $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                $summaryData['total_amount'] = $query->sum('total_amount');

                $header = [
                    'Дата визита' => 'string',
                    'Номер анкеты' => 'string',
                    'Проект' => 'string',
                    'Адрес локации' => 'string',
                    'Сумма за визит' => 'string',
                    'Доплата' => 'string',
                    'Штраф' => 'string',
                    'Итого' => 'string',
                    'Статус визита' => 'string',
                    'Накопительный бонус' => 'string',
                ];

                $pageName = 'Визиты';

                break;
            case 'payment':
                $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']]);
                break;
            case 'payed':
                $dataProvider->query->andFilterWhere(['is_payed' => 'Payed']);
                $header = [
                    'Дата визита' => 'string',
                    'Номер анкеты' => 'string',
                    'Проект' => 'string',
                    'Адрес локации' => 'string',
                    'Сумма за визит' => 'string',
                    'Доплата' => 'string',
                    'Штраф' => 'string',
                    'Накопительный бонус' => 'string',
                    'Итого' => 'string',
                    'Последнее изменение' => 'string',
                    'Статус оплаты' => 'string',
                ];
                $pageName = 'Оплаченные';
                break;
            case 'accumulation':
                $dataProvider->query->andFilterWhere(['is_payed' => 'Not_payed']);
                $dataProvider->query->andFilterWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']]);
                break;
            default:
                break;
        }

//        $this->debug($dataProvider->getModels());


        $LocationStateRegion = Yii::$app->params['LocationStateRegion'];
        $visit_status_values = [
            'Completed' => 'Проверен',
            'Validation' => 'На проверке',
            'Approved_to_withdrawal' => 'Одобрено для вывода денег',
        ];
        $payment_status_values = [
            'Accumulation' => 'Накопление',
            'Withdrawal' => 'Вывод средств',
            'Charity' => 'Благотворительность',
            'Payed' => 'Оплачен',
            'Approved_to_withdrawal' => 'Одобрено для вывода денег',
        ];

        foreach ($dataProvider->getModels() as $key => $item) {
            $row[$item->anketa_id]['date'] = date('d.m.Y', strtotime($item->visit_date));
            $row[$item->anketa_id]['anketa_id'] = $item->anketa_id;
            $row[$item->anketa_id]['project'] = strip_tags($item->project);
            $row[$item->anketa_id]['location'] = $item->location_city . $item->location_address;
            $row[$item->anketa_id]['pay_rate'] = $item->pay_rate;
            $row[$item->anketa_id]['bonuse'] = $item->bonuse;
            $row[$item->anketa_id]['penalty'] = $item->penalty;
            $row[$item->anketa_id]['accumulated_bonuse'] = $item->accumulated_bonuse;
            $row[$item->anketa_id]['total_amount'] = $item->total_amount + $item->accumulated_bonuse;
            $row[$item->anketa_id]['last_modified'] = $item->last_modified;
            $row[$item->anketa_id]['is_payed'] = $payment_status_values[$item->is_payed] ?? $item->is_payed;
        }

        $writer = new XLSXWriter();
        $filename = "Выгрузка_" . $pageName . '_' . date('d-m-Y-Hi') . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'top', 'wrap_text' => true,
            'widths' => [15, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader($pageName, $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'wrap_text' => true,
                'valign' => 'top'
            ];
            $writer->writeSheetRow($pageName, $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);


        if ($type = Yii::$app->request->get('type')) {

//            var_dump($type); die;
            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];

            $is_payed_values = [
                'Not_payed' => 'Не оплачен',
            ];

            $visit_status_values = [
                'Completed' => 'Проверен',
                'Validation' => 'На проверке',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
            ];

            $payment_status_values = [
                'Accumulation' => 'Накопление',
                'Withdrawal' => 'Вывод средств',
                'Charity' => 'Благотворительность',
                'Payed' => 'Оплачен',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
            ];


            switch (Yii::$app->request->get('type')) {
                case 'index':


                    $visit = $query->all();

                            $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
                        $row++;
                    }

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'accumulation':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['payment_status' => ['Accumulation']])
//                        ->andWhere(['visit_status' => ['Completed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = $query
                        ->sum('accumulated_bonuse');
                    $summaryData['total'] = $query
                        ->sum('accumulated_bonuse + total_amount');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Накопительный бонус')
                        ->setCellValue('J1', 'Итого с бонусом')
                        ->setCellValue('K1', 'Последнее изменение')
//                        ->setCellValue('L1', 'Статус оплаты')
                        ->setCellValue('L1', 'Срок депозита');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);


                        $startDate = strtotime($item->last_modified);
                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                        $now_date = time();
                        $datediff = $target_date - $now_date;
                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';


                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');

                        $row++;
                    }

                    $row++;
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    );
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $summaryData['bonuse']);

                    break;

                case 'payment':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Дата изменения статуса');
//                        ->setCellValue('K1', 'Статус платежа');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);

                    break;

                case 'payed':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Последнее изменение')
                        ->setCellValue('K1', 'Статус оплаты');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
//                            $sheet->getStyle("A4")->getFont()->setBold(true);
//                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
//                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);


                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

            }
        } else {
            die('Не корректный запрос');
        }

        $filename = "Visits-" . date("d-m-Y-Hi") . ".xls";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');

        exit;
    }

//    public function actionExportExcel()
//    {
//        if ($type = Yii::$app->request->get('type')) {
//
////            var_dump($type); die;
//            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
//            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];
//
//            $is_payed_values = [
//                'Not_payed' => 'Не оплачен',
//            ];
//
//            $visit_status_values = [
//                'Completed' => 'Проверен',
//                'Validation' => 'На проверке',
//                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
//
//            ];
//
//            $payment_status_values = [
//                'Accumulation' => 'Накопление',
//                'Withdrawal' => 'Вывод средств',
//                'Charity' => 'Благотворительность',
//                'Payed' => 'Оплачен',
//                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
//
//            ];
//
//            $objPHPExcel = new \PHPExcel();
//            $objPHPExcel->setActiveSheetIndex(0);
//
//            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
//
//            switch (Yii::$app->request->get('type')) {
//                case 'index':
//                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-start-date')));
//                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-end-date')));
//
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['!=', 'total_amount', 0])
//                        ->andWhere(['is_payed' => 'Not_payed'])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//
//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
//
//                    $visit = $query->all();
//
//                    $objPHPExcel->getActiveSheet()->setTitle('Таблица визитов')
//                        ->setCellValue('A1', 'Дата визита')
//                        ->setCellValue('B1', 'Номер анкеты')
//                        ->setCellValue('C1', 'Проект')
//                        ->setCellValue('D1', 'Адрес локации')
//                        ->setCellValue('E1', 'Сумма за визит')
//                        ->setCellValue('F1', 'Доплата')
//                        ->setCellValue('G1', 'Штраф')
//                        ->setCellValue('H1', 'Итого')
//                        ->setCellValue('I1', 'Статус визита')
//                        ->setCellValue('J1', 'Накопительный бонус');
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
//                        array(
//                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
//                            'style' => PHPExcel_Style_Border::BORDER_THIN
//                        )
//                    );
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN
//                            )
//                        )
//                    );
//
//                    $row = 2;
//                    foreach ($visit as $item) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
//                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
//                        $row++;
//                    }
//
//                    $row++;
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
//                        array(
//                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
//                            'style' => PHPExcel_Style_Border::BORDER_THIN
//                        )
//                    );
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN
//                            )
//                        )
//                    );
//
//                    $styleColor = array(
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'FFFFFF')
//                        )
//                    );
//
//                    $styleArray = array(
//                        'font' => array(
//                            'size' => 12,
//                            'bold' => true,
////                            $sheet->getStyle("A4")->getFont()->setBold(true);
////                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
////                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
//                        ),
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'DDDDDD')
//                        )
//                    );
//
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN
//                            ),
//                            'inside' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
//                                'color' => array('rgb' => '666666')
//                            ),
//                        )
//                    );
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
//                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
//                    unset($styleArray);
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);
//
//                    break;
//
//                case 'accumulation':
//                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
//                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));
////                    $query = Visit::find()
////                        ->where(['user_id' => $userSecurityObjectId])
////                        ->andWhere(['payment_status' => ['Accumulation']])
////                        ->andWhere(['visit_status' => ['Completed']])
////                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
////                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
////                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
//                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
//                        ->andWhere(['in', 'payment_status', [null]])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//
//                    $summaryData['bonuse'] = $query
//                        ->sum('accumulated_bonuse');
//                    $summaryData['total'] = $query
//                        ->sum('accumulated_bonuse + total_amount');
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
//
//                    $visit = $query->all();
//
//                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
//                        ->setCellValue('A1', 'Дата визита')
//                        ->setCellValue('B1', 'Номер анкеты')
//                        ->setCellValue('C1', 'Проект')
//                        ->setCellValue('D1', 'Адрес локации')
//                        ->setCellValue('E1', 'Сумма за визит')
//                        ->setCellValue('F1', 'Доплата')
//                        ->setCellValue('G1', 'Штраф')
//                        ->setCellValue('H1', 'Итого')
//                        ->setCellValue('I1', 'Накопительный бонус')
//                        ->setCellValue('J1', 'Итого с бонусом')
//                        ->setCellValue('K1', 'Последнее изменение')
////                        ->setCellValue('L1', 'Статус оплаты')
//                        ->setCellValue('L1', 'Срок депозита');
//
//                    $row = 2;
//                    foreach ($visit as $item) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
//                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
////                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);
//
//
//                        $startDate = strtotime($item->last_modified);
//                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
//                        $now_date = time();
//                        $datediff = $target_date - $now_date;
//                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';
//
//
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');
//
//                        $row++;
//                    }
//
//                    $row++;
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->applyFromArray(
//                        array(
//                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
//                            'style' => PHPExcel_Style_Border::BORDER_THIN
//                        )
//                    );
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN
//                            )
//                        )
//                    );
//
//                    $styleColor = array(
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'FFFFFF')
//                        )
//                    );
//
//                    $styleArray = array(
//                        'font' => array(
//                            'size' => 12,
//                            'bold' => true,
////                            $sheet->getStyle("A4")->getFont()->setBold(true);
////                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
////                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
//                        ),
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'DDDDDD')
//                        )
//                    );
//
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN
//                            ),
//                            'inside' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
//                                'color' => array('rgb' => '666666')
//                            ),
//                        )
//                    );
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
//                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
//                    unset($styleArray);
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $summaryData['bonuse']);
//
//                    break;
//
//                case 'payment':
//                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
//                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
////                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//                    $summaryData['bonuse'] = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//                        ->sum('accumulated_bonuse');
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
//
//                    $visit = $query->all();
//
//                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
//                        ->setCellValue('A1', 'Дата визита')
//                        ->setCellValue('B1', 'Номер анкеты')
//                        ->setCellValue('C1', 'Проект')
//                        ->setCellValue('D1', 'Адрес локации')
//                        ->setCellValue('E1', 'Сумма за визит')
//                        ->setCellValue('F1', 'Доплата')
//                        ->setCellValue('G1', 'Штраф')
//                        ->setCellValue('H1', 'Накопительный бонус')
//                        ->setCellValue('I1', 'Итого')
//                        ->setCellValue('J1', 'Дата изменения статуса');
////                        ->setCellValue('K1', 'Статус платежа');
//
//                    $row = 2;
//
//                    foreach ($visit as $item) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
//                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
////                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);
//
//                        $row++;
//                    }
//
//                    $row++;
//
//                    $styleColor = array(
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'FFFFFF')
//                        )
//                    );
//
//                    $styleArray = array(
//                        'font' => array(
//                            'size' => 12,
////                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
////                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
//                        ),
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'DDDDDD')
//                        )
//                    );
//
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN
//                            ),
//                            'inside' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
//                                'color' => array('rgb' => '666666')
//                            ),
//                        )
//                    );
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
//                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
//                    unset($styleArray);
//
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);
//
//                    break;
//
//                case 'payed':
//                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
//                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['in', 'is_payed', ['Payed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//
////                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//                    $summaryData['bonuse'] = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['in', 'is_payed', ['Payed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//                        ->sum('accumulated_bonuse');
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
//
//                    $visit = $query->all();
//
//                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
//                        ->setCellValue('A1', 'Дата визита')
//                        ->setCellValue('B1', 'Номер анкеты')
//                        ->setCellValue('C1', 'Проект')
//                        ->setCellValue('D1', 'Адрес локации')
//                        ->setCellValue('E1', 'Сумма за визит')
//                        ->setCellValue('F1', 'Доплата')
//                        ->setCellValue('G1', 'Штраф')
//                        ->setCellValue('H1', 'Накопительный бонус')
//                        ->setCellValue('I1', 'Итого')
//                        ->setCellValue('J1', 'Последнее изменение')
//                        ->setCellValue('K1', 'Статус оплаты');
//
//                    $row = 2;
//
//                    foreach ($visit as $item) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
//                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);
//
//                        $row++;
//                    }
//
//                    $row++;
//
//                    $styleColor = array(
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'FFFFFF')
//                        )
//                    );
//
//                    $styleArray = array(
//                        'font' => array(
//                            'size' => 12,
//                            'bold' => true,
////                            $sheet->getStyle("A4")->getFont()->setBold(true);
////                            'underline' => \PHPExcel_Style_Font::UNDERLINE_SINGLE,
////                            'border' => \PHPExcel_Style_Border::BORDER_MEDIUM,
//                        ),
//                        'fill' => array(
//                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
//                            'color' => array('rgb' => 'FFFFFF')
//                        )
//                    );
//
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN
//                            ),
//                            'inside' => array(
//                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
//                                'color' => array('rgb' => '666')
//                            ),
//                        )
//                    );
//
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
//                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
//                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
//                    unset($styleArray);
//
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);
//
//                    break;
//
//            }
//        } else {
//            die('Не корректный запрос');
//        }
//
//        $filename = "Visits-" . date("d-m-Y-Hi") . ".xls";
//
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename=' . $filename);
//        header('Cache-Control: max-age=0');
//        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//
//        $objWriter->save('php://output');
//
//        exit;
//    }

    public function actionExportExcel()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'Дата визита' => 'string',
            'Номер анкеты' => 'integer',
            'id пользователя' => 'integer',
            'Проект' => 'string',
            'Адрес локации' => 'string',
            'Сумма за визит' => 'string',
            'Бонус' => 'string',
            'Штраф' => 'string',
            'Сумма' => 'string',
            'Оплачен' => 'string',
            'Последнее изменение' => 'string',
            'Статус визита' => 'string',
            'Статус оплаты' => 'string',
            'Срок депозита' => 'string'
        ];

        $userId = User::findOne(Yii::$app->user->id)->user_id;

        if ($type = Yii::$app->request->get('type') /*&&
            $userId = Yii::$app->request->get('userId')*/) {
//            $this->debug($type);
            switch ($type) {
                case 'index':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId, 'is_payed' => 'Not_payed'])
                        ->all();
                    $sheetName = 'Таблица визитов';
                    break;
                case 'accum':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['not', ['is_payed' => 'Payed']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->all();
                    $sheetName = 'Накопление средств';
                    break;

                case 'paym':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->all();

                    $sheetName = 'Выплаты';
                    break;

                default:
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->all();
                    $sheetName = 'Все визиты';
            }
        } else {
            return $this->redirect(['index']);
        }

        $paymentStatus = Yii::$app->params['paymentStatus'];

        if (isset($visit)) {
            foreach ($visit as $item) {
                $row[$item->id]['visit_date'] = $item['visit_date'];
                $row[$item->id]['anketa_id'] = $item['anketa_id'];
                $row[$item->id]['user_id'] = $item['user_id'];
                $row[$item->id]['project'] = $item['project'];
                $row[$item->id]['location'] = $item['location_city'] . ' ' . $item['location_address'];
                $row[$item->id]['pay_rate'] = $item['pay_rate'];
                $row[$item->id]['bonuse'] = $item['bonuse'];
                $row[$item->id]['penalty'] = $item['penalty'];
                $row[$item->id]['total_amount'] = $item['total_amount'];
                $row[$item->id]['is_payed'] = $paymentStatus[$item['is_payed']];
                $row[$item->id]['last_modified'] = $item['last_modified'];
                $row[$item->id]['visit_status'] = $item['visit_status'];
                $row[$item->id]['payment_status'] = $item['payment_status'];
                $row[$item->id]['deposit_term'] = $item['deposit_term'];
            }
        } else {
            return $this->redirect(['index']);
        }

        $writer = new XLSXWriter();
        $filename = "Visits_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 20, 20, 40, 30]
        ];

        $writer->writeSheetHeader('Лог пользователя', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Лог пользователя', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportPdf()
    {
        if (Yii::$app->request->get('type')) {

            $userSecurityObjectId = User::find()->where(['id' => Yii::$app->user->identity->id])->one()->user_id;
            $LocationStateRegion = Yii::$app->params['LocationStateRegion'];

            $is_payed_values = [
                'Not_payed' => 'Не оплачен',
            ];

            $visit_status_values = [
                'Completed' => 'Проверен',
                'Validation' => 'На проверке',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
            ];

            $payment_status_values = [
                'Accumulation' => 'Накопление',
                'Withdrawal' => 'Вывод средств',
                'Approved_to_withdrawal' => 'Одобрено для вывода денег',
                'Charity' => 'Благотворительность',
                'Payed' => 'Оплачен',
            ];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

            switch (Yii::$app->request->get('type')) {
                case 'index':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('index-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['!=', 'total_amount', 0])
                        ->andWhere(['is_payed' => 'Not_payed'])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->setTitle('Таблица визитов')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Статус визита')
                        ->setCellValue('J1', 'Накопительный бонус');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $visit_status_values[$item['visit_status']] ?? $item['visit_status']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['accumulated_bonuse']);
                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':j' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);


//                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)
//                        ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
//
//                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
//                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
//                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'accumulation':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('accumulation-end-date')));

//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['payment_status' => ['Accumulation']])
//                        ->andWhere(['visit_status' => ['Completed']])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
//
//                    $query = Visit::find()
//                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
//                        ->andWhere(['visit_status' => 'Approved_to_withdrawal'])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = $query
                        ->sum('accumulated_bonuse');
                    $summaryData['total'] = $query
                        ->sum('accumulated_bonuse + total_amount');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
//                    $summaryData['total_amount'] = $query
//                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
//                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Накопление средств')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Итого')
                        ->setCellValue('I1', 'Накопительный бонус')
                        ->setCellValue('J1', 'Итого с бонусом')
                        ->setCellValue('K1', 'Последнее изменение')
//                        ->setCellValue('L1', 'Статус оплаты')
                        ->setCellValue('L1', 'Срок депозита');

                    $row = 2;
                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['total_amount']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $startDate = strtotime($item->last_modified);
                        $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                        $now_date = time();
                        $datediff = $target_date - $now_date;
                        $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';


                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) . ' дней');


                        $row++;
                    }

                    $row++;

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );
//                    $BStyle = array(
//                        'borders' => array(
//                            'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN
//                            )
//                        )
//                    );

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'DDDDDD')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->applyFromArray($styleColor)->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':L' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:L' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;

                case 'payment':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payment-end-date')));

                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);

                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Rejected', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');

//                    $summaryData['bonuse'] = Visit::summaryData($userSecurityObjectId, $startDate, $endDate)['bonuse'];
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Выплаты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Дата изменения статуса');
//                        ->setCellValue('K1', 'Статус платежа');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
//                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['payment_status']] ?? $item['payment_status']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':J' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);

                    $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);

                    $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['bonuse']);

                    break;

                case 'payed':
                    $startDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-start-date')));
                    $endDate = date('Y-m-d', strtotime(Yii::$app->session->get('payed-end-date')));
                    $query = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')]);
                    $summaryData['bonuse'] = Visit::find()
                        ->where(['user_id' => $userSecurityObjectId])
                        ->andWhere(['in', 'is_payed', ['Payed']])
                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
                        ->sum('accumulated_bonuse');
                    $summaryData['total_amount'] = $query
                        ->sum('total_amount');
                    $visit = $query->all();

                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setTitle('Оплаченные анкеты')
                        ->setCellValue('A1', 'Дата визита')
                        ->setCellValue('B1', 'Номер анкеты')
                        ->setCellValue('C1', 'Проект')
                        ->setCellValue('D1', 'Адрес локации')
                        ->setCellValue('E1', 'Сумма за визит')
                        ->setCellValue('F1', 'Доплата')
                        ->setCellValue('G1', 'Штраф')
                        ->setCellValue('H1', 'Накопительный бонус')
                        ->setCellValue('I1', 'Итого')
                        ->setCellValue('J1', 'Последнее изменение')
                        ->setCellValue('K1', 'Статус оплаты');

                    $row = 2;

                    foreach ($visit as $item) {
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, date('d.m.Y', strtotime($item['visit_date'])));
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, strip_tags($item['project']));
                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['location_city'] . $item['location_address']);
                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['pay_rate']);
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['penalty']);
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['total_amount'] + $item['accumulated_bonuse']);
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['last_modified']);
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $payment_status_values[$item['is_payed']] ?? $item['is_payed']);

                        $row++;
                    }

                    $row++;

                    $styleColor = array(
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $styleArray = array(
                        'font' => array(
                            'size' => 12,
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE
                            ),
                            'inside' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_NONE,
                                'color' => array('rgb' => '666')
                            ),
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->getAlignment()->applyFromArray(
                        array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            'style' => PHPExcel_Style_Border::BORDER_NONE
                        )
                    );

                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_NONE
                            )
                        )
                    );

                    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)->applyFromArray($styleColor); //->applyFromArray($BStyle);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':K' . $row)->applyFromArray($styleArray); //->applyFromArray($BStyle);
                    unset($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:K' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Итого: ');
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $summaryData['total_amount']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $summaryData['bonuse']);

                    break;
            }
        } else {
            die('Не корректный запрос');
        }

        $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibraryPath = Yii::getAlias("@vendor") . "/phpoffice/phpexcel/Classes/PHPExcel/Writer/PDF/mPDF.php";

        \PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath);

        $filename = "Visits-" . date("d-m-Y-Hi") . ".pdf";

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        $objWriter->setSheetIndex(0);
        $objWriter->save('php://output');

        exit;
    }
}
