<?php

namespace frontend\widgets\underMenuInfoLine;

use common\models\Visit;
use Yii;
use yii\base\Widget;

class UnderMenuLineWidget extends Widget
{
    public $items;

    public function run()
    {
//        var_dump($this->items); die;

//        bonuses
//        total
//        pending_payment
//        charity
//        payed

        $userId = Yii::$app->user->id;
        $userSecurityId = Yii::$app->user->identity->user_id;
//var_dump($userSecurityId); die;
        $page = Yii::$app->controller->action->id;

        $startDate = Yii::$app->session->get("$page-start-date");
        $endDate = Yii::$app->session->get("$page-end-date");

        $summaryData['bonuse'] = Visit::summaryData($userSecurityId, $startDate, $endDate)['bonuse'];
//        $summaryData['total_amount'] = Visit::summaryData($this->userSecurityId, $startDate, $endDate)['total_amount'];

        $summaryData['total_amount'] = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');

        return $this->render('underMenuLine', ['summaryData' => $summaryData]);
    }
}
