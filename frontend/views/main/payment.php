<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'ВЫПЛАТЫ';

$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('payment-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('payment-end-date')));

$selectedDate = $startDate . ' - ' . $endDate;

$paymentStatus = [
    'Rejected' => 'Отклонен',
    'Payed' => 'Оплачен',
    'Not_payed' => 'Не оплачен',
    'Waiting_pay' => 'Ожидает выплату',
    'Sent_to_pay' => 'Отправлен на оплату',
    'Charity' => 'Благотворительность',
    'Все' => 'Все',
    'Статус платежа' => 'Статус платежа',
    'Payed_not_bonus' => 'Отправлено на оплату без накопления бонусов',
];

?>
<div class="table-filter-popup">
    <div class="filter-popup-content">
        <i class="close"></i>
        <p>выплата будет произведена в течение 40 рабочих дней от <span id="survey-date"
                                                                        class="bold"><?= $minDate ?></span></p>
        <div class="btn-wrapp">
            <button class="ok">ОК</button>
        </div>
    </div>
</div>
<div class="bonus-wrapper">
    <section class="under-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between payment-info">
                        <li class="under-menu_item">ОЖИДАЕТ ВЫПЛАТ: <?= round($summaryData['waiting_pay'], 2) ?? 0 ?>
                            RUB
                        </li>
                        <!--<li class="under-menu_item">БОНУСОВ: <?/*= round($summaryData['bonuses'], 2) ?? 0 */?>
                            RUB
                        </li>
                        <li class="under-menu_item">БЛАГОТВОРИТЕЛЬНОСТЬ: <?/*= round($summaryData['charity'], 2) ?? 0 */?>
                            RUB
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="/js/libs/customSelect.js?v=1.0.1"></script>

<script>
    var surveyDate = document.getElementById('survey-date').innerHTML;

    function changeSelect(e) {
        // $index = e.target.selectedIndex;
        // console.log($index);
        //
        // if( $index > 1 ) {
        //     filter = $( $(e.target).find('option')[$index] ).attr('data-flitter-parm');
        //     console.log( filter );
        //     $.ajax({
        //         type: 'POST',
        //         url: '/main/payment',
        //         data: {
        //             filter: filter,
        //         },
        //         success: function (data) {
        //             console.log(data);
        //         },
        //         error: function (data) {
        //
        //         }
        //     });
        // }
        $index = e.target.selectedIndex;
        console.log($index);
        if ($index == 5 && surveyDate !== ' --- ') {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            $('.table-filter-popup').fadeIn();
            document.querySelector('.close').onclick = () => {
                $('.table-filter-popup').fadeOut();
                window.location.href = $href;
            }
            document.querySelector('.ok').onclick = () => {
                $('.table-filter-popup').fadeOut();
                window.location.href = $href;
            }
        } else {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            if ($href !== '' || $href !== 'undefind') {
                window.location.href = $href;
            }
        }
    }
</script>

<style>
    body .about-page-popup {
        display: block;
    }

    .row-selector {
        padding-top: 15px;
    }
</style>

<?= $this->render('/modals/payment') ?>

<section class="visits-table" style="min-height: calc(100vh - 280px);">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ВЫПЛАТЫ</p>
                <?= $this->render('/partials/row-selector', ['rowsModel' => $rowsModel]) ?>
            </div>

            <?= $this->render('/partials/data-range', ['page' => 'payment', 'selectedDate' => $selectedDate, 'filterModel' => $filterModel]) ?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'payment']) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['excel/export-pdf', 'type' => 'payment']) ?>
            </div>

        </div>


        <?php $order = $order == 'ASC' ? 'DESC' : 'ASC' ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-tooltip">
                </div>
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="light-grren">
                            <th class="titles"><?= Html::a('Дата визита', ['main/payment', 'sort' => 'visit_date', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Номер анкеты', ['main/payment', 'sort' => 'anketa_id', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Проект', ['main/payment', 'sort' => 'project', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Адрес локации', ['main/payment', 'sort' => 'location_city', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Сумма', ['main/payment', 'sort' => 'total_amount', 'order' => $order]) ?></th>

                            <?php if (is_array($filter)) {
                                $filter = 'Статус платежа';
                            } ?>

                            <th class="titles">
                                <div class="i-select-wrapper i-custom-select select-no-img n-select-no-img">
                                    <select data-popup-open="popup-1" data-no-change onchange="changeSelect( event )"
                                            name="q-2-8"
                                            id="q-2-2">
                                        <option data-hreff="#" datastelsoption value="Статус платежа"
                                                dataurl="/img/icons/1.png">Статус платежа
                                        </option>
                                        <option data-hreff="/main/payment?filter=Все" data-flitter-parm="Оплачен"
                                                selected datastelsoption value="<?= $paymentStatus[$filter] ?>">Все
                                        </option>
                                        <option data-hreff="/main/payment?filter=Rejected" data-flitter-parm="Отклонен"
                                                value="Отклонен"><span>Отклонен</span>
                                        </option>
                                        <option data-hreff="/main/payment?filter=Waiting_pay"
                                                data-flitter-parm="Ожидает выплату" value="Ожидает выплату"><a
                                                    class="btn" data-popup-open="popup-1">Ожидает выплату</a>
                                        <option data-hreff="/main/payment?filter=Charity"
                                                data-flitter-parm="Благотворительность" value="Благотворительность">
                                            <span>Благотворительность</span></option>
                                    </select>
                                </div>
                            </th>

                            <th class="titles"><?= Html::a('Дата изменения статуса', ['main/payment', 'sort' => 'last_modified', 'order' => $order]) ?></th>

                        </tr>

                        <?php Pjax::begin(); ?>
                        <?php $i = 1 ?>
                        <?php foreach ($visit as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">

                                <!--                                определение цвета отклоненых анкет-->
                                <?php if ($item->payment_status == 0) {
                                    $color = 'denied';
                                } else {
                                    $color = '';
                                } ?>

                                <td class="titles_items <?= $color ?>"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->anketa_id ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->location_address ?></td>
                                <td class="titles_items <?= $color ?> summa" data-surcharge="<?= $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm?>"
                                    data-persent="8"><?= $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm ?></td>

                                <?= ($item->is_payed == 'Rejected')
                                    ? "<td class='titles_items relative'>
                                          <div  data-description='Если Вам не ясна причина отмены платежа, обратитесь в службу поддержки ТП'>
                                               <span class='status_icons'>Отклонен<i class=''></i></span>
                                          </div>
                                       </td>"
                                    : "<td class='titles_items check-status' style='position: relative;'>
                                               <div class='status-tooltip-wrapp'>
                                                    <span>Со сроками выплат Вы можете ознакомится во вкладке <a class='bold' href='/bonus'>«Условия»</a></span>   
                                               </div>
                                            <span class='status-name'>" . $paymentStatus[$item->is_payed] . "</span>
                                      </td>";
                                ?>

                                <td class="titles_items <?= $color ?>"><?= date_create($item->last_modified)->Format('d.m.Y') ?></td>

                            </tr>

                            <?php $i++ ?>

                        <?php } ?>

                        <tr class="light-grren">
                            <td class="titrrrles_items" style="padding: 10px; font-size: 14px; font-weight: 600">
                                Итого:
                            </td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items"
                                style="padding: 10px; font-size: 14px; font-weight: 600"><?= round($summaryData['waiting_pay'], 2) ?? 0 ?></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                        </tr>

                    </table>
                    <!-- Подсказки для колонки "Статус" -->
                    <script>
                        var statusItems = document.querySelectorAll('.check-status');
                        for (let i = 0; i < statusItems.length; i++) {
                            statusItems[i].onmouseover = function (e) {
                                var v = $(statusItems[i]).find('.status-name').text();
                                if (v == "Ожидает выплату") {
                                    $(statusItems[i]).children('.status-tooltip-wrapp').show();
                                }
                            }
                            statusItems[i].onmouseout = function (e) {
                                $(statusItems[i]).children('.status-tooltip-wrapp').hide();
                            }
                        }
                    </script>
                    <!--                    Подсказки в хедере таблицы-->
                    <script>
                        var tableTooltip = document.querySelector('.table-tooltip');
                        var tableTitlels = document.querySelectorAll('.titles');
                        var tableWrapp = document.querySelector('.table-over-wrapp');
                        tableTitlels[5].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Информация о проведении выплаты за визит</p>';
                            tableTooltip.style.top = '-36px';
                            tableTooltip.style.right = '182px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[5].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        tableTitlels[6].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Дата изменения состояния статуса платежа.</p>';
                            tableTooltip.style.top = '-34px';
                            tableTooltip.style.right = '12px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[6].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        //Получение значения скролла таблицы
                        tableWrapp.addEventListener("scroll", function (event) {
                            var scroll = event.target.scrollLeft;
                            return scroll;
                        });
                    </script>

                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => [
                            'class' => 'page-number',
                            'style' => 'display: block; margin: -10px -15px; padding: 10px 15px',
                        ],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <script>
            setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
        </script>

        <?php Pjax::end(); ?>

    </div>
</section>
<style>
    .status-tooltip-wrapp {
        display: none;
        position: absolute;
        left: 0;
        top: -48px;
        background: #ffffff;
        box-shadow: 0px 2px 7px 3px rgba(18, 16, 11, 0.15);
        font-size: 12px;
        border-radius: 4px;
        padding: 8px;
        text-align: center;
    }
</style>
