<?php

$this->title = 'ГЛАВНАЯ';

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Visit;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\models\VisitStatus;

$session = Yii::$app->session;

$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('index-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('index-end-date')));
$selectedDate = $startDate . ' - ' . $endDate;

//var_dump($selectedDate); die;

$order = $order == 'ASC' ? 'DESC' : 'ASC';

$visitStatus = Yii::$app->params['visitStatus'];

$managementStatus = [
    'Accumulation' => '-',
];

$LocationStateRegion = Yii::$app->params['LocationStateRegion'];

$paymentStatus = Yii::$app->params['paymentStatus'];

?>

<input type="hidden" id="please_show_video" value="<?= $showVideo ?? '' ?>">

<script>
    var globalSelect;

    function popupStatusConfirm() {
        console.log('confirmed');
    }

    function popupStatusDismiss() {
        console.log('dismiss');
        window.open('https://4serviceru.shopmetrics.com/document.asp?alias=mystadministration.shopper.profile&MODE=SELECT&frmStep=3', '_blank');
    }

    // function showPopUpPaymentData(e, user_id, payment_method, account) {
    //     $index = e.target.selectedIndex;
    //     //
    //     // console.log(e.target.value);
    //     //
    //     var targeted_popup_class = $(e.target).attr('data-popup-open');
    //     $popUp = $('[data-popup="' + targeted_popup_class + '"]');
    //     $popUp.fadeIn();
    //     //
    //     $popUp.find('a').map((i, v,) => {
    //         // $(v).attr('href', ($(v).attr('data-href') + id));
    //         // $(v).attr('data-id', ($(v).attr('data-href') + id));
    //         $popUp.find('#payment-method').html(payment_method);
    //         $popUp.find('#payment-account').html(account);
    //     //
    //     //     // $.pjax.reload({container: '#pjax-wallet', async: false});
    //     //
    //     });
    //
    //     // alert("popup-data");
    //
    //     // alert(user_id + payment_method + account);
    // }
    // function showPopUp(e, id, amount, newBonuse, bonuseDate) {
    //
    //     $index = e.target.selectedIndex;
    //
    //     console.log(e.target.value);
    //
    //     if (e.target.value === 'Withdrawal') {
    //         var targeted_popup_class = $(e.target).attr('data-popup-open');
    //         $popUp = $('[data-popup="' + targeted_popup_class + '"]');
    //         $popUp.fadeIn(350);
    //
    //         $popUp.find('a').map((i, v,) => {
    //             // $(v).attr('href', ($(v).attr('data-href') + id));
    //             $(v).attr('data-id', ($(v).attr('data-href') + id));
    //             $popUp.find('#amount').html(amount);
    //             $popUp.find('#new-bonus-date').html(bonuseDate);
    //             $popUp.find('#new-bonus').html(newBonuse);
    //
    //             // $.pjax.reload({container: '#pjax-wallet', async: false});
    //
    //         });
    //     } else if (e.target.value === 'Charity') {
    //         $.ajax({
    //             type: 'POST',
    //             url: '/main/management',
    //             data: {
    //                 management: 'Charity',
    //                 id: id
    //             },
    //             success: function (data) {
    //                 // console.log(data);
    //                 window.location.reload();
    //                 $.pjax.reload({container: '#pjax-wallet', async: false});
    //                 $.pjax.reload({container: '#pjax-table', async: false});
    //                 // $.pjax.reload({container: '#pjax-charity', async: false});
    //             },
    //             error: function (data) {
    //             }
    //         });
    //     } else if (e.target.value === 'Accumulation') {
    //         // alert(id);
    //         $('.popup-deposit-term').fadeIn(350);
    //
    //
    //     } else if ($index !== 0) {
    //         $href = $($(e.target).find('option')[$index]).attr('data-hreff');
    //         if ($href !== '' || $href !== 'undefind') {
    //             window.location.href = $href;
    //         }
    //     }
    //     var AccumStatusBtn = document.getElementById('accum-btn');
    //     AccumStatusBtn.onclick = function (f) {
    //         console.log(228);
    //
    //
    //         globalSelect = e.target;
    //         const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
    //             return v.value
    //         });
    //         const currentItem = selectList.indexOf("Accumulation");
    //         console.log(globalSelect);
    //         console.log('currentItem', currentItem);
    //         globalSelect.selectedIndex = 1;
    //         $(globalSelect).parent().find('.select-items').children()[0].click();
    //         globalSelect.parentElement.querySelector('.select-selected').click();
    //         $('#popup1').hide();
    //         $('.popup-deposit-term').show();
    //         // f.preventDefault();
    //     };
    //     var charStatusBtn = document.getElementById('char-btn');
    //     charStatusBtn.onclick = function () {
    //         console.log(22888888);
    //         globalSelect = e.target;
    //         const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
    //             return v.value
    //         });
    //         const currentItem = selectList.indexOf("Charity");
    //         console.log(globalSelect);
    //         console.log('currentItem', currentItem);
    //         globalSelect.selectedIndex = 1;
    //         $(globalSelect).parent().find('.select-items').children()[2].click();
    //         globalSelect.parentElement.querySelector('.select-selected').click();
    //     }
    // }

</script>

<script>
    jQuery(".only_number").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
</script>


<?php if ($showModal) { ?>
    <?= $this->render('/modals/issue') ?>
<?php } else { ?>
    <?= $this->render('/modals/index') ?>
<?php } ?>

<div class="popup" id="popup1" data-popup="popup-1">
    <div class="popup-inner" style="position: relative; padding: 20px 0px 0px 0px;">
        <p class="inform-message" style="margin-top: 0px;">Ты уверен, что хочешь вывести свои накопления?
            <!--<span id="amount" class="bold">500 RUB</span>-->
            <!--            <br><span class="bold" id="new-bonus-date">01.12.2018</span> у тебя будет <span class="bold" id="new-bonus">600 RUB.</span>-->
        </p>
        <a class="popup-close" data-popup-close="popup-1" href="#"><img src="/img/close.png" alt=""></a>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="take_money_popup_btn" data-management="Accumulation" data-id="" id="accum-btn" data-href=""
               style="cursor: pointer;">Продолжить
                накопление</a>
            <!--            <a class="take_money_popup_btn" data-management="Charity" data-id="" id="char-btn" data-href=""-->
            <!--               onclick='popupStatus();' style="cursor: pointer;">Отправить на-->
            <!--                благотворительность</a>-->
            <a class="take_money_popup_btn pt-3" data-management="Withdrawal" data-id="" data-href=""
               style="cursor: pointer;">Отправить в кошелек</a>
        </div>
    </div>
</div>

<div class="popup popup-deposit-term" data-popup="popup-deposit-term">
    <div class="popup-inner" style="position: relative;position: relative; padding: 10px 0px 0px 0px;">
        <div class="number-field-wrapper" style="width: 80%; margin: 0 auto;">
            <p class="inform-message" style="margin: 0px 0px">Количество дней
                срока накопления</p>
            <p class="inform-message" style="margin: 0px 0px; color: red">Минимальный срок накопления 91 день</p>
            <input style="width: 100%; border: 1px solid #053b06; padding: 5px; outline: none;" type="text"
                   class="only_number" id="deposit-term">
        </div>
        <a class="popup-close" data-popup-close="popup-deposit-term" href="#"><img src="/img/close.png" alt=""></a>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-center"
             style="position: absolute; bottom: 20px; width: 100%; padding: 0px 45px;">
            <a class="form-buttons popup_btn_deposit_term" data-management="" data-id="100" id="confirm-btn"
               data-href=""
               href="#" onclick=""
               style="cursor: pointer; line-height: 2.3;">Сохранить</a>
        </div>
    </div>
</div>

<div class="home-calc-popup">
    <div class="calculator-popup-wrapper d-flex">
        <div class="calculator-image d-flex justify-content-center">
            <img class="calculator-icon" src="/img/calculator.png" alt="">
        </div>
        <div class="calculator-info">
            <p class="mt-3"><span class="bold">Это</span> <span class="upper bold">депозитный калькулятор.</span></p>
            <p class="mt-3">С его помощью ты можешь <span class="bold f-14">посчитать свой бонус</span>
                за пользование депозитом.</p>
            <div class="calc-confirmation mt-4 ml-5">
                <a class="calc-confirm-btn" href="">Ознакомлен</a>
            </div>
        </div>
    </div>
</div>

<div class="charity-popup">
    <div class="charity-popup-wrapper d-flex flex-column">
        <div class="charity_popup_icon d-flex justify-content-center">
            <img src="/img/support.png" alt="">
        </div>
        <div class="charity_popup_description mt-3">
            <p class="size">БОЛЬШЕ ИНФОРМАЦИИ</p>
            <p class="size">ВО ВКЛАДКЕ</p>
            <p class="bold">БЛАГОТВОРИТЕЛЬНОСТЬ</p>
        </div>
    </div>
</div>

<div class="bonus-wrapper" style="">
    <section class="under-menu" style="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between" style="padding: 0;">
                        <!--<li class="under-menu_item">БОНУСОВ на <? /*= date('d.m.Y', time()) */ ?>
                            : <? /*= round($summaryData['accumulated_bonuse'], 2) ?? 0 */ ?> RUB
                        </li>-->
                        <li class="under-menu_item">
                            ИТОГО: <?= round($summaryData['total_amount'], 2) ?>
                            RUB
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<?= $this->render('/partials/slider', ['sliders' => $sliders]) ?>

<section class="calc">
    <div class="container">
        <div class="row section-margin">

            <?= $this->render('/partials/calculator') ?>

            <!--            блок скрыт по решению от 14/07/2021-->
            <?php if (0) { ?>
                <div class="col-lg-6 col-sm-12 ml-auto achievement-wrapper"> <!--achievement-container-->
                    <p class="achievement-title title-size" style="padding-bottom: 10px;">
                        ВАШЕ ПОСЛЕДНЕЕ ДОСТИЖЕНИЕ
                    </p>
                    <div class="row achievement">
                        <!--                    --><?php //Pjax::begin(['id' => 'pjax-charity']); ?>

                        <?php if (!empty($charitiesDone)) { ?>
                            <div class="all-progress-wrapper">
                                <?php foreach ($charitiesDone as $item) { ?>
                                    <?php if (!empty($item->done)) { ?>
                                        <div class="prog-icon-item tooltip-box">
                                            <p class="icon-name <?= $item->image ?><?= $item->done ? '' : '-disabled-' ?>"><?= $item->done ? $item->title : '' ?></p>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="col-lg-6 col-sm-12 kitty">
                                <img class="kitty_img" src="/img/kitty.png" alt="">
                            </div>
                            <div class="col-lg-6">
                                <p class="achievement_description">
                                    У вас еще нет достижений!
                                    <br>
                                    Измените это прямо сейчас
                                </p>
                            </div>
                        <?php } ?>

                        <!--                    --><?php //Pjax::end(); ?>

                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ТАБЛИЦА ВИЗИТОВ</p>

                <?= $this->render('/partials/row-selector', ['rowsModel' => $rowsModel]) ?>

            </div>

            <?= $this->render('/partials/data-range', ['page' => 'index', 'selectedDate' => $selectedDate, 'filterModel' => $filterModel]) ?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'index']) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['excel/export-pdf', 'type' => 'index']) ?>
            </div>

        </div>

        <script src="/js/libs/customSelect.js"></script>

        <div class="row">
            <div class="col-lg-12 p-r">
                <div class="table-tooltip">
                </div>
                <div class="table-over-wrapp">

                    <?php Pjax::begin(['id' => 'pjax-table']); ?>

                    <table class="table-with-visits" style="max-width: 100%;">
                        <tr class="lite-green">
                            <th class="titles"><?= Html::a('Дата визита', ['main/index', 'sort' => 'visit_date', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles"><?= Html::a('Номер анкеты', ['main/index', 'sort' => 'anketa_id', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles"><?= Html::a('Проект', ['main/index', 'sort' => 'project', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles"><?= Html::a('Адрес локации', ['main/index', 'sort' => 'location_city', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles"><?= Html::a('Стоимость  визита', ['main/index', 'sort' => 'total_amount', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles" style="min-width: 250px;">
                                <?= Html::a('Накопительный Бонус', ['main/index', 'sort' => 'bonuse', 'order' => $order, 'page' => 1]) ?>
                            </th>
                            <th class="titles"><?= Html::a('Статус визита', ['main/index', 'sort' => 'visit_status', 'order' => $order, 'page' => 1]) ?></th>
                            <th class="titles"><?= Html::a('Управление средствами', ['main/index', 'sort' => 'payment_status', 'order' => $order, 'page' => 1]) ?></th>
                        </tr>

                        <!-- для разметки стилей через ряд -->
                        <?php $i = 1 ?>
                        <?php foreach ($visit as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="titles_items"><?= date('d.m.Y', strtotime($item->visit_date)) ?></td>
                                <td class="titles_items"><?= $item->anketa_id ?></td>
                                <td class="titles_items"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="titles_items"><?= $item->location_city . '<br>' . $item->location_address ?></td>
                                <td class="titles_items summa"
                                    data-surcharge=""
                                    data-persent=""><?= $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm ?>
                                    <div class="tooltip-table-with-visits flex">
                                        <img src="/img/wallet2.png" alt="wallet-tooltip">
                                        <p class="info">
                                            Дополнительные платежные пункты - <span
                                                    class="surcharge"><?= $item->bonuse . ' ' . $item->payroll_currency ?? '' ?></span>
                                            <br>
                                            % - <span class="persent"><?= $item->user->bonuse_percent ?></span>
                                        </p>
                                    </div>
                                </td>

                                <!--определение даты бонуса-->
                                <?php
                                $startDate1 = date('d.m.Y', time());
                                $target_date = date('d.m.Y', strtotime($startDate1) + 86400);
                                ?>

                                <td class="titles_items">
                                    <?php foreach ($bonuseResult[$item->anketa_id] as $result_date => $result_item) { ?>
                                        <p class="bonus-result"><?= date('d.m.Y', strtotime($result_date)) ?> бонус
                                            составит <?= $result_item ?> руб</p>
                                    <?php } ?>
                                </td>


                                <?php if ($item->visit_status == 'Rejected') { ?>
                                    <td class='titles_items check-status' style='position: relative;'>
                                        <div class='status-tooltip-wrapp'>
                                            <span>Неверная сумма по анкете обратитесь в СП ТП</span>
                                        </div>
                                        <span class='status-name'><?= $visitStatus[$item->visit_status] ?></span>
                                    </td>
                                <?php } else { ?>
                                    <td class="titles_items" <?= ($item->is_prompt_payment || $item->visit_status == VisitStatus::WAITING_NOT_BONUS) ? 'style="background-color: yellow"' : '' ?>>
                                        <?php if ($item->visit_status == VisitStatus::WAITING_NOT_BONUS): ?>
                                            <?= $visitStatus[$item->visit_status] ?>
                                        <?php elseif ($item->is_prompt_payment): ?>
                                            Гиперсрочная выплата
                                        <?php else: ?>
                                            <?= $visitStatus[$item->visit_status] ?? 'На проверке' ?>
                                        <?php endif; ?>
                                    </td>
                                <?php } ?>

                                <td class="titrrrles_items" style="padding: 10px;">
                                    <?php if ($item->is_prompt_payment
                                        || ($item->visit_status == 'Approved_to_withdrawal' && date('Y-m-d') >= $item->accumulation_start_date)
                                        || $item->visit_status == VisitStatus::WAITING_NOT_BONUS
                                    ) { ?>
                                        <div class="i-serrrlect-wrapper i-select-wrapper i-custom-select">
                                            <select data-popup-open="popup-1"
                                                    onchange="showPopUp(event, <?= $item->id; ?>, <?= $item->total_amount + $item->accumulated_bonuse ?>, <?= $item->total_amount + $item->accumulated_bonuse + Visit::bunusePerDay($item->total_amount + $item->accumulated_bonuse) ?>, '<?= $target_date ?>', '<?= $item->user_id ?>')"
                                                    name="q-2-8" id="q-2-2">

                                                <?php if ($item->visit_status == VisitStatus::WAITING_NOT_BONUS): ?>
                                                    <option datastelsoption>-</option>
                                                    <option value="waiting-not-bonus" data-hreff="#"
                                                            id="waiting-not-bonus"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<?= $item->id ?>">
                                                        <p class="btn">
                                                            Подтвердить срочную выплату без бонусов
                                                        </p>
                                                    </option>
                                                <?php elseif ($item->is_prompt_payment): ?>
                                                    <option datastelsoption
                                                            value="<?= $managementStatus[$item->payment_status] ?? '-' ?>"
                                                            dataurl="/img/icons/1.png">-
                                                        <a class="btn" data-popup-open="popup-deposit-term">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <option value="Withdrawal" data-hreff="#" id="prompt-payment"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<?= $item->id ?>">
                                                        <p class="btn">
                                                            Подтвердить срочную выплату
                                                        </p>
                                                    </option>
                                                    <option value="allPaymants" data-hreff="#" id="prompt-payment-all"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<?= $item->id ?>">
                                                        <p class="btn">
                                                            Подтвердить все срочные выплаты
                                                        </p>
                                                    </option>
                                                <?php endif; ?>
                                                <!--<?php /*} else { */ ?>
                                                    <option datastelsoption
                                                            value="<? /*= $managementStatus[$item->payment_status] ?? '-' */ ?>"
                                                            dataurl="/img/icons/1.png">-
                                                        <a class="btn" data-popup-open="popup-deposit-term">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <option value="Withdrawal" data-href="link3"
                                                            dataurl="/img/icons/2.png">
                                                        <a class="btn" data-popup-open="popup-1">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <!-<option value="allPaymantsAnkets" data-hreff="#" id="prompt-payment-all"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<? /* = $item->id */ ?>">
                                                        <p class="btn">
                                                            Отправить все в кошелек
                                                        </p>
                                                    </option>-->
                                                <!--                                                    <option value="Charity" dataurl="/img/icons/3.png">-->
                                                <!--                                                        Отправить на <br>благотворительность-->
                                                <!--                                                    </option>
                                                <?php /*} */ ?> -->
                                            </select>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                        <tr class="light-grren">
                            <td class="titrrrles_items" style="padding: 10px; font-size: 14px; font-weight: 600">
                                Итого:
                            </td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items"
                                style="padding: 10px; font-size: 14px; font-weight: 600"><?= $table_total ?></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                        </tr>
                        </tr>
                    </table>

                    <script>
                        setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
                    </script>
                    <script>
                        var tableTooltip = document.querySelector('.table-tooltip');
                        var tableTitlels = document.querySelectorAll('.titles');
                        var tableWrapp = document.querySelector('.table-over-wrapp');
                        tableTitlels[5].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Бонус, который Вы можете получить, если не будете сразу выводить оплату за визит</p>';
                            tableTooltip.style.top = '-55px';
                            tableTooltip.style.right = '245px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[5].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        tableTitlels[6].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Состояние анкеты после выполнения визита (На проверке; Проверен; Одобрено к оплате;  Одобрено для вывода денег.)</p>';
                            tableTooltip.style.top = '-93px';
                            tableTooltip.style.right = '92px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[6].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        tableTitlels[7].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Здесь Вы можете сами решить, что делать дальше с анкетой: отдать на благое дело, отправить на оплату или оставить накапливать бонусы</p>';
                            tableTooltip.style.top = '-93px';
                            tableTooltip.style.right = '29px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[7].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }

                        //Получение значения скролла таблицы
                        tableWrapp.addEventListener("scroll", function (event) {
                            var scroll = event.target.scrollLeft;
                            return scroll;
                        });
                        // window.addEventListener('resize',function(e){
                        //     const width  = window.innerWidth || document.documentElement.clientWidth ||
                        //         document.body.clientWidth;
                        //     if(width < 995){
                        //         tableTooltip.style.display = 'none';
                        //     }
                        //     // console.log(width);
                        // });
                    </script>

                    <?php Pjax::end(); ?>

                    <!--                    pagination-->
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => [
                            'class' => 'page-number',
                            'style' => 'display: block; margin: -10px -15px; padding: 10px 15px',
                        ],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

    </div>
</section>

<script src="/js/libs/modal.js"></script>
<script>

    // var expressPeyment = document.querySelectorAll('.select-items div:last-child');
    // console.log(expressPeyment);
    // for (let i = 0; i < expressPeyment.length; i++) {
    //     expressPeyment[i].addEventListener('click', function () {
    //         var management = 'Withdrawal';
    //         var id = $('#prompt-payment').attr('data-id');
    //         $.ajax({
    //             type: 'POST',
    //             url: '/main/management',
    //             data: {
    //                 management: management,
    //                 id: id
    //             },
    //         }).done(function () {
    //             $.pjax.reload({container: '#pjax-table', async: false});
    //             $.pjax.reload({container: '#pjax-wallet', async: false});
    //         }).fail(function () {
    //             alert('Чтото пошло не так');
    //         });
    //     });
    // }
</script>
<!-- media style-->
<style>
    .take_money_popup_btn {

        padding: 8px 8px !important;
    }

    .form-buttons {
        display: block;
        width: 120px;
        height: 40px;
        background-color: #d5d5d5;
        color: #053b06;
        padding: 8px 10px;
        font-size: 10px;
        line-height: 1.2;
        text-decoration: none;
        font-weight: bold;
        text-align: center;
    }

    body {
        overflow-x: hidden;
    }

    body .about-page-popup {
        display: block;
    }

    .row-selector {
        padding-top: 15px;
    }

    .download-file {
        margin-bottom: 0px !important;
    }

    @media (max-width: 992px) {
        .achievement-wrapper {
            margin-top: 20px;
        }

        .calculator-text-wrapper {
            margin: 0 auto;
        }

        .center-text {
            text-align: center;
        }

        .reports {
            align-items: center !important;
        }
    }

    .reports a .download-file {
        cursor: pointer;
    }

    .status-tooltip-wrapp {
        display: none;
        position: absolute;
        left: 0;
        top: -48px;
        background: #ffffff;
        box-shadow: 0px 2px 7px 3px rgba(18, 16, 11, 0.15);
        font-size: 12px;
        border-radius: 4px;
        padding: 8px;
        text-align: center;
    }
</style>

<!-- Подсказки для колонки "Статус" -->
<script>
    var statusItems = document.querySelectorAll('.check-status');
    for (let i = 0; i < statusItems.length; i++) {
        statusItems[i].onmouseover = function (e) {
            var v = $(statusItems[i]).find('.status-name').text();
            if (v === "Расхождение в суммах") {
                $(statusItems[i]).children('.status-tooltip-wrapp').show();
            }
        }
        statusItems[i].onmouseout = function (e) {
            $(statusItems[i]).children('.status-tooltip-wrapp').hide();
        }
    }
</script>
