<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Вопросы / ответы";

?>

<div class="container">
    <h1>ВОПРОСЫ / ОТВЕТЫ «SHOPPER BONUS»</h1>

    <div class="content">
<!--        <thead>-->
<!--        <tr>-->
<!--            <th>Вопрос</th>-->
<!--            <th>Ответ</th>-->
<!--        </tr>-->
<!--        </thead>-->
        <div class="acc">

        <?php foreach ($model as $item) { ?>
            <div class="acc__card">
                <div class="acc__title" style="font-weight: bold;"><?= $item->question ?></div>
                <div class="acc__panel">
                    <span style="word-break:normal;"><?= nl2br($item->answer) ?></span>
                </div>
            </div>
        <?php } ?>

        </div>
    </div>
</div>

<script>
    /* ACCORDEON */
    $(function() {
        $('.acc__title').click(function(j) {

            var dropDown = $(this).closest('.acc__card').find('.acc__panel');
            $(this).closest('.acc').find('.acc__panel').not(dropDown).slideUp();

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).closest('.acc').find('.acc__title.active').removeClass('active');
                $(this).addClass('active');
            }

            dropDown.stop(false, true).slideToggle();
            j.preventDefault();
        });
    });
</script>
