<?php


use kartik\daterange\DateRangePicker;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$session = Yii::$app->session;
$userId = $session->has('userId') ? $session->get('userId') : 1;
$userLogin = $session->has('userLogin') ? $session->get('userLogin') : 'admin';

$this->title = 'НАКОПЛЕНИЯ';

$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('accumulation-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('accumulation-end-date')));

$selectedDate = $startDate . ' - ' . $endDate;

$visitStatus = [
    'Completed' => 'Проверен',
    'Validation' => 'На проверке',
    'On Hold' => 'В процессе',
    'No items' => 'Неопределен',
];

$order = $order == 'ASC' ? 'DESC' : 'ASC';

?>
<style>
    .popup-button {
        width: 100%;
        text-align: center;
        padding: 15px 20px;
    }

    body .popup .popup-inner {
        border-radius: 20px
    }
</style>

<div class="popup" id="popup-send-payment" data-popup="popup-send-payment">
    <div class="popup-inner" style="position: relative; padding: 20px 0px 0px 0px;">
        <a class="popup-close" data-popup-close="popup-send-payment" href="#"><img src="/img/close.png" alt=""></a>
        <p style="margin-left: 20px;text-align: center;padding-top: 20px;">Выберите следующие действия</p>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="form-buttons pay-button" data-id="one" data-popup-close="popup-send-payment" data-popup-open="popup-attention-payment"
               style="cursor: pointer;width: 140px;">Оплатить одну анкету</a>
            <a class="form-buttons pay-button" data-id="all" data-popup-close="popup-send-payment" data-popup-open="popup-attention-payment"
               style="cursor: pointer;width: 220px;">Оплатить все накапливаемые анкеты</a>
        </div>
    </div>
</div>

<div class="popup" id="popup-attention-payment" data-popup="popup-attention-payment">
    <div class="popup-inner" style="position: relative; padding: 20px 0px 0px 0px; min-height: 200px">
        <a class="popup-close" data-popup-close="popup-send-payment" href="#"><img src="/img/close.png" alt=""></a>
        <p style="margin-left: 20px">Подавая на оплату анкеты Вы перестаете накапливать бонусы</p>
        <p style="margin-left: 20px">Вы действительно хотите завершить накопление?</p>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a href="javascript:void(0)" class="form-buttons pay-button" id="pay" data-id="" data-popup-close="popup-attention-payment"
               style="cursor: pointer;">Да, хочу вывести</a>
            <a href="javascript:void(0)" class="form-buttons pay-button" id="" data-id="" data-popup-close="popup-attention-payment"
               style="cursor: pointer;width: 160px">Продолжить накопление</a>
        </div>
    </div>
</div>

<div class="bonus-wrapper">
    <section class="under-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between">
<!--                        <li class="under-menu_item">ИТОГО К ОПЛАТЕ: --><?//= round($summaryData['accumulation'], 2) ?? 0 ?><!-- RUB</li>-->
                        <li class="under-menu_item">ИТОГО К ОПЛАТЕ: <?= round(\common\models\User::findOne(Yii::$app->user->id)->totalAccumulation, 2) ?? 0 ?> RUB</li>
                        <li class="under-menu_item">СУММА БОНУСОВ: <?= round($summaryData['bonus'], 2) ?? 0 ?> RUB</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<style>
    body .about-page-popup {
        display: block;
    }
</style>

<?= $this->render('/modals/accumulation') ?>

<div class="home-calc-popup">
    <div class="calculator-popup-wrapper d-flex">
        <div class="calculator-image d-flex justify-content-center">
            <img class="calculator-icon" src="/img/calculator.png" alt="">
        </div>
        <div class="calculator-info">
            <p class="mt-3"><span class="bold">Это</span> <span class="upper bold">депозитный калькулятор.</span></p>
            <p class="mt-3">С его помощью ты можешь <span class="bold f-14">посчитать свой бонус</span>
                за пользование депозитом.</p>
            <div class="calc-confirmation mt-4 ml-5">
                <a class="calc-confirm-btn" href="">Ознакомлен</a>
            </div>
        </div>
    </div>
</div>

<section class="visits-table" style="min-height: calc(100vh - 280px);">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size-accumulation">НАКОПЛЕНИЯ</p>
            </div>

            <?= $this->render('/partials/data-range', ['page' => 'accumulation', 'selectedDate' => $selectedDate, 'filterModel' => $filterModel]) ?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-btn download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'accumulation']) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['excel/export-pdf', 'type' => 'accumulation']) ?>
            </div>

        </div>
    </div>
    <div class="table-main-container" style="width: 97%; margin: 0 auto;">

        <?php Pjax::begin(['id' => 'pjax-table']); ?>

        <div class="visit-table-container" style="width: 90%; margin: 0 auto; position: relative;">
            <div class="table-tooltip">
            </div>
            <div class="table-over-wrapp">
                <table class="table-with-visits">
                    <tr class="light-grren">

                        <th class="titles-admin"><?= Html::a('Дата визита', ['main/accumulation', 'sort' => 'visit_date', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Номер анкеты', ['main/accumulation', 'sort' => 'anketa_id', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Проект', ['main/accumulation', 'sort' => 'project', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Адрес локации', ['main/accumulation', 'sort' => 'location_address', 'order' => $order]) ?></th>
                        <th class="titles-admin">
                            Сумма
                            <table class="sum-table">
                                <tr>
                                    <th class="sum-column_title">За визит</th>
                                    <th class="sum-column_title">Штраф</th>
                                    <th class="sum-column_title">Доплата</th>
                                    <th class="sum-column_title no-border"><?= Html::a('Итого', ['main/accumulation', 'sort' => 'total_amount', 'order' => $order]) ?></th>
                                </tr>
                            </table>
                        </th>
                        <th class="titles-admin"><?= Html::a('Управление средствами', ['main/accumulation', 'sort' => 'is_payed', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Бонус', ['main/accumulation', 'sort' => 'last_modified', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Срок накопления', ['main/accumulation', 'sort' => 'deposit_term', 'order' => $order]) ?></th>
                    </tr>

                    <?php $i = 1 ?>
                    <?php foreach ($visit as $item) { ?>
                        <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                            <td class="table_data"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                            <td class="table_data"><?= $item->anketa_id ?></td>
                            <td class="table_data"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                            <td class="table_data"><?= $item->location_city . '<br>' . $item->location_address ?></td>
                            <td class="table_data">
                                <table>
                                    <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                    <td class="sum_column_data"><?= $item->penalty ?? 0 ?></td>
                                    <td class="sum_column_data"><?= round(($item->bonuse ?? 0) + $item->wait_bonuse - $item->wait_bonuse_sm, 2) ?></td>

                                    <?php @$summaryData['tableTotal'] += $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm ?>

                                    <td class="sum_column_data <?= $i % 2 == 0 ? 'no-border' : '' ?>"><?= $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm?></td>
                                </table>
                            </td>
                            <td class="table_data acumulation"><?= $item->is_payed == 'Wallet' ? 'В кошельке' : 'Накопление' ?>
                                <div class="btn-wrapper" style="margin-top: 5px;">
                                    <?php if($item->is_payed != 'Wallet') { ?>
                                    <button data-popup-open="<?= $item->is_payed != 'Wallet' ? 'popup-send-payment' : '' ?>"
                                            class="payment-button"
                                            data-id="<?= $item->anketa_id ?>"
                                            style="cursor: pointer;">Оплатить
                                    </button>
                                    <?php } ?>
                                </div>
                            </td>
                            <td class="table_data"><?= $item->accumulated_bonuse ?? 0 ?></td>
                            <td class="table_data">
                                <?= isset($item->accumulation_start_date)
//                                && strtotime($item->accumulation_start_date) > strtotime('2021-06-20')
                                && time() >= strtotime($item->accumulation_start_date)
                                    ? round((strtotime(date('d.m.Y')) - strtotime($item->accumulation_start_date)) / (3600 * 24), 0)
                                    : 0 ?>
                                дней
                            </td>
                        </tr>
                        <?php $i++ ?>
                    <?php } ?>

                    <?php if (!empty($visit)) { ?>
                        <tr class="light-grren" style="height: 70px">
                            <td class="titles-admin text-left" colspan="3">
                                <p style="font-weight: bold;     font-family: arial;">Итого:</p>
                            </td>
                            <td class="titles-admin"></td>
                            <td class="titles-admin text-right" style="margin-right: 15px; font-weight: bold; font-family: arial;"><?= ($summaryData['tableTotal'] ?? 0) ?></td>
                            <td class="titles-admin"></td>
                            <td class="titles-admin">
                                <p style="font-weight: bold; font-family: arial;"><?= round($summaryData['bonus'], 2) ?? 0 ?></p>
                            </td>
                            <td class="titles-admin"></td>
                        </tr>
                    <?php } ?>

                    </tr>
                </table>

                <script>
                    var tableTooltip = document.querySelector('.table-tooltip');
                    var tableTitlels = document.querySelectorAll('.titles-admin');
                    console.log(tableTitlels);
                    var tableWrapp = document.querySelector('.table-over-wrapp');
                    tableTitlels[5].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Возможность получить выплату, нажав в необходимый момент кнопку "оплатить"</p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '193px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[5].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'hidden';
                    }
                    tableTitlels[6].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Сумма бонуса, которая накапливается если не выводить деньги на оплату </p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '81px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[6].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'hidden';
                    }
                    tableTitlels[7].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Количество дней, на протяжении которых, Вы не выводите деньги  </p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '4px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[7].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'visible';
                    }
                    //Получение значения скролла таблицы
                    tableWrapp.addEventListener("scroll", function (event) {
                        var scroll = event.target.scrollLeft;
                        return scroll;
                    });
                </script>
            </div>
        </div>

        <?php Pjax::end(); ?>

        <!--                    pagination-->
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
            'hideOnSinglePage' => true,
            'prevPageLabel' => '&laquo; ',
            'nextPageLabel' => ' &raquo;',

            'linkOptions' => [
                'class' => 'page-number',
                'style' => 'display: block; margin: -10px -15px; padding: 10px 15px',
            ],
            'activePageCssClass' => 'active-page',
        ]);
        ?>

    </div>
</section>

<script>
    $('.take_money_popup_btn_pay').on('click', function () {

        var management = $(this).attr('data-management');
        var id = $(this).attr('data-id');

        alert(management + id);

        $.ajax({
            type: 'POST',
            url: '/main/management',
            data: {
                management: management,
                id: id
            },
            success: function (data) {
                console.log(data);
                $.pjax.reload({container: '#pjax-wallet', async: false});
                $.pjax.reload({container: '#pjax-table', async: false});
                document.location.reload(true);
            },
            error: function (data) {

            }
        });
    });

    $('.visits-table').on('click', '.payment-button',function () {
        $('#pay').attr('data-id', $(this).attr('data-id'));
    });
    $('#popup-send-payment a.pay-button').on('click', function () {
        $('#pay').attr('data-method', $(this).attr('data-id'));
    });
    $('#pay').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax/get-to-pay',
            dataType: "json",
            data: {
                userId: <?= Yii::$app->user->identity->user_id ?>,
                anketaId: $(this).attr('data-id'),
                method: $(this).attr('data-method')
            },
         }).done(function (data) {
            console.log('Запрос принят' + data );
            $.pjax.reload({container: "#pjax-wallet", async: false});
             $.pjax.reload({container: "#pjax-table", async: false});
         }).fail(function (data) {
             console.log(data);
             console.log('Запрос не принят');
         });
    });

    // todo change item->id to item->anketa_id
    function makePay(id) {
        var payBtn = document.querySelectorAll('.press-to-pay');
        for (let i = 0; i < payBtn.length; i++) {
            $(payBtn[i]).attr('data-id', (id));
            $(payBtn[i]).attr('data-management', 'Withdrawal');
            $(payBtn[i]).on('click', function (e) {
                var management = $(this).attr('data-management');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: 'POST',
                    url: '/main/management',
                    data: {
                        management: management,
                        id: id,
                        action: 'press_button'
                    },
                }).done(function () {
                    // $('#popup-payment-bonuse .popup-close').click();
                    $.pjax.reload({container: '#pjax-table', async: false});
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    document.location.reload(true);
                }).fail(function () {
                    alert('Что-то пошло не так');
                });
            });
        }
    };
</script>
