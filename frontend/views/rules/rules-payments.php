<?php

use yii\helpers\Url;

$this->title = "ПРАВИЛА";

?>

<section class="charity_rules section-margin">
    <div class="container">
        <div class="row">
            <p class="news_title mb-4">КАК ПОЛЬЗОВАТЬСЯ ПОРТАЛОМ</p>
            <div class="big-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="rules-btns-container">
                            <a href="<?= Url::to(['rules-index']) ?>"><button type="button" class="btn-rules">ГЛАВНАЯ</button></a>
                            <a href="<?= Url::to(['rules-payments']) ?>"><button type="button" class="btn-rules active-btn">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button></a>
                            <a href="<?= Url::to(['rules-bonus']) ?>"><button type="button" class="btn-rules">НАКОПЛЕНИЕ</button></a>
                            <a href="<?= Url::to(['rules-charity']) ?>"><button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button></a>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <p class="your_progress" style="padding-bottom: 10px; margin-left: 0;">Это страница сохраняет историю <span class="bold"> твоих платежей. </span>
                    </p>
                </div>
                <div class="wrapper">

                    <div class="row">
                        <div class="col-lg-12">
                            <img class="payment-example" src="/img/rules/7.png" alt="example" style="width: 100%; max-width: 100%;">
                        </div>
                    </div>
                    <div class="row mt-30">
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/8.png" alt="example">
                        </div>
                        <div class="col-lg-8">
                            <p class="ex-description">
                                Тут ты можешь отслеживать <span class="bold">статус</span>
                                денежных транзакцийстатус денежных транзакций.
                            </p>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <p class="ex-description2" style="margin-left: 0; padding-bottom: 10px;">Если ты поучаствуешь в <span class="bold">благотворительном</span>
                                проекте, эта транзакция тоже отобразится в выплатах.</p>
                            <img class="payment-example" src="/img/rules/9.png" alt="example" style="width: 100%; max-width: 100%;">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <p class="ex-description2" style="margin-left: 0;">Во вкладке «Оплаченные» ты сможешь видеть <span class="bold">архив</span>  своих <span class="bold">выплат,</span> т.е. <br> те визиты, что уже были оплачены и дату выплат. </p>
                            <img class="payment-example" src="/img/rules/excemple.png" alt="example" style="width: 100%; max-width: 100%; margin-top: 20px;">
                        </div>
                    </div>
                    <div class="row progress-content">
                        <div class="col-lg-12" style="padding: 0;">
                            <div class="rules-btns-container-bottom">
                                <a href="<?= Url::to(['rules-index']) ?>"><button type="button" class="btn-rules">ГЛАВНАЯ</button></a>
                                <a href="<?= Url::to(['rules-payments']) ?>"><button type="button" class="btn-rules active-btn">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button></a>
                                <a href="<?= Url::to(['rules-bonus']) ?>"><button type="button" class="btn-rules">НАКОПЛЕНИЕ</button></a>
                                <a href="<?= Url::to(['rules-charity']) ?>"><button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
