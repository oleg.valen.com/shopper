<?php

use yii\helpers\Url;

$this->title = "ПРАВИЛА";

?>

<section class="charity_rules section-margin">
    <div class="container">
        <div class="row">
            <p class="news_title mb-4">КАК ПОЛЬЗОВАТЬСЯ ПОРТАЛОМ</p>
            <div class="big-wrapper">
                <div class="col-lg-12">
                    <div class="rules-btns-container">
                        <a href="<?= Url::to(['rules-index']) ?>">
                            <button type="button" class="btn-rules active-btn">ГЛАВНАЯ</button>
                        </a>
                        <a href="<?= Url::to(['rules-payments']) ?>">
                            <button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button>
                        </a>
                        <a href="<?= Url::to(['rules-bonus']) ?>">
                            <button type="button" class="btn-rules">НАКОПЛЕНИЕ</button>
                        </a>
                        <a href="<?= Url::to(['rules-charity']) ?>">
                            <button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button>
                        </a>
                    </div>
                </div>
                <div class="wrapper">
                    <p class="your_progress">Это <span class="bold">главная</span> страница, сюда попадают твои
                        выполненные визиты.
                    </p>
                </div>
                <div class="wrapper">
                    <div class="row">
                        <div class="col-lg-8">
                            <p class="ex-description2">Отслеживай статус их вычитки в столбце <span class="bold">Статус визита.</span>
                                <br> Как только они проходят проверку, сумма за визит <br> начинает участвовать в
                                бонусной программе и уже
                                <br> в следующем месяце ты получишь бонус.</p>
                        </div>
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/1.png" alt="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/2.png" alt="example">
                        </div>
                        <div class="col-lg-7 ml-auto">
                            <p class="ex-description">Сумму с начисленным бонусом за конкретный визит ты можешь
                                посмотреть в столбце <span class="bold">Накопительный бонус</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <p class="ex-description2">Отправить сумму на выплату ты можешь в столбце <span
                                        class="bold">Управление средствами,</span> выбрав опцию Отправить на оплату.</p>
                        </div>
                        <div class="col-lg-6" style="display: flex; justify-content: space-around;">
                            <img class="payment-example" src="/img/rules/3.png" alt="example">
                            <img style="max-width: 229px;" class="payment-example" src="/img/rules/4.png" alt="example">
                        </div>
                    </div>
                    <div class="row reverse_it">
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/5_1.png" alt="example" style="max-width: 235px; width: 100%;">
                        </div>
                        <div class="col-lg-7 ml-auto">
                            <p class="ex-description">Когда ты выберешь все нужные тебе визиты, <br> они будут
                                отображаться в, <span class="bold">Кошельке,</span>
                                <br> как в корзине интернет-магазина.</p>
                            <p class="ex-description">Там ты сможешь сверить итоговую сумму за количество визитов в одной выплате. <br>
                                И общую сумму, которую отправил на благотворительность.
                            </p>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-5">
                            <span class="bold">Изменить</span> ЛЮБУЮ личную информацию <br> можно в <strong>shopmetrics,</strong> нажав на «Редактировать профиль» .
                        </div>
                        <div class="col-lg-5  ml-auto">
                            <img class="payment-example" src="/img/rules/6.png" alt="example" style="max-width: 250px; width: 80%;">
                        </div>
                    </div>
                    <div class="row progress-content">
                        <div class="col-lg-12" style="padding: 0;">
                            <div class="rules-btns-container-bottom">
                                <a href="<?= Url::to(['rules-index']) ?>">
                                    <button type="button" class="btn-rules active-btn">ГЛАВНАЯ</button>
                                </a>
                                <a href="<?= Url::to(['rules-payments']) ?>">
                                    <button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button>
                                </a>
                                <a href="<?= Url::to(['rules-bonus']) ?>">
                                    <button type="button" class="btn-rules">НАКОПЛЕНИЕ</button>
                                </a>
                                <a href="<?= Url::to(['rules-charity']) ?>">
                                    <button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    @media (max-width: 992px) {
        .reverse_it{
            display: flex;
            flex-direction: column-reverse;
        }
    }
</style>
