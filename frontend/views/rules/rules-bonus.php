<?php

use yii\helpers\Url;

$this->title = "ПРАВИЛА";

?>

<section class="charity_rules section-margin">
    <div class="container">
        <div class="row">
            <p class="news_title mb-4">КАК ПОЛЬЗОВАТЬСЯ ПОРТАЛОМ</p>
            <div class="big-wrapper">
                <div class="col-lg-12">
                    <div class="rules-btns-container">
                        <a href="<?= Url::to(['rules-index']) ?>"><button type="button" class="btn-rules">ГЛАВНАЯ</button></a>
                        <a href="<?= Url::to(['rules-payments']) ?>"><button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button></a>
                        <a href="<?= Url::to(['rules-bonus']) ?>"><button type="button" class="btn-rules active-btn">НАКОПЛЕНИЕ</button></a>
                        <a href="<?= Url::to(['rules-charity']) ?>"><button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button></a>
                    </div>
                </div>
                <div class="wrapper">
<!--                    <p class="your_progress">Это таблица <span class="bold">бонусов,</span> которые ты можешь получать, <br>-->
<!--                        выполняя простые условия: выводить на оплату <br>-->
<!--                        как можно большее количество визитов за раз, <br>-->
<!--                        или наоборот - не выводить визиты на оплату <br>-->
<!--                        и копить бонусы за их хранение.-->
<!--                    </p>-->
<!--                    <p class="benefit">Выгода как не крути!</p>-->
                    <div class="row" style="margin-top: 40px;">
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/10.png" alt="" style="width: 100%; max-width: 270px;">
                        </div>
                        <div class="col-lg-8">
                            <p class="ex-description2">На этой странице ты сможешь <span class="bold">контролировать</span>
                                когда у тебя заканчивается срок накоплений,
                                какой % уже начислен.
                            </p>
                        </div>
                    </div>
                    <div class="row mt-30">
                        <div class="col-lg-8">
                            <p class="ex-description2">
                                Или если срочно нужно будет вывести деньги на оплату, это можно сделать только
                                с потерей бонусов в этой вкладке, колонка <span class="bold">«Управление средствами».</span>
                            </p>
                        </div>
                        <div class="col-lg-4">
                            <img class="payment-example" src="/img/rules/11.png" alt="">
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div class="rules-btns-container-bottom">
                            <a href="<?= Url::to(['rules-index']) ?>"><button type="button" class="btn-rules">ГЛАВНАЯ</button></a>
                            <a href="<?= Url::to(['rules-payments']) ?>"><button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button></a>
                            <a href="<?= Url::to(['rules-bonus']) ?>"><button type="button" class="btn-rules active-btn">НАКОПЛЕНИЕ</button></a>
                            <a href="<?= Url::to(['rules-charity']) ?>"><button type="button" class="btn-rules">БЛАГОТВОРИТЕЛЬНОСТЬ</button></a>
                        </div>
                    </div>
                </div>
<!--                <div class="shadow-wrapper">-->
<!--                    <div class="row progress-content">-->
<!---->
<!--                        <div class="col-lg-6">-->
<!--                            <p class="bonus_title">БОНУСНЫЕ СТАВКИ</p>-->
<!--                            <table class="bonus_table left_table">-->
<!--                                <tr class="light-green">-->
<!--                                    <th class="bonus_table_title">Бонус за депозит</th>-->
<!--                                    <th class="bonus_table_title">%</th>-->
<!--                                    <th class="bonus_table_title">Активно</th>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">до 3 мес </td>-->
<!--                                    <td class="bonus_table_data">53%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 3 до 6 мес</td>-->
<!--                                    <td class="bonus_table_data">45%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">от 6 до 9 мес</td>-->
<!--                                    <td class="bonus_table_data">21%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 9 до 12 мес</td>-->
<!--                                    <td class="bonus_table_data">8%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">от 12 до 24 мес</td>-->
<!--                                    <td class="bonus_table_data">27%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 24 мес</td>-->
<!--                                    <td class="bonus_table_data">4%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6 mt-6">-->
<!--                            <table class="bonus_table right-table">-->
<!--                                <tr class="light-green">-->
<!--                                    <th class="bonus_table_title">Дополнительный бонус</th>-->
<!--                                    <th class="bonus_table_title">%</th>-->
<!--                                    <th class="bonus_table_title">Активно</th>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">до 500 RUB</td>-->
<!--                                    <td class="bonus_table_data">53%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 500 до 1000 RUB</td>-->
<!--                                    <td class="bonus_table_data">45%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">от 1000 до 1500 RUB</td>-->
<!--                                    <td class="bonus_table_data">21%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 1500 до 2000 RUB</td>-->
<!--                                    <td class="bonus_table_data">8%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td class="bonus_table_data">от 2000 до 2500 RUB</td>-->
<!--                                    <td class="bonus_table_data">27%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                                <tr class="light-green">-->
<!--                                    <td class="bonus_table_data">от 2500 RUB</td>-->
<!--                                    <td class="bonus_table_data">4%</td>-->
<!--                                    <td class="bonus_table_data"></td>-->
<!--                                </tr>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</section>
