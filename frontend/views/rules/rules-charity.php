<?php

use yii\helpers\Url;

$this->title = "ПРАВИЛА";

?>

<section class="charity_rules section-margin">
    <div class="container">
        <div class="row">
            <p class="news_title mb-4">КАК ПОЛЬЗОВАТЬСЯ ПОРТАЛОМ</p>
            <div class="big-wrapper">
                <div class="col-lg-12">
                    <div class="rules-btns-container">
                        <a href="<?= Url::to(['rules-index']) ?>">
                            <button type="button" class="btn-rules">ГЛАВНАЯ</button>
                        </a>
                        <a href="<?= Url::to(['rules-payments']) ?>">
                            <button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button>
                        </a>
                        <a href="<?= Url::to(['rules-bonus']) ?>">
                            <button type="button" class="btn-rules">НАКОПЛЕНИЕ</button>
                        </a>
                        <a href="<?= Url::to(['rules-charity']) ?>">
                            <button type="button" class="btn-rules active-btn">БЛАГОТВОРИТЕЛЬНОСТЬ</button>
                        </a>
                    </div>
                </div>
                <div class="wrapper">
                    <p class="your_progress">Это страница с твоими <span
                                class="bold">достижениями в добрых делах.</span></p>
                    <div class="row progress-content">
                        <div class="col-lg-6 mt-4">
                            <p class="progress_title">Отслеживай свой <span class="bold">личный прогресс</span> <br>
                                и изучай <span class="bold">прогресс компании</span> <br>
                                в благотворительных проектах.</p>
                        </div>
                        <div class="col-lg-6 mt-4">
                            <div class="your_progress_description">
                                <img class="progress-icons" src="/img/rules/12.png" alt="progress" style="">
                            </div>
                        </div>
                    </div>
                    <div class="row progress-content" style="margin-top: 30px;">
                        <div class="col-lg-6">
                            <div class="report-main-container">
                                <p class="report-title">Вы можете ознакомиться с отчетом по собранным благотворительным
                                    средствам:</p>
                                <div class="month-wrapp">
                                    <button class="month_report">Январь</button>
                                    <button class="month_report">Февраль</button>
                                    <button class="month_report">Март</button>
                                    <button class="month_report">Апрель</button>
                                    <button class="month_report disable">Май</button>
                                    <button class="month_report disable">Июнь</button>
                                    <button class="month_report disable">Июль</button>
                                    <button class="month_report disable">Август</button>
                                    <button class="month_report disable">Сентябрь</button>
                                    <button class="month_report disable">Октябрь</button>
                                    <button class="month_report disable">Ноябрь</button>
                                    <button class="month_report disable">Декабрь</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <p class="month-report-details">Нажав на кнопку с названием месяца, ты сможешь загрузить
                                <span class="bold">отчет</span> по благотворительной деятельности компании.</p>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
<!--                    <div class="row progress-content mt-5">-->
<!--                        <div class="col-lg-6">-->
<!--                            <p class="how-to-use">Теперь ты можешь сам определять <br>-->
<!--                                <span class="bold">как будут использованы средства,</span> <br>-->
<!--                                которые ты перечисляешь-->
<!--                                на благотворительный проект.-->
<!--                                Для этого достаточно просто определится-->
<!--                                с выбором и подтвердить его,-->
<!--                                нажав на кнопку.</p>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <p class="choice-title">выберите, на что будут использованы средства</p>-->
<!--                            <div class="choice_buttons_wrapp">-->
<!--                                <button class="choice">Волонтерская организация</button>-->
<!--                                <button class="choice ml-1 mr-1">Частное лицо</button>-->
<!--                                <button class="choice">Благотворительная организация</button>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="row progress-content">
                        <div class="col-lg-6 mt-4">
                            <p class="progress_title">Или более детально узнать про <span class="bold">сотрудничество компания с благотворительным фондом</span></p>
                        </div>
                        <div class="col-lg-6 mt-4">
                                <img src="/img/rules/14_1.png" alt="progress" style="width: 100%; margin: 0 auto;">
                        </div>
                    </div>
                    <div class="row progress-content">
                        <div class="col-lg-12">
                            <div class="rules-btns-container-bottom">
                                <a href="<?= Url::to(['rules-index']) ?>">
                                    <button type="button" class="btn-rules">ГЛАВНАЯ</button>
                                </a>
                                <a href="<?= Url::to(['rules-payments']) ?>">
                                    <button type="button" class="btn-rules">ВЫПЛАТЫ/ОПЛАЧЕННИЕ</button>
                                </a>
                                <a href="<?= Url::to(['rules-bonus']) ?>">
                                    <button type="button" class="btn-rules">НАКОПЛЕНИЕ</button>
                                </a>
                                <a href="<?= Url::to(['rules-charity']) ?>">
                                    <button type="button" class="btn-rules active-btn">БЛАГОТВОРИТЕЛЬНОСТЬ</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
