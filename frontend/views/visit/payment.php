<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VisitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Visit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'visit_date',
            'anketa_id',
            'survey_title:ntext',
            //'client_name',
            //'location_name',
            //'location_state_region',
            //'location_city',
            //'location_address:ntext',
            //'pay_rate',
            //'penalty',
            //'payroll_currency',
            //'total_amount',
            //'payroll_items:ntext',
            //'bonuse',
            //'accumulated_bonuse',
            //'payment_method',
            //'is_payed',
            //'payed_amount',
            //'visit_status',
            //'payment_status',
            //'is_prompt_payment',
            //'deposit_term',
            //'deposit_left',
            //'management',
            //'project',
            //'is_payment_status_changed',
            //'work_flow_status',
            //'survey_status',
            //'accumulation_start_date',
            //'last_modified',
            //'created_at',
            //'updated_at',
            //'first_import_date',
            //'sent_wallet',
            //'sent_charity',
            //'sent_pay',
            //'delete_wallet',
            //'pat_payment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
