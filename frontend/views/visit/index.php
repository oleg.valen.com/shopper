<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'ГЛАВНАЯ';

?>

<div class="bonus-wrapper" style="">
    <section class="under-menu" style="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between" style="padding: 0;">
                        <li class="under-menu_item">БОНУСОВ на <?= date('d.m.Y', time()) ?>
                            : <?= round($summaryData['bonuse'] ?? 0, 2) ?> RUB
                        </li>
                        <li class="under-menu_item">
                            ИТОГО: <?= round(($summaryData['total_amount'] ?? 0) + ($summaryData['accumulated_bonuse'] ?? 0), 2) ?>
                            RUB
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<?= $this->render('/partials/slider', ['sliders' => $sliders]) ?>

<section class="calc">
    <div class="container">
        <div class="row section-margin">

            <?= $this->render('/partials/calculator') ?>

            <div class="col-lg-6 col-sm-12 ml-auto achievement-wrapper"> <!--achievement-container-->
                <p class="achievement-title title-size" style="padding-bottom: 10px;">
                    ВАШЕ ПОСЛЕДНЕЕ ДОСТИЖЕНИЕ
                </p>
                <div class="row achievement">
                    <!--                    --><?php //Pjax::begin(['id' => 'pjax-charity']); ?>

                    <?php if (!empty($charitiesDone)) { ?>
                        <div class="all-progress-wrapper">
                            <?php foreach ($charitiesDone as $item) { ?>
                                <?php if (!empty($item->done)) { ?>
                                    <div class="prog-icon-item tooltip-box">
                                        <p class="icon-name <?= $item->image ?><?= $item->done ? '' : '-disabled-' ?>"><?= $item->done ? $item->title : '' ?></p>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="col-lg-6 col-sm-12 kitty">
                            <img class="kitty_img" src="/img/kitty.png" alt="">
                        </div>
                        <div class="col-lg-6">
                            <p class="achievement_description">
                                У вас еще нет достижений!
                                <br>
                                Измените это прямо сейчас
                            </p>
                        </div>
                    <?php } ?>

                    <!--                    --><?php //Pjax::end(); ?>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ТАБЛИЦА ВИЗИТОВ</p>

                <?/*= $this->render('/partials/row-selector', ['rowsModel' => $rowsModel]) */?>

            </div>

            <?/*= $this->render('/partials/data-range', ['page' => 'index', 'selectedDate' => $selectedDate, 'filterModel' => $filterModel]) */?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'index']) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['excel/export-pdf', 'type' => 'index']) ?>
            </div>

        </div>

        <script src="/js/libs/customSelect.js"></script>

        <div class="row">
            <div class="col-lg-12 p-r">
                <div class="table-tooltip">
                </div>
                <div class="table-over-wrapp">

                    <?php Pjax::begin(['id' => 'pjax-table']); ?>

                    <table class="table-with-visits" style="max-width: 100%;">
                        <tr class="lite-green">
                            <th class="titles"><?= Html::a('Дата визита', ['visit/index', 'sort' => 'visit_date']) ?></th>
                            <th class="titles"><?= Html::a('Номер анкеты', ['visit/index', 'sort' => 'anketa_id']) ?></th>
                            <th class="titles"><?= Html::a('Проект', ['visit/index', 'sort' => 'project']) ?></th>
                            <th class="titles"><?= Html::a('Адрес локации', ['visit/index', 'sort' => 'location_city']) ?></th>
                            <th class="titles"><?= Html::a('Стоимость  визита', ['visit/index', 'sort' => 'total_amount']) ?></th>
                            <th class="titles" style="min-width: 250px;">
                                <?= Html::a('Накопительный Бонус', ['visit/index', 'sort' => 'bonuse']) ?>
                            </th>
                            <th class="titles"><?= Html::a('Статус визита', ['visit/index', 'sort' => 'visit_status']) ?></th>
                            <th class="titles"><?= Html::a('Управление средствами', ['visit/index', 'sort' => 'payment_status']) ?></th>
                        </tr>

                        <!-- для разметки стилей через ряд -->
                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="titles_items"><?= date('d.m.Y', strtotime($item->visit_date)) ?></td>
                                <td class="titles_items"><?= $item->anketa_id ?></td>
                                <td class="titles_items"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="titles_items"><?= $item->location_city . '<br>' . $item->location_address ?></td>
                                <td class="titles_items summa"
                                    data-surcharge=""
                                    data-persent=""><?= $item->total_amount ?>
                                    <div class="tooltip-table-with-visits flex">
                                        <img src="/img/wallet2.png" alt="wallet-tooltip">
                                        <p class="info">
                                            Дополнительные платежные пункты - <span
                                                    class="surcharge"><?= $item->bonuse . ' ' . $item->payroll_currency ?? '' ?></span>
                                            <br>
                                            % - <span class="persent"><?= $item->user->bonuse_percent ?></span>
                                        </p>
                                    </div>
                                </td>

                                <!--определение даты бонуса-->
                                <?php
                                $startDate1 = date('d.m.Y', time());
                                $target_date = date('d.m.Y', strtotime($startDate1) + 86400);
                                ?>

                                <td class="titles_items">
                                    <?php /*foreach ($bonuseResult[$item->anketa_id] as $result_date => $result_item) { */?><!--
                                        <p class="bonus-result"><?/*= date('d.m.Y', strtotime($result_date)) */?> бонус
                                            составит <?/*= $result_item */?> руб</p>
                                    --><?php /*} */?>
                                </td>

                                <td class="titles_items">
                                    <?= $visitStatus[$item->visit_status] ?? null ?>
                                </td>

                                <td class="titrrrles_items" style="padding: 10px;">
                                    <?php if ($item->visit_status == 'Approved_to_withdrawal') { ?>
                                        <div class="i-serrrlect-wrapper i-select-wrapper i-custom-select">
                                            <select data-popup-open="popup-1"
                                                    onchange="showPopUp(event, <?= $item->id; ?>, <?= $item->total_amount + $item->accumulated_bonuse ?>, <?= $item->total_amount + $item->accumulated_bonuse + Visit::bunusePerDay($item->total_amount + $item->accumulated_bonuse) ?>, '<?= $target_date ?>')"
                                                    name="q-2-8" id="q-2-2">


                                                <?php if ($item->is_prompt_payment) { ?>
                                                    <option datastelsoption
                                                            value="<?= $managementStatus[$item->payment_status] ?? '-' ?>"
                                                            dataurl="/img/icons/1.png">-
                                                        <a class="btn" data-popup-open="popup-deposit-term">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <option value="Withdrawal" data-hreff="#" id="prompt-payment"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<?= $item->id ?>">
                                                        <p class="btn">
                                                            Подтвердить срочную выплату
                                                        </p>
                                                    </option>
                                                <?php } else { ?>
                                                    <option datastelsoption
                                                            value="<?= $managementStatus[$item->payment_status] ?? '-' ?>"
                                                            dataurl="/img/icons/1.png">-
                                                        <a class="btn" data-popup-open="popup-deposit-term">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <option value="Withdrawal" data-href="link3"
                                                            dataurl="/img/icons/2.png">
                                                        <a class="btn" data-popup-open="popup-1">
                                                            Отправить в кошелек
                                                        </a>
                                                    </option>
                                                    <option value="Charity" dataurl="/img/icons/3.png">
                                                        Отправить на <br>благотворительность
                                                    </option>
                                                <?php } ?>
                                                <!--<option value="Accumulation" data-hreff="#" dataurl="/img/icons/1.png">
                                                    <a class="btn" data-popup-open="popup-deposit-term"
                                                       onclick="showPopupDepositTerm(event)">
                                                        Продолжить накопление
                                                    </a>
                                                </option>-->


                                            </select>
                                        </div>
                                    <?php } else if ($item->visit_status == 'Completed' && $item->is_prompt_payment) { ?>
                                        <div class="i-serrrlect-wrapper i-select-wrapper i-custom-select">
                                            <select data-popup-open="popup-1"
                                                    onchange="showPopUp(event, <?= $item->id; ?>, <?= $item->total_amount ?>, <?= $item->total_amount + $item->accumulated_bonuse + Visit::bunusePerDay($item->total_amount + $item->accumulated_bonuse) ?>, '<?= $target_date ?>')"
                                                    name="q-2-8" id="q-2-2">

                                                <option datastelsoption
                                                        value="<?= $managementStatus[$item->payment_status] ?? '-' ?>"
                                                        dataurl="/img/icons/1.png">-
                                                    <a class="btn" data-popup-open="popup-deposit-term">
                                                        Отправить в кошелек
                                                    </a>
                                                </option>
                                                <?php if ($item->is_prompt_payment) { ?>
                                                    <option data-hreff="#" id="prompt-payment"
                                                            dataurl="/img/icons/1.png"
                                                            data-id="<?= $item->id ?>">
                                                        <p class="btn">
                                                            Подтвердить срочную выплату
                                                        </p>
                                                    </option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                        <tr class="light-grren">
                            <td class="titrrrles_items" style="padding: 10px; font-size: 14px; font-weight: 600">
                                Итого:
                            </td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items"
                                style="padding: 10px; font-size: 14px; font-weight: 600"><?= '' /*$table_total*/ ?></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                        </tr>
                        </tr>
                    </table>

                    <script>
                        setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
                    </script>
                    <script>
                        var tableTooltip = document.querySelector('.table-tooltip');
                        var tableTitlels = document.querySelectorAll('.titles');
                        var tableWrapp = document.querySelector('.table-over-wrapp');
                        tableTitlels[5].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Бонус, который Вы можете получить, если не будете сразу выводить оплату за визит</p>';
                            tableTooltip.style.top = '-55px';
                            tableTooltip.style.right = '245px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[5].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        tableTitlels[6].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Состояние анкеты после выполнения визита (На проверке; Проверен; Одобрено к оплате;  Одобрено для вывода денег.)</p>';
                            tableTooltip.style.top = '-93px';
                            tableTooltip.style.right = '92px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[6].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        tableTitlels[7].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Здесь Вы можете сами решить, что делать дальше с анкетой: отдать на благое дело, отправить на оплату или оставить накапливать бонусы</p>';
                            tableTooltip.style.top = '-93px';
                            tableTooltip.style.right = '29px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[7].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }

                        //Получение значения скролла таблицы
                        tableWrapp.addEventListener("scroll", function (event) {
                            var scroll = event.target.scrollLeft;
                            return scroll;
                        });
                        // window.addEventListener('resize',function(e){
                        //     const width  = window.innerWidth || document.documentElement.clientWidth ||
                        //         document.body.clientWidth;
                        //     if(width < 995){
                        //         tableTooltip.style.display = 'none';
                        //     }
                        //     // console.log(width);
                        // });
                    </script>

                    <?php Pjax::end(); ?>

                    <!--                    pagination-->
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => [
                            'class' => 'page-number',
                            'style' => 'display: block; margin: -10px -15px; padding: 10px 15px',
                        ],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

    </div>
</section>
