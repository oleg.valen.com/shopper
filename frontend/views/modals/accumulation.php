<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('accumulation-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information-light-green.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это</span> <span class="bold upper">список твоих визитов с накоплениями.</span>
                </p>
                <p class="mt-3"><span class="size">Отслеживай</span> <span
                        class="bold upper"> и подсчитывай выгоду</span>
                </p>
                <p class="mt-3"><span class="size">Как только срок накопления бонуса подойдет к концу, визит снова вернется на <span
                            class="bold upper">ГЛАВНУЮ,</span> но уже с процентами.</span></p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['main/modal', 'page' => 'accumulation']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
