<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('payed-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это </span> <span class="bold upper">СТРАНИЦА ВЫПЛАТ</span></p>
                <p class="mt-3">
                    <span class="size">Данная вкладка поможет тебе держать руку на пульсе по уже оплаченным визитам</span>
                </p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['/main/modal', 'page' => 'payed']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
