<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('charity-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information-pink.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это </span> <span class="bold">страница благотворительности.</span></p>
                <p class="mt-3"><span class="size">Здесь ты можешь отслеживать свой </span> <span class="bold">прогресс достижений</span>
                    <span class="size"> в добрых делах</span></p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn"
                       href="<?= Url::to(['main/modal', 'page' => 'charity']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
