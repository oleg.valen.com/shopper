<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('payment-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information-blue.png" alt="">
            </div>
            <div class="about-page_description">

                <p><span class="size">Это таблица твоих</span> <span class="bold upper">выплат.</span></p>
                <p class="mt-3"><span class="size">Тут ты сможешь увидеть какие визиты ожидают выплату, </span>
                    <span class="size">какие уже отправлены, непосредственно,</span>
                    <span class="bold upper"> на оплату</span></p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['main/modal', 'page' => 'payment']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
