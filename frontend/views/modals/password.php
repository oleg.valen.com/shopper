<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('index-password')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="bold upper">ВАМ НЕОБХОДИМО ИЗМЕНИТЬ ПАРОЛЬ!</span></p>
                <br>
                <p><span class="bold" style="color: red">Дождитесь письма на почту!</span></p>
                <p class="mt-3">Для смены пароля перейдите по ссылке:
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['/site/send-email', 'email' => $email]) ?>">Сменить пароль</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
