<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('index-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это </span> <span class="bold upper">ГЛАВНАЯ СТРАНИЦА</span></p>
                <p class="mt-3">
                    <span class="size">Здесь находятся твои </span>
                    <span class="bold">ВИЗИТЫ,</span>
                    <span class="size"> которые проходят либо уже прошли проверку.</span>
                </p>
                <!--<p class="mt-3">На этой странице ты можешь определить их дальнейшую судьбу: <span class="bold upper">ОТПРАВИТЬ НА БОНУСНЫЙ СЧЕТ, ВЫВЕСТИ НА ОПЛАТУ</span>
                    <span class="size">или</span> <span class="bold upper">СДЕЛАТЬ ДОБРОЕ ДЕЛО.</span></p>-->
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['/main/modal', 'page' => 'index']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
