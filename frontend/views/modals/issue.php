<?php

use yii\helpers\Url;

?>

<?php if (!Yii::$app->getRequest()->getCookies()->has('issue-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information.png" alt="">
            </div>
            <div class="about-page_description">
                <h6>Уважаемый Тайный покупатель!</h6>
                <p>При начислении бонусов и выводе их из копилки произошел технический сбой.</p>
                <p>Просьба <span style="color: red">повторно отправить бонусы на оплату</span> или накопить недостающую сумму.</p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['/main/modal', 'page' => 'issue']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
