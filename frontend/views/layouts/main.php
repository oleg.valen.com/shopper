<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\components\HeaderWidget;

$session = Yii::$app->session;
//$session->destroy();

$userId = $session->has('userId') ? $session->get('userId') : 1;
$userLogin = $session->has('userLogin') ? $session->get('userLogin') : '';

AppAsset::register($this);

$cookies = Yii::$app->request->cookies;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .header {
            background: #053b06;
            position: fixed;
            z-index: 99;
            width: 100%;
        }

        .navbar {
            padding: .5rem 0rem;
        }

        .header .above-menu {
            padding-bottom: 0px;
        }

        .navbar-brand {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .navbar-nav .nav-link {
            padding-right: .1rem !important;
            padding-left: .1rem !important;
        }
        #navbarNav .nav-item {
            margin: 0 5px !important;
        }
        .header .icons {
            margin-left: 10px !important;
        }
    </style>
</head>

<body>
<?php $this->beginBody() ?>

<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="header-wrapper" style="position: relative; min-height: 108px;">
        <header id="header" class="header">
            <div class="container">
                <!--            <div class="row above-menu">-->
                <!--                <div style="width: 50%;">-->
                <!--                    <a style="float: right" class="rules" href="-->
                <? //= Url::to(['rules/rules-index']) ?><!--">FAQ. Вопрос/Ответ</a>-->
                <!--                </div>-->
                <!--                <div class="col-md-12 shopmetrics-link-wrapper">-->
                <!--                    <a class="shopmetrics" href="https://4serviceru.shopmetrics.com" target="_blank"><img-->
                <!--                                class="return-arrow" src="/img/return-arrow.png" alt="">shopmetrics</a>-->
                <!--                </div>-->
                <!--            </div>-->
                <nav class="navbar navbar-expand-lg navbar-dark">
                    <a class="navbar-brand" href="<?= Url::to(['main/index']) ?>"><img class="logo"
                                                                                       src="/img/logoSB.png"
                                                                                       alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?= Url::to(['main/index']) ?>">ГЛАВНАЯ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Url::to(['main/payment']) ?>">ВЫПЛАТЫ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Url::to(['main/payed']) ?>">ОПЛАЧЕННЫЕ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Url::to(['main/accumulation']) ?>">НАКОПЛЕНИЯ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Url::to(['main/bonus']) ?>">УСЛОВИЯ</a>
                            </li>
                            <!--<li class="nav-item">
                                <a class="nav-link" href="<?/*= Url::to(['main/charity']) */?>">БЛАГОТВОРИТЕЛЬНОСТЬ</a>
                            </li>-->
                            <!--<li>
                                <?/*= Html::a("Выйти", ['/site/logout'], [
                                    'data' => ['method' => 'post'],
                                    'class' => 'nav-link text-center',
                                ]); */?>
                            </li>-->
                        </ul>

                        <?= HeaderWidget::widget() ?>

                    </div>
                </nav>
            </div>
        </header>
    </div>

<?php } ?>

<div class="">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php if (!Yii::$app->user->isGuest) { ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 contacts">
                    <div class="rules-wrapper">
                        <a class="rules" href="<?= Url::to('questions') ?>">FAQ. Вопрос/Ответ</a>
                    </div>
                    <a target="_blank" class="feedback" href="https://docs.google.com/forms/d/1dVw57BYNJ4UVGC11d9KUWhH_rWs-SNf6hmM2AqkDnrE/viewform?edit_requested=true">Замечания и
                        Предложения</a><!--https://forms.gle/nHBMvgNAV8Mhv1vc7-->
                    <p class="support">
                        <a href="https://4service.group/ru/kontakty-polevogo-personala/ "
                           style="text-decoration: none; color: #fff" target="_blank">
                            <img class="operator" src="/img/tel-personal.svg" alt="">
                            <span>Служба поддержки</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
