<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SendEmailForm */
/* @var $form ActiveForm */
?>
<div class="site-sendEmail" style="padding: 20px 20px">

    <p>Необходимо ввести e-mail пользователя:</p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->label(false) ?>

    <div class="green-button-wrapp">
        <?= Html::submitButton('Отправить', ['class' => 'green__button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- site-sendEmail -->
