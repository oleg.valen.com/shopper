<?php


?>

    <style>
        h3.centerText {
            text-align: center;
            margin: 2em;
            font-size: 2.2rem;
            font-weight: bold;
            color: #69896a;
        }
        .blockNoVisit {
            min-height: 65vh;
        }
        .center-text, .noVisittext {
            font-size: 2rem;
            color: #69896a;
            font-weight: bold;
        }
    </style>
    <div class="container">
        <div class="blockNoVisit">
            <h3 class="centerText">У вас нет выполненных визитов</h3>
            <p class="noVisittext">Программа "Шоппер бонус" станет доступной после совершения любого визита</p>
        </div>

    </div>



