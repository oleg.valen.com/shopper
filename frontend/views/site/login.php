<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'options' => [
            'class' => 'login-main-form'
        ],
    ]
); ?>

<?php if ($model->scenario === 'loginWithEmail') { ?>
    <label class="login-container"><input class="enter-login login-data" type="text" name="LoginForm[email]"></label>
<?php } else { ?>
    <label class="login-container"><input class="enter-login login-data" type="text" name="LoginForm[username]"></label>
<?php } ?>
<label class="password-container"><input class="password login-data" type="password" name="LoginForm[password]"></label>
<input type="checkbox" class="form-check-input" id="сheck-me" name="LoginForm[rememberMe]">
<label class="remember" for="сheck-me">Запомнить меня</label>
<?= $form->field($model, 'rememberMe')->hiddenInput(['class' => 'form-control'])->label(false, ['class' => 'label-class']) ?>

<div class="green-button-wrapp">
    <?= Html::submitButton('Войти', ['class' => 'green__button', 'name' => 'login-button']) ?>
</div>

<?= Html::a('Забыли пароль?', ['/site/send-email']) ?>

<?php ActiveForm::end(); ?>


