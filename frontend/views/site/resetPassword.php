<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ResetPasswordForm */
/* @var $form ActiveForm */
?>
<div class="site-resetPassword" style="padding: 20px">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'password')->label('Укажите новый пароль') ?>

    <div class="green-button-wrapp">
        <?= Html::submitButton('Изменить', ['class' => 'green__button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- site-resetPassword -->
