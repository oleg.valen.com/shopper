<?php
//
///* @var $this yii\web\View */
//
//$this->title = 'Главная страница';
//
//?>
<!---->
<!--<section class="news">-->
<!--    <div class="container">-->
<!--        <p class="news_title">АКЦИИ/НОВОСТИ</p>-->
<!--        <div class="row">-->
<!--            <div class="col-lg-12">-->
<!--                <div class="grey"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--<section class="calc">-->
<!--    <div class="container">-->
<!--        <div class="row section-margin">-->
<!--            <div class="col-lg-3">-->
<!--                <p class="calculation-title title-size">КАЛЬКУЛЯТОР БОНУСА</p>-->
<!--                <div class="calculation">-->
<!--                    <div class="pt-3">-->
<!--                        <div class="title-in-calculator">-->
<!--                            <p class="result">Калькулятор бонусов</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="calc_buttons_wrapper">-->
<!--                        <div class="btn-container">-->
<!--                            <p class="btn-name">Сумма</p>-->
<!--                            <input class="calc_inp" id="sum" autofocus tabindex="1">-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <p class="btn-name date">Срок накопления</p>-->
<!--                            <input class="calc_inp" id="data" tabindex="2">-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <p class="btn-name">Процент</p>-->
<!--                            <input class="calc_inp" id="percent" tabindex="3">-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <p class="btn-name">Бонус</p>-->
<!--                            <input class="calc_inp" id="bonus" tabindex="4">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-6 col-sm-12 ml-auto achievement-container">-->
<!--                <p class="achievement-title text-center title-size">ВАШЕ ПОСЛЕДНЕЕ ДОСТИЖЕНИЕ</p>-->
<!--                <div class="row achievement">-->
<!---->
<!--                    <div class="col-lg-6 col-sm-12 kitty">-->
<!--                        <img class="kitty_img" src="img/kitty.png" alt="">-->
<!--                    </div>-->
<!--                    <div class="col-lg-6"><p class="achievement_description">У вас еще нету достижений! <br> Измените-->
<!--                            это прямо сейчас</p></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--<section class="visits-table">-->
<!--    <div class="container">-->
<!--        <div class="row section-margin">-->
<!--            <div class="col-lg-3">-->
<!--                <p class="title-size">ТАБЛИЦА ВИЗИТОВ</p>-->
<!--            </div>-->
<!--            <div class="col-lg-3 ml-auto">-->
<!--                <button class="download-file"> Загрузить</button>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-lg-12">-->
<!--                <table class="table-with-visits">-->
<!--                    <tr class="lite-green">-->
<!--                        <th class="titles">Дата визита</th>-->
<!--                        <th class="titles">Номер анкеты</th>-->
<!--                        <th class="titles">Проект</th>-->
<!--                        <th class="titles">Адрес локации</th>-->
<!--                        <th class="titles">Сумма</th>-->
<!--                        <th class="titles">Бонус</th>-->
<!--                        <th class="titles">Статус визита</th>-->
<!--                        <th class="titles">Управление средствами</th>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">—</td>-->
<!--                    </tr>-->
<!--                    <tr class="light-grren">-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">—</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">Накопление</td>-->
<!--                    </tr>-->
<!--                    <tr class="light-grren">-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">—</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">—</td>-->
<!--                    </tr>-->
<!--                    <tr class="light-grren">-->
<!--                        <td class="titles_items">05.09.18</td>-->
<!--                        <td class="titles_items">63834936</td>-->
<!--                        <td class="titles_items">Лукойл</td>-->
<!--                        <td class="titles_items">ул. Руденко 41</td>-->
<!--                        <td class="titles_items">500</td>-->
<!--                        <td class="titles_items">01.12.2018 <p>сумма составит:</p>-->
<!--                            <p class="price">600 RUB</p></td>-->
<!--                        <td class="titles_items">Проверка</td>-->
<!--                        <td class="titles_items">—</td>-->
<!--                    </tr>-->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
