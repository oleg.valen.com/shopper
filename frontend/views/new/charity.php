<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'БЛАГОТВОРИТЕЛЬНОСТЬ';
$month = Yii::$app->params['month'];
$charityReport = Yii::$app->params['charityReport'];

?>

<div class="bonus-wrapper">
    <section class="under-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between">
                        <li class="under-menu_item">СУММА БЛАГОТВОРИТЕЛЬНОСТИ: <?= round($charityTotal, 2) ?? 0 ?>RUB
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<style>
    .modal-header .close {
        margin-left: 0;
    }

    body .about-page-popup {
        display: block;
    }
</style>

<div class="certificate-overlay">
    <div class="cert-image">
        <div class="close-popup">+</div>

    </div>
</div>

<!-- или пользователь сменил пароль -->
    <?= $this->render('/modals/charity') ?>

<!--Модальное окошко о достижениях-->
<?php Modal::begin([
    'size' => 'modal-lg',
    'options' => [
        'id' => 'charity-report',
    ],
    'header' => '<h4>Отчет по благо за <span id="indicated-month"></span> <span id="indicated-year"></span> года</h4>',
]);
?>

<table class="table table-striped">
    <tr>
        <td>Статья</td>
        <td>Сумма</td>
    </tr>
    <tr>
        <td>количество людей которые пожертвовали деньги за месяц</td>
        <td id="indicated-people">89</td>
    </tr>
    <tr>
        <td>сумма, собранная за месяц</td>
        <td id="indicated-total-amount">5 000</td>
    </tr>
    <tr>
        <td>сумма, собранная за квартал</td>
        <td id="indicated-quarter-amount">15 000</td>
    </tr>
    <tr>
        <td>сумма, собранная с начала года (или всего сколько жертвуем)</td>
        <td id="indicated-year-amount">55 000</td>
    </tr>
    <tr>
        <td><strong>ИТОГО</strong></td>
        <td id="sum-amount"><strong>55 000</strong></td>
    </tr>
</table>

<?php Modal::end(); ?>

<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <img class="charity-banner" src="/img/charity.png" alt="banner">
        </div>
        <div class="col-lg-6">
            <p class="news_title">ВАШИ ДОСТИЖЕНИЯ</p>
            <div class="all-progress-wrapper">

                <?php foreach ($charitiesDone as $item) { ?>
                    <div class="prog-icon-item tooltip-box">
                        <div class="tooltip-text">
                            <i>
                                <?= $item->description ?>
                            </i>
                        </div>
                        <p class="icon-name <?= $item->image ?><?= $item->done ? '' : '-disabled' ?>"><?= $item->title ?></p>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-6">
            <h4 class="month-report-title">Вы можете ознакомиться с отчетом по собранным благотворительным
                средствам:</h4>
            <div class="month-wrapp-btn">

                <?php for ($i = 0; $i <= 11; $i++) { ?>
                    <button class="month_report-btn <?= (date('m') <= $i) ? 'disable-btn' : 'enable-btn' ?>"
                            data-month="<?= $month[$i] ?>"
                            data-year="<?= date('Y', time()) ?>"
                            data-peoples="<?= count($charities_table[$i]['people_total'] ?? []) ?>"
                            data-total-amount="<?= $charities_table[$i]['total_amount'] ?? 0 ?>"
                            data-quarter-amount="<?= '-' ?>"
                            data-year-amount="<?= '-' ?>"
                            data-sum-amount="<?= '-' ?>"
                    >
                        <?= $month[$i] ?>
                    </button>
                <?php } ?>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="choose-action">
                <p class="choice-title">
                    КОМПАНИЯ 4SERVICE - ПАРТНЕР БЛАГОТВОРИТЕЛЬНОГО ФОНДА
                    <a class="go-to-site"
                       href="https://www.life-line.ru/o-fonde/korporativnyy-klub/"
                       target="_blank">"ЛИНИЯ ЖИЗНИ"</a>
                </p>
            </div>
            <div class="cert-wrapper" id="show-cert">
            </div>
        </div>
    </div>
</div>

<script>
    var show = document.getElementById('show-cert');
    show.addEventListener('click', function () {
        document.querySelector('.certificate-overlay').style.display = 'flex';
    });
    document.querySelector('.close-popup').addEventListener('click', function () {
        document.querySelector('.certificate-overlay').style.display = 'none';
    });
    $(document).mouseup(function (e) {
        var popup = $('.cert-image');
        if (e.target != popup[0] && popup.has(e.target).length === 0) {
            $('.certificate-overlay').fadeOut();
        }
    });
</script>

<script>
    $(document).ready(function () {
        $(".enable-btn").click(function () {
            var monthName = $(this).attr('data-month');
            var year = $(this).attr('data-year');
            var peopleQuantity = $(this).attr('data-peoples');
            var totalAmount = $(this).attr('data-total-amount');
            var quarterAmount = $(this).attr('data-quarter-amount');
            var yearAmount = $(this).attr('data-year-amount');
            var sumAmount = $(this).attr('data-sum-amount');
            $("#indicated-month").text(monthName);
            $("#indicated-people").text(peopleQuantity);
            $("#indicated-total-amount").text(totalAmount);
            $("#indicated-quarter-amount").text(quarterAmount);
            $("#indicated-year-amount").text(yearAmount);
            $("#indicated-year").text(year);
            $("#sum-amount").text(sumAmount);
            $("#charity-report").modal("show");
        });
    });
</script>
