<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'ВЫПЛАТЫ';

$paymentStatus = Yii::$app->params['paymentStatus'];

?>
<div class="bonus-wrapper">
    <section class="under-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between payment-info">
                        <li class="under-menu_item">ОПЛАЧЕНО: <?= round($summaryData['payed'] ?? 0, 2) ?> RUB</li>
                        <li class="under-menu_item">БОНУСОВ: <?= round($summaryData['bonuses'] ?? 0, 2) ?> RUB
                        </li>
                        <li class="under-menu_item">БЛАГОТВОРИТЕЛЬНОСТЬ: <?= round($summaryData['charity'] ?? 0, 2) ?>
                            RUB
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="/js/libs/customSelect.js"></script>
<script>
    function changeSelect(e) {
        $index = e.target.selectedIndex;
        if ($index !== 0) {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            if ($href !== '' || $href !== 'undefind') {
                window.location.href = $href;
            }
        }
    }
</script>

<style>
    body .about-page-popup {
        display: block;
    }
</style>

<!-- или пользователь сменил пароль -->
<?= $this->render('/modals/payed') ?>

<style>
    .row-selector {
        padding-top: 15px;
    }
</style>

<section class="visits-table" style="min-height: calc(100vh - 280px);">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ОПЛАЧЕННЫЕ АНКЕТЫ</p>

                <?= $this->render('/partials/row-selector-new', ['rowsModel' => $searchModel]) ?>

            </div>

            <?= $this->render('/partials/data-range2', ['page' => 'payed', 'filterModel' => $searchModel]) ?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>',
                    ['export/excel', 'type' => 'payed', 'params' => Yii::$app->request->queryParams]) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['export/export-pdf', 'type' => 'payed']) ?>
            </div>

        </div>

        <?php Pjax::begin(); ?>

        <?php $order = 'ASC' ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-tooltip">
                </div>
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="light-grren">
                            <th class="titles"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('total_amount') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('payment_status') ?></th>
                            <th class="titles"><?= $dataProvider->sort->link('last_modified') ?></th>
                        </tr>

                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">

                                <!-- определение цвета отклоненых анкет-->
                                <?php if ($item->payment_status == 0) {
                                    $color = 'denied';
                                } else {
                                    $color = '';
                                } ?>

                                <td class="titles_items <?= $color ?>"><?= date('d.m.Y', strtotime($item->visit_date)) ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->anketa_id ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="titles_items <?= $color ?>"><?= $item->location_address ?></td>
                                <td class="titles_items <?= $color ?> summa" data-surcharge="<?= $item->bonuse ?>" data-persent="8"><?= $item->payed_amount ?></td>
                                <?php if ($item->is_payed == "Rejected") { ?>
                                    <td class='titles_items relative'>
                                        <div data-description='Если Вам не ясна причина отмены платежа, обратитесь в службу поддержки ТП'>
                                            <span class='status_icons'>Отклонен<i class=''></i></span>
                                        </div>
                                    </td>
                                <?php } else { ?>
                                    <td class='titles_items'><?= $paymentStatus[$item->is_payed] ?></td>
                                <?php } ?>
                                <td class="titles_items <?= $color ?>"><?= date('d.m.Y', strtotime($item->last_modified)) ?></td>
                            </tr>

                            <?php $i++ ?>

                        <?php } ?>

                        <tr class="light-grren">
                            <td class="titrrrles_items" style="padding: 10px; font-size: 14px; font-weight: 600">
                                Итого:
                            </td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items"
                                style="padding: 10px; font-size: 14px; font-weight: 600"><?= $summaryData['total_amount'] ?? 0 ?></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                            <td class="titrrrles_items" style="padding: 10px"></td>
                        </tr>

                    </table>
                    <script>
                        var tableTooltip = document.querySelector('.table-tooltip');
                        var tableTitlels = document.querySelectorAll('.titles');
                        var tableWrapp = document.querySelector('.table-over-wrapp');
                        tableTitlels[5].onmouseover = () => {
                            tableTooltip.innerHTML = '<p>Подтверждение факта оплаты за проведенный визит </p>';
                            tableTooltip.style.top = '-36px';
                            tableTooltip.style.right = '182px';
                            tableTooltip.style.opacity = '1';
                            tableTooltip.style.visibility = 'visible';
                        }
                        tableTitlels[5].onmouseout = () => {
                            tableTooltip.style.opacity = '0';
                            tableTooltip.style.visibility = 'hidden';
                        }
                        //Получение значения скролла таблицы
                        tableWrapp.addEventListener("scroll", function (event) {
                            var scroll = event.target.scrollLeft;
                            return scroll;
                        });
                    </script>
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => [
                            'class' => 'page-number',
                            'style' => 'display: block; margin: -10px -15px; padding: 10px 15px',
                        ],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <script>
            setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
        </script>

        <?php Pjax::end(); ?>

    </div>
</section>

<style>
    body {
        position: relative;
        min-height: 100vh;
    }
</style>
