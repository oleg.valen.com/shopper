<?php


use kartik\daterange\DateRangePicker;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$session = Yii::$app->session;
$userId = $session->has('userId') ? $session->get('userId') : 1;
$userLogin = $session->has('userLogin') ? $session->get('userLogin') : 'admin';

$this->title = 'НАКОПЛЕНИЯ';

$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('accumulation-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('accumulation-end-date')));

$selectedDate = $startDate . ' - ' . $endDate;

$visitStatus = [
    'Completed' => 'Проверен',
    'Validation' => 'На проверке',
    'On Hold' => 'В процессе',
    'No items' => 'Неопределен',
];

$order = $order == 'ASC' ? 'DESC' : 'ASC';

?>
<div class="bonus-wrapper">
    <section class="under-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between">
                        <li class="under-menu_item">ИТОГО К ОПЛАТЕ: <?= round($summaryData['accumulation'], 2) ?? 0 ?> RUB</li>
                        <li class="under-menu_item">СУММА БОНУСОВ: <?= round($summaryData['bonus'], 2) ?? 0 ?> RUB</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<!--<script>-->
<!--    function showPopUpToPay(id) {-->
<!--        $popUp = $('[data-popup="popup-payment-bonuse"]');-->
<!--        $popUp.find('#confirm-btn-pay').attr('data-id', (id));-->
<!--        $popUp.find('#confirm-btn-pay').attr('data-management', 'Withdrawal');-->
<!--    }-->
<!--</script>-->

<style>
    body .about-page-popup {
        display: block;
    }
</style>

<!-- или пользователь сменил пароль -->
<?= $this->render('/modals/accumulation') ?>

<div class="home-calc-popup">
    <div class="calculator-popup-wrapper d-flex">
        <div class="calculator-image d-flex justify-content-center">
            <img class="calculator-icon" src="/img/calculator.png" alt="">
        </div>
        <div class="calculator-info">
            <p class="mt-3"><span class="bold">Это</span> <span class="upper bold">депозитный калькулятор.</span></p>
            <p class="mt-3">С его помощью ты можешь <span class="bold f-14">посчитать свой бонус</span>
                за пользование депозитом.</p>
            <div class="calc-confirmation mt-4 ml-5">
                <a class="calc-confirm-btn" href="">Ознакомлен</a>
            </div>
        </div>
    </div>
</div>

<!--<section class="calc">-->
<!--    <div class="container">-->
<!--        <div class="row section-margin">-->
<!--            <div class="col-lg-3">-->
<!--                <p class="calculation-title title-size"></p>-->
<!--                <div class="calculation">-->
<!--                    <div class="pt-3">-->
<!--                        <div class="title-for-calculator">-->
<!--                            <p class="result">' ◡ '</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="calc_buttons_wrapper">-->
<!--                        <div class="btn-container">-->
<!--                            <button class="calc_btn btn_mt" id="sum"> +</button>-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <button class="calc_btn btn_mt" id="minus" tabindex="2">-</button>-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <button class="calc_btn" id="multiply" tabindex="2">×</button>-->
<!--                        </div>-->
<!--                        <div class="btn-container">-->
<!--                            <button class="calc_btn" id="equal" tabindex="2">=</button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section class="visits-table" style="min-height: calc(100vh - 280px);">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size-accumulation">НАКОПЛЕНИЯ</p>
            </div>

            <?= $this->render('/partials/data-range', ['page' => 'accumulation', 'selectedDate' => $selectedDate, 'filterModel' => $filterModel]) ?>

            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-btn download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'accumulation']) ?>
                <?= Html::a('<button class="download-pdf-file">Загрузить PDF</button>',
                    ['excel/export-pdf', 'type' => 'accumulation']) ?>
            </div>

        </div>
    </div>
    <div class="table-main-container" style="width: 97%; margin: 0 auto;">

        <?php Pjax::begin(['id' => 'pjax-table']); ?>

        <div class="visit-table-container" style="width: 90%; margin: 0 auto; position: relative;">
            <div class="table-tooltip">
            </div>
            <div class="table-over-wrapp">
                <table class="table-with-visits">
                    <tr class="light-grren">

                        <th class="titles-admin"><?= Html::a('Дата визита', ['main/accumulation', 'sort' => 'visit_date', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Номер анкеты', ['main/accumulation', 'sort' => 'anketa_id', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Проект', ['main/accumulation', 'sort' => 'project', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Адрес локации', ['main/accumulation', 'sort' => 'location_address', 'order' => $order]) ?></th>
                        <th class="titles-admin">
                            Сумма
                            <table class="sum-table">
                                <tr>
                                    <th class="sum-column_title">За визит</th>
                                    <th class="sum-column_title">Штраф</th>
                                    <th class="sum-column_title">Доплата</th>
                                    <th class="sum-column_title no-border"><?= Html::a('Итого', ['main/accumulation', 'sort' => 'total_amount', 'order' => $order]) ?></th>
                                </tr>
                            </table>
                        </th>
                        <th class="titles-admin"><?= Html::a('Управление средствами', ['main/accumulation', 'sort' => 'is_payed', 'order' => $order]) ?></th>

                        <th class="titles-admin"><?= Html::a('Бонус', ['main/accumulation', 'sort' => 'last_modified', 'order' => $order]) ?></th>
                        <th class="titles-admin"><?= Html::a('Срок накопления', ['main/accumulation', 'sort' => 'deposit_term', 'order' => $order]) ?></th>
                        <!--                        <th class="titles-admin">Осталось</th>-->
                    </tr>

                    <?php $i = 1 ?>
                    <?php foreach ($visit as $item) { ?>
                        <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                            <td class="table_data"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                            <td class="table_data"><?= $item->anketa_id ?></td>
                            <td class="table_data"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                            <td class="table_data"><?= $item->location_city . '<br>' . $item->location_address ?></td>
                            <td class="table_data">
                                <table>
                                    <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                    <td class="sum_column_data"><?= $item->penalty ?? 0 ?></td>
                                    <td class="sum_column_data"><?= $item->bonuse ?? 0 ?></td>
                                    <td class="sum_column_data <?= $i % 2 == 0 ? 'no-border' : '' ?>"><?= $item->total_amount ?></td>
                                </table>
                            </td>
                            <td class="table_data acumulation">Накопление
                                <div class="btn-wrapper" style="margin-top: 5px;">
                                    <button onclick="makePay(<?= $item->id ?>)" data-management="Withdrawal" data-id=""
                                            class="press-to-pay"
                                            data-popup-close="popup-payment-bonuse"
                                            data-href="" style="cursor: pointer;">Оплатить
                                    </button>
                                </div>
                            </td>
                            <!--                            <td class="table_data">--><!--</td>-->
                            <!-- last_modified -->
                            <td class="table_data"><?= $item->accumulated_bonuse ?? 0 ?></td>

                            <!--                            <td class="table_data">-->

                            <!-- дней</td>-->


                            <?php
                            $startDate = strtotime($item->last_modified);
                            $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                            $now_date = time();
                            $datediff = $target_date - $now_date;
                            $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';
                            ?>

                            <td class="table_data"><?= round((time() - strtotime($item->accumulation_start_date)) / (60 * 60 * 24), 0) ?>
                                дней
                            </td>
                        </tr>
                        <?php $i++ ?>
                    <?php } ?>

                    <?php if (!empty($visit)) { ?>
                        <tr class="light-grren" style="height: 70px">
                            <td class="titles-admin text-left" colspan="3">
                                <p style="font-weight: bold;     font-family: arial;">Итого:</p>
                            </td>
                            <td class="titles-admin"></td>
                            <td class="titles-admin"></td>
                            <td class="titles-admin"></td>
                            <td class="titles-admin">
                                <p style="font-weight: bold;     font-family: arial;"><?= round($summaryData['bonus'], 2) ?? 0 ?></p>
                            </td>
                            <td class="titles-admin"></td>

                            <!--                            <td class="titles-admin"></td>-->
                        </tr>
                    <?php } ?>

                    </tr>
                </table>
                <script>
                    var tableTooltip = document.querySelector('.table-tooltip');
                    var tableTitlels = document.querySelectorAll('.titles-admin');
                    console.log(tableTitlels);
                    var tableWrapp = document.querySelector('.table-over-wrapp');
                    tableTitlels[5].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Возможность получить выплату, нажав в необходимый момент кнопку "оплатить"</p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '193px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[5].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'hidden';
                    }
                    tableTitlels[6].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Сумма бонуса, которая накапливается если не выводить деньги на оплату </p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '81px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[6].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'hidden';
                    }
                    tableTitlels[7].onmouseover = () => {
                        tableTooltip.innerHTML = '<p> Количество дней, на протяжении которых, Вы не выводите деньги  </p>';
                        tableTooltip.style.top = '-28px';
                        tableTooltip.style.right = '4px';
                        tableTooltip.style.opacity = '1';
                        tableTooltip.style.visibility = 'visible';
                    }
                    tableTitlels[7].onmouseout = () => {
                        tableTooltip.style.opacity = '0';
                        tableTooltip.style.visibility = 'visible';
                    }
                    //Получение значения скролла таблицы
                    tableWrapp.addEventListener("scroll", function (event) {
                        var scroll = event.target.scrollLeft;
                        return scroll;
                    });
                </script>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>

<script>
    $('.take_money_popup_btn_pay').on('click', function () {

        var management = $(this).attr('data-management');
        var id = $(this).attr('data-id');

        alert(management + id);

        $.ajax({
            type: 'POST',
            url: '/main/management',
            data: {
                management: management,
                id: id
            },
            success: function (data) {
                console.log(data);
                $.pjax.reload({container: '#pjax-wallet', async: false});
                $.pjax.reload({container: '#pjax-table', async: false});
                document.location.reload(true);
            },
            error: function (data) {

            }
        });
    });

    // todo change item->id to item->anketa_id
    function makePay(id) {
        var payBtn = document.querySelectorAll('.press-to-pay');
        for (let i = 0; i < payBtn.length; i++) {
            $(payBtn[i]).attr('data-id', (id));
            $(payBtn[i]).attr('data-management', 'Withdrawal');
            $(payBtn[i]).on('click', function (e) {
                var management = $(this).attr('data-management');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: 'POST',
                    url: '/main/management',
                    data: {
                        management: management,
                        id: id,
                        action: 'press_button'
                    },
                }).done(function () {
                    // $('#popup-payment-bonuse .popup-close').click();
                    $.pjax.reload({container: '#pjax-table', async: false});
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    document.location.reload(true);
                }).fail(function () {
                    alert('Чтото пошло не так');
                });
            });
        }
    };
</script>


<!--<div class="popup" id="popup-payment-bonuse" data-popup="popup-payment-bonuse">-->
<!--    <div class="popup-inner">-->
<!--        <p class="inform-message">Спасибо. Выплата за данный визит будет произведена в течение 40 рабочих дней</p>-->
<!--        <a class="popup-close" data-popup-close="popup-payment-bonuse" href="#"><img src="/img/close.png" alt=""></a>-->
<!--        <div class="take_money_popup_btns_wrapper d-flex">-->
<!--            <a class="form-buttons" data-management="Withdrawal" data-id="" id="confirm-btn-pay"-->
<!--               data-popup-close="popup-payment-bonuse"-->
<!--               onclick=''-->
<!--               data-href="" style="cursor: pointer;">-->
<!--                OK-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
