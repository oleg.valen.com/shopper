<?php

use yii\widgets\ActiveForm;

?>

<div class="row-selector">
    Строк на странице
    <?php $form = ActiveForm::begin(['id' =>'row-selector-form']); ?>
    <?= $form->field($rowsModel, 'rows')
        ->radioList(array(10 => '10', 20 => '20', 50 => '50'), ['id' => 'rowsSelector'])->label(false);; ?>
    <?php ActiveForm::end() ?>
</div>

<script>
    $('#rowsSelector').change(function () {
        $('#row-selector-form').submit();
    })
</script>
