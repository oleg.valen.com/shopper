<?php

use yii\helpers\Html;

?>

<section class="news">
    <div class="container">
        <p class="news_title">АКЦИИ/НОВОСТИ</p>
        <div class="row">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php for ($i = 0; $i < count($sliders); $i++) { ?>
                            <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>"
                                class="<?= $i == 0 ? 'active' : '' ?>">
                            </li>
                        <?php } ?>
                    </ol>
                    <div class="carousel-inner">
                        <?php $firstId = $sliders[0]->id ?>
                        <?php foreach ($sliders as $image) { ?>
                            <div class="carousel-item <?= $image->id === $firstId ? 'active' : '' ?>">
                                <?= Html::img('/uploads/' . $image->filename, [
                                    'class' => 'd-block w-100',
                                    'width' => '100%',
                                    'alt' => 'Slider Image',
                                    'id' => $image->filename == 'third.jpg' ? 'show-video' : '',
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Popup with video-->
<div class="slider-popup-overlay">
    <div class="slider-popup-body">
        <span class="close-slider-popup">X</span>
        <div class="slider-popup-content">
            <video id="my-video" playsinline controls controlsList="nodownload" muted autoplay >
                <source src="/images/main.mp4" type="video/mp4">
                Ваш браузер не поддерживает HTML видео.
            </video>
        </div>
    </div>
</div>

<!-- Video popup styles-->
<style>
    .slider-popup-overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        display: none;
        z-index: 99999;
    }
    .slider-popup-body {
        margin: 70px auto;
        padding: 40px 30px 40px 30px;
        background: #fff;
        border-radius: 5px;
        width: fit-content;
        position: relative;
        max-height: 85vh;
        overflow: hidden;
    }
    .close-slider-popup {
        position: absolute;
        display: block;
        color: #000;
        font-size: 16px;
        right: 15px;
        top: 10px;
        border: 1px solid #000;
        padding: 5px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
        text-align: center;
        line-height: 20px;
        cursor: pointer;
    }
    .close-slider-popup:hover {
        background: #000;
        color: #fff;
        transition: .3s;
    }
    .slider-popup-content{
        /*max-height: 71vh;*/
        /*overflow-y: auto;*/
        margin-top: 20px;
    }
    #my-video{
        width: 100%    !important;
        height: auto   !important;
        max-width: 900px;
    }
    #show-video {
        cursor: pointer;
    }
    @media  (max-width: 500px) {
        .slider-popup-body{
            padding: 5px;
            background: none;
        }
        .close-slider-popup {
            color: #fff;
            font-size: 16px;
            right: 6px;
            top: 23px;
            border: none;
            padding: 5px;
            width: 20px;
            height: 20px;
            z-index: 9999;
        }
    }
    @media  (max-height: 500px) {
        .slider-popup-body {
            padding: 5px;
            background: transparent;
            margin: 20px auto;
        }
        .close-slider-popup {
            color: #fff;
            font-size: 16px;
            right: 6px;
            top: 23px;
            border: none;
            padding: 5px;
            width: 20px;
            height: 20px;
            z-index: 9999;
        }
    }
</style>

<script>
    let  myVideo = document.getElementById("my-video");
    let showVideoFirstTime = document.getElementById("please_show_video").value;

    if(showVideoFirstTime === '1'){
        $('.slider-popup-overlay').fadeIn();
        myVideo.muted = true;
        myVideo.play();
    }





    /* Show-Close Popup with video */
    $('#show-video').on('click', ()=> {
        $('.slider-popup-overlay').fadeIn();
        myVideo.muted = false;
        myVideo.play();
        // if ($(window).width() < 500) {
        //     myVideo.requestFullscreen();
        // }
    })
    $('.close-slider-popup').on('click', ()=> {
        $('.slider-popup-overlay').hide();
        stopVideo();
    });
    $(document).mouseup(function (e) {
        var popup = $('.slider-popup-body');
        if (e.target!=popup[0]&&popup.has(e.target).length === 0){
            $('.slider-popup-overlay').hide();
            stopVideo();
        }
    });

    function stopVideo(){
        myVideo.pause();
        myVideo.currentTime = 0;
    }

</script>