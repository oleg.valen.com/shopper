<?php

?>

<div class="col-lg-2">
    <div class="calculations">
        <div class="pt-3">
            <div class="title-in-calculator">
                <p class="result">Калькулятор бонусов</p>
            </div>
        </div>
        <div class="calc_button_wrapper">
            <div class="btn-container">
                <p class="btn-name">Сумма</p>
                <input class="calc_inp" id="sum" maxlength="7" autofocus tabindex="1"
                       onkeypress='validate(event)'>
            </div>
            <div class="btn-container">
                <p class="btn-name date">Срок накопления</p>
                <input class="calc_inp" id="data" maxlength="7" tabindex="2" onkeypress='validate(event)'
                       oninput='calculate()'>
            </div>
            <div class="btn-container">
                <p class="btn-name">Процент</p>
                <input class="calc_inp" id="percent" tabindex="3" disabled>
            </div>
            <div class="btn-container">
                <p class="btn-name">Бонус</p>
                <input class="calc_inp" id="bonus" tabindex="4" disabled>
            </div>
        </div>
    </div>
</div>
<style>
    .calculator-text {
        font-size: 11.3px;
    }
</style>

<div class="col-sm-8 col-lg-4 calculator-text-wrapper" style="margin-top: 8px">
    <p class="calculator-text center-text">Хочешь узнать сколько бонусов можно заработать?</p>
    <p class="calculator-text center-text">Введи данные и калькулятор их посчитает.</p><br>
    <p class="calculator-text"><b>Сумма</b> – вознаграждение за совершенные визитов, которые Вы отправите (-ли) на накопление - вводится вручную.</p>
    <p class="calculator-text"><b>Срок накопления</b> – общее количество календарных дней хранения вознаграждения в Шопер Бонусе</p>
    <p class="calculator-text"><b>Процент</b> – автоматически  определяется в соответствии с указанным  сроком накопления.</p>
    <p class="calculator-text"><b>Бонус</b> – процентный коэффициент, рассчитанный по формуле при определенном сроке накопления</p>
</div>

<script>
    function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

    function calculate() {
        var sum = document.getElementById('sum').value;
        var data = document.getElementById('data').value;
        var percent = document.getElementById('percent');
        var bonus = document.getElementById('bonus');

        if (data < 0 && data !== '') {
            percent.value = '0%';
            bonus.value = (sum * 0 / 365) * data;
            bonus.value = (bonus.value * 1).toFixed(2);
        } else if (data >= 1 && data <= 180) {
            percent.value = '9%';
            bonus.value = (sum * 0.09 / 365) * data;
            bonus.value = (bonus.value * 1).toFixed(2);
        } else if (data >= 180 && data < 364) {
            percent.value = '10%';
            bonus.value = (sum * 0.10 / 365) * data;
            bonus.value = (bonus.value * 1).toFixed(2);
        } else if (data >= 364) {
            percent.value = '11%';
            bonus.value = (sum * 0.11 / 365) * data;
            bonus.value = (bonus.value * 1).toFixed(2);
        } else if (data === '') {
            percent.value = '';
            bonus.value = '';
        }
    }
</script>
