<?php

use kartik\daterange\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="filters">

    <?php $form = ActiveForm::begin([
        'id' => 'date-selector',
        'action' => [$page],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <!--        блок выбора даты-->
    <div class="box">
        <?php

        //        $filterModel->date = $selectedDate;

        echo $form->field($searchModel, 'date', [
            'options' => ['class' => 'drp-container form-group', 'id' => 'select-date']])
            ->widget(DateRangePicker::classname(), [
                'convertFormat' => true,
                'value' => $searchModel->date,
                'presetDropdown' => false,
                'hideInput' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' - ',
                    ],
//                                'timePicker'=>true,
                    'opens' => 'right',
                    'ranges' => [
                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"],
                    ],
                ],

            ])
            ->label(false);
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script>
        $('input.form-control').change(function () {
            $("#date-selector").submit()
        });
    </script>
</div>
