<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class RowForm extends Model
{
    public $rows;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rows'], 'integer'],
        ];
    }
}
