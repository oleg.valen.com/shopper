<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadImage extends Model{

    public $image;
    public $imageFile;

    public function rules(){
        return[
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', /*'skipOnEmpty' => true, */ 'extensions' => 'png, jpg, jpeg, gif, png, tif, tiff, pdf'],
        ];
    }

    public function upload_image()
    {
        if (!empty($this) && $this->validate()) {
            if ($this->imageFile) {
                $time = time();
                $fileName = $this->imageFile->baseName . $time;
                $extansionFile = $this->imageFile->extension;

                if ($this->imageFile) {
                    $userImage = User::find()->where(['id' => \Yii::$app->user->id])->one();
                    $this->imageFile->saveAs("uploads/{$fileName}.{$extansionFile}");
                    $this->image = "{$fileName}.{$extansionFile}";
                    $this->imageFile = "{$fileName}.{$extansionFile}";

                    $userImage->avatar = "{$fileName}.{$extansionFile}";
                    $userImage->save(false);
                }
            }
        } else {
            return false;
        }
    }

}