<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

if ($userData->payment_method == 'Банковская карта') {
    $account = ($userData->bank_name != 'Другое' ? $userData->bank_name : '') . ' ' . $userData->bank_account;
} else if ($userData->payment_method == 'QIWI кошелек') {
    $account = $userData->payment_method . ' ' . $userData->qiwi_wallet;
} else if ($userData->payment_method == 'Пополнение счета на мобильном телефоне') {
    $account = 'Пополнение телефона ' . $userData->phone_number;
} else {
    $account = 'Не определен';
}

?>

<div class="popup" data-popup="popup-charity" id="popup-charity">
    <div class="popup-inner" style="height: 180px">
        <p class="inform-message">Благодарим, что дарите шанс на здоровое будущее детям! <br>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="form-buttons" data-management="" data-id="" id="confirm-btn" data-href=""
               href="<?= Url::to(['main/pay']) ?>"
               onclick="popupStatusConfirm();" style="cursor: pointer;">Да</a>
            <a class="form-buttons" data-popup-close="popup-charity" data-management="" data-id="" id="confirm-btn"
               data-href=""
               href="<?= Url::to(['main/pay']) ?>"
               onclick="" style="cursor: pointer;">Закрыть</a>
        </div>
    </div>
</div>

<div class="popup" data-popup="popup-payment" id="popup-payment">
    <div class="popup-inner">
        <p class="inform-message" style="text-align: center; padding: 30px 10px 0px 10px; margin-top: 0;">Со сроками выплат Вы можете ознакомится во вкладке <a class="bold"
                                                                                                                                                                href="/bonus"
                                                                                                                                                                target="_blank">«Условия»</a>
        </p>
        <a class="popup-close" data-popup-close="popup-payment" href="#"><img src="/img/close.png" alt=""></a>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="form-buttons" data-management="" data-id="" id="confirm-btn" data-href=""
               href="<?= Url::to(['main/pay']) ?>"
               onclick="popupStatusConfirm();" style="cursor: pointer;">Да</a>
            <!--<a class="form-buttons" data-management="" data-id="" id="confirm-btn" data-popup-close="popup-payment"
               data-popup-open="popup-payment-data"
               onclick='showPopUpPaymentData(event, "<? /*= $userData->user_id */ ?>", "<? /*= $userData->payment_method; */ ?>", "<? /*= $account; */ ?>" )'
               data-href="" style="cursor: pointer; line-height: 22px;">
                Да
            </a>-->

            <a class="form-buttons" data-management="" data-id="" id="dismiss-btn" data-href=""
               data-popup-close="popup-payment"
               onclick="" style="cursor: pointer; line-height: 22px;">Нет</a>
        </div>
    </div>
</div>

<div class="popup" data-popup="popup-bonus-anketa" id="no-bonus-anketa">
    <div class="popup-inner" style="height: 240px">
        <p class="inform-message"></p>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="form-buttons" data-popup-close="popup-bonus-anketa" style="cursor: pointer;margin: 0 auto;">OK</a>
        </div>
    </div>
</div>

<div class="d-flex icons ml-auto">
    <div class="shopmetrics-link-wrapper">
        <a class="shopmetrics" href="https://4serviceru.shopmetrics.com" target="_blank">
            <img class="return-arrow" src="/img/return-arrow.png" alt="">shopmetrics
        </a>
    </div>
    <div class="user-nav-icons" style="display: flex;">
        <?php Pjax::begin(['id' => 'pjax-avatar']); ?>
        <div class="avatar">
            <div class="avatar-pop-up i-pop-up">
                <div class="name">
                    <?= $userData->family_name . ' ' . $userData->name ?>
                    <p style="text-transform: uppercase"><?= $userData->username ?></p>
                    <img src="/img/county-flags/russia.svg" alt="" class="flag">
                </div>
                <div class="any-fields">
                    <!--<p>Ставка: <? /*= $userData->bonuse_percent */ ?> %</p>
                    <p>Бонусов: <? /*= $userData->accumulated_bonuse ?? 0 */ ?> RUB</p>
                    <p>E-mail в Solar Staff: <a href="mailto:<? /*= $userData->email */ ?>"><? /*= $userData->email */ ?></a></p>-->
                    <!--<p>Способ оплаты: <? /*= $userData->payment_method */ ?></p>-->
                    <?php /*if ($userData->payment_method == 'Банковская карта') { */ ?><!--
                        <p>Реквизиты: <? /*= $userData->bank_account */ ?></p>
                    <?php /*} else if ($userData->payment_method == 'QIWI кошелек') { */ ?>
                        <p>Реквизиты: <? /*= $userData->qiwi_wallet */ ?></p>
                    <?php /*} else if ($userData->payment_method == 'Пополнение счета на мобильном телефоне') { */ ?>
                        <p>Реквизиты: <? /*= $userData->phone_number */ ?></p>
                    <?php /*} else { */ ?>
                        <p>Реквизиты: - </p>
                    <?php /*} */ ?>
                    <p>e-mail: <a href="mailto:<? /*= $userData->email */ ?>"><? /*= $userData->email */ ?></a></p>-->
                </div>
                <div class="bottom-green-help-info">
                    <a href="https://4serviceru.shopmetrics.com/document.asp?alias=mystadministration.shopper.profile&MODE=SELECT&frmStep=3"
                       target="_blank">Редактировать профиль</a>
                    <a href="#" id="image-apply"><p>Сменить аватар</p></a>
                </div>
            </div>
        </div>
        <?php Pjax::end() ?>

        <?php Pjax::begin(['id' => 'pjax-wallet', 'options' => ['style' => 'display:flex']]); ?>

        <div class="wallet wallet-ankets" style="padding-bottom: 10px">
            <div class="wallet-image">
                <img class="" src="/img/wallet.png" alt="wallet">
                <?php if ($walletCount > 0) { ?>
                    <span class="badge badge-light"><?= $walletCount ?? '-' ?></span>
                <?php } ?>
            </div>
            <div class="wallet-pop-up i-pop-up" id="wallet-items" style="">
                <?php if (isset($wallet[0])) { ?>
                    <table class="wallet-table-header">
                        <thead>
                        <tr>
                            <th>Номер анкеты</th>
                            <th>Сумма</th>
                            <th>Удалить</th>
                            <th>Назначение платежа</th>
                        </tr>
                        </thead>
                    </table>
                    <div class="walet-table-wrapper waller-yable-scroller">
                        <table class="wallet-table-body">
                            <tbody>

                            <?php $payment_status_values = [
                                'Charity' => 'Благотворительность',
                                'Withdrawal' => 'Вывод',
                            ] ?>

                            <?php $summ = 0 ?>
                            <?php $summCharity = 0 ?>
                            <?php foreach ($wallet as $item) { ?>
                                <tr id="walet-<?= $item['id'] ?>">
                                    <td><?= $item->anketa_id ?></td>
                                    <td><?= ($item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm) ?></td>
                                    <td>
                                        <p class="remove-item" data-anketa-id="<?= $item->anketa_id ?>">X</p>
                                    </td>
                                    <td><?= $payment_status_values[$item->payment_status] ?? 'Вывод' ?></td>
                                    <?php $summ += $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm ?>
                                    <?php $summCharity += $item->payment_status == 'Charity' ? $item->total_amount : 0 ?>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </div>

                    <div class="bonus-bottom-info flex j-s-b">
                        <div>Всего к оплате</div>
                        <div id="payment-amount" data-amount="<?= $summ ?? 0 ?>"><?= $summ ?? 0 ?> RUB</div>
                    </div>
<!--                    <div class="bonus-bottom-info flex j-s-b">-->
<!--                        <div>Всего на Благотворительность</div>-->
<!--                        <div id="charity-amount" data-amount="--><?//= $summCharity ?? 0 ?><!--">--><?//= $summCharity ?? 0 ?><!-- RUB</div>-->
<!--                    </div>-->
                    <div class="bonus-bottom-btn flex j-c">
                        <button class="i-btn" onclick="popupLauncher()">
                            <p id="pay-button">Отправить на оплату</p>
                        </button>
                    </div>

                <?php } else { ?>
                    <p><strong>Кошелек пуст</strong></p>
                <?php } ?>

            </div>
        </div>
        <div class="wallet" style="margin: -7px 0 0 10px; display: block;">
            <img width="50" src="/img/pig-icon.png" alt="bonus">
            <div style="margin: -27px 0 0 -8px;font-size: 9px;text-align: center;"><?= $bonus = round($bonus, 2) ?? 0 ?></div>
            <div class="wallet-pop-up i-pop-up">
                <?php if ($bonus < 50): ?>
                    <div class="bonus-bottom-info">
                        <div>Минимальная сумма вывода - 50 RUB</div>
                        <div style="color: #2c6f3b;">Осталось накопить - <?= 50 - $bonus ?> RUB</div>
                    </div>

                <?php else: ?>
                    <table class="wallet-table-header">
                        <thead>
                        <tr>
                            <th>Сумма</th>
                            <th>Назначение платежа</th>
                        </tr>
                        </thead>
                        <tbody style="font-size: 9px;">
                        <tr>
                            <td><?= $bonus ?></td>
                            <td>Выплата бонусов</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="bonus-bottom-info flex j-s-b">
                        <div>Итого на оплату</div>
                        <div><?= $bonus ?> RUB</div>
                    </div>
                    <div class="bonus-bottom-btn flex j-c">
                        <?php if(empty($summ)) { ?>
                        <button class="pay-bonus i-btn">
                            <p id="pay-button">Отправить на оплату бонусы</p>
                        </button>
                        <?php } ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <script>
        var globalSelect;

        function popupLauncher() {
            var paymentAmount = $('#payment-amount').attr('data-amount');
            var charityAmount = $('#charity-amount').attr('data-amount');
            var charityLink = $('.i-btn').attr('data-popup-open');
            var textButton = $('#pay-button');

            // if (charityAmount > 0) {
            // $('.i-btn').attr('data-popup-open', 'popup-charity');
            // textButton.text('Отправить на благотворительность');

            // }
            if (paymentAmount > 0) {
                // textButton.text('Отправить на оплату');
                $('#popup-payment').fadeIn();
            } else {
                $('#popup-charity').fadeIn();

            }

        }

        function showPopupDepositTerm(event) {
            alert(event);
        }

        function popupStatusConfirm() {
            console.log('confirmed');
        }

        function sentToPay() {
            $.ajax({
                type: 'POST',
                url: '/main/pay',
                data: {
                },
                success: function (data) {
                    // console.log(data);
                    // window.location.reload();
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    $.pjax.reload({container: '#pjax-table', async: false});
                    // $.pjax.reload({container: '#pjax-charity', async: false});
                },
                error: function (data) {
                }
            });
        }

        function popupStatusCharity() {
            // console.log('charity');
        }

        function popupStatusDismiss() {
            // console.log('dismiss');
            window.open('https://4serviceru.shopmetrics.com/document.asp?alias=mystadministration.shopper.profile&MODE=SELECT&frmStep=3', '_blank');
        }

        function showPopUpPaymentData(e, user_id, payment_method, account) {
            $index = e.target.selectedIndex;

            var targeted_popup_class = $(e.target).attr('data-popup-open');
            $popUp = $('[data-popup="' + targeted_popup_class + '"]');
            $popUp.fadeIn();

            $popUp.find('a').map((i, v,) => {
                // $(v).attr('href', ($(v).attr('data-href') + id));
                // $(v).attr('data-id', ($(v).attr('data-href') + id));
                $popUp.find('#payment-method').html(payment_method);
                $popUp.find('#payment-account').html(account);
                //
                //     // $.pjax.reload({container: '#pjax-wallet', async: false});
                //
            });

            // alert("popup-data");

            // alert(user_id + payment_method + account);
        }

        function showPopUp(e, id, amount, newBonuse, bonuseDate, userId) {

            $index = e.target.selectedIndex;

            console.log(e.target.value);

            if (e.target.value === 'Withdrawal') {
                var targeted_popup_class = $(e.target).attr('data-popup-open');
                $popUp = $('[data-popup="' + targeted_popup_class + '"]');
                $popUp.fadeIn(350);

                $popUp.find('a').map((i, v,) => {
                    // $(v).attr('href', ($(v).attr('data-href') + id));
                    $(v).attr('data-id', ($(v).attr('data-href') + id));
                    $popUp.find('#amount').html(amount);
                    $popUp.find('#new-bonus-date').html(bonuseDate);
                    $popUp.find('#new-bonus').html(newBonuse);

                    // $.pjax.reload({container: '#pjax-wallet', async: false});

                });
            } else if (e.target.value === 'Charity') {
                $.ajax({
                    type: 'POST',
                    url: '/main/management',
                    data: {
                        management: 'Charity',
                        id: id
                    },
                    success: function (data) {
                        // console.log(data);
                        // window.location.reload();
                        $.pjax.reload({container: '#pjax-wallet', async: false});
                        $.pjax.reload({container: '#pjax-table', async: false});
                        // $.pjax.reload({container: '#pjax-charity', async: false});
                    },
                    error: function (data) {
                    }
                });
            } else if (e.target.value === 'Accumulation') {
                // alert(id);
                $('.popup-deposit-term').fadeIn(350);

                $('.popup_btn_deposit_term').on('click', function (e) {

                    // e.preventDefault();

                    var term = $('#deposit-term').val();
                    // var id = $(this).attr('data-id');
                    if (term > 90) {
                        $.ajax({
                            type: 'POST',
                            url: '/main/deposit',
                            data: {
                                term: term,
                                id: id
                            },
                        }).done(function () {
                            $('.popup-close').click();
                            $.pjax.reload({container: '#pjax-table', async: false});
                        }).fail(function () {
                            alert('Чтото пошло не так');
                        });
                    } else {
                        alert('Срок не может быть менее 91 дня');
                    }
                });
            } else if (e.target.value == 'allPaymants') {
                $.ajax({
                    type: 'POST',
                    url: "/ajax/get-prompt",
                    dataType: "json",
                    data: {
                        userId: userId
                    },
                }).done(function (data) {
                    console.log(data);
                    $.pjax.reload({container: "#pjax-wallet", async: false});
                    $.pjax.reload({container: "#pjax-table", async: false});
                }).fail(function (data) {
                    console.log(data);
                    console.log('Запрос не принят');
                });
            } else if (e.target.value == 'waiting-not-bonus') {
                $.ajax({
                    type: 'POST',
                    url: "/ajax/waiting-not-bonus",
                    dataType: "json",
                    data: {
                        id: id
                    },
                }).done(function (data) {
                    console.log(data);
                    $.pjax.reload({container: "#pjax-wallet", async: false});
                    $.pjax.reload({container: "#pjax-table", async: false});
                }).fail(function (data) {
                    console.log(data);
                    console.log('Запрос не принят');
                });
            } else if (e.target.value == 'allPaymantsAnkets') {
                console.log('allPaymantsAnkets' + userId);
                $.ajax({
                    type: 'POST',
                    url: "/ajax/get-all-to-pay",
                    dataType: "json",
                    data: {
                        userId: userId
                    },
                }).done(function (data) {
                    console.log(data);
                    $.pjax.reload({container: "#pjax-wallet", async: false});
                    $.pjax.reload({container: "#pjax-table", async: false});
                }).fail(function (data) {
                    console.log(data);
                    console.log('Запрос не принят');
                });
            } else if ($index !== 0) {
                $href = $($(e.target).find('option')[$index]).attr('data-hreff');
                if ($href !== '' || $href !== 'undefind') {
                    window.location.href = $href;
                }
            }
            var AccumStatusBtn = document.getElementById('accum-btn');
            AccumStatusBtn.onclick = function (f) {
                globalSelect = e.target;
                const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
                    return v.value
                });
                const currentItem = selectList.indexOf("Accumulation");
                // console.log(globalSelect);
                // console.log('currentItem', currentItem);
                globalSelect.selectedIndex = 1;
                $(globalSelect).parent().find('.select-items').children()[0].click();
                globalSelect.parentElement.querySelector('.select-selected').click();
                $('#popup1').fadeOut();
                // $('.popup-deposit-term').show();
                // f.preventDefault();
            };
            var charStatusBtn = document.getElementById('char-btn');
            charStatusBtn.onclick = function () {
                globalSelect = e.target;
                const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
                    return v.value
                });
                const currentItem = selectList.indexOf("Charity");
                // console.log(globalSelect);
                // console.log('currentItem', currentItem);
                globalSelect.selectedIndex = 1;
                $(globalSelect).parent().find('.select-items').children()[2].click();
                globalSelect.parentElement.querySelector('.select-selected').click();
            }
        }

        // var aprovePeyment = document.querySelectorAll('.select-items div[dataurl="/img/icons/1.png"]');
        // //console.log(aprovePeyment);
        // for(let i = 0; i < aprovePeyment.length; i++){
        //     console.log(aprovePeyment[i]);
        //     aprovePeyment[i].onclick = () => {
        //         $('#popup1').fadeIn();
        //     }
        // }

    </script>

    <script>
        $('.remove-item').on('click', function () {
            var anketa_id = $(this).attr('data-anketa-id');
            $.ajax({
                type: 'POST',
                url: '/main/delete',
                data: {
                    anketa_id: anketa_id
                },
                success: function (data) {
                    // console.log(data);
                    // window.location.reload();
                    const pjsxWalet = $.pjax.reload({container: '#pjax-wallet', timeout: false, async: false});
                    $.pjax.reload({container: '#pjax-table', async: false})

                    pjsxWalet.then(() => {
                        $('.i-pop-up').css('transition', '0s');
                        $('.wallet-ankets').addClass('fake-hover');
                        $('.wallet').on('mouseout', () => {
                            $('.wallet').removeClass('fake-hover');
                            $('.i-pop-up').css('transition', '0.4s');
                        });
                    });
                },
                error: function (data) {
                }
            });
        });
        $('.pay-bonus').on('click', function () {
            $.post('/main/pay-bonus', {'sum': <?= $bonus ?>}).done(function (data) {
                $('#no-bonus-anketa').find('.inform-message').text(data);
                $('#no-bonus-anketa').show();
                $.pjax.reload({container: '#pjax-table', async: false});
                $.pjax.reload({container: '#pjax-wallet', async: false});
            });
        });
    </script>
    <?php Pjax::end(); ?>
    <script>
        $(function () {
            $('.take_money_popup_btn').on('click', function (e) {
                var management = $(this).attr('data-management');
                // console.log(management);
                var id = $(this).attr('data-id');
                $.ajax({
                    type: 'POST',
                    url: '/main/management',
                    data: {
                        management: management,
                        id: id
                    },
                }).done(function () {
                    // $('.popup-close').click();
                    $('#popup1 .popup-close').click();
                    $.pjax.reload({container: '#pjax-table', async: false});
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    // $.pjax.reload({container: '#pjax-charity', async: false});
                }).fail(function () {
                    alert('Чтото пошло не так');
                });
            });
        });
    </script>

    <script>
        var paymentAmount = $('#payment-amount').attr('data-amount');
        var charityAmount = $('#charity-amount').attr('data-amount');
        var charityLink = $('.i-btn').attr('data-popup-open');
        var textButton = $('#pay-button');

        if (charityAmount > 0) {
            // $('.i-btn').attr('data-popup-open', 'popup-charity');
            textButton.text('Отправить на благотворительность');
        }
        if (paymentAmount > 0) {
            textButton.text('Отправить на оплату');
        }
    </script>


</div>

<?php if (isset($uploadImage)) { ?>
    <?php $form = ActiveForm::begin([
        'id' => 'file-upload',
        'method' => 'post'
    ]) ?>
    <?= $form->field($uploadImage, 'imageFile')->fileInput(['style' => 'display: none'])->label(false) ?>
    <button style="display: none">Загрузить</button>
    <?php ActiveForm::end() ?>
<?php } ?>

<script>
    $(document).ready(function () {
        $('#image-apply').click(function () {
            $('#uploadimage-imagefile').click();
        });

        $('#file-upload').on('change', function () {
            $("#file-upload").submit()
        });
    });
</script>


<!--<div class="popup" data-popup="popup-payment-data">-->
<!--    <div class="popup-inner" style="height: 180px">-->
<!--        <p class="inform-message">Оплата будет на эти реквизиты? <br>-->
<!--            <span id="payment-method" class="bold"></span>-->
<!--            <span class="bold" id="payment-account"></span>-->
<!--            <a class="popup-close" data-popup-close="popup-payment-data" href="#"><img src="/img/close.png" alt=""></a>-->
<!--        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">-->
<!--            <a class="form-buttons" data-management="" data-id="" id="confirm-btn" data-href=""-->
<!--               href="--><? //= Url::to(['main/pay']) ?><!--"-->
<!--               onclick="popupStatusConfirm();" style="cursor: pointer;">Да</a>-->
<!--            <a class="form-buttons wide" data-management="" data-id="" id="dismiss-btn"-->
<!--               data-popup-close="popup-payment-data"-->
<!--               data-href="https://4serviceru.shopmetrics.com/document.asp?alias=mystadministration.shopper.profile&MODE=SELECT&frmStep=3"-->
<!--               onclick="popupStatusDismiss();" style="cursor: pointer; line-height: 1" target="_blank">Изменить-->
<!--                реквизиты</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<style>
    /*.header .avatar {*/
    /*    width: 49px;*/
    /*    height: 49px;*/
    /*    border-radius: 50%;*/
    /*    background-image: url("/*/
    <?//= $avatar_image ?> /*");*/
    /*    background-size: cover;*/
    /*    background-position: center;*/
    /*    background-repeat: no-repeat;*/
    /*    margin-right: 20px;*/
    /*}*/

    .take_money_popup_btn {
        padding: 8px 10px !important;
    }

    .form-buttons {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 120px;
        min-height: 40px;
        background-color: #d5d5d5;
        color: #053b06;
        padding: 8px 10px;
        font-size: 12px;
        line-height: 1.2;
        text-decoration: none;
        font-weight: bold;
        text-align: center;
    }

    body {
        overflow-x: hidden;
    }
</style>
