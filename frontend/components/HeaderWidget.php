<?php

namespace frontend\components;

use common\models\BonusesToUser;
use common\models\Visit;
use frontend\models\UploadImage;
use Yii;
use yii\base\Widget;
use common\models\User;
use yii\web\UploadedFile;

class HeaderWidget extends Widget
{
    public function run()
    {
        $userId = Yii::$app->user->id;
        $userSecurityId = Yii::$app->user->identity->user_id;

        $avatar_image = User::findOne(['id' => $userId])->avatar;

        if (!empty($avatar_image)) {
            $avatar_image = 'uploads/' . $avatar_image;
        } else {
            $avatar_image = 'img/avatar.png';
        }

        $userData = User::find()->where(['user_id' => $userSecurityId])->one();

        // upload user logo image
        $uploadImage = new UploadImage();
        if ($uploadImage->load(Yii::$app->request->post())) {
            $uploadImage->imageFile = UploadedFile::getInstance($uploadImage, 'imageFile');
            $uploadImage->upload_image();
            Yii::$app->response->redirect(Yii::$app->request->referrer); //['main/index']);
        }

        $wallet = Visit::find()
//            ->select(['payment_status', 'total_amount', 'accumulated_bonuse', 'id', 'anketa_id'])
//            ->where(['payment_status' => ['Withdrawal', 'Charity']])
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Wallet'])
            ->all();

        $walletCount = count($wallet) ;
//        Visit::find()
//            ->select(['payment_status', 'total_amount', 'accumulated_bonuse', 'id', 'anketa_id'])
//            ->where(['payment_status' => ['Withdrawal', 'Charity']])
//            ->andWhere(['user_id' => $userSecurityId])
//            ->andWhere(['is_payed' => 'Wallet'])
//            ->count();

        $walletCharityCount = Visit::find()
            ->select(['payment_status', 'total_amount', 'accumulated_bonuse', 'id', 'anketa_id'])
            ->where(['payment_status' => ['Charity']])
            ->andWhere(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Wallet'])
            ->count();

        $bonus = BonusesToUser::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['status' => BonusesToUser::STATUS_WALLET])
            ->sum('amount');

        return $this->render('header', compact('userData', 'bonus', 'avatar_image', 'uploadImage', 'wallet', 'walletCount'));
    }
}
