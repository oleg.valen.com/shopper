//handle parameters from modal window on index.php for send these to controller
$(function () {
    $('.take_money_popup_btn').on('click', function () {

        var management = $(this).attr('data-management');
        var id = $(this).attr('data-id');

        // alert(management+ id);

        $.ajax({
            type: 'POST',
            url: '/main/management',
            data: {
                management: management,
                id: id
            },
            success: function (data) {
                console.log(data);
                $.pjax.reload({container: '#pjax-wallet', async: false});
                $.pjax.reload({container: '#pjax-table', async: false});
            },
            error: function (data) {

            }
        });
    });
});

$(function () {
    $('#confirm-btn-pay').on('click', function () {

        var id = $(this).attr('data-id');

        $.ajax({
            type: 'POST',
            url: '/main/management',
            data: {
                id: id
            },
            success: function (data) {
                console.log(data);
            },
            error: function (data) {

            }
        });
    });
});

//handle the filter parameter from select on payment.php
$(function () {
    $('.xxxxxxxxxxxxxx').on('click', function () {

        var filter = $(this).attr('data-filter');

        $.ajax({
            type: 'POST',
            url: '/main/payment',
            data: {
                filter: filter,
            },
            success: function (data) {
                console.log(data);
            },
            error: function (data) {

            }
        });
    });
});
