<?php

$this->title = 'ГЛАВНАЯ';

use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Visit;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$session = Yii::$app->session;
//$cookies = Yii::$app->response->cookies;
$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('index-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('index-end-date')));

$selectedDate = $startDate . ' - ' . $endDate;

?>

<!--<script src="/js/jquery.js"></script>-->

<script>
    var globalSelect;

    function showPopUp(e, id, amount, newBonuse, bonuseDate) {

        $index = e.target.selectedIndex;

        console.log(e.target.value);

        if (e.target.value === 'Вывести средства') {
            var targeted_popup_class = $(e.target).attr('data-popup-open');
            $popUp = $('[data-popup="' + targeted_popup_class + '"]');
            $popUp.fadeIn(350);

            $popUp.find('a').map((i, v,) => {
                // $(v).attr('href', ($(v).attr('data-href') + id));
                $(v).attr('data-id', ($(v).attr('data-href') + id));
                $popUp.find('#amount').html(amount);
                $popUp.find('#new-bonus-date').html(bonuseDate);
                $popUp.find('#new-bonus').html(newBonuse);

                // $.pjax.reload({container: '#pjax-wallet', async: false});

            });
        } else if (e.target.value === 'Благотворительность') {
            $.ajax({
                type: 'POST',
                url: '/main/management',
                data: {
                    management: 'Благотворительность',
                    id: id
                },
                success: function (data) {
                    // console.log(data);
                    //window.location.reload();
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    $.pjax.reload({container: '#pjax-table', async: false});
                    $.pjax.reload({container: '#pjax-charity', async: false});
                },
                error: function (data) {
                }
            });
        } else if (e.target.value === 'Накопление') {
            $.ajax({
                type: 'POST',
                url: '/main/management',
                data: {
                    management: 'Накопление',
                    id: id
                },
                success: function (data) {
                    // console.log(data);
                    //window.location.reload();
                    $.pjax.reload({container: '#pjax-wallet', async: false});
                    $.pjax.reload({container: '#pjax-table', async: false});
                    $.pjax.reload({container: '#pjax-charity', async: false});
                },
                error: function (data) {
                }
            });
        } else if ($index !== 0) {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            if ($href !== '' || $href !== 'undefind') {
                window.location.href = $href;
            }
        }
        var AccumStatusBtn = document.getElementById('accum-btn');
        AccumStatusBtn.onclick = function () {
            // console.log(228);
            globalSelect = e.target;
            const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
                return v.value
            });
            const currentItem = selectList.indexOf("Накопление");
            // console.log(globalSelect);
            // console.log('currentItem',currentItem);
            globalSelect.selectedIndex = 1;
            $(globalSelect).parent().find('.select-items').children()[0].click();
            globalSelect.parentElement.querySelector('.select-selected').click();
        }
        var charStatusBtn = document.getElementById('char-btn');
        charStatusBtn.onclick = function () {
            console.log(22888888);
            globalSelect = e.target;
            const selectList = Array.from(globalSelect.querySelectorAll('option')).map(v => {
                return v.value
            });
            const currentItem = selectList.indexOf("Благотворительность");
            console.log(globalSelect);
            console.log('currentItem', currentItem);
            globalSelect.selectedIndex = 1;
            $(globalSelect).parent().find('.select-items').children()[2].click();
            globalSelect.parentElement.querySelector('.select-selected').click();
        }

    }
</script>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <p class="inform-message">Ты уверен, что хочешь вывести свои <span id="amount" class="bold">500 RUB</span>? <br><span
                    class="bold" id="new-bonus-date">01.12.2018</span> у тебя будет <span class="bold" id="new-bonus">600 RUB.</span>
        </p>
        <a class="popup-close" data-popup-close="popup-1" href="#"><img src="/img/close.png" alt=""></a>
        <div class="take_money_popup_btns_wrapper d-flex justify-content-between">
            <a class="take_money_popup_btn " data-management="Накопление" data-id="" id="accum-btn" data-href=""
               onclick="popupStatusAccumulation();" style="cursor: pointer;">Продолжить
                накопление</a>
            <a class="take_money_popup_btn" data-management="Благотворительность" data-id="" id="char-btn" data-href=""
               onclick='popupStatus();' onclick="popupStatusCharity();" style="cursor: pointer;">Отправить на
                благотворительность</a>
            <a class="take_money_popup_btn pt-3" data-management="Вывод средств" data-id="" data-href=""
               style="cursor: pointer;">Вывести</a>
        </div>

    </div>
</div>

<?php if (!Yii::$app->getRequest()->getCookies()->has('index-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это </span> <span class="bold upper">ГЛАВНАЯ СТРАНИЦА</span></p>
                <p class="mt-3">
                    <span class="size">Здесь находятся твои</span>
                    <span class="bold">ВИЗИТЫ,</span>
                    <span class="size"> которые проходят либо уже прошли проверку.</span>
                </p>
                <p class="mt-3">На этой странице ты можешь определить их дальнейшую судьбу: <span class="bold upper">ПОЛОЖИТЬ НА ДЕПОЗИТНЫЙ СЧЕТ, ВЫВЕСТИ</span>
                    <span class="size">или</span> <span class="bold upper">ПОМОЧЬ НУЖДАЮЩИМСЯ.</span></p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn" href="<?= Url::to(['/main/modal', 'page' => 'index']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="home-calc-popup">
    <div class="calculator-popup-wrapper d-flex">
        <div class="calculator-image d-flex justify-content-center">
            <img class="calculator-icon" src="/img/calculator.png" alt="">
        </div>
        <div class="calculator-info">
            <p class="mt-3"><span class="bold">Это</span> <span class="upper bold">депозитный калькулятор.</span></p>
            <p class="mt-3">С его помощью ты можешь <span class="bold f-14">посчитать свой бонус</span>
                за пользование депозитом.</p>
            <div class="calc-confirmation mt-4 ml-5">
                <a class="calc-confirm-btn" href="">Ознакомлен</a>
            </div>
        </div>
    </div>
</div>

<div class="charity-popup">
    <div class="charity-popup-wrapper d-flex flex-column">
        <div class="charity_popup_icon d-flex justify-content-center">
            <img src="/img/support.png" alt="">
        </div>
        <div class="charity_popup_description mt-3">
            <p class="size">БОЛЬШЕ ИНФОРМАЦИИ</p>
            <p class="size">ВО ВКЛАДКЕ</p>
            <p class="bold">БЛАГОТВОРИТЕЛЬНОСТЬ</p>
        </div>
    </div>
</div>


<section class="news">
    <div class="container">
        <p class="news_title">АКЦИИ/НОВОСТИ</p>
        <div class="row">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                        <?php for ($i = 0; $i < count($sliders); $i++) { ?>
                            <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>"
                                class="<?= $i == 0 ? 'active' : '' ?>">
                            </li>
                        <?php } ?>

                    </ol>
                    <div class="carousel-inner">

                        <?php $firstId = $sliders[0]->id ?>
                        <?php foreach ($sliders as $image) { ?>
                            <div class="carousel-item <?= $image->id === $firstId ? 'active' : '' ?>">
                                <?= Html::img('/uploads/' . $image->filename, [
                                    'class' => 'd-block w-100',
                                    'height' => '150px',
                                    'width' => '1110px',
                                    'alt' => 'Slider Image'
                                ]) ?>
                            </div>
                        <?php } ?>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="calc">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-6">
                <p class="calculation-title title-size">КАЛЬКУЛЯТОР БОНУСА</p>
                <div class="calculations">
                    <div class="pt-3">
                        <div class="title-in-calculator">
                            <p class="result">Калькулятор бонусов</p>
                        </div>
                    </div>
                    <div class="calc_button_wrapper">
                        <div class="btn-container">
                            <p class="btn-name">Сумма</p>
                            <input class="calc_inp" id="sum" maxlength="7" autofocus tabindex="1"
                                   onkeypress='validate(event)'>
                        </div>
                        <div class="btn-container">
                            <p class="btn-name date">Срок накопления</p>
                            <input class="calc_inp" id="data" maxlength="7" tabindex="2" onkeypress='validate(event)'
                                   oninput='calculate()'>
                        </div>
                        <div class="btn-container">
                            <p class="btn-name">Процент</p>
                            <input class="calc_inp" id="percent" tabindex="3" disabled>
                        </div>
                        <div class="btn-container">
                            <p class="btn-name">Бонус</p>
                            <input class="calc_inp" id="bonus" tabindex="4" disabled>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function validate(evt) {
                    var theEvent = evt || window.event;
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                    var regex = /[0-9]|\./;
                    if (!regex.test(key)) {
                        theEvent.returnValue = false;
                        if (theEvent.preventDefault) theEvent.preventDefault();
                    }
                }

                function calculate() {
                    var sum = document.getElementById('sum').value;
                    var data = document.getElementById('data').value;
                    var percent = document.getElementById('percent');
                    var bonus = document.getElementById('bonus');

                    if (data < 91 && data !== '') {
                        percent.value = '0%';
                        bonus.value = (sum * 0 / 365) * data;
                        bonus.value = (bonus.value * 1).toFixed(2);
                    } else if (data > 91 && data <= 180) {
                        percent.value = '10%';
                        bonus.value = (sum * 0.1 / 365) * data;
                        bonus.value = (bonus.value * 1).toFixed(2);
                    } else if (data >= 180 && data < 364) {
                        percent.value = '11%';
                        bonus.value = (sum * 0.11 / 365) * data;
                        bonus.value = (bonus.value * 1).toFixed(2);
                    } else if (data >= 364) {
                        percent.value = '12%';
                        bonus.value = (sum * 0.12 / 365) * data;
                        bonus.value = (bonus.value * 1).toFixed(2);
                    } else if (data === '') {
                        percent.value = '';
                        bonus.value = '';
                    }
                }
            </script>

            <div class="col-lg-6 col-sm-12 ml-auto "> <!--achievement-container-->
                <p class="achievement-title title-size" style="padding-bottom: 10px;">
                    ВАШЕ ПОСЛЕДНЕЕ ДОСТИЖЕНИЕ
                </p>
                <div class="row achievement">
                    <?php Pjax::begin(['id' => 'pjax-charity']); ?>

                    <?php if (!empty($charitiesDone)) { ?>

                        <div class="all-progress-wrapper">

                            <?php foreach ($charitiesDone as $item) { ?>
                                <div class="prog-icon-item <?= $item->done ? 'tooltip-box' : '' ?>">
                                    <div class="tooltip-text">
                                        <i>
                                            <?= $item->description ?>
                                        </i>
                                    </div>
                                    <p class="icon-name <?= $item->image ?><?= $item->done ? '' : '-disabled-' ?>"><?= $item->done ? $item->title : '' ?></p>
                                </div>
                            <?php } ?>

                        </div>

                        <!--                    <?php //if (!empty($charities_to_view)) { ?>-->
                        <!--                        <div class="all-progress-wrapper d-flex">-->
                        <!--                            <?php //foreach ($charities_to_view as $item) { ?>-->
                        <!--                                <div class="prog-icon-item" style="margin: 0px;">-->
                        <!--                                    <p class="icon-name --><? //= $item->image ?><!--">--><? //= $item->title ?><!--</p>-->
                        <!--                                </div>-->
                        <!--                            <?php //} ?>-->
                        <!--                        </div>-->
                    <?php } else { ?>
                        <div class="col-lg-6 col-sm-12 kitty">
                            <img class="kitty_img" src="/img/kitty.png" alt="">
                        </div>
                        <div class="col-lg-6">
                            <p class="achievement_description">
                                У вас еще нету достижений!
                                <br>
                                Измените это прямо сейчас
                            </p>
                        </div>
                    <?php } ?>

                    <?php Pjax::end(); ?>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="visits-table">
    <div class="container">

        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ТАБЛИЦА ВИЗИТОВ</p>
            </div>

            <div class="filters">

                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'post',
                    'options' => [
                        'data-pjax' => 1
                    ],
                ]); ?>

                <!-- date selection -->
                <div class="box">
                    <?php

                    $filterModel->date = $selectedDate;

                    echo $form->field($filterModel, 'date', [
                        'options' => ['class' => 'drp-container form-group']])
                        ->widget(DateRangePicker::classname(), [
                            'convertFormat' => true,
                            'value' => $selectedDate,
                            'presetDropdown' => false,
                            'hideInput' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-m-Y',
                                    'separator' => ' - ',
                                ],
                                'opens' => 'right',
//                                'timePicker'=>true,
                                'ranges' => [
                                    'Сегодня' => ["moment().startOf('day')", "moment()"],
                                    'Вчера' => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                                    'Прошлая неделя' => ["moment().startOf('week').subtract(6, 'days')", "moment()"],
                                    'Эта неделя' => ["moment().startOf('week')", "moment()"],
                                    'Этот месяц' => ["moment().startOf('month')", "moment()"],
                                    'Этот год' => ["moment().startOf('year')", "moment()"],
                                ],
                            ],

                        ])
                        ->label(false);
                    ?>

                </div>

                <div class="form-group">
                    <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <script>
                    $('input#select-date').change(function () {
                        $("#w0").submit()
                    });
                </script>
            </div>

            <div class="col-lg-3 ml-auto">
                <?= Html::a('<button class="download-file">Загрузить</button>',
                    ['excel/export-excel', 'type' => 'index', 'userId' => Yii::$app->user->identity->id]) ?>
            </div>

        </div>

        <?php $order = $order == 'ASC' ? 'DESC' : 'ASC' ?>

        <script src="/js/libs/customSelect.js"></script>

        <div class="row">
            <div class="col-lg-12 p-r">
                <div class="table-over-wrapp">

                    <?php Pjax::begin(['id' => 'pjax-table']); ?>

                    <table id="<!--pjax-table-->" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles"><?= Html::a('Дата визита', ['main/index', 'sort' => 'visit_date', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Номер анкеты', ['main/index', 'sort' => 'anketa_id', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Проект', ['main/index', 'sort' => 'project', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Адрес локации', ['main/index', 'sort' => 'location', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Сумма', ['main/index', 'sort' => 'amount', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Бонус', ['main/index', 'sort' => 'bonuse', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Статус визита', ['main/index', 'sort' => 'visit_status', 'order' => $order]) ?></th>
                            <th class="titles"><?= Html::a('Управление средствами', ['main/index', 'sort' => 'payment_status', 'order' => $order]) ?></th>
                        </tr>


                        <?php $i = 1 ?>
                        <?php foreach ($visit as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="titles_items"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                                <td class="titles_items"><?= $item->anketa_id ?></td>
                                <td class="titles_items"><?= $item->project ?></td>
                                <td class="titles_items"><?= $item->location ?></td>
                                <td class="titles_items summa" data-surcharge="<?= $item->bonuse ?>"
                                    data-persent="8"><?= $item->amount ?></td>

                                <!--определение даты бонуса-->
                                <?php
                                $startDate = strtotime($item->last_modified);
                                $target_date = date('d.m.Y', strtotime("+{$item->deposit_term} day", $startDate));
                                ?>

                                <td class="titles_items"><?= $target_date ?> <p>сумма составит:</p>
                                    <p class="price"><?= $item->amount + $item->bonuse ?> RUB</p></td>
                                <td class="titles_items">
                                    <?= $item->visit_status ?>
                                </td>

                                <td class="titrrrles_items" style="padding: 10px;">
                                    <?php if ($item->visit_status == 'Одобрено') { ?>
                                        <div class="i-serrrlect-wrapper i-select-wrapper i-custom-select">
                                            <select data-popup-open="popup-1"
                                                    onchange="showPopUp(event, <?= $item->id; ?>, <?= $item->amount; ?>, <?= $item->amount + $item->bonuse; ?>, '<?= $target_date; ?>')"
                                                    name="q-2-8" id="q-2-2">
                                                <option datastelsoption value="<?= $item->payment_status ?>"
                                                        dataurl="/img/icons/1.png">-
                                                </option>
                                                <option value="Накопление" data-hreff="#" dataurl="/img/icons/1.png">
                                                    Продолжить накопление
                                                </option>
                                                <option data-href="link3" value="Вывести средства"
                                                        dataurl="/img/icons/2.png">
                                                    <a class="btn" data-popup-open="popup-1">
                                                        Вывести средства
                                                    </a>
                                                </option>
                                                <option value="Благотворительность" dataurl="/img/icons/3.png">
                                                    Отправить на <br>благотворительность
                                                </option>
                                            </select>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                        </tr>
                    </table>

                    <script>
                        setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
                    </script>

                    <?php Pjax::end(); ?>

                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                    <div class="tooltip-table-with-visits flex">
                        <img src="/img/wallet2.png" alt="wallet-tooltip">
                        <p class="info">
                            Доплата - <span class="surcharge">80 грн</span> <br>
                            % - <span class="persent">20 грн</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<script>
    Array.from(document.querySelectorAll('.table-with-visits tr td.summa')).map((v) => {
        v.addEventListener('mouseover', function (e) {
            var rect = this.getBoundingClientRect(),
                tooltip = document.querySelector('.tooltip-table-with-visits');

            tooltip.querySelector('.surcharge').innerHTML = this.getAttribute('data-surcharge');
            tooltip.querySelector('.persent').innerHTML = this.getAttribute('data-persent');

            tooltip.style.opacity = '1';
            tooltip.style.left = this.offsetLeft + rect.width / 2 + 'px';
            tooltip.style.top = this.offsetTop + rect.height / 2 + 'px';

        });
        v.addEventListener('mouseout', function (e) {
            var rect = this.getBoundingClientRect(),
                tooltip = document.querySelector('.tooltip-table-with-visits');

            tooltip.style.opacity = '0';
        });
    });
</script>

<script>
    $(function () {
        $('.take_money_popup_btn').on('click', function (e) {

            // e.preventDefault();

            var management = $(this).attr('data-management');
            var id = $(this).attr('data-id');

            $.ajax({
                type: 'POST',
                url: '/main/management',
                data: {
                    management: management,
                    id: id
                },
            }).done(function () {
                $('.popup-close').click();
                $.pjax.reload({container: '#pjax-table', async: false});
                $.pjax.reload({container: '#pjax-wallet', async: false});
                $.pjax.reload({container: '#pjax-charity', async: false});
            }).fail(function () {
                alert('Чтото пошло не так');
            });

        });
    });
</script>
<script src="/js/libs/modal.js"></script>
