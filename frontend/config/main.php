<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name'=>'Shopperbonus4service',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => [/*'*', '127.0.0.1', '::1', */'217.66.97.137', '31.43.56.3']
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'enableSession' => false,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                    'categories' => ['api'],
                    'logFile' => '@app/runtime/logs/api.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
//            'enableStrictParsing' => true,
//            'enableStrictParsing' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api',
                    'pluralize' => false,
                    'except' => ['delete'],
                ],
                [
                    'pattern' => 'api/upd/<anketa_id:[\d-]+>/<user_id:[\w+]+>',
                    'route' => 'api/upd',
                ],
                [
                    'pattern' => 'api/anketa-data/<anketa_id:[\d-]+>',
                    'route' => 'api/anketa-data',
                ],
                '' => 'main/index',
                'auth' => 'site/auth',
                'log' => 'site/log',
                'payment' => 'main/payment',
                'questions' => 'main/questions',
                'main/management' => 'main/management',
                'accumulation' => 'main/accumulation',
                'payed' => 'main/payed',
                'bonus' => 'main/bonus',
                'charity' => 'main/charity',
                'rules' => 'rules/rules-index',
                'rules/rules-payments' => 'rules/rules-payments',
                'rules/rules-bonus' => 'rules/rules-bonus',
                'rules/rules-charity' => 'rules/rules-charity',
                'pay' => 'main/pay',
                'login' => 'site/login',
                'logout' => 'site/logout',
                'entrance' => 'site/entrance',
//                'signup' => 'site/signup',
                'excel/export-pdf' => 'excel/export-pdf',
                'excel/export-excel' => 'excel/export-excel',
                'excel/import-excel' => 'excel/import-excel',
//                'main/delete' => 'main/delete/<id:[\d-]+>',
                [
                    'pattern' => 'management/<management:[\d-]+>/<id:[\d-]+>',
                    'route' => 'main/management',
                ],
                [
                    'pattern' => 'pay/<id:[\d-]+>',
                    'route' => 'main/pay',
                ],
                [
                    'pattern' => 'delete/<id:[\d-]+>',
                    'route' => 'main/delete',
                ],
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            ],
        ],
        'assetManager' => [
//            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
//                    'js' => ['/js/jquery.min.js']
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],

            ],
        ],
    ],
    'params' => $params,
];
