<?php
return [
    'adminEmail' => 'admin@example.com',
//    'supportEmail' => 'no-reply@shopperbonus4service.com',
    'supportEmail' => 'guesttrack.supp@gmail.com',
    'senderEmail' => 'guesttrack.supp@gmail.com',
    'noreplyEmail' => 'saycoffeemania@4service-group.com',
//    'noreplyEmail' => 'guesttrack.supp@gmail.com',
    'senderName' => 'Shopperbonus Support',
    'receiverEmail' => 'oleg.valen.com@gmail.com',
    'receiverName' => 'Oleh',
    'user.passwordResetTokenExpire' => 3600,
    'paymentStatus' => [
        'Rejected' => 'Отклонен',
        'Payed' => 'Оплачен',
        'Partial_payed' => 'Частичная оплата',
        'Not_payed' => 'Не оплачен',
        'Waiting_pay' => 'Ожидает выплату',
        'Sent_to_pay' => 'Отправлен на оплату',
        'Charity' => 'Благотворительность',
        'advance_payment' => 'Оплачено авансовыми',
        'Payed_not_bonus' => 'Отправлено на оплату без накопления бонусов',
    ],
    'visitStatus' => [
        'Rejected' => 'Расхождение в суммах',
        'Completed' => 'Проверен',
        'Validation' => 'На проверке',
        'Approved_to_pay' => 'Одобрено к оплате',
        'Approved_to_withdrawal' => 'Одобрено для вывода денег',
        'Waiting_not_bonus' => 'Срочная оплата без накопления бонусов',
    ]
];
