<?php
return [
    'LocationStateRegion' => [
        'AD' => 'Республика Адыгея (Адыгея) / Adygey',
        'AL' => 'Алтай / Altay',
        'AM' => 'Амурская область / Amur',
        'AR' => "Архангельская область / Arkhangel'sk",
        'AS' => 'Астраханская область / Astrakhan',
        'BK' => 'Башкортостан / Bashkortostan',
        'BL' => 'Белгородская область / Belgorod',
        'BR' => 'Брянская область / Bryansk',
        'BU' => 'Бурятия / Buryat',
        'CK' => 'Чукотский автономный округ / Chukot',
        'CL' => 'Челябинская область / Chelyabinsk',
        'CN' => 'Чечня / Chechnya',
        'CV' => 'Чувашия / Chuvash',
        'DA' => 'Дагестан / Dagestan',
        'EN' => 'Эвенкийский автономный округ / Evenk',
        'GA' => 'Горный Алтай / Gorno - Altay',
        'IN' => 'Ингушская Республика / Ingush',
        'IR' => 'Иркутская область / Irkutsk',
        'IV' => 'Ивановская область / Ivanovo',
        'KA' => 'Камчатская область / Kamchatka',
        'KB' => 'Кабардино - Балкарская Республика / Kabardin - Balkar',
        'KC' => 'Карачаево - Черкесская Республика / Karachay - Cherkess',
        'KD' => 'Краснодарский край / Krasnodar',
        'KE' => 'Кемеровская область / Kemerovo',
        'KG' => 'Калужская область / Kaluga',
        'KH' => 'Хабаровск / Khabarovsk',
        'KI' => 'Карелия / Karelia',
        'KK' => 'Хакасия / Khakass',
        'KL' => 'Калмыкия / Kalmyk',
        'KM' => 'Ханты - Мансийский автономный округ / Khanty - Mansiy',
        'KN' => 'Калининградская область / Kaliningrad',
        'KO' => 'Коми / Komi',
        'KR' => 'Корякский автономный округ / Koryak',
        'KS' => 'Курская область / Kursk',
        'KT' => 'Костромская область / Kostroma',
        'KU' => 'Курганская область / Kurgan',
        'KV' => 'Кировская область / Kirov',
        'KY' => 'Красноярский край / Krasnoyarsk',
        'LN' => 'Ленинградская область / Leningrad',
        'LP' => 'Липецкая область / Lipetsk',
        'MC' => 'Москва / Moscow City',
        'ME' => 'Марий Эл / Mariy - El',
        'MG' => 'Магаданская область / Magadan',
        'MM' => 'Мурманская область / Murmansk',
        'MR' => 'Республика Мордовия / Mordovia',
        'MS' => 'Московская область / Moskva',
        'NG' => 'Новгородская область / Novgorod',
        'NN' => 'Ненецкий автономный округ / Nenets',
        'NO' => 'Северная Осетия / North Ossetia',
        'NS' => 'Новосибирская область / Novosibirsk',
        'NZ' => 'Нижегородская область / Nizhegorod',
        'OB' => 'Оренбургская область / Orenburg',
        'OL' => 'Орловская область / Orel',
        'OM' => 'Омская область / Omsk',
        'PE' => "Пермский край / Perm'",
        'PR' => "Приморский край / Primor'ye",
        'PS' => 'Псковская область / Pskov',
        'PZ' => 'Пензенская область / Penza',
        'RO' => 'Ростовская область / Rostov',
        'RZ' => "Рязанская область / Ryazan'",
        'SA' => "Самарская область / Samara",
        'SK' => 'Саха / Sakha',
        'SL' => 'Сахалинская область / Sakhalin',
        'SM' => 'Смоленская область / Smolensk',
        'SR' => 'Саратовская область / Saratov',
        'ST' => "Ставропольский край / Stavropol'",
        'SV' => 'Свердловская область / Sverdlovsk',
        'TB' => 'Тамбовская область / Tambov',
        'TL' => 'Тульская область / Tula',
        'TM' => 'Таймыр / Taymyr',
        'TO' => 'Томская область / Tomsk',
        'TT' => 'Татарстан / Tatarstan',
        'TU' => 'Тыва / Tuva',
        'TV' => "Тверская область / Tver'",
        'TY' => "Тюменская область / Tyumen'",
        'UB' => 'Усть - Орда / Ust - Orda Buryat',
        'UD' => 'Удмуртия / Udmurt',
        'UL' => "Ульяновская область / Ul'yanovsk",
        'VG' => 'Волгоградская область / Volgograd',
        'VL' => 'Владимирская область / Vladimir',
        'VO' => 'Вологодская область / Vologda',
        'VR' => 'Воронежская область / Voronezh',
        'YN' => 'Ямало - Ненецкий автономный округ / Yamal - Nenets',
        'YS' => "Ярославская область / Yaroslavl'",
        'YV' => 'Еврейская автономная область / Yevrey',
    ],
];
