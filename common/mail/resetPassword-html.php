<?php

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 'token' => $user->password_reset_token]);

?>

<p>Привет <?= Html::encode($user->name) ?></p>

<p>Для смены пароля перейдите по ссылке.</p>

<p><?= $resetLink ?></p>
