<?php

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 'token' => $user->password_reset_token]);

?>

Привет <?= Html::encode($user->name) ?>

Для смены пароля перейдите по ссылке.

<?= $resetLink ?>
