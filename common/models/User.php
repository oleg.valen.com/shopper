<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 'user';
    const ROLE_MODER = 'moder';
    const ROLE_ADMIN = 'admin';

    public $password;

    protected $oldAttributes;
    protected $password_hash_alternative = '$2y$13$3GiFsrFO7/VDbd9tfLrCHeOQxHxTZg7GUb/g/KJm9uQ5mCRrnaS8S';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function afterFind()
    {
        $this->oldAttributes = $this->attributes;
        return parent::afterFind();
    }

    public function beforeSave($options = [])
    {
        if ($this->attributes == $this->oldAttributes)
            return false;

        return parent::beforeSave($this->isNewRecord);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['username', 'email', 'password'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'bonuse_percent', 'user_id'], 'integer'],
            [['username', 'password_hash', 'verification_token', 'password_reset_token', 'email', 'role', 'title', 'country',
                'name', 'family_name', 'avatar', 'address', 'payment_method',
                'payment_type'], 'string', 'max' => 255],
            [['bank_details'], 'string'],
            [['auth_key'], 'string', 'max' => 32],
            [['bank_account', 'bank_name', 'qiwi_wallet', 'phone_number'], 'string', 'max' => 25],
            [['username'], 'unique'],
            [['accumulated_bonuse'], 'number'],
            [['email'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE], //STATUS_INACTIVE
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    // for email login
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash)
            || Yii::$app->security->validatePassword($password, $this->password_hash_alternative);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID',
            'username' => 'Логин',
            'email' => 'Электронынй адрес',
            'role' => 'Права доступа',
            'status' => 'Статус',
            'total_bonuses' => 'Всего бонусов',
            'wallet_bonuses' => 'Бонусов в кошельке',
        ];
    }

    public function getBonusesToUsers()
    {
        return $this->hasMany(BonusesToUser::className(), ['user_id' => 'user_id']);
    }

    public function getTotalBonuses()
    {
        return $this->hasMany(Visit::className(), ['user_id' => 'user_id'])->andWhere(['visit.is_payed' => 'Not_payed'])->sum('accumulated_bonuse');
    }

    public function getWalletBonuses()
    {
        return $this->hasMany(BonusesToUser::className(), ['user_id' => 'user_id'])->andwhere(['bonuses_to_user.status' => BonusesToUser::STATUS_WALLET])->sum('amount');
    }

    public function getTotalAccumulation()
    {
        return $this->hasMany(Visit::className(), ['user_id' => 'user_id'])->andwhere(['visit.is_payed' => 'Not_payed', 'visit.visit_status' => 'Approved_to_withdrawal'])
            ->andWhere(['<=', 'accumulation_start_date', date('Y-m-d')])->sum('total_amount');
    }
}
