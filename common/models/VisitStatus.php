<?php

namespace common\models;

class VisitStatus
{
    const COMPLETED = 'Completed';
    const APROOVED_TO_WITHDRAWAL = 'Approved_to_withdrawal';
    const VALIDATION = 'Validation';
    const APPROVED_TO_PAY = 'Approved_to_pay';
    const REJECTED = 'Rejected';
    const WAITING_NOT_BONUS = 'Waiting_not_bonus';
    const DELETED = 'Deleted';

    public static $values = [
        self::COMPLETED => 'Проверен',
        self::APROOVED_TO_WITHDRAWAL => 'Одобрено для вывода денег',
        self::VALIDATION => 'На проверке',
        self::APPROVED_TO_PAY => 'Одобрено к оплате',
        self::REJECTED => 'Расхождение в суммах',
        self::WAITING_NOT_BONUS => 'Срочная оплата без накопления бонусов',
        self::DELETED => 'Deleted',
    ];
}
