<?php

namespace common\models;

use frontend\controllers\SiteController;
use Yii;
use yii\db\ActiveRecord;
use SimpleXMLElement;

class Api extends \yii\db\ActiveRecord
{
    static function getVisitData($date)
    {
        $url = "https://4serviceru.shopmetrics.com/custom/4serviceru/ShopperBonusDataExport.asp";
        $key_name = 'ss';
        $key_value = '7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C';
        $post_data = json_encode([$key_name => $key_value]);
        $post_string = $key_name . '=' . $key_value;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "date=" . $date . "&ss=7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic Og=="
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $xml = $response;
        $xml_data = (array)simplexml_load_string($xml);
        $json = json_encode($xml_data);
        $resultArray = json_decode($json, true);

//        SiteController::debug($resultArray);
//        SiteController::debug(json_decode(json_encode((array)simplexml_load_string($response)),true));

        return $resultArray;
    }

    static  function getHistoricalData($date)
    {
        $url = "https://4serviceru.shopmetrics.com/custom/4serviceru/ShopperBonusHistoricalDataExport.asp";
        $key_name = 'ss';
        $key_value = '7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C';
        $post_data = json_encode([$key_name => $key_value]);
        $post_string = $key_name . '=' . $key_value;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "date=" . $date . "&ss=7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic Og=="
            ),
        ));

        $response = curl_exec($curl);

//        SiteController::debug($response);

        curl_close($curl);

        $xml = $response;
        $xml_data = (array)simplexml_load_string($xml);
        $json = json_encode($xml_data);
        $resultArray = json_decode($json, true);

//        SiteController::debug($resultArray);

        return $resultArray;
    }
}
