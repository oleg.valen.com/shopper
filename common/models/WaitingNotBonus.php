<?php

namespace common\models;

use yii\db\ActiveRecord;

class WaitingNotBonus extends ActiveRecord
{
    public static function tableName()
    {
        return 'waiting_not_bonus';
    }
}
