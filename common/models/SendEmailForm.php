<?php


namespace common\models;

use Yii;
use yii\base\Model;

class SendEmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => [
                    'status' => User::STATUS_INACTIVE,
                ],
                'message' => 'Такой емейл не зарегистрирован',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Емейл'
        ];
    }

    public function sendEmail()
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            $user->generatePasswordResetToken();

            if ($user->save(false)) {
                return Yii::$app
                    ->mailer
                    ->compose(['html' => 'resetPassword-html', 'text' => 'resetPassword-text'],
                        ['user' => $user])
                    ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name . ' отправлено роботом'])
                    ->setTo($this->email)
                    ->setSubject('Сброс пароля для ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
