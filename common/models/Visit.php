<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\rest\User;

/**
 * Visit model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $anketa_id
 * @property integer $pay_rate
 * @property string|null $freez_to_date
 * @property integer $bonuse
 * @property integer $penalty
 * @property integer $wait_bonuse
 * @property integer $wait_bonuse_sm
 * @property integer $is_prompt_payment
 * @property string|null $accumulated_bonuse
 * @property integer $deposit_term
 * @property integer $deposit_left
 * @property string $last_modified
 * @property string $first_import_date
 * @property string $is_payment_status_changed
 * @property string $work_flow_status
 * @property string $survey_status
 * @property string $payment_status
 * @property string $management
 * @property string $visit_date
 * @property string $visit_status
 * @property string $verification_token
 * @property string $accumulation_start_date
 * @property string $is_payed
 * @property string $total_amount
 * @property string $survey_title
 * @property string $client_name
 * @property string $location_address
 * @property string $location_city
 * @property string $location_state_region
 * @property string $location_name
 * @property string $project
 * @property string $payroll_currency
 * @property string $payroll_items
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $acconter_updated
 */
class Visit extends \yii\db\ActiveRecord
{
//    public $deposit_left;
//    public $management;
//    public $charity;

//    public $dateFrom;
//    public $dateTo;
//    public $date;
//    public $rows;

    protected $oldAttributes;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visit';
    }

    public function afterFind()
    {
        $this->oldAttributes = $this->attributes;
        return parent::afterFind();
    }

    public function beforeSave($options = [])
    {
        if ($this->attributes == $this->oldAttributes)
            return false;

        return parent::beforeSave($this->isNewRecord);
    }

    public function getUserEmail()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'user_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public function getUsername()
    {
        return $this->hasOne(\common\models\rest\User::className(), ['user_id' => 'user_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pay_rate', 'visit_date', 'last_modified', 'created_at', 'updated_at', 'is_payment_status_changed', 'acconter_updated', 'sent_to_pay'], 'safe'],
//            [['anketa_id', 'project'], 'required'],
            [['payed_amount', 'wait_bonuse', 'wait_bonuse_sm'/*, 'date', 'rows'*/], 'safe'],
            [['anketa_id', 'penalty',
                'user_id', 'deposit_left', 'deposit_term'], 'integer'],
            [['pay_rate', 'accumulated_bonuse', 'total_amount', 'bonuse'], 'number'],
            [['project', 'is_payed'], 'string', 'max' => 255],
            [['management', 'payment_method'], 'string', 'max' => 10],
//            [['is_payment_status_changed'], 'string', 'max' => 1],
            [['visit_status', 'payment_status'], 'string', 'max' => 50],
            [['survey_title', 'client_name', 'location_name', 'location_state_region', 'location_city', 'location_address'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'visit_date' => 'Дата визита',
            'anketa_id' => 'Номер анкеты',
            'user_id' => 'Номер ТП',
            'project' => 'Проект',
            'location_city' => 'Адрес локации',
            'bonuse' => 'Доплата',
            'penalty' => 'Штраф',
            'pay_rate' => 'За визит',
            'extra_charge' => 'Доплата',
            'charity' => 'Благотворительность',
            'is_payed' => 'Статус платежа',
            'last_modified' => 'Дата смены статуса',
            'visit_status' => 'Статус визита',
            'total_amount' => 'Сумма',
            'payment_status' => 'Статус анкеты',
            'payment_date' => 'Дата оплаты',
            'payment_method' => 'Способ оплаты',
            'management' => 'Управление средствами',
            'deposit_left' => 'Осталось',
            'deposit_term' => 'Срок депозита',
            'is_payment_status_changed' => 'Изменен',
            'acconter_updated' => 'Изменен',
            'sent_to_pay' => 'Отправлено на оплату',
            'accumulated_bonuse' => 'Накопленный бонус',
            'is_prompt_payment' => 'Гипер'
        ];
    }

//    public static function bonuseCalculating($userSecurityId)
//    {
//        $visitForBonuse = Visit::find()
//            ->where(['user_id' => $userSecurityId])
//            ->andWhere(['or', ['payment_status' => 'Accumulation'], ['payment_status' => null]])
//            ->andWhere(['is_payed' => 'Not_payed'])
//            ->andWhere(['visit_status' => 'Completed'])
//            ->all();
//
//        foreach ($visitForBonuse as $item) {
//            $item->total_amount = $item->pay_rate + $item->bonuse + $item->penalty;
//            $bonuseStartDate = $item->visit_date < '2021-06-01' ? '2021-06-01' : $item->visit_date;
//            $item->accumulated_bonuse = $item->is_prompt_payment ? 0 : round(((time() - strtotime($bonuseStartDate) + 86400) / 86400) * 0.1 * $item->total_amount / 365, 2);
//            $item->save(false);
//        }
//
//        return true;
//    }

    public static function bunusePerDay($sum)
    {
        return round($sum * 0.1 / 365, 2);
    }

    public static function summaryData($userSecurityId, $startDate, $endDate)
    {
        $summaryData['bonuse'] = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('accumulated_bonuse');
        $summaryData['total_amount'] = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['in', 'is_payed', ['Not_payed']])
            ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
            ->sum('total_amount');

        return $summaryData;
    }

    public static function charitiesCount($userSecurityId)
    {
        return Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['payment_status' => 'Charity'])
            ->count();
    }

    public static function charitiesAmount($userSecurityId)
    {
        return Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['payment_status' => 'Charity'])
            ->sum('total_amount');
    }

    public static function charitiesPerMonth($userSecurityId)
    {
        return Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['payment_status' => 'Charity'])
            ->sum('total_amount');
    }

    public static function getCharitiesToView($charitiesList, $charities_count, $charities_amount)
    {
        $charities_to_view = [];
        if (!empty($charitiesList)) {
            foreach ($charitiesList as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                }

                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                }
            }
        }

        return $charities_to_view;
    }

    public static function charitiesReport()
    {
        return Visit::find()
            ->where(['payment_status' => 'Charity'])
            ->all();
    }

    public static function getReviseBonusData()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            select u.username,
                   t.user_id,
                   t.anketa_id,
                   t.accumulated_bonuse,
                   t.visit_date,
                   t.sent_to_pay,
                   t.accumulation_start_date,
                   t.date_diff,
                   t.bonuse_persent,
                   t.amount_calc,
                   t.amount_calc - t.accumulated_bonuse amount_diff,
                   t.total_amount,
                   t.visit_status,
                   t.is_payed
            from (
                     select v.user_id,
                            v.anketa_id,
                            v.accumulated_bonuse,
                            DATE_FORMAT(v.visit_date, '%e-%m-%y')  visit_date,
                            DATE_FORMAT(v.sent_to_pay, '%e-%m-%y') sent_to_pay,
                            DATE_FORMAT(v.accumulation_start_date, '%e-%m-%y') accumulation_start_date,
                            DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                     v.accumulation_start_date)    date_diff,
                            case
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 365
                                    THEN
                                    0.11
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 180
                                    then 0.10
                                ELSE
                                    0.09
                                END                                bonuse_persent,
                            round(DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                           v.accumulation_start_date)
                                      * case
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 365
                                                THEN
                                                0.11
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 180
                                                then 0.10
                                            ELSE
                                                0.09
                                      END
                                      * v.total_amount / 365, 2)   amount_calc,
                            v.total_amount,
                            v.visit_status,
                            v.is_payed
                     from visit v
                     where v.is_payed in ('Waiting_pay')
                 ) t
                     left join user u
                               on t.user_id = u.user_id
            where t.user_id=:userId  
#            and t.visit_date >= '2021-06-01'
#            and t.date_diff > 0
#            and t.amount_calc - t.accumulated_bonuse > 0
            order by t.visit_date
    ")
            ->bindValue(':userId', Yii::$app->session->get('target-user-id') ?? Yii::$app->user->id);
        return $command->queryAll();
    }

    public function getBonuseToUser()
    {
        return $this->hasOne(BonusesToUser::className(), ['anketa_id' => 'anketa_id']);
    }

    public function getUsers()
    {
        return $this->hasOne(\common\models\User::className(), ['user_id' => 'user_id']);
    }
}
