<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Select extends \yii\db\ActiveRecord
{
    public $date;

    public function rules()
    {
        return [
            [['date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => 'Период',
        ];
    }
}
