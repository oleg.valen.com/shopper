<?php


namespace common\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use yii\base\InvalidArgumentException;

class ResetPasswordForm extends Model
{
    public $password;
    public $_user;

    public function rules()
    {
        return [
            ['password', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль'
        ];
    }

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token))
            throw new InvalidArgumentException('Токен не может быть пустым.');
        $this->_user = User::findByPasswordResetToken($token);

        if (!$this->_user)
            throw new InvalidArgumentException('Не верный или устаревший токен!');

        parent::__construct($config);
    }

    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
