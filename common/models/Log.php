<?php

namespace common\models;

use Yii;
use yii\base\BaseObject;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $visit_id
 * @property string $action
 * @property string|null $user_ip
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'action'], 'required'],
            [['user_id', 'visit_id'], 'integer'],
            [['action'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_ip'], 'string', 'max' => 18],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'visit_id' => 'Анкета',
            'action' => 'Действие',
            'user_ip' => 'IP пользователя',
            'created_at' => 'Дата и время',
            'updated_at' => 'Updated At',
        ];
    }

//    public function behaviors()
//    {
//        return [
//            [
//                'class' => TimestampBehavior::className(),
//                'attributes' => [
//                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
//                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
//                ],
//                'value' => new Expression('NOW()'),
//            ],
//        ];
//    }

    public static function setLog($action, $visitId = null)
    {
        $log = new Log();

        $log->user_id  = Yii::$app->user->id;
        $log->action   = $action;
        $log->visit_id = $visitId ?? null;
        $log->user_ip = Yii::$app->getRequest()->getUserIP();
        $log->created_at = date('Y-m-d H:i:s');

        if ($log->save(false)) {
            return true;
        }

        return false;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
