<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $last_entrance
 * @property string|null $phone_number
 * @property string|null $country
 * @property string|null $name
 * @property string|null $family_name
 * @property string|null $avatar
 * @property string|null $address
 * @property int|null $bonuse_percent
 * @property string|null $payment_method
 * @property string|null $bank_details
 * @property string|null $bank_account
 * @property string|null $qiwi_wallet
 * @property string|null $bank_name
 * @property float|null $accumulated_bonuse
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'bonuse_percent'], 'integer'],
            [['last_entrance', 'created_at', 'updated_at'], 'safe'],
            [['bank_details'], 'string'],
            [['accumulated_bonuse'], 'number'],
            [['phone_number', 'country'], 'string', 'max' => 50],
            [['name', 'family_name', 'avatar', 'address', 'payment_method'], 'string', 'max' => 250],
            [['bank_account', 'qiwi_wallet'], 'string', 'max' => 30],
            [['bank_name'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'last_entrance' => 'Last Entrance',
            'phone_number' => 'Phone Number',
            'country' => 'Country',
            'name' => 'Name',
            'family_name' => 'Family Name',
            'avatar' => 'Avatar',
            'address' => 'Address',
            'bonuse_percent' => 'Bonuse Percent',
            'payment_method' => 'Payment Method',
            'bank_details' => 'Bank Details',
            'bank_account' => 'Bank Account',
            'qiwi_wallet' => 'Qiwi Wallet',
            'bank_name' => 'Bank Name',
            'accumulated_bonuse' => 'Accumulated Bonuse',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
}
