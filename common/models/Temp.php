<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "temp".
 *
 * @property int $id
 * @property int $visit_id
 * @property int $date_stamp
 * @property string $date_new
 */
class Temp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visit_id', 'date_stamp', 'date_new'], 'required'],
            [['visit_id', 'date_stamp'], 'integer'],
            [['date_new'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'visit_id' => 'Visit ID',
            'date_stamp' => 'Date Stamp',
            'date_new' => 'Date New',
        ];
    }
}
