<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bonuse;

/**
 * BonuseSearch represents the model behind the search form of `common\models\Bonuse`.
 */
class BonuseSearch extends Bonuse
{
    public $dates;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'payment_anketa_id', 'anketas_data', 'created_at', 'updated_at'], 'integer'],
            [['payment_date', 'login', 'deadline_date'], 'safe'],
            [['amount'], 'number'],
            [['dates'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bonuse::find()->with('bonusesToUser');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->dates)) {
            $dates = explode(' - ', $this->dates);
            $query->andFilterWhere(['between', 'payment_date',
                date('Y-m-d', strtotime($dates[0])),
                date('Y-m-d', strtotime($dates[1]))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'payment_date' => $this->payment_date,
            'amount' => $this->amount,
            'payment_anketa_id' => $this->payment_anketa_id,
            'anketas_data' => $this->anketas_data,
            'deadline_date' => $this->deadline_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login]);

        return $dataProvider;
    }
}
