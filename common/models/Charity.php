<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "charity".
 *
 * @property int $id
 * @property string $title
 * @property int $numbers
 * @property int $amount
 * @property string $image
 * @property int $status
 */
class Charity extends \yii\db\ActiveRecord
{
    public $done;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'numbers', 'amount', 'image'], 'required'],
            [['numbers', 'amount', 'status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 150],
            [['description'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'numbers' => 'Numbers',
            'amount' => 'Amount',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }
}
