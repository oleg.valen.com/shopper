<?php

namespace common\models;

class PaymentStatus
{
    const WITHDRAWAL = 'Вывод средств';
    const ACCUMULATION = 'Накопление';
    const CHARITY = 'Благотворительность';

    public static $values = [
        self::WITHDRAWAL => 'Вывод средств',
        self::ACCUMULATION => 'Накопление',
        self::CHARITY => 'Благотворительность',
    ];
}
