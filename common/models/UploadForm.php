<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $xlsFile;

    public function rules()
    {
        return [
            [['xlsFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }

    public function upload()
    {

        if ($this->validate()) {
            if ($this->xlsFile) {
                $this->xlsFile->saveAs("uploads/{$this->xlsFile->baseName}.{$this->xlsFile->extension}");
                $this->xlsFile = "{$this->xlsFile->baseName}.{$this->xlsFile->extension}";
            }
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'xlsFile' => 'Файл excel',
        ];
    }
}