<?php

namespace common\models\rest;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

class User extends \common\models\User
{
    public static function tableName()
    {
        return 'user';
    }

    public function fields()
    {
        return ['username'];
    }
}
