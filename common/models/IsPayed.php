<?php

namespace common\models;

class IsPayed
{
    const PAYED = 'Payed';
    const NOT_PAYED = 'Not_payed';
    const WAITING_PAY = 'Waiting_pay';
    const WALLET = 'Wallet';
    const ADVANCE_PAYMENT = 'advance_payment';
    const RECALC = 'Recalc';
    const PAYED_NOT_BONUS = 'Payed_not_bonus';
    const DELETED = 'Deleted';

    public static $values = [
        self::PAYED => 'Оплачен',
        self::NOT_PAYED => 'Не оплачен',
        self::WAITING_PAY => 'Ожидает выплату',
        self::WALLET => 'В кошельке',
        self::ADVANCE_PAYMENT => 'Оплачено авансовыми',
        self::RECALC => 'Перерасчет',
        self::PAYED_NOT_BONUS => 'Отправлено на оплату без накопления бонусов',
        self::DELETED => 'Deleted',
    ];
}
