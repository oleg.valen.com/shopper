<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "bonuse".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $payment_date
 * @property string|null $login
 * @property float|null $amount
 * @property int|null $payment_anketa_id
 * @property string|null $anketas_data
 * @property string|null $deadline_date
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Bonuse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bonuse';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_anketa_id', 'created_at', 'updated_at'], 'integer'],
            [['payment_date', 'deadline_date'], 'safe'],
            [['amount'], 'number'],
            [['anketas_data'], 'string'],
            [['login'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payment_date' => 'Дата вывода бонусов',
            'login' => 'Логин',
            'amount' => 'Сумма бонусов',
            'payment_anketa_id' => '№ Анкеты на которую прикреплены бонусы',
            'anketas_data' => 'Номера анкет/сумма по начисленным бонусам',
            'deadline_date' => 'Дата до которой нужно внести изменения в ШМ',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ]
            ]
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public function getVisit()
    {
        return $this->hasOne(Visit::className(), ['anketa_id' => 'payment_anketa_id']);
    }

    public function getBonusesToUser()
    {
        return $this->hasMany(BonusesToUser::className(), ['user_id' => 'user_id']);
    }
}
