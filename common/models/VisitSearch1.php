<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Visit;

/**
 * VisitSearch represents the model behind the search form of `common\models\Visit`.
 */
class VisitSearch extends Visit
{
    public $dateFrom;
    public $dateTo;
    public $date;
    public $rows = 10;
    public $is_payed_filter;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'anketa_id', 'penalty', 'total_amount', 'bonuse', 'is_prompt_payment',
                'deposit_term', 'is_payment_status_changed'], 'integer'],
            [['visit_date', 'survey_title', 'client_name', 'location_name', 'location_state_region',
                'location_city', 'location_address', 'payroll_currency', 'payroll_items', 'payment_method',
                'payed_amount', 'visit_status', 'payment_status', 'management', 'project', 'work_flow_status',
                'survey_status', 'accumulation_start_date', 'last_modified', 'created_at', 'updated_at', 'first_import_date',], 'safe'],
            [['is_payed'], 'string', 'max' => 50],
            [['date', 'dateFrom', 'dateTo', 'rows', 'is_payed_filter'], 'safe'],
            [['pay_rate', 'accumulated_bonuse'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Visit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->rows,
            ],
            'sort' => [
                'attributes' => [
                    'visit_date' => [
                        'asc' => ['visit_date' => SORT_ASC],
                        'desc' => ['visit_date' => SORT_DESC],
                    ],
                    'anketa_id' => [
                        'asc' => ['anketa_id' => SORT_ASC],
                        'desc' => ['anketa_id' => SORT_DESC],
                    ],
                    'project' => [
                        'asc' => ['project' => SORT_ASC],
                        'desc' => ['project' => SORT_DESC],
                    ],
                    'location_city' => [
                        'asc' => ['location_city' => SORT_ASC],
                        'desc' => ['location_city' => SORT_DESC],
                    ],
                    'location_address' => [
                        'asc' => ['location_address' => SORT_ASC],
                        'desc' => ['location_address' => SORT_DESC],
                    ],
                    'pay_rate' => [
                        'asc' => ['pay_rate' => SORT_ASC],
                        'desc' => ['pay_rate' => SORT_DESC],
                    ],
                    'penalty' => [
                        'asc' => ['penalty' => SORT_ASC],
                        'desc' => ['penalty' => SORT_DESC],
                    ],
                    'bonuse' => [
                        'asc' => ['bonuse' => SORT_ASC],
                        'desc' => ['bonuse' => SORT_DESC],
                    ],
                    'last_modified' => [
                        'asc' => ['last_modified' => SORT_ASC],
                        'desc' => ['last_modified' => SORT_DESC],
                    ],
                    'visit_status' => [
                        'asc' => ['visit_status' => SORT_ASC],
                        'desc' => ['visit_status' => SORT_DESC],
                    ],
                    'is_payed' => [
                        'asc' => ['is_payed' => SORT_ASC],
                        'desc' => ['is_payed' => SORT_DESC],
                    ],
                    'deposit_term' => [
                        'asc' => ['deposit_term' => SORT_ASC],
                        'desc' => ['deposit_term' => SORT_DESC],
                    ],
                    'total_amount' => [
                        'asc' => ['total_amount' => SORT_ASC],
                        'desc' => ['total_amount' => SORT_DESC],
                    ],
                    'payed_amount' => [
                        'asc' => ['payed_amount' => SORT_ASC],
                        'desc' => ['payed_amount' => SORT_DESC],
                    ],
                    'payment_status' => [
                        'asc' => ['payment_status' => SORT_ASC],
                        'desc' => ['payment_status' => SORT_DESC],
                    ],
                    'accumulated_bonuse' => [
                        'asc' => ['accumulated_bonuse' => SORT_ASC],
                        'desc' => ['accumulated_bonuse' => SORT_DESC],
                    ],

                    'defaultOrder' => [
                        'visit_date' => SORT_DESC
                    ]
                ]
            ]
        ]);

        $this->load($params);

        if (!empty($params['is_payed'])) {
            $this->is_payed = $params['is_payed'];
        }

//        var_dump($this->date);die;
        if (empty($this->date)) {
            $startDate = date('d-m-Y', strtotime($dataProvider->query->min('visit_date') ?? date('d-m-Y')));
            $endDate = date('d-m-Y', strtotime($dataProvider->query->max('visit_date') ?? date('d-m-Y')));
            $this->date = implode(' - ', [$startDate, $endDate]);
        }
        list($this->dateFrom, $this->dateTo) = explode(' - ', $this->date);

        $dataProvider->setPagination(['pageSize' => $this->rows]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'visit_date' => $this->visit_date,
            'anketa_id' => $this->anketa_id,
            'pay_rate' => $this->pay_rate,
            'penalty' => $this->penalty,
            'total_amount' => $this->total_amount,
            'bonuse' => $this->bonuse,
            'is_payed' => $this->is_payed,
            'accumulated_bonuse' => $this->accumulated_bonuse,
            'is_prompt_payment' => $this->is_prompt_payment,
            'deposit_term' => $this->deposit_term,
            'is_payment_status_changed' => $this->is_payment_status_changed,
            'accumulation_start_date' => $this->accumulation_start_date,
            'last_modified' => $this->last_modified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'first_import_date' => $this->first_import_date,
        ]);

        if (isset($this->dateFrom) && isset($this->dateTo)) {
            $query->andFilterWhere(['>=', 'visit_date', date('Y-m-d H:i', strtotime($this->dateFrom . ' 00:00'))]);
            $query->andFilterWhere(['<=', 'visit_date', date('Y-m-d H:i', strtotime($this->dateTo . ' 23:59'))]);
        }

        $query->andFilterWhere(['like', 'survey_title', $this->survey_title])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'location_name', $this->location_name])
            ->andFilterWhere(['like', 'location_state_region', $this->location_state_region])
            ->andFilterWhere(['like', 'location_city', $this->location_city])
            ->andFilterWhere(['like', 'location_address', $this->location_address])
            ->andFilterWhere(['like', 'payroll_currency', $this->payroll_currency])
            ->andFilterWhere(['like', 'payroll_items', $this->payroll_items])
            ->andFilterWhere(['like', 'payment_method', $this->payment_method])
            ->andFilterWhere(['like', 'is_payed', $this->is_payed])
            ->andFilterWhere(['like', 'payed_amount', $this->payed_amount])
            ->andFilterWhere(['like', 'visit_status', $this->visit_status])
            ->andFilterWhere(['like', 'payment_status', $this->payment_status])
            ->andFilterWhere(['like', 'management', $this->management])
            ->andFilterWhere(['like', 'project', $this->project])
            ->andFilterWhere(['like', 'work_flow_status', $this->work_flow_status])
            ->andFilterWhere(['like', 'survey_status', $this->survey_status]);

        return $dataProvider;
    }
}
