<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Log;

/**
 * LogSearch represents the model behind the search form of `common\models\Log`.
 */
class LogSearch extends Log
{
    public $dateFrom;
    public $dateTo;
    public $date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'visit_id'], 'integer'],
            [['action', 'user_ip', 'created_at', 'updated_at', 'date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Log::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (empty($this->date)) {
            $startDate = date('d-m-Y', strtotime(Visit::find()->min('visit_date') ?? date('d-m-Y')));
            $endDate = date('d-m-Y', strtotime(Visit::find()->max('visit_date') ?? date('d-m-Y')));
            $this->date = implode(' - ', [$startDate, $endDate]);
        } else {
            list($this->dateFrom, $this->dateTo) = explode(' - ', $this->date);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'visit_id' => $this->visit_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if (isset($this->dateFrom) && isset($this->dateTo)) {
            $query->andFilterWhere(['>=', 'created_at', date('Y-m-d H:i', strtotime($this->dateFrom . ' 00:00'))]);
            $query->andFilterWhere(['<=', 'created_at', date('Y-m-d H:i', strtotime($this->dateTo . ' 23:59'))]);
        }

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'user_ip', $this->user_ip]);

        return $dataProvider;
    }
}
