<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bonuses_to_user".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $anketa_id
 * @property float|null $amount
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class BonusesToUser extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PAYED = 1;
    const STATUS_WALLET = 2;
    const STATUS_APPROVED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bonuses_to_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'anketa_id', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'anketa_id' => 'Anketa ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getVisit()
    {
        return $this->hasOne(Visit::className(), ['anketa_id' => 'anketa_id']);
    }
}
