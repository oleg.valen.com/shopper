<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $filename
 * @property string $date
 * @property int $status
 */
class Slider extends \yii\db\ActiveRecord
{
    public $imageFile;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filename'], 'required'],
            [['date'], 'safe'],
            [['status'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['filename'], 'unique'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Имя файла',
            'date' => 'Дата',
            'status' => 'Статус',
            'imageFile' => 'Файл с изображением',
        ];
    }

    public function upload()
    {
        if (1/*$this->validate()*/) {
            if ($this->imageFile) {
                $this->imageFile->saveAs(Yii::getAlias('@uploads') . "/{$this->imageFile->baseName}.{$this->imageFile->extension}");
                $this->filename = "{$this->imageFile->baseName}.{$this->imageFile->extension}";
                $this->imageFile = "{$this->imageFile->baseName}.{$this->imageFile->extension}";
            }
        } else {
            return false;
        }
    }
}
