<?php

namespace console\controllers;

use common\models\Api;
use common\models\Visit;
use common\models\User;
use Yii;
use yii\console\Controller;


Class ApiController extends Controller
{
    public function actionGetApiData()
    {
        $date = '2020-01-24';
//        $date = date('Y-m-d', strtotime("-1 day"));
        // $visitDataApi - info from visit table
        // $historicalDataApi - info with user data
        $visitDataApi = Api::getVisitData($date);
        $historicalDataApi = Api::getHistoricalData($date);

        // filterring $visitDataApi
        $visitIndex = [];
        foreach ($visitDataApi as $itemList) {
            foreach ($itemList as $itemData) {
                foreach ($itemData as $item) {
                    if (!array_intersect(explode(' ', strtolower($item['ClientName'])), [
                        '_certifications',
                        '!пустой',
                        '!delete',
                        '_extended profile',
                        'demo client',
                        '????на замену',
                        '_appeals',
                        '_repository',
                        'demo',
                        'demo client',
                        'demo Тайный соискатель',
                        'demo_Банк',
                    ])) {
                        $visitIndex[] = $item['InstanceID'];
                        @$resultVisitData[$item['InstanceID']][] = $item;
                    }
                }
            }
        }

        $issetVisitIds = [];
        $allVisits = Visit::find()->all();
        if ($allVisits) {
            foreach ($allVisits as $item) {
                @$issetVisitIds[] = $item->anketa_id;
            }
        }


        $historicalIndex = [];
        // filterring $historicalData - массив с фильтрованными историческими анкетами
        foreach ($historicalDataApi as $itemsList) {
            foreach ($itemsList as $itemData) {
                foreach ($itemData as $item) {
                    if (!array_intersect(explode(' ', strtolower($item['ClientName'])), [
                        '_certifications',
                        '!пустой',
                        '!delete',
                        '_EXTENDED PROFILE',
                        '_extended profile',
                        'demo client',
                        '????на замену',
                        '_appeals',
                        '_repository',
                        'demo',
                        'demo client',
                        'demo Тайный соискатель',
                        'demo_Банк',
                    ])) {
                        @$resultHistoricalData[$item['SurveyInstanceID']] = $item;
                        $historicalIndex[] = $item['SurveyInstanceID'];
                    }
                }
            }
        }

        $resultCompactData = [];
        foreach ($resultVisitData as $instance_id => $data) {
            $resultCompactData[$instance_id] = $data[0];
            if (!empty($resultHistoricalData[$instance_id])) {
                $resultCompactData[$instance_id]['Historical'] = $resultHistoricalData[$instance_id];
            } else {
                die('empty values from Historical data intance_ID = ' . $instance_id);
            }
        }

        foreach ($resultCompactData as $instanceId => $item) {
            // calculating bonuse and penalties
            $bonuse = 0;
            $penalty = 0;

            if (!empty($item['PayrollItems'])) {
                foreach ($item['PayrollItems'] as $payrollItem) {
                    if (!empty($payrollItem['Amount'])) {
                        if ($payrollItem['Amount'] >= 0) {
                            $bonuse += $payrollItem['Amount'];
                        } else {
                            $penalty += $payrollItem['Amount'];
                        }
                    }
                }
            }

            $visit = Visit::find()->where(['anketa_id' => $instanceId])->one();
            if(!$visit) {
                $visit = new Visit();
            }

            $visit->visit_date = date('Y-m-d H:i:s', strtotime($item['Date']));
            $visit->anketa_id = $item['InstanceID'];
            $visit->user_id = $item['Historical']['SecurityObjectUserID'];
            $visit->project = $item['ClientName'] . "\n" . $item['SurveyFormName'];
            $visit->location = is_array($item['LocationAddress']) ? implode(' ', $item['LocationAddress']) : $item['LocationAddress']; /*. "\n" . $item['City']*/
            $visit->amount = (int)$item['PayRate'] + $bonuse + $penalty;
            $visit->bonuse = $bonuse;
            $visit->penalty = $penalty;
            $visit->extra_charge = null;
            $visit->payment_method = $item['ShopperBonusAnswernull'];
            $visit->is_payed = 'Не оплачен';
//            $visit->visit_status = null;
//            $visit_status = [];
            if (preg_match("/Completed/", $item['SurveyStatus'])) {
                $visit->visit_status = 'Completed';
            } else if (preg_match("/Validation/", $item['SurveyStatus'])) {
                $visit->visit_status = 'Validation';
            } else if (preg_match("/On Hold/", $item['SurveyStatus'])) {
                $visit->visit_status = 'On Hold';
            } else {
                $visit->visit_status = 'No items';
            }

            $visit->payment_status = 'Accumulation';
            $visit->deposit_term = 45;
            $visit->deposit_left = 0;
            $visit->management = null;
            $visit->charity = null;
            $visit->is_payment_status_changed = null;

            $visit->save(false);
        }

        foreach ($resultCompactData as $instanceId => $item) {
            // add user to table if not exist
            $user = User::find()
                    ->where(['user_id' => $item['Historical']['SecurityObjectUserID']])
                    ->orWhere(['username' => $item['Historical']['Login']])
                    ->one();
            if(!$visit) {
                $visit = new User();
            }
            
            $historicalData = $item['Historical'];

            $user->user_id = $historicalData['SecurityObjectUserID'];
            $user->username = $historicalData['Login'];
            $user->name = $historicalData['Name'];
            $user->family_name = $historicalData['FamilyName'];

            $user->country = $historicalData['Country'];
            $user->payment_type = $historicalData['ShopperBonusAnswer'];
//            $user->bank_data = isset($historicalData['ShopperBonusComment']) && is_array($historicalData['ShopperBonusComment']) ? implode(' ', $historicalData['ShopperBonusComment']) : $historicalData['ShopperBonusComment'];
//            $user->account = $historicalData['ShopperBonusComment'] && is_array($historicalData['ShopperBonusComment']) ? implode(' ', $historicalData['ShopperBonusComment']) : $historicalData['ShopperBonusComment'];
            $user->email = $historicalData['Email'];
            $user->password_hash = '$2y$13$Sne7BSaOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLweT9LUpaKDkbWyta';//Yii::$app->security->generatePasswordHash($historicalData['Login']);

            $user->save(false);
        }
    }
}
