<?php

namespace console\controllers;

use console\services\CronService;
use frontend\models\ContactForm;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;

class CronController extends Controller
{

    private $cron;

    public function init()
    {
        parent::init();
        $this->cron = new CronService();
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if (CronService::$errors) {
            $form = new ContactForm([
                'email' => Yii::$app->params['receiverEmail'],
                'name' => Yii::$app->params['receiverName'],
                'subject' => 'Shopperbonus api error ' . date('H:i:s d-m-Y'),
                'body' => Json::encode(CronService::$errors, JSON_PRETTY_PRINT),
            ]);
            try {
                $form->sendEmail(Yii::$app->params['receiverEmail']);
            } catch (\Exception $e) {
                Yii::error([
                    'name' => 'Start: ' . __METHOD__,
                    'time' => $start = microtime(true),
                    'error' => $e,
                ], 'api');
            }
        }

        return $result;
    }

    public function actionGetApiData()
    {
        Yii::info([
            'name' => 'Start: ' . __METHOD__,
            'time' => $start = microtime(true),
        ], 'api');
        $this->cron->getApiData();
        Yii::info([
            'name' => 'Finish: ' . __METHOD__,
            'time' => microtime(true) - $start,
            'memory' => memory_get_usage(true),
        ], 'api');
    }

    public function actionBonuseCalculation()
    {
        Yii::info([
            'name' => 'Start: ' . __METHOD__,
            'time' => $start = microtime(true),
        ], 'api');
        $this->cron->calcBonus();
        Yii::info([
            'name' => 'Finish: ' . __METHOD__,
            'time' => microtime(true) - $start,
            'memory' => memory_get_usage(true),
        ], 'api');
    }

    public function actionGetWaitingNotBonus()
    {
        Yii::info([
            'name' => 'Start: ' . __METHOD__,
            'time' => $start = microtime(true),
        ], 'api');
        $this->cron->getWaitingNotBonus();
        Yii::info([
            'name' => 'Finish: ' . __METHOD__,
            'time' => microtime(true) - $start,
            'memory' => memory_get_usage(true),
        ], 'api');
    }

}
