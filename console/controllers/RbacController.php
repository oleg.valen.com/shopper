<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{

    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные

        //Создадим для примера права для доступа к админке
        $dashboard = $auth->createPermission('adminPanel');
        $dashboard->description = 'Админ панель';
        $auth->add($dashboard);

        $userPanel = $auth->createPermission('userPanel');
        $userPanel->description = 'Управление пользователями';
        $auth->add($userPanel);

        $moderate = $auth->createPermission('moderate');
        $moderate->description = 'Управление задачами';
        $auth->add($moderate);

        //Добавляем объект определяющий правила для ролей пользователей, он будет сохранен в файл rules.php
        $rule = new UserRoleRule();
        $auth->add($rule);

        //Добавляем роли
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $user->ruleName = $rule->name;
        $auth->add($user);

        $moder = $auth->createRole('moder');
        $moder->description = 'Модератор';
        $moder->ruleName = $rule->name;
        $auth->add($moder);

        //Добавляем потомков
        $auth->addChild($moder, $userPanel);
        $auth->addChild($moder, $dashboard);
        $auth->addChild($moder, $moderate);

        $auth->addChild($user, $userPanel);
        $auth->addChild($admin, $userPanel);
        $auth->addChild($admin, $dashboard);

        $auth->addChild($admin, $moder);
    }

}
