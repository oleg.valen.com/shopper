<?php

namespace console\services;

use common\models\Api;
use common\models\User;
use common\models\Visit;
use common\models\VisitStatus;
use Yii;
use yii\helpers\ArrayHelper;

class CronService
{

    public static $errors = [];

    public function getApiData()
    {
//        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 2400);

        $users = [];

        $startDate = date('Y-m-d', strtotime(date('Y-m-d') . 'first day of last month'));
        $endDate = date('Y-m-d', time());

//        $startDate = '2021-06-07';
//        $endDate = '2021-06-07';

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        $startTime = time();

        foreach ($dates as $date) {
            try {
                // $visitDataApi      - info from visit table
                // $historicalDataApi - info with user data
                $visitData = ArrayHelper::index(Api::getVisitData($date)['items']['item'], 'InstanceID');
                $historicalData = ArrayHelper::index(Api::getHistoricalData($date)['items']['item'], 'SurveyInstanceID');

                // parsing exist array to database
                foreach ($visitData as $instanceId => $item) {
                    if (isset($historicalData[$instanceId])) {
                        // убираем лишние анкеты
                        if (isset($historicalData[$instanceId]['IsOkForPayroll'])
                            && isset($historicalData[$instanceId]['ClientName'])
                            && $historicalData[$instanceId]['IsOkForPayroll']) {

                            if (array_intersect(explode(' ', strtolower($historicalData[$instanceId]['ClientName'])), [
                                '_CERTIFICATIONS',
                                '_certifications',
                                '!пустой',
                                '!delete',
                                '_extended profile',
                                'demo client',
                                '????на замену',
                                '_appeals',
                                '_repository',
                                'demo',
                                'demo client',
                                'demo тайный соискатель',
                                'demo_банк',
                            ])) {
                                continue;
                            }
                        }

                        $visit = Visit::find()->where(['anketa_id' => $instanceId])->one();
                        if ($visit == null) {
                            $visit = new Visit();
                        }

                        $visit->anketa_id = $instanceId;
                        $visit->user_id = $historicalData[$instanceId]['SecurityObjectUserID'];
                        $visit->visit_date = date('Y-m-d H:i:s', strtotime($item['Date']));
                        $visit->survey_title = $item['SurveyFormName'];
                        $visit->client_name = $item['ClientName'];
                        $visit->location_address = isset($item['LocationAddress']) && is_array($item['LocationAddress']) ? implode(',', $item['LocationAddress']) : $item['LocationAddress'] ?? null;
                        $visit->location_city = isset($item['LocationCity']) && is_array($item['LocationCity']) ? implode(',', $item['LocationCity']) : $item['LocationCity'] ?? null;
                        $visit->location_state_region = isset($item['LocationStateRegion']) && is_array($item['LocationStateRegion']) ? implode($item['LocationStateRegion']) : $item['LocationStateRegion'] ?? null;
                        $visit->location_name = isset($historicalData[$instanceId]['LocationName']) && is_array($historicalData[$instanceId]['LocationName']) ? implode(',', $historicalData[$instanceId]['LocationName']) : $historicalData[$instanceId]['LocationName'] ?? null;
                        $visit->project = $item['ClientName'] . "\n" . $item['SurveyFormName'];
                        $visit->payroll_currency = $historicalData[$instanceId]['PayrollCurrency'] ?? null;
                        $visit->payroll_items = json_encode($item['PayrollItems'] ?? null);

                        // до 11 числа следующего от визита месяца
                        $visitMonth = (int)date('m', strtotime($visit->visit_date));
                        if (time() < mktime(0, 0, 0, $visitMonth + 1, 11, (int)date('Y', time()))) {
                            $visit->pay_rate = $item['PayRate'] ?? $visit->pay_rate ?? null;
                            if (!empty($item['SurveyStatus'])) {
                                if (preg_match("/Completed/", $item['SurveyStatus'])) {
                                    $visit->visit_status = 'Completed';
                                } else if (preg_match("/Validation/", $item['SurveyStatus'])) {
                                    $visit->visit_status = 'Validation';
                                } else {
                                    $visit->visit_status = null;
                                }
                            }
                            if (!empty($item['WorkFlowStatus'])) {
                                if (preg_match("/Completed/", $item['WorkFlowStatus'])) {
                                    $visit->visit_status = 'Completed';
                                } else if (preg_match("/Validation/", $item['WorkFlowStatus'])) {
                                    $visit->visit_status = 'Validation';
                                } else {
                                    $visit->visit_status = null;
                                }
                            }

                            // calculating bonuse and penalties
                            $bonuse = $penalty = 0;
                            $wait_bonuse_sm = null;
                            if (isset($item['PayrollItems']['PayrollItem'])) {
                                if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                    if (isset($oneItem['Amount'])) {
                                        if ($oneItem['Amount'] >= 0) {
                                            @$bonuse += $oneItem['Amount'];
                                        } else if ($oneItem['Amount'] < 0) {
                                            @$penalty += $oneItem['Amount'];
                                        }
                                        if ($oneItem['Name'] == 'Доплата накоплений Шопербонуса') {
                                            @$wait_bonuse_sm = $oneItem['Amount'];
                                        }
                                    }
                                } else {
                                    foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                        if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                            if ($payrollItem['Amount'] >= 0) {
                                                @$bonuse += $payrollItem['Amount'];
                                            } else if ($payrollItem['Amount'] < 0) {
                                                @$penalty += $payrollItem['Amount'];
                                            }

                                            if ($payrollItem['Name'] == 'Доплата накоплений Шопербонуса') {
                                                @$wait_bonuse_sm = $payrollItem['Amount'];
                                            }
                                        }
                                    }
                                }
                            }
                            $visit->wait_bonuse_sm = $wait_bonuse_sm ?? null; /*$historicalData[$instanceId]['PrecalcPayrollItemsSum'] ??*/
                            $visit->bonuse = $bonuse ?? 0;/*$historicalData[$instanceId]['PrecalcPayrollItemsSum'] ??*/

                            $visit->penalty = $penalty ?? 0;
                            $visit->accumulated_bonuse = $visit->accumulated_bonuse ?? 0;
                            $visit->total_amount = $visit->pay_rate + $visit->bonuse + $visit->penalty;
                            $visit->work_flow_status = $historicalData[$instanceId]['WorkFlowStatus'] ?? null;
                            $visit->survey_status = $historicalData[$instanceId]['SurveyStatusName'] ?? null;
                            $visit->payment_status = null;
                            $visit->deposit_term = 0;
                            $visit->deposit_left = 0;
                            $visit->management = null;
                            $visit->payment_method = null;
                        } else {
                            if ($visit->visit_status == 'Validation' || $visit->visit_status == null) {
                                $visit->visit_status = 'Completed';
                            }
                        }

                        // wait_bonus_sm
                        if (isset($item['PayrollItems']['PayrollItem'])) {
                            if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                if (isset($oneItem['Amount'])) {
                                    if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                        $visit->is_prompt_payment = true;
                                        $visit->visit_status = 'Approved_to_withdrawal';
                                        $visit->accumulated_bonuse = 0;
                                    }
                                }
                            } else {
                                foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                    if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                        if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                            $visit->is_prompt_payment = true;
                                            $visit->visit_status = 'Approved_to_withdrawal';
                                            $visit->accumulated_bonuse = 0;
                                        }
                                    }
                                }
                            }
                        }

                        // до 60 дней от даты визита
                        if (time() < strtotime('+60 days', strtotime($visit->visit_date))) {
                            if (isset($item['PayrollItems']['PayrollItem'])) {
                                if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                    if (isset($oneItem['Amount'])) {
                                        if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                            $visit->is_prompt_payment = true;
                                            $visit->visit_status = 'Approved_to_withdrawal';
                                            $visit->accumulated_bonuse = 0;
                                        }
                                    }
                                } else {
                                    foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                        if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                            if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                                $visit->is_prompt_payment = true;
                                                $visit->visit_status = 'Approved_to_withdrawal';
                                                $visit->accumulated_bonuse = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $isPromptPaymentWaitingNotBonus = false;
//                        if (time() > strtotime('+50 days', strtotime($visit->visit_date))) {
                        if (isset($item['PayrollItems']['PayrollItem'])) {
                            if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                if (isset($oneItem['Amount'])) {
                                    if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                        $isPromptPaymentWaitingNotBonus = true;
                                    }
                                }
                            } else {
                                foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                    if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                        if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                            $isPromptPaymentWaitingNotBonus = true;
                                        }
                                    }
                                }
                            }
                        }
//                        }
                        if ($isPromptPaymentWaitingNotBonus) {
                            $visit->is_prompt_payment = true;
                            $visit->visit_status = VisitStatus::WAITING_NOT_BONUS;
                        }

                        $visit->is_payed = $visit->is_payed ?? 'Not_payed';
                        $visit->last_modified = $visit->last_modified ?? date('Y-m-d H:i:s');
                        $visit->is_payment_status_changed = $visit->is_payment_status_changed ?? 0;
                        $visit->first_import_date = $visit->first_import_date ?? date('Y-m-d H:i:s', time());

                        $visit->save(false);

                        $hash = md5($historicalData[$instanceId]['SecurityObjectUserID'] . $historicalData[$instanceId]['Login']);
                        //предполагаем, что комбинация SecurityObjectUserID и Login не меняется
                        if (array_key_exists($hash, $users)) {
                            $user = $users[$hash];
                        } else {
//                        // выбираем user из БД или создаем новую запись
                            $user = User::find()
                                ->where(['user_id' => $historicalData[$instanceId]['SecurityObjectUserID']])
                                ->orWhere(['username' => $historicalData[$instanceId]['Login']])
                                ->one();
                            if ($user == null) {
                                $user = new User();
                            }
                            $users[$hash] = $user;
                        }

                        $user->user_id = $historicalData[$instanceId]['SecurityObjectUserID'];
                        $user->username = $user->username ?? trim($historicalData[$instanceId]['Login']);
                        $user->email = !empty($historicalData[$instanceId]['Email']) ? trim($historicalData[$instanceId]['Email']) : $user->email;
                        $user->name = !empty($historicalData[$instanceId]['Name']) ? trim($historicalData[$instanceId]['Name']) : $user->name;// $user->name ?? trim($historicalData[$instanceId]['Name']);
                        $user->family_name = !empty($historicalData[$instanceId]['FamilyName']) ? trim($historicalData[$instanceId]['FamilyName']) : $user->family_name;// $user->family_name ?? trim($historicalData[$instanceId]['FamilyName']);
                        $user->country = !empty($historicalData[$instanceId]['Country']) ? trim($historicalData[$instanceId]['Country']) : $user->country;// $user->country ?? trim($historicalData[$instanceId]['Country']);
                        $user->role = 'user';
                        $user->status = 10;
                        $user->bonuse_percent = 10;
                        $user->password_hash = '$2y$13$Sne7BSauy67UYFOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLsdfghjkertyuweT9LUpaKDkbWyta'; //Yii::$app->security->generatePasswordHash($user->email); //'$2y$13$Sne7BSaOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLweT9LUpaKDkbWyta';

                        $user->save(false);
                    }
                    unset($visit, $user);
                    unset($item[$instanceId]);
                }
//            echo('Получено user анкет ' . $date . ' : ' . count($visitData) . '<br>');
//            echo('Получено historical анкет ' . $date . ' : ' . count($historicalData) . '<br>');
                @$anketsCount += count($visitData);
                unset($visitData, $historicalData);

            } catch (\Exception $e) {
                self::$errors[] = $e->getMessage();
                Yii::error([
                    'name' => 'api',
                    'data' => ['date' => $date],
                    'error' => $e->getMessage()], 'api');
            }

        }

//        $finishTime = time();
//        $scriptTime = $finishTime - $startTime;
//        echo 'Scripttime = ' . $scriptTime;
//        echo 'Total ankets proceed = ' . ($anketsCount ?? 0);

//        $this->actionBonuseCalculation(); //делаем отдельным cron
    }

    public function calcBonus()
    {
        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 2400);

//        $startDate = '2020-01-01';
        $startDate = '2021-06-01';
        $endDate = date('Y-m-d');

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        $startTime = time();
        $counter = 0;
        foreach ($dates as $date) {
            try {
                $visits = Visit::find()
                    ->where(['DATE(visit_date)' => $date])
                    ->andWhere(['is_payed' => 'Not_payed'])
                    ->indexBy('anketa_id')
                    ->all();

                foreach ($visits as $item) {

                    $visitMonth = date('n', strtotime($item->visit_date));
                    $visitYear = date('Y', strtotime($item->visit_date));

                    // add condition for Elena's letter
                    if (date('Y-m-d', strtotime($item->visit_date)) >= '2021-06-01') {
                        $item->accumulation_start_date = date('Y-m-d', mktime(0, 0, 0, $visitMonth + 1, 21, $visitYear));
                    } else {
                        $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
                    }

                    // crutch for Ihor
                    if ($item->is_prompt_payment && $item->accumulated_bonuse > 0) {
                        $item->is_prompt_payment = null;
                    }

                    if (date('Y-m-d') >= $item->accumulation_start_date && $item->accumulation_start_date != null) {
                        $bonuseTerm = $this->dateDiff($item->accumulation_start_date, date('Y-m-d'));
                        $bonusePersent = $bonuseTerm > 365 ? 0.11 : ($bonuseTerm > 180 ? 0.10 : 0.09);
                        $accumulatedBonuse = round($bonuseTerm * $bonusePersent * $item->total_amount / 365, 2);
//                        $item->accumulated_bonuse = $accumulatedBonuse > 0 ? $accumulatedBonuse : 0;
                        if ($item->visit_status != VisitStatus::WAITING_NOT_BONUS) {
                            if ($item->wait_bonuse == $item->wait_bonuse_sm) {
                                $item->visit_status = 'Approved_to_withdrawal';
                            } else {
                                $item->visit_status = 'Rejected';
                            }
                        }

                    } else {
                        $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
                    }

                    // задача от 06.2021 не начислять бонусы для сотрудников 4сервис
                    $emailParts = explode('@', $item->users->email);
                    if ($emailParts[1] === '4service-group.com') {
//                    $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
//                    $item->save(false);
//                    continue;
                    }

                    $item->save(false);

                    unset($item);

                    $counter++;
                }

                unset($visits);

            } catch (\Exception $e) {
                self::$errors[] = $e->getMessage();
                Yii::error([
                    'name' => 'bonuseCalculation',
                    'data' => ['date' => $date],
                    'error' => $e->getMessage(),
                ], 'api');
            }

        }

        $endTime = time();

        echo 'counts = ' . $counter . '<br>';
        echo 'time = ' . ($endTime - $startTime) . '<br>';
        return true;
    }

    public function getWaitingNotBonus()
    {
//        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 2400);

        $startDate = '2021-06-01';
        $endDate = date('Y-m-d', time());
//        $startDate = '2021-10-01';
//        $endDate = '2021-10-31';

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        foreach ($dates as $date) {
            try {
                $visitData = ArrayHelper::index(Api::getVisitData($date)['items']['item'], 'InstanceID');
                foreach ($visitData as $instanceId => $item) {
                    $isPromptPaymentWaitingNotBonus = false;
//                  if (time() > strtotime('+50 days', strtotime($visit->visit_date))) {
                    if (isset($item['PayrollItems']['PayrollItem'])) {
                        if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                            if (isset($oneItem['Amount'])) {
                                if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                    $isPromptPaymentWaitingNotBonus = true;
                                }
                            }
                        } else {
                            foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                    if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                        $isPromptPaymentWaitingNotBonus = true;
                                    }
                                }
                            }
                        }
                    }
//                  }
                    if ($isPromptPaymentWaitingNotBonus && $visit = Visit::find()->where(['anketa_id' => $instanceId])->one()) {
                        $visit->is_prompt_payment = true;
                        $visit->visit_status = VisitStatus::WAITING_NOT_BONUS;
                        $visit->save(false);
                    }
                    unset($visit, $item[$instanceId]);
                }
                unset($visitData);

            } catch (\Exception $e) {
                self::$errors[] = $e->getMessage();
                Yii::error([
                    'name' => 'api',
                    'data' => ['date' => $date],
                    'error' => $e->getMessage()], 'api');
            }
        }
    }

    public function dateDiff($date1, $date2)
    {
        return strtotime($date2) >= strtotime($date1) ? round((strtotime($date2) - strtotime($date1)) / (60 * 60 * 24), 0) : 0;
    }

}
