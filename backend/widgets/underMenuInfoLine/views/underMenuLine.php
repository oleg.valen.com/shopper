<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="bonus-wrapper" style="">
    <section class="under-menu" style="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="d-flex justify-content-between" style="padding: 0;">
                        <li class="under-menu_item">БОНУСОВ: <?= round($summaryData['bonuse'], 2) ?? 0 ?> RUB</li>
                        <li class="under-menu_item">ИТОГО: <?= round($summaryData['total_amount'], 2) ?> RUB</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
