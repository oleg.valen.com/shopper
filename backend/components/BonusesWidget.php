<?php

namespace backend\components;

use common\models\BonusesToUser;
use Yii;
use yii\base\Widget;

class BonusesWidget extends Widget
{
    public function run()
    {
        $userSecurityId = Yii::$app->session->get('target-user-id', 1);
        $bonusesWallet = BonusesToUser::find()->where(['user_id' => $userSecurityId])->sum('amount');

        return $this->render('bonuses', ['bonusesWallet' => $bonusesWallet]);
    }
}
