<?php

namespace backend\components;

use common\models\Visit;
use Yii;
use yii\base\Widget;
use common\models\User;
use yii\web\UploadedFile;

class HeaderWidget extends Widget
{
    public function run()
    {
        $session = Yii::$app->session;

        if ($session->has('target-user-id')) {
            $userId = $session->get('target-user-id');

            $userData = User::findOne(['user_id' => $userId]);
            if($userData != null) {
                $userSecurityId = $userData->user_id;
                $query = Visit::find()
                    ->select(['payment_status', 'total_amount', 'accumulated_bonuse', 'id', 'anketa_id'])
                    ->where(['is_payed' => 'Wallet'])
                    ->andWhere(['user_id' => $userSecurityId]);
                $wallet = $query->all();
                $walletCount = $query->count();
            }
        }

        return $this->render('header', compact('userData', 'wallet', 'walletCount', 'userData'));
    }
}
