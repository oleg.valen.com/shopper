<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<div class="wallet" style="padding-bottom: 10px; width: 35px;">
    <div class="wallet-image">
        <img class="" src="/img/wallet.png" alt="wallet">
        <!--                                        <span class="badge badge-light">******</span>-->
    </div>
    <div class="wallet-pop-up i-pop-up" id="wallet-items" style="">
        <table class="wallet-table-header">
            <thead>
            <tr>
                <th>Номер анкеты</th>
                <th>Сумма</th>
<!--                <th>Удалить</th>-->
                <th>Назначение платежа</th>
            </tr>
            </thead>
        </table>
        <div class="walet-table-wrapper waller-yable-scroller">
            <table class="wallet-table-body">
                <tbody>

                <?php $payment_status_values = [
                    'Charity' => 'Благотворительность',
                    'Withdrawal' => 'Вывод',
                ] ?>

                <?php $summ = 0 ?>
                <?php $summCharity = 0 ?>
                <?php foreach ($wallet as $item) { ?>
                    <tr id="walet-<?= $item['id'] ?>">
                        <td><?= $item->anketa_id ?></td>
                        <td><?= $item->total_amount ?></td>
                        <td><?= $payment_status_values[$item['payment_status']] ?? '' ?></td>
                        <?php $summ += $item->is_payed === 'Wallet' ? $item->total_amount : 0 ?>
                        <?php $summCharity += $item['payment_status'] == 'Charity' ? $item['total_amount']+$item['accumulated_bonuse'] : 0 ?>
                    </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
        <div class="bonus-bottom-info flex j-s-b">
            <div>Всего к оплате</div>
            <div id="payment-amount" data-amount=""><?= $summ ?? 0 ?> RUB</div>
        </div>
        <!--<div class="bonus-bottom-info flex j-s-b">
            <div>Всего на Благотворительность</div>
            <div id="charity-amount" data-amount=""><?/*= $summCharity ?? 0 */?> RUB</div>
        </div>-->
        <!--<div class="bonus-bottom-btn flex j-c">
            <button class="i-btn" onclick="popupLauncher()">
                <p id="pay-button">Оправить на оплату</p>
            </button>
        </div>-->
        <!--                                        <p><strong>Кошелек пуст</strong></p>-->
    </div>
</div>
