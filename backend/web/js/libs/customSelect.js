var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("i-custom-select");
function setSelct( arr ){
    for (i = 0; i < arr.length; i++) {
        selElmnt = arr[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.querySelector('option').value;

        if( selElmnt.querySelector('option[selected]') && !selElmnt.hasAttribute('data-no-change') ) {
            a.innerHTML = selElmnt.querySelector('option[selected]').value;
        }
        else{
            a.innerHTML = selElmnt.querySelector('option').value;
        }

        arr[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {

            c = document.createElement("DIV");
            c.innerHTML =  '<img src="' + selElmnt.options[j].getAttribute('dataurl') + '">' + selElmnt.options[j].innerHTML;
            c.setAttribute('dataurl', selElmnt.options[j].getAttribute('dataurl'));
            c.addEventListener("click", function(e) {
                /*when an item is clicked, update the original select box,
                 and the selected item:*/
                // console.log(222);
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                console.log(h);
                for (i = 0; i < s.length; i++) {
                    var eee = this.innerHTML;

                    // console.log( i );
                    if ( ( '<img src="' + s.options[i].getAttribute('dataurl') + '">' + s.options[i].innerHTML ) == eee) {
                        //console.log();
                        if( $(s.options[i]).attr('dataprezurl') == undefined ) {
                            $( '#prez-url' ).removeAttr('href');
                        }
                        else {
                            $( '#prez-url' ).attr('href',$(s.options[i]).attr('dataprezurl'));
                        }
                        $( 'iframe' ).attr('src',$(s.options[i]).attr('datavidurl'));
                        s.selectedIndex = i;
                        if( !s.hasAttribute('data-no-change') ) {
                            h.innerHTML = eee;
                        }
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }

                // console.log( 'done' );
                $(this).parents('.i-select-wrapper').find('select').change();
                h.click();
            });
            b.appendChild(c);
        }
        arr[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /*when the select box is clicked, close any other select boxes,
             and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            //console.log( this.parentElement.querySelector('select') );
        });
    }
}
setSelct(x);

//$(x).change();
for( let c = 0; c < x.length; c++ ) {
    console.log('asedfsefse')
    if ( !$(x[c]).find('select')[0].hasAttribute('data-no-change') ) {
        $(x[c]).find('select')[0].selectedIndex = $(x[c]).find('select')[0].selectedIndex;
        console.log('asedfsefse')
    }
    else {
        console.log('asedfsefse')
        $(x[c]).find('select')[0].selectedIndex = 0;
    }
    $(x[c]).find('select').change();
}
function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
     except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
 then close all select boxes:*/
document.addEventListener("click", closeAllSelect);