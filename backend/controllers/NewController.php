<?php

namespace backend\controllers;

use common\models\AdminDone;
use common\models\Api;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\User;
use common\models\Visit;
use common\models\Charity;
use Yii;
use common\models\VisitSearch;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Html;
use Exception;

/**
 * Site controller
 */
class NewController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => [
//                    'index', 'payment', 'payed', 'accumulation', 'charity', 'finduser', 'validate', 'save', 'get-data', 'get-api-data', 'count', 'cancel-charity'
//                ],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['login', 'get-api-data'],
//                        'roles' => ['?', 'admin'],
//                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'index', 'payment', 'payed', 'accumulation', 'charity', 'finduser', 'validate', 'get-data', 'save', 'get-api-data', 'count', 'cancel-charity', 'get-api-data'
//                        ],
//                        'roles' => ['admin'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'admin-main';
        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['user_id' => $userSecurityId, 'is_payed' => 'Not_payed']);

//        $this->debug($dataProvider->getModels());


        // todo переписать
        //форма комемнтария админа
        $commentModel = new AdminDone();
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {
            $actions = Yii::$app->request->post('actions');
            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['anketa_id' => $action[0]])
                    ->one();
                //запись в таблицу БД
                $visit->visit_status = $action[1];
                $visit->payment_status = 'Accumulation';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }
            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => $userSecurityId])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

//        $visit = Visit::find()
//            ->where(['user_id' => $userSecurityId])
//            ->andWhere(['is_payed' => 'Not_payed'])
//            ->orderBy("$sort $order");

//        $countQuery = clone($visit);
//        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
//        $visit = Visit::find()
//            ->where(['user_id' => $userSecurityId])
//            ->andWhere(['is_payed' => 'Not_payed'])
//            ->orderBy("$sort $order")
//            ->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['user_id', 'username'])
            ->asArray()
            ->all(), 'user_id');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users
        ]);
    }

    public function actionCancelCharity()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()
                ->where(['id' => $_POST['id']])
                ->one();
            //получение измененных админом данных
//        $request = \Yii::$app->getRequest();
//
//        if ($request->isPost) {
//            $id = Yii::$app->request->post('id');
//            $model = Visit::find()->where(['anketa_id' => $id])->one();
            $model->payment_status = null;
            $model->is_payed = 'Not_payed';
            $model->last_modified = date('Y-m-d H:i:s', time());
            $model->save(false);

            return true;
        }
        return false;
    }

    public function actionPayed($sort = 'visit_date', $order = 'DESC', $filter = ['Payed', 'Charity'])
    {
        $this->layout = 'admin-main';

        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['and', ['user_id' => $userSecurityId], ['in', 'is_payed', ['Payed', 'Charity']]]);

        if ($filter == 'Благотворительность') {
            $filter = 'Charity';
        }

        if ($filter == 'Оплачен') {
            $filter = 'Payed';
        }

        //форма комемнтария админа
        $commentModel = new AdminDone();

        //получение измененных админом данных с фронта
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {

            $actions = Yii::$app->request->post('actions');

            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['id' => $action[0]])
                    ->one();

                //запись в таблицу БД
                $visit->is_payed = 'Payed';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }

            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => Yii::$app->session->get('target-user-id')])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['id', 'username'])
            ->asArray()
            ->all(), 'id');

        return $this->render('payed', [
            'uploadModel' => $uploadModel,
            'filter' => $filter,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayment($sort = 'visit_date', $order = 'DESC', $filter = ['Rejected', 'Payed', 'Waiting_pay', 'Sent_to_pay', 'Charity'])
    {
        $this->layout = 'admin-main';

        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['and', ['user_id' => $userSecurityId], ['in', 'is_payed', ['Waiting_pay']]]);


        //форма комемнтария админа
        $commentModel = new AdminDone();

        //получение измененных админом данных с фронта
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {
            $actions = Yii::$app->request->post('actions');
            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['id' => $action[0]])
                    ->one();
                //запись в таблицу БД
                $visit->is_payed = 'Waiting_pay';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }

            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => Yii::$app->session->get('target-user-id')])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();



        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {

            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();

            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['id', 'username'])
            ->asArray()
            ->all(), 'id');


        return $this->render('payment', [
            'uploadModel' => $uploadModel,
            'filter' => $filter,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAccumulation($sort = 'visit_date', $order = 'DESC')
    {
        $this->layout = 'admin-main';

        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([
            'user_id' => $userSecurityId,
            'payment_status' => 'Accumulation',
            'visit_status' => 'Completed'
        ]);

//        $visit = Visit::find()
//            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
//            ->andWhere(['payment_status' => 'Accumulation'])
//            ->andWhere(['visit_status' => 'Completed'])
//            ->orderBy("$sort $order");

//        $countQuery = clone($visit);

//        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

//        $visit = $countQuery
//            ->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();
//        var_dump(Yii::$app->session->get('targetUserId'));
//        die;
        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {

            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();

            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        return $this->render('accumulation', [
//            'visit' => $visit,
//            'order' => $order,
            'uploadModel' => $uploadModel,
//            'pages' => $pages,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCharity()
    {
        $this->layout = 'admin-main';

        $userSecurityId = Yii::$app->session->get('target-user-id');
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($userSecurityId);
        $charities_amount = Visit::charitiesAmount($userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($userSecurityId);

        $charities_to_view = Visit::getCharitiesToView($charitiesList, $charities_count, $charities_amount);

        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }

                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        $charities_report = Visit::find()
            ->where(['payment_status' => 'Charity'])
            ->all();

        foreach ($charities_report as $item) {
            @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['total_amount'] += $item->total_amount;

            if (!isset($charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'] = [];
            }
            if (!in_array($item->user_id, $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'][] += $item->user_id;
            }
        }

        $charityTotal = Visit::charitiesAmount($userSecurityId);

        $month = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ];

        return $this->render('charity', compact('month', 'charityTotal', 'charitiesDone', 'charitiesList'));
    }

    public function actionFinduser()
    {
        if ($login = Yii::$app->request->get('login')) {
            if (!is_null($userData = User::find()
                ->where(['username' => $login])
                ->one())) {
                Yii::$app->session->set('target-user-id', $userData->user_id);
                Yii::$app->session->set('target-user-login', $userData->username);
                Yii::$app->session->set('target-user-accumulated-bonuse', $userData->accumulated_bonuse);
                Yii::$app->session->set('target-user-bonuce-percent', $userData->bonuse_percent);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLogin()
    {
        $this->layout = 'login';

        $loginWithEmail = Yii::$app->params['loginWithEmail'];

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = $loginWithEmail ? new LoginForm(['scenario' => 'loginWithEmail']) : new LoginForm();

        if ($model->load(Yii::$app->request->post())) {

//            echo '<pre>';
//            var_dump($model->attributes);
//            die;

//            if ($model->username == 'admin' || $model->email = 'budonnyi@gmail.com') {
//                Yii::$app->session->destroy();
//                return $this->redirect('/admin/login');
//            }

            $model->login();

            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $this->layout = 'admin-main';

        $model = Visit::find()
            ->where(['id' => $id])
            ->one();

        if ($model->load(Yii::$app->request->post())) {

            $model->payment_status = 'Накопление';
            $model->last_modified = date('d.m.Y');
            $model->save(false);

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

//сохранение комментариев админа
    public function actionSave()
    {
        //форма комемнтария админа
        $commentModel = new AdminDone();

        if ($commentModel->load(\Yii::$app->request->post()) && $commentModel->save()) {
            return true;
        } else {
            return false;
        }

        return $this->render('index', compact('commentModel'));
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
