<?php

namespace backend\controllers;

use common\models\Select;
use common\models\User;
use common\models\Visit;
use Yii;
use common\models\Log;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex($user_id = null)
    {
        $this->layout = 'admin-main';

        $session = Yii::$app->session;
        if($user_id == null ) {
            $user_id = $session->get('target-user-id');
        }

        $userData = User::findOne(['user_id' => $user_id]);
        if($user_id == null) {
            return $this->redirect(['site/index']);
        }

        if(!is_null($userData)) {

            if (!$session->has('log-start-date')) {
                $session->set('log-start-date', date('Y-m-d', strtotime(Log::find()
                        ->where(['user_id' => $userData->id])
                        ->min('created_at') ?? date('Y-m-d'))));
                $session->set('log-end-date', date('Y-m-d', strtotime(Log::find()
                        ->where(['user_id' => $userData->id])
                        ->max('created_at') ?? date('Y-m-d'))));
            }
            $startDate = $session->get('log-start-date');
            $endDate = $session->get('log-end-date');

            if (!empty($request = Yii::$app->request->post())) {
                if (!empty($request['Select']['date'])) {
                    list($startDate, $endDate) = explode(' - ', $request['Select']['date']);
                }
                $session->set('log-start-date', $startDate);
                $session->set('log-end-date', $endDate);
            }

            $query = Log::find()
                ->andFilterWhere(['>', 'created_at', date('Y-m-d H:i:s', strtotime($startDate . ' 00:00:00'))])
                ->andFilterWhere(['<', 'created_at', date('Y-m-d H:i:s', strtotime($endDate . ' 23:59:59'))])
                ->orderBy(['created_at' => SORT_DESC])
                ->andWhere(['user_id' => $userData->id]);

            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);
            
            if (!is_null($userData)) {
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);
            }
        }

        $filterModel = new Select();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
            'userData' => $userData,
            'pages' => $pages
        ]);
    }

    /**
     * Displays a single Log model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Log model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Log();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Log model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Log model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Log model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Log the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
