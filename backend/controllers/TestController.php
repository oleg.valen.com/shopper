<?php

namespace backend\controllers;

use common\models\AdminDone;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\User;
use Yii;
use common\models\Visit;
use common\models\VisitSearch;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Html;
use common\models\Charity;

use SimpleXMLElement;

/**
 * Site controller
 */
class TestController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'payment', 'accumulation', 'charity', 'finduser', 'validate', 'save'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'payment', 'accumulation', 'charity', 'finduser', 'validate', 'save'
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionFirstData()
    {
        $url = "https://4serviceru.shopmetrics.com/custom/4serviceru/ShopperBonusHistoricalDataExport.asp";
//        $url = "https://4serviceru.shopmetrics.com/custom/4serviceru/ShopperBonusDataExport.asp";
        $key_name = 'ss';
        $key_value = '7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C';
        $post_data = json_encode([$key_name => $key_value]);
        $post_string = $key_name . '=' . $key_value;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "date=2019-12-20&ss=7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic Og=="
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $XMLresponse = new SimpleXMLElement($response);

        return $this->render('first-data', compact('XMLresponse'));
    }

    public function actionBonusData()
    {
        $url = "https://4serviceru.shopmetrics.com/custom/4serviceru/ShopperBonusDataExport.asp";
        $key_name = 'ss';
        $key_value = '7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C';
        $post_data = json_encode([$key_name => $key_value]);
        $post_string = $key_name . '=' . $key_value;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "date=2019-12-20&ss=7CF61BD1A6F74C9A99EE888405D5C55785B5887BDBA546DD8BFC1A6D4620D90C",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic Og=="
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $XMLresponse = new SimpleXMLElement($response);

        return $this->render('bonus-data', compact('XMLresponse'));
    }
}
