<?php

namespace backend\controllers;

use common\models\AdminDone;
use common\models\Api;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\User;
use common\models\Visit;
use common\models\Charity;
use Yii;
use common\models\VisitSearch;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Html;
use Exception;

//set_time_limit(0);
//ini_set('memory_limit', '-1');

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'payment', 'payed', 'accumulation', 'charity', 'finduser', 'validate', 'save', 'get-data', 'get-api-data', 'count', 'cancel-charity'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'get-api-data'],
                        'roles' => ['?', 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'payment', 'payed', 'accumulation', 'charity', 'finduser', 'validate', 'get-data', 'save', 'get-api-data', 'count', 'cancel-charity', 'get-api-data'
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if ($exception->statusCode == 404)
                return $this->render('error404', ['exception' => $exception]);
            else
                return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionCount()
    {
        $this->layout = 'admin-main';

        $visits = Visit::find()->all();
        $users = User::find()->all();

        foreach ($visits as $item) {
            $date = date('Y-m-d', strtotime($item->visit_date));
            $results[$date][] = $item;
        }

        foreach ($results as $date => $item) {
            @$counts[$date] = count($item);
        }

        return $this->render('count', [
            'visits' => $visits,
            'users' => $users,
            'counts' => $counts
        ]);
    }

    public function dateDiff($date1, $date2)
    {
        return strtotime($date2) >= strtotime($date1) ? round((strtotime($date2) - strtotime($date1)) / (60 * 60 * 24), 0) : 0;
    }

    public function actionBonuseCalculation()
    {
        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 300);

        $visits = Visit::find()
            ->where(['is_payed' => ['Not_payed', 'Waiting_pay']])
            ->andWhere(['>=', 'visit_date', '2021-05-01'])
            ->select(['id', 'anketa_id', 'pay_rate', 'bonuse', 'penalty', 'visit_date', 'total_amount', 'accumulated_bonuse', 'last_modified', 'accumulation_start_date', 'is_payed'])
            ->all();

        foreach ($visits as $item) {
            $visitMonth = date('n', strtotime($item->visit_date));
            $visitYear = date('y', strtotime($item->visit_date));
            $startBonuse = date('Y-m-d', mktime(0, 0, 0, $visitMonth + 1, 21, $visitYear));
            $item->total_amount = $item->pay_rate + $item->bonuse + $item->penalty;
            $item->accumulation_start_date = $startBonuse;

            if (time() > strtotime($item->accumulation_start_date)) {
                $bonuseTerm = $this->dateDiff($startBonuse, date('Y-m-d'));
                $bonusePersent = $bonuseTerm > 365 ? 0.12 : ($bonuseTerm > 180 ? 0.11 : 0.10);
                $accumulatedBonuse = round($bonuseTerm * $bonusePersent * $item->total_amount / 365, 2);
                $item->accumulated_bonuse = $accumulatedBonuse > 0 ? $accumulatedBonuse : 0;
            } else {
                $item->accumulated_bonuse = 0;
            }

            $nextMonthDate = date('Y-m-d', strtotime($item->visit_date . 'first day of next month'));
            $approvedToWithdrawalStatusDate = date('Y-m-d', strtotime($nextMonthDate . '+20 days'));
            if ($item->is_prompt_payment || ($approvedToWithdrawalStatusDate <= date('Y-m-d', time()) && date('Y-m-d', time()) > $item->accumulation_start_date)) {
                $item->visit_status = 'Approved_to_withdrawal';
            }

            $item->save(false);
        }

        return count($visits);
    }

    public function actionGetApiData()
    {
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 300);

        $todayDay = (int)date('d', time());

        if($todayDay >= 11) {
            $startDate  = date('Y-m-d', mktime(0, 0, 0, (int)date('m', time()), 1, (int)date('Y', time())));
            $endDate    = date('Y-m-d', time());
        } else {
            $startDate  = date('Y-m-d', strtotime(date('Y-m-d') . 'first day of last month'));
            $endDate    = date('Y-m-d', time());
        }
//                $startDate = '2021-06-02';
//                $endDate = '2021-06-02';
        $step       = '+1 day';
        $dates      = array();
        $current    = strtotime($startDate);
        $last       = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }
//        $this->debug($dates);

        foreach ($dates as $date) {
            // $visitDataApi      - info from visit table
            // $historicalDataApi - info with user data
            $visitData = ArrayHelper::index(Api::getVisitData($date)['items']['item'], 'InstanceID');
            $historicalData = ArrayHelper::index(Api::getHistoricalData($date)['items']['item'], 'SurveyInstanceID');

            // get all anketas numbers from $visitDataApi
//            $visitData = ArrayHelper::index($visitData, 'InstanceID');
//            $historicalData = ArrayHelper::index($historicalData, 'SurveyInstanceID');

//            unset($historicalDataApi);
//            unset($visitDataApi);

            // filterring $historicalData - массив с фильтрованными историческими анкетами
            foreach ($historicalData as $instance => $item) {
                if ($item['IsOkForPayroll']) {
                    if (array_intersect(explode(' ', strtolower($item['ClientName'])), [
                        '_CERTIFICATIONS',
                        '_certifications',
                        '!пустой',
                        '!delete',
                        '_extended profile',
                        'demo client',
                        '????на замену',
                        '_appeals',
                        '_repository',
                        'demo',
                        'demo client',
                        'demo тайный соискатель',
                        'demo_банк',
                    ])) {
                        unset($item[$instance]);
                    }
                }
            }

            echo('Получено user анкет ' . $date . ' : ' . count($visitData) . '<br>');
            echo('Получено historical анкет ' . $date . ' : ' . count($historicalData) . '<br>');

            // parsing exist array to database
            foreach ($visitData as $instanceId => $item) {
                // выбираем visit из БД или создаем новую запись
                $visit = Visit::find()->where(['anketa_id' => $instanceId])->one();
                if ($visit == null) {
                    $visit = new Visit();
                }

                if (isset($historicalData[$instanceId])) {
                    $visit->anketa_id       = $instanceId;
                    $visit->user_id         = $historicalData[$instanceId]['SecurityObjectUserID'];
                    $visit->visit_date      = date('Y-m-d H:i:s', strtotime($item['Date']));
                    $visit->survey_title    = $item['SurveyFormName'];
                    $visit->client_name     = $item['ClientName'];
                    $visit->location_address = isset($item['LocationAddress']) && is_array($item['LocationAddress']) ? implode(',', $item['LocationAddress']) : $item['LocationAddress'] ?? null;
                    $visit->location_city   = isset($item['LocationCity']) && is_array($item['LocationCity']) ? implode(',', $item['LocationCity']) : $item['LocationCity'] ?? null;
                    $visit->location_state_region = isset($item['LocationStateRegion']) && is_array($item['LocationStateRegion']) ? implode($item['LocationStateRegion']) : $item['LocationStateRegion'] ?? null;
                    $visit->location_name   = isset($historicalData[$instanceId]['LocationName']) && is_array($historicalData[$instanceId]['LocationName']) ? implode(',', $historicalData[$instanceId]['LocationName']) : $historicalData[$instanceId]['LocationName'] ?? null;
                    $visit->project         = $item['ClientName'] . "\n" . $item['SurveyFormName'];
                    $visit->payroll_currency = $historicalData[$instanceId]['PayrollCurrency'] ?? null;
                    $visit->payroll_items   = json_encode($item['PayrollItems'] ?? null);
                    $visit->pay_rate = $visit->pay_rate ?? $item['PayRate'] ?? null;

                    if (!empty($item['SurveyStatus'])) {
                        if (preg_match("/Completed/", $item['SurveyStatus'])) {
                            $visit->visit_status = 'Completed';
                        } else if (preg_match("/Validation/", $item['SurveyStatus'])) {
                            $visit->visit_status = 'Validation';
                        } else {
                            $visit->visit_status = null;
                        }
                    }

                    if (!empty($item['WorkFlowStatus'])) {
                        if (preg_match("/Completed/", $item['WorkFlowStatus'])) {
                            $visit->visit_status = 'Completed';
                        } else if (preg_match("/Validation/", $item['WorkFlowStatus'])) {
                            $visit->visit_status = 'Validation';
                        } else {
                            $visit->visit_status = null;
                        }
                    }

                    // calculating bonuse and penalties
                    $bonuse = $penalty = 0;
                    if (isset($item['PayrollItems']['PayrollItem'])) {
                        if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                            if (isset($oneItem['Amount'])) {
                                if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                    $visit->is_prompt_payment = true;
                                    $visit->visit_status = 'Approved_to_withdrawal';
                                } else {
                                    // calculate bonuses and penalties
                                    if ($oneItem['Amount'] >= 0) {
                                        @$bonuse += (int)$oneItem['Amount'];
                                    } else if ($oneItem['Amount'] < 0) {
                                        @$penalty += (int)$oneItem['Amount'];
                                    }
                                }
                            }
                        } else {
                            foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                    if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                        $visit->is_prompt_payment = true;
                                        $visit->visit_status = 'Approved_to_withdrawal';
                                    } else {
                                        // calculate bonuses and penalties
                                        if ($payrollItem['Amount'] >= 0) {
                                            @$bonuse += (int)$payrollItem['Amount'];
                                        } else if ($payrollItem['Amount'] < 0) {
                                            @$penalty += (int)$payrollItem['Amount'];
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $visit->bonuse = $bonuse ?? $historicalData[$instanceId]['PrecalcPayrollItemsSum'] ?? 0;
                    $visit->penalty = $penalty ?? 0;
                    $visit->accumulated_bonuse = 0;
                    $visit->total_amount = $visit->pay_rate + $visit->bonuse + $visit->penalty;
                    $visit->is_payed = $visit->is_payed ?? 'Not_payed';
                    $visit->payment_status = null;
                    $visit->deposit_term = 0;
                    $visit->deposit_left = 0;
                    $visit->work_flow_status = $historicalData[$instanceId]['WorkFlowStatus'] ?? null;
                    $visit->survey_status = $historicalData[$instanceId]['SurveyStatusName'] ?? null;
                    $visit->management = null;
                    $visit->payment_method = null;
                    $visit->last_modified = $visit->last_modified ?? null;
                    $visit->is_payment_status_changed = $visit->is_payment_status_changed ?? 0;
                    $visit->first_import_date = $visit->first_import_date ?? date('Y-m-d H:i:s', time());

                    $visit->save(false);

                    // выбираем user из БД или создаем новую запись
                    $user = User::find()
                        ->where(['user_id' => $historicalData[$instanceId]['SecurityObjectUserID']])
                        ->orWhere(['username' => $historicalData[$instanceId]['Login']])
                        ->one();

                    if ($user == null) {
                        $user = new User();
                    }

                    $user->user_id = $historicalData[$instanceId]['SecurityObjectUserID'];
                    $user->username = trim($historicalData[$instanceId]['Login']);
                    $user->email = trim($historicalData[$instanceId]['Email']);
                    $user->name = trim($historicalData[$instanceId]['Name']);
                    $user->family_name = $historicalData[$instanceId]['FamilyName'];
                    $user->country = $historicalData[$instanceId]['Country'];

                    $user->role = 'user';
                    $user->status = 10;
                    $user->password_hash = '$2y$13$Sne7BSaOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLweT9LUpaKDkbWyta';
                    $user->bonuse_percent = 10;

                    $user->save(false);
                }
            }

            unset($visitData);
            unset($historicalData);
        }

        $this->actionBonuseCalculation();

        exit(0);
    }

    public function actionIndex($sort = 'visit_date', $order = 'DESC')
    {
        $this->layout = 'admin-main';
        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        // todo переписать
        //форма комемнтария админа
        $commentModel = new AdminDone();
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {
            $actions = Yii::$app->request->post('actions');
            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['anketa_id' => $action[0]])
                    ->one();
                //запись в таблицу БД
                $visit->visit_status = $action[1];
                $visit->payment_status = 'Accumulation';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }
            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => $userSecurityId])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $visit = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->orderBy("$sort $order");
        $countQuery = clone($visit);
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $visit = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->orderBy("$sort $order")
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['user_id', 'username'])
            ->asArray()
            ->all(), 'user_id');

        return $this->render('index', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users
        ]);
    }

    public function actionCancelCharity()
    {
        if (Yii::$app->request->isAjax) {
            $model = Visit::find()
                ->where(['id' => $_POST['id']])
                ->one();
            //получение измененных админом данных
//        $request = \Yii::$app->getRequest();
//
//        if ($request->isPost) {
//            $id = Yii::$app->request->post('id');
//            $model = Visit::find()->where(['anketa_id' => $id])->one();
            $model->payment_status = null;
            $model->is_payed = 'Not_payed';
            $model->last_modified = date('Y-m-d H:i:s', time());
            $model->save(false);

            return true;
        }
        return false;
    }

    public function actionPayed($sort = 'visit_date', $order = 'DESC', $filter = ['Payed', 'Charity'])
    {
        $this->layout = 'admin-main';

        if ($filter == 'Благотворительность') {
            $filter = 'Charity';
        }

        if ($filter == 'Оплачен') {
            $filter = 'Payed';
        }
        //форма комемнтария админа
        $commentModel = new AdminDone();

        //получение измененных админом данных с фронта
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {

            $actions = Yii::$app->request->post('actions');

            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['id' => $action[0]])
                    ->one();

                //запись в таблицу БД
                $visit->is_payed = 'Payed';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }

            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => Yii::$app->session->get('target-user-id')])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $paymentStatus = array_flip(Yii::$app->params['paymentStatus']);
        if (Yii::$app->request->get('filter')) {
            $filter = $paymentStatus[Yii::$app->request->get('filter')];
        }

        $query = Visit::find()
            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
            ->andWhere(['is_payed' => $filter])
            ->orderBy("$sort $order");

        $countQuery = clone($query);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $visit = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['id', 'username'])
            ->asArray()
            ->all(), 'id');

        return $this->render('payed', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'filter' => $filter,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users
        ]);
    }

    public function actionPayment($sort = 'visit_date', $order = 'DESC', $filter = ['Rejected', 'Payed', 'Waiting_pay', 'Sent_to_pay', 'Charity'])
    {
        $this->layout = 'admin-main';

        //форма комемнтария админа
        $commentModel = new AdminDone();

        //получение измененных админом данных с фронта
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {

            $actions = Yii::$app->request->post('actions');

            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['id' => $action[0]])
                    ->one();

                //запись в таблицу БД
                $visit->is_payed = 'Waiting_pay';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }

            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => Yii::$app->session->get('target-user-id')])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $paymentStatus = array_flip(Yii::$app->params['paymentStatus']);
        if (Yii::$app->request->get('filter')) {
            $filter = $paymentStatus[Yii::$app->request->get('filter')];
        }

        $query = Visit::find()
            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
            ->andWhere(['is_payed' => $filter])
            ->orderBy("$sort $order");

        $countQuery = clone($query);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $visit = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {

            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();

            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['id', 'username'])
            ->asArray()
            ->all(), 'id');

        return $this->render('payment', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'filter' => $filter,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users

        ]);
    }

    public function actionAccumulation($sort = 'visit_date', $order = 'DESC')
    {
        $this->layout = 'admin-main';

        $visit = Visit::find()
            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
            ->andWhere(['payment_status' => 'Accumulation'])
            ->andWhere(['visit_status' => 'Completed'])
            ->orderBy("$sort $order");

        $countQuery = clone($visit);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $visit = $countQuery
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        var_dump(Yii::$app->session->get('targetUserId'));
//        die;
        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {

            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();

            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        return $this->render('accumulation', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
        ]);
    }

    public function actionCharity()
    {
        $this->layout = 'admin-main';

        $userSecurityId = Yii::$app->session->get('target-user-id');
        $charitiesList = Charity::find()->all();
        $charities_count = Visit::charitiesCount($userSecurityId);
        $charities_amount = Visit::charitiesAmount($userSecurityId);
        $charities_per_month = Visit::charitiesPerMonth($userSecurityId);

        $charities_to_view = Visit::getCharitiesToView($charitiesList, $charities_count, $charities_amount);

        $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
        if (!empty($charitiesDone)) {
            foreach ($charitiesDone as $item) {
                if ($charities_count >= $item->numbers && $item->numbers != 0) {
                    $charities_to_view[1] = $item;
                    $item->done = 1;
                }

                if ($charities_amount >= $item->amount && $item->amount != 0) {
                    $charities_to_view[2] = $item;
                    $item->done = 1;
                }
            }
        }

        $charities_report = Visit::find()
            ->where(['payment_status' => 'Charity'])
            ->all();

        foreach ($charities_report as $item) {
            @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['total_amount'] += $item->total_amount;

            if (!isset($charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'] = [];
            }
            if (!in_array($item->user_id, $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'][] += $item->user_id;
            }
        }

        $charityTotal = Visit::charitiesAmount($userSecurityId);

        $month = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ];

        return $this->render('charity', compact('month', 'charityTotal', 'charitiesDone', 'charitiesList'));
    }

    public function actionFinduser()
    {
        if ($login = Yii::$app->request->get('login')) {
            if (!is_null($userData = User::find()
                ->where(['username' => $login])
                ->one())) {
                Yii::$app->session->set('target-user-id', $userData->user_id);
                Yii::$app->session->set('target-user-login', $userData->username);
                Yii::$app->session->set('target-user-accumulated-bonuse', $userData->accumulated_bonuse);
                Yii::$app->session->set('target-user-bonuce-percent', $userData->bonuse_percent);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionLogin()
    {
        $this->layout = 'login';

        $loginWithEmail = Yii::$app->params['loginWithEmail'];

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = $loginWithEmail ? new LoginForm(['scenario' => 'loginWithEmail']) : new LoginForm();

        if ($model->load(Yii::$app->request->post())) {

//            echo '<pre>';
//            var_dump($model->attributes);
//            die;

//            if ($model->username == 'admin' || $model->email = 'budonnyi@gmail.com') {
//                Yii::$app->session->destroy();
//                return $this->redirect('/admin/login');
//            }

            $model->login();

            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $this->layout = 'admin-main';

        $model = Visit::find()
            ->where(['id' => $id])
            ->one();

        if ($model->load(Yii::$app->request->post())) {

            $model->payment_status = 'Накопление';
            $model->last_modified = date('d.m.Y');
            $model->save(false);

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

//сохранение комментариев админа
    public function actionSave()
    {
        //форма комемнтария админа
        $commentModel = new AdminDone();

        if ($commentModel->load(\Yii::$app->request->post()) && $commentModel->save()) {
            return true;
        } else {
            return false;
        }

        return $this->render('index', compact('commentModel'));
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
