<?php

namespace backend\controllers;

use common\models\AdminDone;
use common\models\Api;
use common\models\Bonuse;
use common\models\BonuseSearch;
use common\models\BonusesToUser;
use common\models\LoginForm;
use common\models\UploadForm;
use common\models\User;
use common\models\UserSearch;
use common\models\Visit;
use common\models\Charity;
use common\models\VisitStatus;
use console\services\CronService;
use Yii;
use common\models\VisitSearch;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Html;
use Exception;

//set_time_limit(0);
//ini_set('memory_limit', '-1');

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'mass', 'index', 'payment', 'payed', 'accumulation', 'charity', 'bonus', 'finduser',
                    'validate', 'save', 'get-data', 'get-api-data', 'count', 'cancel-charity'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'get-api-data'],
                        'roles' => ['?', 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'logout', 'index', 'mass', 'payment', 'payed', 'accumulation', 'bonus', 'charity', 'finduser', 'validate',
                            'get-data', 'save', 'get-api-data', 'count', 'cancel-charity', 'get-api-data'
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function debug($str)
    {
        \yii\helpers\VarDumper::dump($str, 100, true);
        die('EOF');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin()
    {
        $this->layout = 'login';

        $loginWithEmail = Yii::$app->params['loginWithEmail'];

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = $loginWithEmail ? new LoginForm(['scenario' => 'loginWithEmail']) : new LoginForm();

        if ($model->load(Yii::$app->request->post())) {

//            echo '<pre>';
//            var_dump($model->attributes);
//            die;

//            if ($model->username == 'admin' || $model->email = 'budonnyi@gmail.com') {
//                Yii::$app->session->destroy();
//                return $this->redirect('/admin/login');
//            }

            $model->login();

            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if ($exception->statusCode == 404)
                return $this->render('error404', ['exception' => $exception]);
            else
                return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionCount()
    {
        $this->layout = 'admin-main';

        $visits = Visit::find()->all();
        $users = User::find()->all();

        foreach ($visits as $item) {
            $date = date('Y-m-d', strtotime($item->visit_date));
            $results[$date][] = $item;
        }

        foreach ($results as $date => $item) {
            @$counts[$date] = count($item);
        }

        return $this->render('count', [
            'visits' => $visits,
            'users' => $users,
            'counts' => $counts
        ]);
    }

    public function dateDiff($date1, $date2)
    {
        return strtotime($date2) >= strtotime($date1) ? round((strtotime($date2) - strtotime($date1)) / (60 * 60 * 24), 0) : 0;
    }


//    public function actionClearBonuses()
//    {
//        $startDate = '2020-08-01';
//        $endDate = '2021-05-31';
//
//        Visit::updateAll(['accumulation_start_date' => null, 'accumulated_bonuse' => 0],
//            ['and',
//                ['>=', 'visit_date', $startDate . ' 00:00:00'],
//                ['<=', 'visit_date', $endDate . ' 23:59:59']
//            ]);
//
//        die('11');
//    }
//    public function actionRefactorBonuses()
//    {
//        $counter = 0;
//        $bonuses = BonusesToUser::find()->all();
//        foreach ($bonuses as $item) {
//            if (date('Y-m-d', strtotime($item->visit->visit_date)) < '2021-06-01') {
//                $item->delete();
//                $counter++;
//            }
//        }
//
//        echo 'Counter = ' . $counter;
//        die('EOO');
//    }

    public function actionAdvancedPayedBonuses()
    {
        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 2400);

        $startDate = date('Y-m-d', strtotime(date('Y-m-d') . 'first day of last month'));
        $endDate = date('Y-m-d', time());

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        foreach ($dates as $date) {
            $visits = Visit::find()
                ->where(['is_payed' => 'advance_payment'])
                ->andWhere(['>=', 'visit_date', $date . ' 00:00:00'])
                ->andWhere(['<=', 'visit_date', $date . ' 23:59:59'])
                ->select(['id', 'user_id', 'anketa_id', 'pay_rate', 'bonuse', 'penalty', 'visit_date', 'total_amount',
                    'accumulated_bonuse', 'last_modified', 'accumulation_start_date', 'is_payed',
                    'is_prompt_payment', 'wait_bonuse', 'wait_bonuse_sm', 'sent_to_pay'])
                ->all();

//            echo $date . ' => ' . count($visits) . '<br>';

            foreach ($visits as $item) {
                $item->accumulated_bonuse = 0;
                $item->save(false);
            }

            unset($visits);
        }

        return true;
    }

    public function actionBonuseCalculation()
    {
        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 2400);

//        $startDate = '2020-01-01';
        $startDate = '2021-06-01';
        $endDate = date('Y-m-d');

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        $startTime = time();
        $counter = 0;
        foreach ($dates as $date) {
            try {
                $visits = Visit::find()
                    ->where(['DATE(visit_date)' => $date])
                    ->andWhere(['is_payed' => 'Not_payed'])
                    ->indexBy('anketa_id')
                    ->all();

                foreach ($visits as $item) {

                    $visitMonth = date('n', strtotime($item->visit_date));
                    $visitYear = date('Y', strtotime($item->visit_date));

                    // add condition for Elena's letter
                    if (date('Y-m-d', strtotime($item->visit_date)) >= '2021-06-01') {
                        $item->accumulation_start_date = date('Y-m-d', mktime(0, 0, 0, $visitMonth + 1, 21, $visitYear));
                    } else {
                        $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
                    }

                    // crutch for Ihor
                    if ($item->is_prompt_payment && $item->accumulated_bonuse > 0) {
                        $item->is_prompt_payment = null;
                    }

                    if (date('Y-m-d') >= $item->accumulation_start_date && $item->accumulation_start_date != null) {
                        $bonuseTerm = $this->dateDiff($item->accumulation_start_date, date('Y-m-d'));
                        $bonusePersent = $bonuseTerm > 365 ? 0.11 : ($bonuseTerm > 180 ? 0.10 : 0.09);
                        $accumulatedBonuse = round($bonuseTerm * $bonusePersent * $item->total_amount / 365, 2);
                        $item->accumulated_bonuse = $accumulatedBonuse > 0 ? $accumulatedBonuse : 0;
                        if ($item->visit_status != VisitStatus::WAITING_NOT_BONUS) {
                            if ($item->wait_bonuse == $item->wait_bonuse_sm) {
                                $item->visit_status = 'Approved_to_withdrawal';
                            } else {
                                $item->visit_status = 'Rejected';
                            }
                        }

                    } else {
                        $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
                    }

                    // задача от 06.2021 не начислять бонусы для сотрудников 4сервис
                    $emailParts = explode('@', $item->users->email);
                    if ($emailParts[1] === '4service-group.com') {
//                    $item->accumulation_start_date = null;
                        $item->accumulated_bonuse = 0;
//                    $item->save(false);
//                    continue;
                    }

                    $item->save(false);

                    unset($item);

                    $counter++;
                }

                unset($visits);

            } catch (\Exception $e) {
                Yii::error([
                    'name' => 'bonuseCalculation',
                    'data' => ['date' => $date],
                    'error' => $e->getMessage(),
                ], 'api');
            }

        }

        $endTime = time();

        echo 'counts = ' . $counter . '<br>';
        echo 'time = ' . ($endTime - $startTime) . '<br>';
        return true;
    }

//    public function actionRefactor()
//    {
//        $counter = 0;
//        $bonuses = BonusesToUser::find()->all();
//        foreach ($bonuses as $item) {
//            $visit = Visit::findOne(['anketa_id' => $item->anketa_id]);
//            if ($item->amount != $visit->accumulated_bonuse) {
//                $item->amount = $visit->accumulated_bonuse;
//                $item->save(false);
//                $counter++;
//            }
//        }
//        echo 'ounts = ' . $counter;
//        exit(0);
//    }

    public function actionNewBonuseCalculation()
    {
        ini_set("memory_limit", "1024M");
        ini_set('max_execution_time', 2400);

//        $startDate = '2020-01-01';
        $startDate = '2021-06-01';
        $endDate = date('Y-m-d');

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        $startTime = time();
        $counter = 0;
        foreach ($dates as $date) {
            $visits = Visit::find()
                ->where(['is_payed' => 'Waiting_pay'])
                ->andWhere(['>=', 'visit_date', $date . ' 00:00:00'])
                ->andWhere(['<=', 'visit_date', $date . ' 23:59:59'])
                ->select(['id', 'user_id', 'anketa_id', 'pay_rate', 'bonuse', 'penalty', 'visit_date', 'total_amount',
                    'accumulated_bonuse', 'last_modified', 'accumulation_start_date', 'is_payed',
                    'is_prompt_payment', 'wait_bonuse', 'wait_bonuse_sm', 'sent_to_pay'])
                ->all();

            if ($visits == null) continue;

            foreach ($visits as $item) {

                $visitMonth = date('n', strtotime($item->visit_date));
                $visitYear = date('Y', strtotime($item->visit_date));

                $item->accumulation_start_date = null;
                $item->accumulated_bonuse = 0;

                // add condition for Elena's letter
                if (date('Y-m-d', strtotime($item->visit_date)) >= '2021-06-01') {
                    $item->accumulation_start_date = date('Y-m-d', mktime(0, 0, 0, $visitMonth + 1, 21, $visitYear));
                }

                if ($item->is_prompt_payment && $item->accumulated_bonuse > 0) {
                    $item->is_prompt_payment = null;
                }

                if (!empty($item->sent_to_pay) && date('Y-m-d') >= $item->accumulation_start_date && $item->accumulation_start_date != null) {
                    if ($item->wait_bonuse == $item->wait_bonuse_sm) {
                        $bonuseTerm = $this->dateDiff($item->accumulation_start_date, date('Y-m-d', strtotime($item->sent_to_pay)));
                        $bonusePersent = $bonuseTerm > 365 ? 0.11 : ($bonuseTerm > 180 ? 0.10 : 0.09);
                        $accumulatedBonuse = round($bonuseTerm * $bonusePersent * $item->total_amount / 365, 2);
                        $item->accumulated_bonuse = $accumulatedBonuse > 0 ? $accumulatedBonuse : 0;
                        $item->visit_status = 'Approved_to_withdrawal';
                    } else {
                        $item->visit_status = 'Rejected';
                    }

                } else {
                    $item->accumulation_start_date = null;
                    $item->accumulated_bonuse = 0;
                }
                // задача от 06.2021 не начислять бонусы для сотрудников 4сервис
                $emailParts = explode('@', $item->users->email);
                if ($emailParts[1] === '4service-group.com') {
                    $item->accumulated_bonuse = 0;
                }

                $item->save(false);

                $counter++;
            }

            unset($visits);
        }

        $endTime = time();

        echo 'counts = ' . $counter . '<br>';
        echo 'time = ' . ($endTime - $startTime) . '<br>';
        return true;
    }

    public function actionGetApiData()
    {
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 2400);

        $users = [];

        $startDate = date('Y-m-d', strtotime(date('Y-m-d') . 'first day of last month'));
        $endDate = date('Y-m-d', time());

        $startDate = '2021-10-19';
        $endDate = '2021-10-19';

        $step = '+1 day';
        $dates = array();
        $current = strtotime($startDate);
        $last = strtotime($endDate);

        // get datas for query to API
        while ($current <= $last) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime($step, $current);
        }

        $startTime = time();

        foreach ($dates as $date) {
            try {
                // $visitDataApi      - info from visit table
                // $historicalDataApi - info with user data
                $visitData = ArrayHelper::index(Api::getVisitData($date)['items']['item'], 'InstanceID');
                $historicalData = ArrayHelper::index(Api::getHistoricalData($date)['items']['item'], 'SurveyInstanceID');

                // parsing exist array to database
                foreach ($visitData as $instanceId => $item) {
                    if (isset($historicalData[$instanceId])) {
                        // убираем лишние анкеты
                        if (isset($historicalData[$instanceId]['IsOkForPayroll'])
                            && isset($historicalData[$instanceId]['ClientName'])
                            && $historicalData[$instanceId]['IsOkForPayroll']) {

                            if (array_intersect(explode(' ', strtolower($historicalData[$instanceId]['ClientName'])), [
                                '_CERTIFICATIONS',
                                '_certifications',
                                '!пустой',
                                '!delete',
                                '_extended profile',
                                'demo client',
                                '????на замену',
                                '_appeals',
                                '_repository',
                                'demo',
                                'demo client',
                                'demo тайный соискатель',
                                'demo_банк',
                            ])) {
                                continue;
                            }
                        }

                        if (!in_array($instanceId, [9158842]))
                            continue;

//                        $isPromptPaymentWaitingNotBonus = false;
////                        if (time() > strtotime('+50 days', strtotime($visit->visit_date))) {
//                        if (isset($item['PayrollItems']['PayrollItem'])) {
//                            if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
//                                if (isset($oneItem['Amount'])) {
//                                    if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
//                                        $isPromptPaymentWaitingNotBonus = true;
//                                    }
//                                }
//                            } else {
//                                foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
//                                    if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
//                                        if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
//                                            $isPromptPaymentWaitingNotBonus = true;
//                                        }
//                                    }
//                                }
//                            }
//                        }
////                        }
//
//                        if ($isPromptPaymentWaitingNotBonus && $visit = Visit::find()->where(['anketa_id' => $instanceId])->one()) {
//                            $visit->is_prompt_payment = true;
//                            $visit->visit_status = VisitStatus::WAITING_NOT_BONUS;
//                            $visit->save(false);
//                        }
//                        continue;

                        $visit = Visit::find()->where(['anketa_id' => $instanceId])->one();
                        if ($visit == null) {
                            $visit = new Visit();
                        }

                        $visit->anketa_id = $instanceId;
                        $visit->user_id = $historicalData[$instanceId]['SecurityObjectUserID'];
                        $visit->visit_date = date('Y-m-d H:i:s', strtotime($item['Date']));
                        $visit->survey_title = $item['SurveyFormName'];
                        $visit->client_name = $item['ClientName'];
                        $visit->location_address = isset($item['LocationAddress']) && is_array($item['LocationAddress']) ? implode(',', $item['LocationAddress']) : $item['LocationAddress'] ?? null;
                        $visit->location_city = isset($item['LocationCity']) && is_array($item['LocationCity']) ? implode(',', $item['LocationCity']) : $item['LocationCity'] ?? null;
                        $visit->location_state_region = isset($item['LocationStateRegion']) && is_array($item['LocationStateRegion']) ? implode($item['LocationStateRegion']) : $item['LocationStateRegion'] ?? null;
                        $visit->location_name = isset($historicalData[$instanceId]['LocationName']) && is_array($historicalData[$instanceId]['LocationName']) ? implode(',', $historicalData[$instanceId]['LocationName']) : $historicalData[$instanceId]['LocationName'] ?? null;
                        $visit->project = $item['ClientName'] . "\n" . $item['SurveyFormName'];
                        $visit->payroll_currency = $historicalData[$instanceId]['PayrollCurrency'] ?? null;
                        $visit->payroll_items = json_encode($item['PayrollItems'] ?? null);

                        // до 11 числа следующего от визита месяца
                        $visitMonth = (int)date('m', strtotime($visit->visit_date));
                        if (time() < mktime(0, 0, 0, $visitMonth + 1, 11, (int)date('Y', time()))) {
                            $visit->pay_rate = $item['PayRate'] ?? $visit->pay_rate ?? null;
                            if (!empty($item['SurveyStatus'])) {
                                if (preg_match("/Completed/", $item['SurveyStatus'])) {
                                    $visit->visit_status = 'Completed';
                                } else if (preg_match("/Validation/", $item['SurveyStatus'])) {
                                    $visit->visit_status = 'Validation';
                                } else {
                                    $visit->visit_status = null;
                                }
                            }
                            if (!empty($item['WorkFlowStatus'])) {
                                if (preg_match("/Completed/", $item['WorkFlowStatus'])) {
                                    $visit->visit_status = 'Completed';
                                } else if (preg_match("/Validation/", $item['WorkFlowStatus'])) {
                                    $visit->visit_status = 'Validation';
                                } else {
                                    $visit->visit_status = null;
                                }
                            }

                            // calculating bonuse and penalties
                            $bonuse = $penalty = 0;
                            $wait_bonuse_sm = null;
                            if (isset($item['PayrollItems']['PayrollItem'])) {
                                if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                    if (isset($oneItem['Amount'])) {
                                        if ($oneItem['Amount'] >= 0) {
                                            @$bonuse += $oneItem['Amount'];
                                        } else if ($oneItem['Amount'] < 0) {
                                            @$penalty += $oneItem['Amount'];
                                        }
                                        if ($oneItem['Name'] == 'Доплата накоплений Шопербонуса') {
                                            @$wait_bonuse_sm = $oneItem['Amount'];
                                        }
                                    }
                                } else {
                                    foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                        if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                            if ($payrollItem['Amount'] >= 0) {
                                                @$bonuse += $payrollItem['Amount'];
                                            } else if ($payrollItem['Amount'] < 0) {
                                                @$penalty += $payrollItem['Amount'];
                                            }

                                            if ($payrollItem['Name'] == 'Доплата накоплений Шопербонуса') {
                                                @$wait_bonuse_sm = $payrollItem['Amount'];
                                            }
                                        }
                                    }
                                }
                            }
                            $visit->wait_bonuse_sm = $wait_bonuse_sm ?? null; /*$historicalData[$instanceId]['PrecalcPayrollItemsSum'] ??*/
                            $visit->bonuse = $bonuse ?? 0;/*$historicalData[$instanceId]['PrecalcPayrollItemsSum'] ??*/

                            $visit->penalty = $penalty ?? 0;
                            $visit->accumulated_bonuse = $visit->accumulated_bonuse ?? 0;
                            $visit->total_amount = $visit->pay_rate + $visit->bonuse + $visit->penalty;
                            $visit->work_flow_status = $historicalData[$instanceId]['WorkFlowStatus'] ?? null;
                            $visit->survey_status = $historicalData[$instanceId]['SurveyStatusName'] ?? null;
                            $visit->payment_status = null;
                            $visit->deposit_term = 0;
                            $visit->deposit_left = 0;
                            $visit->management = null;
                            $visit->payment_method = null;
                        } else {
                            if ($visit->visit_status == 'Validation' || $visit->visit_status == null) {
                                $visit->visit_status = 'Completed';
                            }
                        }

                        // wait_bonus_sm
                        if (isset($item['PayrollItems']['PayrollItem'])) {
                            if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                if (isset($oneItem['Amount'])) {
                                    if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                        $visit->is_prompt_payment = true;
                                        $visit->visit_status = 'Approved_to_withdrawal';
                                        $visit->accumulated_bonuse = 0;
                                    }
                                }
                            } else {
                                foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                    if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                        if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                            $visit->is_prompt_payment = true;
                                            $visit->visit_status = 'Approved_to_withdrawal';
                                            $visit->accumulated_bonuse = 0;
                                        }
                                    }
                                }
                            }
                        }

                        // до 60 дней от даты визита
                        if (time() < strtotime('+60 days', strtotime($visit->visit_date))) {
                            if (isset($item['PayrollItems']['PayrollItem'])) {
                                if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                    if (isset($oneItem['Amount'])) {
                                        if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                            $visit->is_prompt_payment = true;
                                            $visit->visit_status = 'Approved_to_withdrawal';
                                            $visit->accumulated_bonuse = 0;
                                        }
                                    }
                                } else {
                                    foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                        if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                            if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('ГИПЕРСРОЧНАЯ ОПЛАТА')) {
                                                $visit->is_prompt_payment = true;
                                                $visit->visit_status = 'Approved_to_withdrawal';
                                                $visit->accumulated_bonuse = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $isPromptPaymentWaitingNotBonus = false;
//                        if (time() > strtotime('+50 days', strtotime($visit->visit_date))) {
                        if (isset($item['PayrollItems']['PayrollItem'])) {
                            if (isset($item['PayrollItems']['PayrollItem']['Name']) && $oneItem = $item['PayrollItems']['PayrollItem']) {
                                if (isset($oneItem['Amount'])) {
                                    if (mb_strtoupper($oneItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                        $isPromptPaymentWaitingNotBonus = true;
                                    }
                                }
                            } else {
                                foreach ($item['PayrollItems']['PayrollItem'] as $payrollItem) {
                                    if (isset($payrollItem['Name']) || isset($payrollItem['Amount'])) {
                                        if (mb_strtoupper($payrollItem['Name']) === mb_strtoupper('СП ГИПЕРСРОЧНАЯ ОПЛАТА БЕЗ БОНУСОВ')) {
                                            $isPromptPaymentWaitingNotBonus = true;
                                        }
                                    }
                                }
                            }
                        }
//                        }
                        if ($isPromptPaymentWaitingNotBonus) {
                            $visit->is_prompt_payment = true;
                            $visit->visit_status = VisitStatus::WAITING_NOT_BONUS;
                        }

                        $visit->is_payed = $visit->is_payed ?? 'Not_payed';
                        $visit->last_modified = $visit->last_modified ?? date('Y-m-d H:i:s');
                        $visit->is_payment_status_changed = $visit->is_payment_status_changed ?? 0;
                        $visit->first_import_date = $visit->first_import_date ?? date('Y-m-d H:i:s', time());

                        $visit->save(false);

                        $hash = md5($historicalData[$instanceId]['SecurityObjectUserID'] . $historicalData[$instanceId]['Login']);
                        //предполагаем, что комбинация SecurityObjectUserID и Login не меняется
                        if (array_key_exists($hash, $users)) {
                            $user = $users[$hash];
                        } else {
//                        // выбираем user из БД или создаем новую запись
                            $user = User::find()
                                ->where(['user_id' => $historicalData[$instanceId]['SecurityObjectUserID']])
                                ->orWhere(['username' => $historicalData[$instanceId]['Login']])
                                ->one();
                            if ($user == null) {
                                $user = new User();
                            }
                            $users[$hash] = $user;
                        }

                        $user->user_id = $historicalData[$instanceId]['SecurityObjectUserID'];
                        $user->username = $user->username ?? trim($historicalData[$instanceId]['Login']);
                        $user->email = !empty($historicalData[$instanceId]['Email']) ? trim($historicalData[$instanceId]['Email']) : $user->email;
                        $user->name = !empty($historicalData[$instanceId]['Name']) ? trim($historicalData[$instanceId]['Name']) : $user->name;// $user->name ?? trim($historicalData[$instanceId]['Name']);
                        $user->family_name = !empty($historicalData[$instanceId]['FamilyName']) ? trim($historicalData[$instanceId]['FamilyName']) : $user->family_name;// $user->family_name ?? trim($historicalData[$instanceId]['FamilyName']);
                        $user->country = !empty($historicalData[$instanceId]['Country']) ? trim($historicalData[$instanceId]['Country']) : $user->country;// $user->country ?? trim($historicalData[$instanceId]['Country']);
                        $user->role = 'user';
                        $user->status = 10;
                        $user->bonuse_percent = 10;
                        $user->password_hash = '$2y$13$Sne7BSauy67UYFOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLsdfghjkertyuweT9LUpaKDkbWyta'; //Yii::$app->security->generatePasswordHash($user->email); //'$2y$13$Sne7BSaOuPcfxO8IQVyAl.BUcAa3hT3VJgbLLweT9LUpaKDkbWyta';

                        $user->save(false);
                    }
                    unset($visit, $user);
                    unset($item[$instanceId]);
                }
//            echo('Получено user анкет ' . $date . ' : ' . count($visitData) . '<br>');
//            echo('Получено historical анкет ' . $date . ' : ' . count($historicalData) . '<br>');
                @$anketsCount += count($visitData);
                unset($visitData, $historicalData, $dayVisits);

            } catch (\Exception $e) {
                Yii::error([
                    'name' => 'api',
                    'data' => ['date' => $date],
                    'error' => $e->getMessage()], 'api');
            }

        }

//        $finishTime = time();
//        $scriptTime = $finishTime - $startTime;
//        echo 'Scripttime = ' . $scriptTime;
//        echo 'Total ankets proceed = ' . ($anketsCount ?? 0);

//        $this->actionBonuseCalculation(); //делаем отдельным cron

    }

    public function actionIndex()
    {
        $this->layout = 'admin-main';
        $userSecurityId = Yii::$app->session->get('target-user-id', Yii::$app->user->id);

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere(['user_id' => $userSecurityId]);
        $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Not_payed', 'Wallet', 'Rejected']]);

        $visit = $dataProvider->getModels();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['user_id', 'username'])
            ->asArray()
            ->all(), 'user_id');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'visit' => $visit,
            'uploadModel' => $uploadModel,
            'users' => $users
        ]);
    }

    /*public function actionIndex($sort = 'visit_date', $order = 'DESC')
    {
        $this->layout = 'admin-main';
        $userSecurityId = Yii::$app->session->get('target-user-id', 1);

        // todo переписать
        //форма комемнтария админа
        $commentModel = new AdminDone();
        $request = \Yii::$app->getRequest();
        if ($request->isPost) {
            $actions = Yii::$app->request->post('actions');
            foreach ($actions as $action) {
                $visit = Visit::find()
                    ->where(['anketa_id' => $action[0]])
                    ->one();
                //запись в таблицу БД
                $visit->visit_status = $action[1];
                $visit->payment_status = 'Accumulation';
                $visit->last_modified = date('Y-m-d', strtotime(time()));
                $visit->save(false);
            }
            return true;
        }

        //история действий админа (изменение записей)
        $adminComments = AdminDone::find()
            ->where(['user' => $userSecurityId])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        $visit = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->orderBy("$sort $order");
        $countQuery = clone($visit);
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $visit = Visit::find()
            ->where(['user_id' => $userSecurityId])
            ->andWhere(['is_payed' => 'Not_payed'])
            ->orderBy("$sort $order")
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {
            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();
            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['user_id', 'username'])
            ->asArray()
            ->all(), 'user_id');

        return $this->render('index', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users
        ]);
    }*/

    public function actionPayment()
    {
        $this->layout = 'admin-main';

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['user_id' => Yii::$app->session->get('target-user-id') ?? Yii::$app->user->id]);

        // filterring values by selector
        if (!empty($searchModel->is_payed_filter)) {
            $dataProvider->query->andFilterWhere(['is_payed' => $searchModel->is_payed_filter]);
        } else {
            $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Waiting_pay', 'Rejected']]);
        }

        $summaryData['waiting_pay'] = $dataProvider->query->sum('total_amount');

        return $this->render('payment', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
        ]);
    }

//    public function actionPayment($sort = 'visit_date', $order = 'DESC', $filter = ['Rejected', 'Payed', 'Waiting_pay', 'Sent_to_pay', 'Charity'])
//    {
//        $this->layout = 'admin-main';
//
//        //форма комемнтария админа
//        $commentModel = new AdminDone();
//
//        //получение измененных админом данных с фронта
//        $request = \Yii::$app->getRequest();
//        if ($request->isPost) {
//
//            $actions = Yii::$app->request->post('actions');
//
//            foreach ($actions as $action) {
//                $visit = Visit::find()
//                    ->where(['id' => $action[0]])
//                    ->one();
//
//                //запись в таблицу БД
//                $visit->is_payed = 'Waiting_pay';
//                $visit->last_modified = date('Y-m-d', strtotime(time()));
//                $visit->save(false);
//            }
//
//            return true;
//        }
//
//        //история действий админа (изменение записей)
//        $adminComments = AdminDone::find()
//            ->where(['user' => Yii::$app->session->get('target-user-id')])
//            ->orderBy(['date' => SORT_DESC])
//            ->asArray()
//            ->all();
//
//        $paymentStatus = array_flip(Yii::$app->params['paymentStatus']);
//        if (Yii::$app->request->get('filter')) {
//            $filter = $paymentStatus[Yii::$app->request->get('filter')];
//        }
//
//        $query = Visit::find()
//            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
//            ->andWhere(['is_payed' => $filter])
//            ->orderBy("$sort $order");
//
//        $countQuery = clone($query);
//
//        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
//
//        $visit = $query
//            ->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();
//
//        $uploadModel = new UploadForm;
//        if ($uploadModel->load(Yii::$app->request->post())) {
//
//            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
//            $uploadModel->upload();
//
//            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
//        }
//
//        //получение данных пользователей для крепления в историю
//        $users = ArrayHelper::index(User::find()
//            ->select(['id', 'username'])
//            ->asArray()
//            ->all(), 'id');
//
//        return $this->render('payment', [
//            'visit' => $visit,
//            'order' => $order,
//            'uploadModel' => $uploadModel,
//            'filter' => $filter,
//            'pages' => $pages,
//            'commentModel' => $commentModel,
//            'adminComments' => $adminComments,
//            'users' => $users
//
//        ]);
//    }

    public function actionPayed()
    {
        $this->layout = 'admin-main';

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere(['user_id' => Yii::$app->session->get('target-user-id') ?? Yii::$app->user->id]);
        $dataProvider->query->andFilterWhere(['is_payed' => 'Payed']);

        $summaryData['total_payed'] = $dataProvider->query->sum('payed_amount');

        return $this->render('payed', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
        ]);
        $paymentStatus = array_flip(Yii::$app->params['paymentStatus']);
        if (Yii::$app->request->get('filter')) {
            $filter = $paymentStatus[Yii::$app->request->get('filter')];
        }

        //получение данных пользователей для крепления в историю
        $users = ArrayHelper::index(User::find()
            ->select(['id', 'username'])
            ->asArray()
            ->all(), 'id');

        return $this->render('payed', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'filter' => $filter,
            'pages' => $pages,
            'commentModel' => $commentModel,
            'adminComments' => $adminComments,
            'users' => $users
        ]);
    }

//    public function actionPayed($sort = 'visit_date', $order = 'DESC', $filter = ['Payed', 'Charity'])
//    {
//        $this->layout = 'admin-main';
//
//        if ($filter == 'Благотворительность') {
//            $filter = 'Charity';
//        }
//
//        if ($filter == 'Оплачен') {
//            $filter = 'Payed';
//        }
//        //форма комемнтария админа
//        $commentModel = new AdminDone();
//
//        //получение измененных админом данных с фронта
//        $request = \Yii::$app->getRequest();
//        if ($request->isPost) {
//
//            $actions = Yii::$app->request->post('actions');
//
//            foreach ($actions as $action) {
//                $visit = Visit::find()
//                    ->where(['id' => $action[0]])
//                    ->one();
//
//                //запись в таблицу БД
//                $visit->is_payed = 'Payed';
//                $visit->last_modified = date('Y-m-d', strtotime(time()));
//                $visit->save(false);
//            }
//
//            return true;
//        }
//
//        //история действий админа (изменение записей)
//        $adminComments = AdminDone::find()
//            ->where(['user' => Yii::$app->session->get('target-user-id')])
//            ->orderBy(['date' => SORT_DESC])
//            ->asArray()
//            ->all();
//
//        $paymentStatus = array_flip(Yii::$app->params['paymentStatus']);
//        if (Yii::$app->request->get('filter')) {
//            $filter = $paymentStatus[Yii::$app->request->get('filter')];
//        }
//
//        $query = Visit::find()
//            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
//            ->andWhere(['is_payed' => $filter])
//            ->orderBy("$sort $order");
//
//        $countQuery = clone($query);
//
//        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
//
//        $visit = $query
//            ->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();
//
//        $uploadModel = new UploadForm;
//        if ($uploadModel->load(Yii::$app->request->post())) {
//            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
//            $uploadModel->upload();
//            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
//        }
//
//        //получение данных пользователей для крепления в историю
//        $users = ArrayHelper::index(User::find()
//            ->select(['id', 'username'])
//            ->asArray()
//            ->all(), 'id');
//
//        return $this->render('payed', [
//            'visit' => $visit,
//            'order' => $order,
//            'uploadModel' => $uploadModel,
//            'filter' => $filter,
//            'pages' => $pages,
//            'commentModel' => $commentModel,
//            'adminComments' => $adminComments,
//            'users' => $users
//        ]);
//    }

//    public function actionCancelCharity()
//    {
//        if (Yii::$app->request->isAjax) {
//            $model = Visit::find()
//                ->where(['id' => $_POST['id']])
//                ->one();
//            //получение измененных админом данных
////        $request = \Yii::$app->getRequest();
////
////        if ($request->isPost) {
////            $id = Yii::$app->request->post('id');
////            $model = Visit::find()->where(['anketa_id' => $id])->one();
//            $model->payment_status = null;
//            $model->is_payed = 'Not_payed';
//            $model->last_modified = date('Y-m-d H:i:s', time());
//            $model->save(false);
//
//            return true;
//        }
//        return false;
//    }

    public function actionBonus()
    {
        $this->layout = 'admin-main';

        $searchModel = new BonuseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bonus', compact('searchModel', 'dataProvider'));
    }

    public function actionMass()
    {
        ini_set("memory_limit", "20480M");
//        ini_set('max_execution_time', 2400);

        $this->layout = 'admin-main';

//        if(!empty($searchModel->dateSentToPay)) {
//            var_dump($searchModel->dateSentToPay);die;
//            list($startDate, $endDate) = explode(' - ', $searchModel->dateSentToPay);
//        }

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Waiting_pay']]);
        $dataProvider->query->andFilterWhere(['>', 'visit_date', '2021-06-01 00:00:00']);

        $totalData = clone $dataProvider;
        $totalData->pagination = false;

        $summaryData = [];
        if (!empty($searchModel->dateSentToPay)) {
            foreach ($totalData->getModels() as $item) {
                @$summaryData['sum'] += $item->total_amount;
                @$summaryData['sum_gipper'] += $item->is_prompt_payment ? $item->total_amount : 0;
                @$summaryData['sum_ordinary'] += $item->is_prompt_payment ? 0 : $item->total_amount;
                @$tayniki[$item->user_id]++;
                @$summaryData['anketa']++;
                @$summaryData['wallet_bonuses'] += $item->accumulated_bonuse;
            }
            $summaryData['tp'] = count($tayniki ?? []);
        }

        if (!empty($searchModel->dateSentToPay)) {
            list($payedFrom, $payedTo) = explode(' - ', $searchModel->dateSentToPay);
            $summaryData['payed_wallet'] = Bonuse::find()
                ->where(['>=', 'payment_date', date('Y-m-d', strtotime($payedFrom)) . ' 00:00:00'])
                ->andWhere(['<=', 'payment_date', date('Y-m-d', strtotime($payedTo)) . ' 23:59:59'])
                ->sum('amount');
//            var_dump($summaryData['payed_wallet']);die;
        }

        return $this->render('mass', compact('searchModel', 'dataProvider', 'summaryData'));
    }

    public function actionAccumulationAnkets()
    {
        ini_set("memory_limit", "20480M");
//        ini_set('max_execution_time', 2400);

        $this->layout = 'admin-main';

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Not_payed']]);
//        $dataProvider->query->andFilterWhere(['in', 'visit_status', ['Approved_to_withdrawal']]);
        $dataProvider->query->andFilterWhere(['>', 'visit_date', '2021-06-01 00:00:00']);

        $totalData = clone $dataProvider;
        $totalData->pagination = false;

        $summaryData = [];
        foreach ($totalData->getModels() as $item) {
            @$summaryData['total_amount'] += $item->total_amount;
            @$summaryData['sum_accumulating'] += $item->is_payed == 'Not_payed' ? $item->total_amount : 0;
            @$summaryData['sum_bonuses'] += $item->accumulated_bonuse ?? 0;
        }

        return $this->render('accumulation-ankets', compact('searchModel', 'dataProvider', 'summaryData'));
    }

    public function actionAnketsArray()
    {
        ini_set("memory_limit", "20480M");
//        ini_set('max_execution_time', 2400);

        $this->layout = 'admin-main';

//        if(!empty($searchModel->dateSentToPay)) {
//            var_dump($searchModel->dateSentToPay);die;
//            list($startDate, $endDate) = explode(' - ', $searchModel->dateSentToPay);
//        }

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Waiting_pay']]);
        $dataProvider->query->andFilterWhere(['>', 'visit_date', '2021-06-01 00:00:00']);

        $totalData = clone $dataProvider;
        $totalData->pagination = false;

        $summaryData = [];
        if (!empty($searchModel->dateSentToPay)) {
            foreach ($totalData->getModels() as $item) {
                @$summaryData['sum'] += $item->total_amount;
                @$summaryData['sum_gipper'] += $item->is_prompt_payment ? $item->total_amount : 0;
                @$summaryData['sum_ordinary'] += $item->is_prompt_payment ? 0 : $item->total_amount;
                @$tayniki[$item->user_id]++;
                @$summaryData['anketa']++;
                @$summaryData['wallet_bonuses'] += $item->accumulated_bonuse;
            }
            $summaryData['tp'] = count($tayniki ?? []);
        }

        return $this->render('ankets-array', compact('searchModel', 'dataProvider', 'summaryData'));
    }

    public function actionMonthlyReport()
    {
        ini_set("memory_limit", "20480M");
//        ini_set('max_execution_time', 2400);

        $this->layout = 'admin-main';

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->andFilterWhere(['in', 'is_payed', ['Waiting_pay']]);
//        $dataProvider->query->andFilterWhere(['>', 'visit_date', '2021-06-01 00:00:00']);

        $totalData = clone $dataProvider;
        $totalData->pagination = false;

        $summaryData = [];
        if (Yii::$app->request->queryParams) {
            foreach ($totalData->getModels() as $item) {
                @$summaryData['total_amount'] += $item->total_amount;
                @$summaryData['sum_gipper'] += $item->is_prompt_payment ? $item->total_amount : 0;
                @$summaryData['sum_ordinary'] += $item->is_prompt_payment ? 0 : $item->total_amount;
                @$summaryData['accumulated_bonuse'] += $item->accumulated_bonuse ? $item->total_amount : 0;
                @$summaryData['accumulated_sum_current'] += ($item->is_payed == 'Not_payed'
                    && $item->visit_status == 'Approved_to_withdrawal'
                    && $item->accumulation_start_date <= date('Y-m-d', time())) ? $item->total_amount : 0;
                @$summaryData['accumulated_bonuse_not_payed'] += $item->is_payed == 'Not_payed' ? $item->accumulated_bonuse : 0;
            }
            $summaryData['bonuses_to_user_amount'] = Visit::find()
                ->innerJoin('bonuses_to_user btu', 'visit.anketa_id = btu.anketa_id')
                ->where(['>=', 'visit_date', date('Y-m-d', strtotime($searchModel->dateFrom))])
                ->andWhere(['<=', 'visit_date', date('Y-m-d', strtotime($searchModel->dateTo))])
                ->andWhere(['status' => BonusesToUser::STATUS_WALLET])
                ->sum('btu.amount');
            $summaryData['bonuse_out'] = Bonuse::find()
                ->where(['>=', 'payment_date', date('Y-m-d', strtotime($searchModel->dateFrom))])
                ->andWhere(['<=', 'payment_date', date('Y-m-d', strtotime($searchModel->dateTo))])
                ->sum('amount');
        }

        return $this->render('monthly-report', compact('searchModel', 'dataProvider', 'summaryData'));
    }

    public function actionReviseBonus()
    {
        ini_set("memory_limit", "20480M");
//        ini_set('max_execution_time', 2400);

        $this->layout = 'admin-main';

        $data = Visit::getReviseBonusData();
        $total = [
            'accumulatedBonuse' => array_sum(array_column($data, 'accumulated_bonuse')),
            'amountCalc' => array_sum(array_column($data, 'amount_calc')),
            'amountDiff' => array_sum(array_column($data, 'amount_diff')),
            'totalAmount' => array_sum(array_column($data, 'total_amount')),
        ];

        return $this->render('revise-bonus', [
            'data' => $data,
            'total' => $total,
        ]);
    }

    public function actionBonuseStatistick()
    {
        $this->layout = 'admin-main';

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bonuse-statistick', compact('searchModel', 'dataProvider'));
    }

    public function actionAccumulation()
    {
        $this->layout = 'admin-main';

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere(['user_id' => Yii::$app->session->get('target-user-id') ?? Yii::$app->user->id]);
        $dataProvider->query->andFilterWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']]);
        $dataProvider->query->andFilterWhere(['or',
            ['<=', 'accumulation_start_date', date('Y-m-d')],
            ['is', 'accumulation_start_date', null]]);
        $dataProvider->query->andFilterWhere(['not in', 'is_payed', ['Payed', 'Waiting_pay', 'Sent_to_pay', 'Recalc']]);

        $summaryData['bonus'] = $dataProvider->query->sum('accumulated_bonuse');
        $summaryData['accumulation'] = $dataProvider->query->sum('total_amount');

        return $this->render('accumulation', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'summaryData' => $summaryData,
        ]);
    }

    /*public function actionAccumulation($sort = 'visit_date', $order = 'DESC')
    {
        $this->layout = 'admin-main';

        $visit = Visit::find()
            ->where(['user_id' => Yii::$app->session->get('target-user-id') ?? null])
            ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
            ->andWhere(['not in', 'is_payed', ['Payed', 'Waiting_pay', 'Sent_to_pay']])
            ->andWhere(['or',
                ['<=', 'accumulation_start_date', date('Y-m-d')],
                ['is', 'accumulation_start_date', null]])
//                        ->andWhere(['between', 'visit_date', ($startDate . ' 00:00:00'), ($endDate . ' 23:59:00')])
//            ->andWhere(['<=', 'accumulation_start_date', date('Y-m-d')])
//            ->andWhere(['is_payed' => 'Accumulation'])
//            ->andWhere(['payment_status' => 'Accumulation'])
//            ->andWhere(['visit_status' => 'Completed'])
            ->orderBy("$sort $order");

        $countQuery = clone($visit);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $visit = $countQuery
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        var_dump(Yii::$app->session->get('targetUserId'));
//        die;
        $uploadModel = new UploadForm;
        if ($uploadModel->load(Yii::$app->request->post())) {

            $uploadModel->xlsFile = UploadedFile::getInstance($uploadModel, 'xlsFile');
            $uploadModel->upload();

            $this->redirect(['excel/import-excel', 'file' => $uploadModel->xlsFile]);
        }

        return $this->render('accumulation', [
            'visit' => $visit,
            'order' => $order,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
        ]);
    }*/

    /* public function actionCharity()
     {
         $this->layout = 'admin-main';

         $userSecurityId = Yii::$app->session->get('target-user-id');
         $charitiesList = Charity::find()->all();
         $charities_count = Visit::charitiesCount($userSecurityId);
         $charities_amount = Visit::charitiesAmount($userSecurityId);
         $charities_per_month = Visit::charitiesPerMonth($userSecurityId);

         $charities_to_view = Visit::getCharitiesToView($charitiesList, $charities_count, $charities_amount);

         $charitiesDone = Charity::find()->orderBy(['id' => SORT_ASC])->all();
         if (!empty($charitiesDone)) {
             foreach ($charitiesDone as $item) {
                 if ($charities_count >= $item->numbers && $item->numbers != 0) {
                     $charities_to_view[1] = $item;
                     $item->done = 1;
                 }

                 if ($charities_amount >= $item->amount && $item->amount != 0) {
                     $charities_to_view[2] = $item;
                     $item->done = 1;
                 }
             }
         }

         $charities_report = Visit::find()
             ->where(['payment_status' => 'Charity'])
             ->all();

         foreach ($charities_report as $item) {
             @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['total_amount'] += $item->total_amount;

             if (!isset($charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                 $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'] = [];
             }
             if (!in_array($item->user_id, $charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'])) {
                 @$charities_table[(int)(date('m', strtotime($item->last_modified)))]['people_total'][] += $item->user_id;
             }
         }

         $charityTotal = Visit::charitiesAmount($userSecurityId);

         $month = [
             'Январь',
             'Февраль',
             'Март',
             'Апрель',
             'Май',
             'Июнь',
             'Июль',
             'Август',
             'Сентябрь',
             'Октябрь',
             'Ноябрь',
             'Декабрь'
         ];

         return $this->render('charity', compact('month', 'charityTotal', 'charitiesDone', 'charitiesList'));
     }*/

    public function actionFinduser()
    {
        if ($login = Yii::$app->request->get('login')) {
            if (!is_null($userData = User::find()
                ->where(['username' => $login])
                ->one())) {
                Yii::$app->session->set('target-user-id', $userData->user_id);
                Yii::$app->session->set('target-user-login', $userData->username);
                Yii::$app->session->set('target-user-accumulated-bonuse', $userData->accumulated_bonuse);
                Yii::$app->session->set('target-user-bonuce-percent', $userData->bonuse_percent);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionUpdate($id)
    {
        $this->layout = 'admin-main';

        $model = Visit::find()
            ->where(['id' => $id])
            ->one();

        if ($model->load(Yii::$app->request->post())) {

            $model->payment_status = 'Накопление';
            $model->last_modified = date('d.m.Y');
            $model->save(false);

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

//сохранение комментариев админа
    /*    public function actionSave()
        {
            //форма комемнтария админа
            $commentModel = new AdminDone();

            if ($commentModel->load(\Yii::$app->request->post()) && $commentModel->save()) {
                return true;
            } else {
                return false;
            }

            return $this->render('index', compact('commentModel'));
        }*/

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionService()
    {
        $cron = new CronService();
//        echo 'start get-api-data' . "\n";
//        $cron->getApiData();
//        echo 'start calc-bonus' . "\n";
//        $cron->calcBonus();
//        echo 'finish' . "\n";
        $cron->getWaitingNotBonus();

        $str = '{"PayrollItem":[{"Name":"\u0413\u0418\u041f\u0415\u0420\u0421\u0420\u041e\u0427\u041d\u0410\u042f \u041e\u041f\u041b\u0410\u0422\u0410","Amount":"0.00"},{"Name":"\u0417\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u043f\u0440\u043e\u0441\u0440\u043e\u0447\u0435\u043d\u043e \u043d\u0430 24 \u0447\u0430\u0441\u0430","Amount":"-50.00"},{"Name":"\u041e\u043f\u043b\u0430\u0442\u0430 \u0437\u0430 \u043a\u043e\u043f\u0438\u0440\u0430\u0439\u0442","Amount":"-50.00"},{"Name":"\u0422\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442","Amount":"450.00"}]}';
        $k = 1;

    }

}
