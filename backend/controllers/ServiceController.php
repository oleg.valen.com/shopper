<?php


namespace backend\controllers;

use common\models\Bonuse;
use common\models\BonuseSearch;
use common\models\BonusesToUser;
use common\models\User;
use common\models\Visit;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ServiceController extends Controller
{
    public function dateDiff($date1, $date2)
    {
        return strtotime($date2) >= strtotime($date1) ? round((strtotime($date2) - strtotime($date1)) / (60 * 60 * 24), 0) : 0;
    }


    public function actionRefactorBonuse()
    {
        $userId = 867091;
        $bonusesToUser = BonusesToUser::find()->where(['user_id' => $userId])->all();
        $bonusesAnketaIds = ArrayHelper::getColumn($bonusesToUser, 'anketa_id');
        $visits = Visit::find()->where([
            'user_id' => $userId,
            'is_payed' => 'Waiting_pay'
        ])->andFilterWhere(['not in', 'anketa_id', $bonusesAnketaIds])->all();

//        foreach ($visits as $visit) {
//            $bonuse = new BonusesToUser();
//            $bonuse->user_id = $userId;
//            $bonuse->anketa_id = $visit->anketa_id;
//            $bonuse->amount = $visit->accumulated_bonuse;
//            $bonuse->status = 2;
//
//            $bonuse->save();
//        }
        echo '<pre>';
        var_dump($visits);
        die();
    }

    public function actionRecalcWaitingPay()
    {

        ini_set("memory_limit", "20480M");

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            select u.username,
                   t.user_id,
                   t.anketa_id,
                   t.accumulated_bonuse,
                   t.visit_date,
                   t.sent_to_pay,
                   t.accumulation_start_date,
                   t.date_diff,
                   t.bonuse_persent,
                   t.amount_calc,
                   t.amount_calc - t.accumulated_bonuse amount_diff,
                   t.total_amount,
                   t.visit_status,
                   t.is_payed
            from (
                     select v.user_id,
                            v.anketa_id,
                            v.accumulated_bonuse,
                            DATE_FORMAT(v.visit_date, '%Y-%m-%e')  visit_date,
                            DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e') sent_to_pay,
                            v.accumulation_start_date,
                            DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                     v.accumulation_start_date)    date_diff,
                            case
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 365
                                    THEN
                                    0.11
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 180
                                    then 0.10
                                ELSE
                                    0.09
                                END                                bonuse_persent,
                            round(DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                           v.accumulation_start_date)
                                      * case
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 365
                                                THEN
                                                0.11
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 180
                                                then 0.10
                                            ELSE
                                                0.09
                                      END
                                      * v.total_amount / 365, 2)   amount_calc,
                            v.total_amount,
                            v.visit_status,
                            v.is_payed
                     from visit v
                     where v.is_payed in ('Waiting_pay')
                 ) t
                     left join user u
                               on t.user_id = u.user_id
            where t.visit_date >= '2021-06-01'
            and t.date_diff > 0
            and t.amount_calc - t.accumulated_bonuse > 0
            order by u.username
    ");

        $result = $command->queryAll();
        foreach ($result as $item) {
            $bonusesToUser = BonusesToUser::find()
                ->where(['user_id' => $item['user_id'], 'anketa_id' => $item['anketa_id']])
                ->all();
            if ($bonusesToUser) {
                if (count($bonusesToUser) > 1) {

                }
                $bonusToUser = $bonusesToUser[0];
                if ($bonusToUser->status != BonusesToUser::STATUS_WALLET) {
                    if ($bonusToUser->amount != $item['amount_calc']) {
                        if ($bonusToUser->status == 0) {

                        } elseif ($bonusToUser->status == 1) {

                        } elseif ($bonusToUser->status == 3) {

                        }
                    }
                } else {
                    if ($bonusToUser->amount != $item['amount_calc']) {
                        //доначисляем
                        $bonusToUser->amount = $item['amount_calc'];
                        $bonusToUser->save(false);
                    }
                }
            } else {
                //создаем новый $bonusesToUser
                $bonusToUser = new BonusesToUser();
                $bonusToUser->user_id = $item['user_id'];
                $bonusToUser->anketa_id = $item['anketa_id'];
                $bonusToUser->amount = $item['amount_calc'];
                $bonusToUser->status = BonusesToUser::STATUS_WALLET;
                $bonusToUser->created_at = strtotime($item['sent_to_pay']);
                $bonusToUser->updated_at = date('Y-m-d H:i:s');
                $bonusToUser->save(false);
            }
        }
        echo 'Ok';
    }

    public function actionRecalcPayed()
    {

        ini_set("memory_limit", "20480M");

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            select u.username,
                   t.user_id,
                   t.anketa_id,
                   t.accumulated_bonuse,
                   t.visit_date,
                   t.sent_to_pay,
                   t.accumulation_start_date,
                   t.date_diff,
                   t.bonuse_persent,
                   t.amount_calc,
                   t.amount_calc - t.accumulated_bonuse amount_diff,
                   t.total_amount,
                   t.visit_status,
                   t.is_payed,
            #        btu.status,
                   t.last_modified,
                   t.payment_date
            from (
                     select v.user_id,
                            v.anketa_id,
                            v.accumulated_bonuse,
                            DATE_FORMAT(v.visit_date, '%Y-%m-%e')  visit_date,
                            DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e') sent_to_pay,
                            v.accumulation_start_date,
                            DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                     v.accumulation_start_date)    date_diff,
                            case
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 365
                                    THEN
                                    0.11
                                when DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                              v.accumulation_start_date) > 180
                                    then 0.10
                                ELSE
                                    0.09
                                END                                bonuse_persent,
                            round(DATEDIFF(DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                           v.accumulation_start_date)
                                      * case
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 365
                                                THEN
                                                0.11
                                            when DATEDIFF(
                                                         DATE_FORMAT(v.sent_to_pay, '%Y-%m-%e'),
                                                         v.accumulation_start_date) >
                                                 180
                                                then 0.10
                                            ELSE
                                                0.09
                                      END
                                      * v.total_amount / 365, 2)   amount_calc,
                            v.total_amount,
                            v.visit_status,
                            v.is_payed,
                            v.last_modified,
                            v.payment_date
                     from visit v
                     where v.is_payed in ('Payed')
                       and v.payment_date is not null
                       and v.accumulation_start_date is not null
                 ) t
                     left join user u on t.user_id = u.user_id
           where t.user_id <> 801223
              and t.visit_date >= '2021-06-01'
              and t.amount_calc - t.accumulated_bonuse > 0
            order by u.username, t.visit_date
    ");
//        where t.user_id = 801223

        $data = [];
        $amountDiff = 0;
        $result = $command->queryAll();
        foreach ($result as $item) {
            $controlDate = $this->getControlDate($item['accumulation_start_date'], 30);
            if ($controlDate < $item['payment_date']) {
                $data[$item['user_id']] = (float)(isset($data[$item['user_id']])
                    ? $data[$item['user_id']] + $item['amount_diff']
                    : $item['amount_diff']);
                $amountDiff += (float)$item['amount_diff'];
            }
        }

        foreach ($data as $key => $value) {
            $visit = new Visit([
                'anketa_id' => '101' . $key,
                'user_id' => $key,
                'visit_date' => '2021-11-04',
                'visit_status' => 'Approved_to_withdrawal',
                'is_payed' => 'Waiting_pay',
                'accumulated_bonuse' => $value,
                'project' => 'Перерасчет',
            ]);
            $visit->save(false);

            $bonusToUser = new BonusesToUser();
            $bonusToUser->user_id = $key;
            $bonusToUser->anketa_id = $visit->anketa_id;
            $bonusToUser->amount = $value;
            $bonusToUser->status = BonusesToUser::STATUS_WALLET;
            $bonusToUser->save(false);
        }

        $str = '<pre>';
        foreach ($data as $key => $value) {
            $str .= $key . ";" . $value . PHP_EOL;
        }
        $str .= '</pre>';

        return $str;
    }

    function getControlDate($date, $workDays)
    {
        if (!defined('SATURDAY')) define('SATURDAY', 6);
        if (!defined('SUNDAY')) define('SUNDAY', 0);

        $start = strtotime($date);
        $end = '';
        while ($workDays > 0) {
            $end = strtotime("+1 day", $start);
            $day = date("w", $end);  // 0=sun, 1=mon, ..., 6=sat
            if ($day != SUNDAY &&
                !($day == SATURDAY)) {
                $workDays--;
            }
            $start = $end;
        }
        return date('Y-m-d', $end);
    }

    public function actionZero()
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            select *
            from visit
            WHERE anketa_id like '101%'
        ");

        $result = $command->queryAll();
        foreach ($result as $item) {
            $model = Visit::findOne($item['id']);
            $model->is_payed = 'Recalc';
            $model->save(false);
        }

    }

}
