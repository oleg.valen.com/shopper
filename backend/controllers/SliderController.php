<?php

namespace backend\controllers;

use common\models\Todo;
use Yii;
use common\models\Slider;
use common\models\SliderSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

Yii::setAlias('uploads', dirname(dirname(__DIR__)) . '/frontend/web/uploads');

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    // изменение размера картинки
    public function resizeImage($filename, $max_width, $max_height)
    {
        $wert = getimagesize($filename);
        $imageInfo = getimagesize($filename);
        $orig_width = $imageInfo[0];
        $orig_height = $imageInfo[1];
        $mime = $imageInfo['mime'];

        $width = $orig_width;
        $height = $orig_height;

        // taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }

        // wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        // создаем картинку под размеры
        $image_p = imagecreatetruecolor($width, $height);

        // В зависимости от расширения картинки вызываем соответствующую функцию
        if ($wert['mime'] == 'image/png') {
            $src = imagecreatefrompng($filename); //создаём новое изображение из файла
        } else if ($wert['mime'] == 'image/jpeg') {
            $src = imagecreatefromjpeg($filename);
        } else if ($wert['mime'] == 'image/gif') {
            $src = imagecreatefromgif($filename);
        } else {
            return false;
        }

        // сохраняем прозрачность
        imageAlphaBlending($image_p, false);
        imageSaveAlpha($image_p, true);
        imagecopyresampled($image_p, $src, 0, 0, 0, 0,
            $width, $height, $orig_width, $orig_height);

        // Сохраняет JPEG/PNG/GIF изображение
        return imagepng($image_p, $filename);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'create', 'update', 'view', 'delete'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'create', 'update', 'view', 'delete'
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'admin-main';

        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $this->layout = 'admin-main';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $this->layout = 'admin-main';

        $model = new Slider();

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->imageFile) {
                $model->upload();
            }

            $routImage = (Yii::getAlias('@uploads') . '/' . $model->imageFile);
            $this->resizeImage($routImage, 1000, 100);

//            echo(Yii::getAlias('@uploads') . "/{$model->imageFile->baseName}.{$model->imageFile->extension}"); die;

            $model->save();

            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->layout = 'admin-main';

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->imageFile) {
                $model->upload();
            }

            $model->save();

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
