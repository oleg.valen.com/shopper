<?php

namespace backend\controllers;

use common\models\Bonuse;
use common\models\BonuseSearch;
use common\models\Log;
use common\models\User;
use common\models\UserSearch;
use common\models\Visit;
use common\models\VisitSearch;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use XLSXWriter;

/**
 * Site controller
 */
class ExcelController extends Controller
{

    // todo правила поведения для входа
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
////                'only' => [
////                    'import-excel', 'export-excel'
////                ],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['*'],
//                        'roles' => ['?'],
//                    ],
////                    [
////                        'allow' => true,
////                        'actions' => [
////                            'charity'
////                        ],
////                        'roles' => ['admin'],
////                    ],
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'import-excel', 'export-excel'
//                        ],
//                        'roles' => ['user'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionExportLog()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'ID' => 'integer',
            'Пользователь' => 'string',
            'Анкета' => 'integer',
            'Активность' => 'string',
            'Дата / Время' => 'string',
            'IP адрес' => 'string',
        ];

        $session = Yii::$app->session;

        $userData = User::findOne(['user_id' => $session->get('target-user-id')]);
        $startDate = $session->get('log-start-date');
        $endDate = $session->get('log-end-date');

        $dataProvider = new ActiveDataProvider([
            'query' => Log::find()
                ->andFilterWhere(['>', 'created_at', date('Y-m-d H:i:s', strtotime($startDate . ' 00:00:00'))])
                ->andFilterWhere(['<', 'created_at', date('Y-m-d H:i:s', strtotime($endDate . ' 23:59:59'))])
                ->andWhere(['user_id' => $userData->id]),
            'pagination' => false,
        ]);

        $row = array();
        if (!empty($dataProvider)) {
            foreach ($dataProvider->getModels() as $id => $item) {
                @$row[$id]['id'] = $item->id;
                @$row[$id]['user_id'] = $userData->username;
                @$row[$id]['visit_id'] = $item->visit_id ?? '-';
                @$row[$id]['action'] = $item->action;
                @$row[$id]['datetime'] = date('d-m-Y H:i:s', strtotime($item->created_at));
                @$row[$id]['ipAddress'] = $item->user_ip ?? '-';
            }
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $writer = new XLSXWriter();
        $filename = "Выгрузка_активность пользователя_" . $userData->username . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 20, 20, 40, 30, 30]
        ];

        $writer->writeSheetHeader('Лог пользователя', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Лог пользователя', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionImportExcel($file = null)
    {
        $session = Yii::$app->session;

        //для проверки на дубликаты
        $DBvisits = Visit::find()->all();

        $file = 'uploads/' . $file;

        try {
            $importFileType = \PHPExcel_IOFactory::identify($file);
            $objReader = \PHPExcel_IOFactory::createReader($importFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error');
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        if ($highestColumn !== 'O') {
            return $this->redirect(Yii::$app->request->referrer);
        }

        for ($row = 1; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row . null, true, false);

//            //проверка на дубликаты
            $duplicateFound = false;

            foreach ($DBvisits as $item) {
                if ($item->anketa_id == $rowData[0][1] &&
                    $item->user_id == $rowData[0][2] &&
                    $item->project == $rowData[0][3] &&
                    $item->location == $rowData[0][4]) {
                    $duplicateFound = true;
                }
            }

            if ($row == 1 || $duplicateFound) {
                continue;
            }

            $visit = new Visit();

            $visit->visit_date = date('Y.m.d', strtotime($rowData[0][0]));
            $visit->anketa_id = $rowData[0][1];
            $visit->user_id = $rowData[0][2];
            $visit->project = $rowData[0][3];
            $visit->location = $rowData[0][4];
            $visit->amount = $rowData[0][5];
            $visit->bonuse = $rowData[0][6];
            $visit->penalty = $rowData[0][7];
            $visit->extra_charge = $rowData[0][8];
            $visit->payment_method = (($rowData[0][9]) != 1) ? $rowData[0][9] : '';
            $visit->is_payed = (($rowData[0][10]) != 1) ? $rowData[0][10] : 'Не оплачен';
            $visit->last_modified = (($rowData[0][11] != 1)) ? date('Y.m.d', strtotime($rowData[0][11])) : date('Y.m.d', time());
            $visit->visit_status = (($rowData[0][12] != 1)) ? $rowData[0][12] : 'Проверка';
            $visit->payment_status = (($rowData[0][13] != 1)) ? $rowData[0][13] : '';
            $visit->deposit_term = (($rowData[0][14] != 1)) ? $rowData[0][14] : '45';

            $visit->save(false);

            $session->setFlash('excelAdded', 'Вы успешно добавили данные.');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionExportExcel()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'Дата визита' => 'string',
            'Номер анкеты' => 'integer',
            'id пользователя' => 'integer',
            'Проект' => 'string',
            'Адрес локации' => 'string',
            'Сумма за визит' => 'string',
            'Бонус' => 'string',
            'Штраф' => 'string',
            'Сумма' => 'string',
            'Оплачен' => 'string',
            'Последнее изменение' => 'string',
            'Статус визита' => 'string',
            'Статус оплаты' => 'string',
            'Срок депозита' => 'string'
        ];

        if ($type = Yii::$app->request->get('type') &&
            $userId = Yii::$app->request->get('userId')) {
            switch ($type) {
                case 'index':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId, 'is_payed' => 'Not_payed'])
                        ->all();
                    $sheetName = 'Таблица визитов';
                    break;
                case 'accum':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->andWhere(['in', 'visit_status', ['Accumulation', 'Approved_to_withdrawal']])
                        ->andWhere(['not', ['is_payed' => 'Payed']])
                        ->andWhere(['in', 'payment_status', [null]])
                        ->all();
                    $sheetName = 'Накопление средств';
                    break;

                case 'paym':
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->andWhere(['in', 'is_payed', ['Payed', 'Waiting_pay', 'Sent_to_pay', 'Charity']])
                        ->all();

                    $sheetName = 'Выплаты';
                    break;

                default:
                    $visit = Visit::find()
                        ->where(['user_id' => $userId])
                        ->all();
                    $sheetName = 'Все визиты';
            }
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $paymentStatus = Yii::$app->params['paymentStatus'];

        if (isset($visit)) {
            foreach ($visit as $item) {
                $row[$item->id]['visit_date'] = $item['visit_date'];
                $row[$item->id]['anketa_id'] = $item['anketa_id'];
                $row[$item->id]['user_id'] = $item['user_id'];
                $row[$item->id]['project'] = $item['project'];
                $row[$item->id]['location'] = $item['location_city'] . ' ' . $item['location_address'];
                $row[$item->id]['pay_rate'] = $item['pay_rate'];
                $row[$item->id]['bonuse'] = $item['bonuse'];
                $row[$item->id]['penalty'] = $item['penalty'];
                $row[$item->id]['total_amount'] = $item['total_amount'];
                $row[$item->id]['is_payed'] = $paymentStatus[$item['is_payed']];
                $row[$item->id]['last_modified'] = $item['last_modified'];
                $row[$item->id]['visit_status'] = $item['visit_status'];
                $row[$item->id]['payment_status'] = $item['payment_status'];
                $row[$item->id]['deposit_term'] = $item['deposit_term'];
            }
        } else {
            return $this->redirect(['/admin/index']);
        }

        $writer = new XLSXWriter();
        $filename = "Visits_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 20, 20, 40, 30]
        ];

        $writer->writeSheetHeader('Лог пользователя', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Лог пользователя', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportBonuse()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'ID' => 'string',
            'Дата вывода бонусов' => 'string',
            'Логин' => 'string',
            'Сумма бонусов' => 'string',
            '№ Анкеты на которую прикреплены бонусы' => 'string',
            'Номера анкет/сумма по начисленным бонусам' => 'string',
            'Дата до которой нужно внести изменения в ШМ' => 'string'
        ];

        $searchModel = new BonuseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $request = json_decode(Yii::$app->request->post('dates'));
        if (!empty($request)) {
            $dates = explode(' - ', $request);
        } else {
            $dates[0] = '2021-01-01';
            $dates[1] = date('Y-m-d');
        }

        $dataProvider->query->andFilterWhere(['between', 'payment_date',
            date('Y-m-d', strtotime($dates[0])),
            date('Y-m-d', strtotime($dates[1]))]);

        $row = array();
        foreach ($dataProvider->getModels() as $item) {
            $row[$item->id]['id'] = $item->id;
            $row[$item->id]['payment_date'] = date('d.m.Y H:i:s', $item->created_at + 3600 * 3) ?? '';
            $row[$item->id]['login'] = $item->login;
            $row[$item->id]['amount'] = $item->amount;
            $row[$item->id]['payment_anketa_id'] = $item->payment_anketa_id;
            $row[$item->id]['anketas_data'] = $item->anketas_data;
            $row[$item->id]['deadline_date'] = date('d.m.Y', strtotime($item->deadline_date)) ?? '';
        }

        $writer = new XLSXWriter();
        $filename = "Bonuses_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 30, 30, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader('Бонусы пользователя', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Бонусы пользователя', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportBonuseStatistick()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'ID' => 'string',
            'Логин' => 'string',
            'E-mail' => 'string',
            'Бонусы в накоплении' => '###0.00',
            'Бонусов в копилке' => '###0.00',
            'Сумма анкет, которая находится в накоплении' => '#####0.00',
        ];

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $row = array();
        foreach ($dataProvider->getModels() as $item) {
            $row[$item->id]['id'] = $item->user_id;
            $row[$item->id]['login'] = $item->username;
            $row[$item->id]['email'] = $item->email;
            $row[$item->id]['bonuses_total'] = $item->totalBonuses;
            $row[$item->id]['bonuses_wallet'] = $item->walletBonuses;
            $row[$item->id]['in_accumulation'] = $item->totalAccumulation;
        }

        $writer = new XLSXWriter();
        $filename = "Bonuse_statistick_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader('Бонусы статистика', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Бонусы статистика', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportArray()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'Период' => 'string',
            'Номер анкеты' => 'string',
            'Название проекта' => 'string',
            'За визит' => '###0.00',
            'Доплата' => '###0.00',
            'Штраф' => '###0.00',
            'Итого' => '###0.00',
            'Гипер' => 'string',
            'Статус анкеты' => 'string',
            'Дата отправки на оплату' => 'string',
            'Сумма бонусов' => '###0.00',
        ];

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $request = json_decode(Yii::$app->request->post('data'));
        if (!empty($request)) {
            $dates = explode(' - ', $request);

            $dataProvider->query->andFilterWhere(['between', 'visit_date',
                date('Y-m-d', strtotime($dates[0])),
                date('Y-m-d', strtotime($dates[1]))]);
        }

        $row = array();
        foreach ($dataProvider->getModels() as $item) {
            $row[$item->id]['period'] = $request ?? '';
            $row[$item->id]['id'] = $item->anketa_id;
            $row[$item->id]['project'] = $item->project ?? '';
            $row[$item->id]['rate'] = $item->pay_rate ?? '';
            $row[$item->id]['bonuse'] = $item->bonuse ?? '';
            $row[$item->id]['penalty'] = $item->penalty ?? '';
            $row[$item->id]['sum'] = $item->total_amount;
            $row[$item->id]['prompt'] = $item->is_prompt_payment ? 'Гипер' : '';
            $row[$item->id]['is_payed'] = $item->is_payed == 'Not_payed' ? 'Накапливается' : 'Выведена на оплату';
            $row[$item->id]['date'] = !empty($item->sent_to_pay) ? date('d.m.Y', strtotime($item->sent_to_pay)) : '';
            $row[$item->id]['accumulated_bonuse'] = !empty($item->accumulated_bonuse) ? $item->accumulated_bonuse : '';
        }

        $writer = new XLSXWriter();
        $filename = "Bonuse_statistick_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader('Бонусы статистика', $header, $header_style);

        foreach ($row as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Бонусы статистика', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportMonthlyReport()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'Всего подлежит оплате Шоппера за отчетный месяц' => '###0.00',
            'Всего массовые гиперы анкеты' => '###0.00',
            'Всего массовые классические проекта' => '###0.00',
            'Всего сумма по визитам, которая была в накоплении хотя бы 1 день визит' => '###0.00',
            'Сумма которая остается в накоплении на день формирования отчета' => '###0.00',
            'Сумма бонусов в накоплении' => '###0.00',
            'Сумма бонусов, которые уже не накапливаются' => '###0.00',
            'Сумма бонусов, которая выведена ТП' => '###0.00',
        ];

        $searchModel = new VisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        $request = json_decode(Yii::$app->request->post('data'));
        if (!empty($request)) {
            $dates = explode(' - ', $request);

            $dataProvider->query->andFilterWhere(['between', 'visit_date',
                date('Y-m-d', strtotime($dates[0])),
                date('Y-m-d', strtotime($dates[1]))]);
        }

        $summaryData = $tmp = [];
        $totalData = clone $dataProvider;
        foreach ($totalData->getModels() as $item) {
            @$tmp['total_amount'] += $item->total_amount;
            @$tmp['sum_gipper'] += $item->is_prompt_payment ? $item->total_amount : 0;
            @$tmp['sum_ordinary'] += $item->is_prompt_payment ? 0 : $item->total_amount;
            @$tmp['accumulated_bonuse'] += $item->accumulated_bonuse ? $item->total_amount : 0;
            @$tmp['accumulated_sum_current'] += ($item->is_payed == 'Not_payed'
                && $item->accumulation_start_date > date('Y-m-d', time())) ? $item->total_amount : 0;
//            @$tmp['bonuses_to_user_amount'] += $item->bonuseToUser ? $item->bonuseToUser->amount : 0;
        }
        $tmp['bonuses_to_user_amount'] = Visit::find()
            ->innerJoin('bonuses_to_user btu')
            ->where(['>=', 'payment_date', date('Y-m-d', strtotime($dates[0]))])
            ->andWhere(['<=', 'payment_date', date('Y-m-d', strtotime($dates[1]))])
            ->sum('btu.amount');
        $tmp['bonuse_out'] = Bonuse::find()
            ->where(['>=', 'payment_date', date('Y-m-d', strtotime($dates[0]))])
            ->andWhere(['<=', 'payment_date', date('Y-m-d', strtotime($dates[1]))])
            ->sum('amount');

        $summaryData[] = $tmp;

        $writer = new XLSXWriter();
        $filename = "Bonuse_statistick_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [30, 30, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader('Бонусы статистика', $header, $header_style);

        foreach ($summaryData as $oneRow) {
            $row_style = [];
            $base_style = [
                'halign' => 'center',
                'valign' => 'center'
            ];
            $writer->writeSheetRow('Бонусы статистика', $oneRow, $base_style, $row_style);
        }

        $writer->writeToStdOut();

        exit(0);
    }

    public function actionExportReviseBonus()
    {
        include_once("xlsxwriter.class.php");
        $filename = "Отчет о сверке бонусов.xlsx";
        header('Content-disposition: attachment; filename="' . \XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $rows = [[
            'Username',
            'User id',
            'Anketa id',
            'Accum. bonus',
            'Visit date',
            'Sent to pay',
            'Accum. start date',
            'Date diff',
            'Bonus persent',
            'Amount calc',
            'Amount diff',
            'Total amount',
            'Visit status',
            'Is payed',
        ]];

        $data = Visit::getReviseBonusData();
        foreach ($data as $item) {
            $rows[] = [
                $item['username'],
                $item['user_id'],
                $item['anketa_id'],
                $item['accumulated_bonuse'],
                $item['visit_date'],
                $item['sent_to_pay'],
                $item['accumulation_start_date'],
                $item['date_diff'],
                $item['bonuse_persent'],
                $item['amount_calc'],
                $item['amount_diff'],
                $item['total_amount'],
                $item['visit_status'],
                $item['is_payed'],
            ];
        }
        $rows[] = [
            '',
            '',
            '',
            array_sum(array_column($data, 'accumulated_bonuse')),
            '',
            '',
            '',
            '',
            '',
            array_sum(array_column($data, 'amount_calc')),
            array_sum(array_column($data, 'amount_diff')),
            array_sum(array_column($data, 'total_amount')),
            '',
            '',
        ];

        $writer = new \XLSXWriter();
        foreach ($rows as $row) {
            $writer->writeSheetRow('Sheet1', $row);
        }
        $writer->writeToStdOut();
        exit(0);
    }

    public function actionExportMass()
    {
        include_once("xlsxwriter.class.php");

        // заголовки столбцов
        $header = [
            'Период' => 'string',
            'Сумма анкет, поданных на оплату' => '###0.00',
            'Сумма анкет с признаком "гипер"' => '###0.00',
            'Сумма анкет без признака "гипер"' => '###0.00',
            'Количество ТП, подавших на оплату' => 'string',
            'Количество анкет, поданных на оплату' => 'string',
            'Сумма выведенных из «копилки» бонусов' => '###0.00',
            'Сумма бонусов, отправленных в «копилку»' => '#####0.00',
        ];

        $summaryData = json_decode(Yii::$app->request->post('data'), true);

        $row['period'] = $summaryData['period'] ?? '-';
        $row['sum'] = $summaryData['sum'] ?? 0;
        $row['sum_gipper'] = $summaryData['sum_gipper'] ?? 0;
        $row['sum_ordinary'] = $summaryData['sum_ordinary'] ?? 0;
        $row['tp'] = $summaryData['tp'] ?? 0;
        $row['anketa'] = $summaryData['anketa'] ?? 0;
        $row['payed_wallet'] = $summaryData['payed_wallet'] ?? 0;
        $row['wallet_bonuses'] = $summaryData['wallet_bonuses'] ?? 0;

        $writer = new XLSXWriter();
        $filename = "Bonuse_statistick_" . date("d-m-Y-His") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $header_style = [
            'fill' => '#ccc', 'font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'valign' => 'center',
            'widths' => [15, 30, 30, 30, 30, 30, 30, 30, 30]
        ];

        $writer->writeSheetHeader('Бонусы статистика', $header, $header_style);

        $row_style = [];
        $base_style = [
            'halign' => 'center',
            'valign' => 'center'
        ];
        $writer->writeSheetRow('Бонусы статистика', $row, $base_style, $row_style);

        $writer->writeToStdOut();

        exit(0);
    }

//    public function actionExportExcel()
//    {
//        if (Yii::$app->request->get('type') &&
//            Yii::$app->request->get('userId')) {
//
//            switch (Yii::$app->request->get('type')) {
//                case 'index':
//                    $visit = Visit::find()
//                        ->where(['user_id' => Yii::$app->request->get('userId')])
//                        ->andWhere(['is_payed' => 'Не оплачен'])
//                        ->andWhere(['<>', 'payment_status', 'Отклонен'])
//                        ->all();
//                    $sheetName = 'Таблица визитов';
//                    break;
//
//                case 'accum':
//                    $visit = Visit::find()
//                        ->where(['user_id' => Yii::$app->request->get('userId')])
//                        ->andWhere(['payment_status' => 'Накопление'])
//                        ->andWhere(['visit_status' => 'Одобрено'])
//                        ->all();
//                    $sheetName = 'Накопление средств';
//                    break;
//
//                case 'paym':
//                    $visit = Visit::find()
//                        ->where(['user_id' => Yii::$app->request->get('userId')])
//                        ->andWhere(['is_payed' => ['Отклонен', 'Оплачен', 'Отправлен на оплату', 'Благотворительность', 'Ожидает выплату']])
//                        ->all();
//
//                    $sheetName = 'Выплаты';
//                    break;
//
//                default:
//                    $visit = Visit::find()
//                        ->where(['user_id' => Yii::$app->request->get('userId')])
//                        ->all();
//                    $sheetName = 'Все визиты';
//            }
//        } else {
//            exit;
//        }
//
//        $objPHPExcel = new \PHPExcel();
//
//        $sheet = 0;
//
//        $objPHPExcel->setActiveSheetIndex($sheet);
//
//        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
//        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
//
//
//        $objPHPExcel->getActiveSheet()->setTitle($sheetName)
//            ->setCellValue('A1', 'Дата визита')
//            ->setCellValue('B1', 'Номер анкеты')
//            ->setCellValue('C1', 'id пользователя')
//            ->setCellValue('D1', 'Проект')
//            ->setCellValue('E1', 'Адрес локации')
//            ->setCellValue('F1', 'Сумма за визит')
//            ->setCellValue('G1', 'Бонус')
//            ->setCellValue('H1', 'Штраф')
//            ->setCellValue('I1', 'Доплата')
//            ->setCellValue('J1', 'Способ оплаты')
//            ->setCellValue('K1', 'Оплачен')
//            ->setCellValue('L1', 'Последнее изменение')
//            ->setCellValue('M1', 'Статус визита')
//            ->setCellValue('N1', 'Статус оплаты')
//            ->setCellValue('O1', 'Срок депозита');
//
//        $row = 2;
//
//        foreach ($visit as $item) {
//            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $item['visit_date']);
//            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['anketa_id']);
//            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $item['user_id']);
//            $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['project']);
//            $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['location']);
//            $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['amount']);
//            $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['bonuse']);
//            $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['penalty']);
//            $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $item['extra_charge']);
//            $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['payment_method']);
//            $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['is_payed']);
//            $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $item['last_modified']);
//            $objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $item['visit_status']);
//            $objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $item['payment_status']);
//            $objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $item['deposit_term']);
//
//            $row++;
//        }
//
//        header('Content-Type: application/vnd.ms-excel');
//        $filename = "Visits_" . date("d-m-Y-His") . ".xls";
//        header('Content-Disposition: attachment;filename=' . $filename . ' ');
//        header('Cache-Control: max-age=0');
////        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//        header('Content-Type: application/vnd.ms-excel');
//
//        $objWriter->save('php://output');
//
//        exit;
//    }
}
