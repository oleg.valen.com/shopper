<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\components\behaviors\StatusBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_WAIT = 5;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    
    public $field_name;
    public $field_password;
    public $field_email;
    public $field_status;
    public $pass;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'statusBehavior' => [
                'class' => StatusBehavior::className(),
            ],
        ];
        
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'],'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_WAIT, self::STATUS_ACTIVE]],
            //[ ['pass'],'string','min'=>6],
            [['email'], 'email'],
            //[['email'], 'unique'],
            [['pass','field_name','field_password','field_email','field_status','item_name','created_at','updated_at','category','access_token',
            'name','surname','position','phone','user_image','company','pass'],'safe'],
            /*['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],*/
        ];
    }
    
     public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'email' => 'Электронный адрес:',
            'field_name' => 'Имя',
            'field_password'=>'Пароль',
            'field_email'=>'Электронная почта',
            'field_status'=>'Статус',
            'item_name' => 'Права доступа',
            'pass' => 'Пароль',
            'category' => 'Ответственный',
        ];
    }

    public function getFullName()
    {
        return $this->name.' '.$this->surname.' компания: '.$this->company;
    }

     /**/
    /*public function getSelectOne()
    {
        return $this->hasOne(Useraccess::className(), ['user_id' => 'id']);
    }

    public function getMakro()
    {
        //return $this->hasOne(Orgmakro::className(), ['id' => 'org_makro_id']);
        return $this->hasOne(Orgmakro::className(), ['id' => 'makro_id'])
            ->viaTable('user_access',['user_id'=>'id']);
    }

    public function getTeritor()
    {
        return $this->hasOne(Orgter::className(), ['id' => 'terr_id'])
            ->viaTable('user_access',['user_id'=>'id']);
    }
    
    public function getOtdel()
    {
        return $this->hasOne(Orginfo::className(), ['id' => 'org_id'])
            ->viaTable('user_access',['user_id'=>'id']);
    }*/

    /**/

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        //return static::findOne(['access_token' => $token, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function beforeSave($insert){
        
        return parent::beforeSave($insert);
    }

}
