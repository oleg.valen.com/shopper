<?php

namespace backend\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'style/bootstrap.min.css',
        'style/normalize.css',
        'style/libs/font-awesome.css',
        'style/style.css?1.1.4',
        'style/media.css?1.1.2',
        'style/i-style/style.css?1.0.2',
        'style/libs/customSelect.css?1.0.2',
        'style/site.css',
        
    ];
    public $js = [
//        'js/jquery-3.3.1.slim.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/main.js',
        'js/libs/modal.js',
        'js/selectPayment.js',
        'js/libs/customSelect.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
//    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
