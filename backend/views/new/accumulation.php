<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Накопления';

$visitStatus = [
    'Completed' => 'Проверен',
    'Validation' => 'На проверке',
    'On Hold' => 'В процессе',
    'No items' => 'Неопределен',
];

?>

<style>
    .disabled {
        color: #000;
    }

    .enabled {
        color: blue;
    }
</style>

<section class="calc">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="calculation-title title-size"></p>
                <div class="calculation">
                    <div class="pt-3">
                        <div class="title-for-calculator">
                            <p class="result">' ◡ '</p>
                        </div>
                    </div>
                    <div class="calc_buttons_wrapper">
                        <div class="btn-container">
                            <button class="calc_btn btn_mt" id="sum">+</button>
                        </div>
                        <div class="btn-container">
                            <button class="calc_btn btn_mt" id="minus" tabindex="2">-</button>
                        </div>
                        <div class="btn-container">
                            <button class="calc_btn" id="multiply" tabindex="2">×</button>
                        </div>
                        <div class="btn-container">
                            <button class="calc_btn" id="equal" tabindex="2">=</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size-accumulation">НАКОПЛЕНИЯ</p>
            </div>
<!--            <div class="col-lg-1 ml-auto">-->
<!--                <div class="edit">-->
<!--                    <img class="pencil-icon" src="/img/edit.png" alt="" onclick="pencil()">-->
<!--                </div>-->
<!--            </div>-->
        </div>

        <?php Pjax::begin(); ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles-admin">
                                Сумма
                                <table class="sum-table">
                                    <tr>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('pay_rate') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('penalty') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('bonuse') ?></th>
                                        <th class="sum-column_title no-border">Итого</th>
                                    </tr>
                                </table>
                            </th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_status') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('is_payed') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('last_modified') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('deposit_term') ?></th>
                            <th class="titles-admin">Осталось</th>
                        </tr>

                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">

                                <td class="table_data"><?= date('d.m.Y', strtotime($item->visit_date)) ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="table_data"><?= $item->location_city . '<br>' . ($LocationStateRegion[$item->location_state_region] ?? '') . '<br>' . $item->location_address ?></td>
                                <td class="table_data">
                                    <table>
                                        <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                        <td class="sum_column_data"><?= $item->penalty ?></td>
                                        <td class="sum_column_data"><?= $item->bonuse ?></td>
                                        <td class="sum_column_data no-border"><?= $item->total_amount ?></td>
                                    </table>
                                </td>
                                <td class="table_data"><?= $visitStatus[$item->visit_status] ?></p></td>
                                <td class="table_data acumulation">Накопление</td>
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->first_import_date)) ?></td>
                                <td class="table_data"><?= $item->deposit_term ?></td>

                                <?php
                                $startDate = strtotime($item->last_modified);
                                $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                                $now_date = time();
                                $datediff = $target_date - $now_date;
                                $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';
                                ?>

                                <td class="table_data"><?= $left ?> дней</td>

                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                    </table>

                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buttons-wrapper">

                <?php $form = ActiveForm::begin(['options' => [
                    'id' => 'upform',
                    'enctype' => 'multipart/form-data',]
                ]) ?>

                <?= $form->field($uploadModel, 'xlsFile')->fileInput(['style' => 'display: none'])->label(false) ?>

                <?= Html::Button('Сохранить', [
                    'id' => 'upload-xls',
                    'class' => 'save-btn',
                    'onclick' => '$("#uploadform-xlsfile").click()',
                ]) ?>

                <?php ActiveForm::end() ?>

                <?= Yii::$app->session->get('target-user-id') ?>
                <?= Html::a('<button class="download-btn">Загрузить </button>',
                    ['excel/export-excel', 'type' => 'accum', 'userId' => Yii::$app->session->get('target-user-id')],
                    ['onclick' => 'uploadForm(event)'])
                ?>

            </div>
        </div>
    </div>
</div>

<script>
    // поведение при нажатии на карандаш
    function pencil() {
        if ($("a.visitstatus").hasClass("disabled")) {
            $("a.visitstatus").removeClass('disabled').addClass('enabled');
            $("a.visitstatus").attr("onClick", "");
        } else {
            $("a.visitstatus").css("onclick", "true");
            $("a.visitstatus").removeClass('enabled').addClass('disabled');
            $("a.visitstatus").attr("onClick", "return false");
        }
    }

    $("a.visitstatus").attr("onClick", "return false");

    // при пустом поле user не производить экспорт данных
    function uploadForm(event) {
        let userField = document.getElementById('user-field');
        if (userField.innerHTML.length < 1) {
            event.preventDefault();
            alert('Для этого действия необходимо выбрать пользователя!');
            return false;
        }
    }

    document.getElementById('uploadform-xlsfile').onchange = function (event) {

        const formData = new FormData($('#upform')[0]);

        $.ajax({
            type: 'post',
            url: '/admin/site/accumulation',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log('Proceed successfully');
            },
            error: function (data) {
                console.log(data, 'Proceed error');
            }
        });

        return true;
    }
</script>
