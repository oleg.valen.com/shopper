<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Благотворительность';

?>

<style>
    .modal-header .close {
        margin-left: 0;
    }
</style>
<div class="certificate-overlay">
    <div class="cert-image">
        <div class="close-popup">+</div>

    </div>
</div>


<?php if (!Yii::$app->getRequest()->getCookies()->has('charity-confirmed')) { ?>
    <div class="about-page-popup">
        <div class="about-page_wrapper d-flex">
            <div class="about-page_icon">
                <img class="inform_icon" src="/img/information-pink.png" alt="">
            </div>
            <div class="about-page_description">
                <p><span class="size">Это </span> <span class="bold">страница благотворительности.</span></p>
                <p class="mt-3"><span class="size">Здесь ты можешь отслеживать свой </span> <span class="bold">прогресс достижений</span>
                    <span class="size"> в добрых делах</span></p>
                <div class="about-page-confirm-btn-wrapp mt-4 d-flex justify-content-center">
                    <a class="about-page-confirm-btn"
                       href="<?= Url::to(['main/modal', 'page' => 'charity']) ?>">Ознакомлен</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php Modal::begin([
    'size' => 'modal-lg',
    'options' => [
        'id' => 'charity-report',
    ],
    'header' => '<h4>Отчет по благо за <span id="indicated-month"></span> 2019 года</h4>',
]);
?>

<table class="table table-striped">

    <tr>
        <td>
            Статья
        </td>
        <td>
            Сумма
        </td>
    </tr>
    <tr>
        <td>
            количество людей которые пожертвовали деньги за месяц
        </td>
        <td>
            89
        </td>
    </tr>
    <tr>
        <td>
            сумма, собранная за месяц
        </td>
        <td>
            5 000
        </td>
    </tr>
    <tr>
        <td>
            сумма, собранная за квартал
        </td>
        <td>
            15 000
        </td>
    </tr>
    <tr>
        <td>
            сумма, собранная с начала года (или всего сколько жертвуем)

        </td>
        <td>
            55 000
        </td>
    </tr>
    <tr>
        <td>
            <strong>ИТОГО</strong>
        </td>
        <td>
            <strong>55 000</strong>
        </td>
    </tr>
</table>

<?php Modal::end(); ?>


<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <img class="charity-banner" src="/img/charity.png" alt="banner">
            <!--            <a href="/img/certificates/line-life-sertificat.png" class="text-uppercase" target="_blank">-->
            <!--                <p class="life-line">СЕРТИФИКАТ ЛИНИЯ ЖИЗНИ</p>-->
            <!--            </a>-->
        </div>
        <div class="col-lg-6">
            <p class="news_title">ВАШИ ДОСТИЖЕНИЯ</p>
            <div class="all-progress-wrapper">

                <?php foreach ($charitiesDone as $item) { ?>
                    <div class="prog-icon-item tooltip-box">
                        <div class="tooltip-text">
                            <i>
                                <?= $item->description ?>
                            </i>
                        </div>
                        <p class="icon-name <?= $item->image ?><?= $item->done ? '' : '-disabled' ?>"><?= $item->title ?></p>
                    </div>
                <?php } ?>


            </div>
            <!--            <div class="choose-action">-->
            <!--                <p class="choice-title">КОМПАНИЯ 4SERVICE - ПАРТНЕР БЛАГОТВОРИТЕЛЬНОГО ФОНДА <a class="go-to-site" href="https://www.life-line.ru/o-fonde/korporativnyy-klub/" target="_blank">"ЛИНИЯ ЖИЗНИ"</a></p>-->


            <!--                <div class="choice-btn-cont">-->
            <!---->
            <!--                    <div class="btn-group-toggle d-flex justify-content-between" data-toggle="buttons">-->
            <!--                        <label class="btn btn-secondary choice">-->
            <!--                            <input class="" type="checkbox" checked autocomplete="off"> Волонтерская организация-->
            <!--                        </label>-->
            <!--                        <label class="btn btn-secondary choice pt-3 ml-1 mr-1">-->
            <!--                            <input class="" type="checkbox" checked autocomplete="off"> <span-->
            <!--                                    class="pt-2">Частное лицо</span>-->
            <!--                        </label>-->
            <!--                        <label class="btn btn-secondary choice">-->
            <!--                            <input class="" type="checkbox" checked autocomplete="off"> Благотворительная организация-->
            <!--                        </label>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-6">
            <h4 class="month-report-title">Вы можете ознакомиться с отчетом по собранным благотворительным
                средствам:</h4>
            <div class="month-wrapp-btn">

                <?php for ($i = 0; $i <= 11; $i++) { ?>
                    <button class="month_report-btn <?= (date('m') <= $i) ? 'disable-btn' : 'enable-btn' ?>"
                            data-month="<?= $month[$i] ?>">
                        <?= $month[$i] ?>
                    </button>
                <?php } ?>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="choose-action">
                <p class="choice-title">КОМПАНИЯ 4SERVICE - ПАРТНЕР БЛАГОТВОРИТЕЛЬНОГО ФОНДА <a class="go-to-site"
                                                                                                href="https://www.life-line.ru/o-fonde/korporativnyy-klub/"
                                                                                                target="_blank">"ЛИНИЯ
                        ЖИЗНИ"</a></p>
            </div>
            <div class="cert-wrapper" id="show-cert">

            </div>
        </div>
    </div>
</div>

<script>
    var show = document.getElementById('show-cert');
    show.addEventListener('click', function () {
        document.querySelector('.certificate-overlay').style.display = 'flex';
    });
    document.querySelector('.close-popup').addEventListener('click', function () {
        document.querySelector('.certificate-overlay').style.display = 'none';
    });
    $(document).mouseup(function (e) {
        var popup = $('.cert-image');
        if (e.target != popup[0] && popup.has(e.target).length === 0) {
            $('.certificate-overlay').fadeOut();
        }
    });
</script>

<script>
    $(document).ready(function () {
        $(".enable-btn").click(function () {
            const monthForPicker = $(this).attr('data-month');
            $("#indicated-month").text(monthForPicker);

            $("#charity-report").modal("show");
        });
    });
</script>
