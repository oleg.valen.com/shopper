<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$paymentStatus = Yii::$app->params['paymentStatus'];

$this->title = 'Выплаты';

?>

<style>
    .disabled {
        color: #000;
    }

    .enabled {
        color: blue;
    }

    textarea.form-control {
        height: 100%;
        width: 100%;
        min-height: 220px;
    }

    .btn {
        margin-right: 10px;
        width: 100px;
    }

    .selector-p {
        display: none;
    }

    .modal-head {
        position: absolute;
    }

    button .close {
        position: relative;
    }

</style>

<script src="/js/libs/customSelect.js"></script>

<script>

    function changeSelect(e) {
        // $index = e.target.selectedIndex;
        // if( $index > 1 ) {
        //     filter = $( $(e.target).find('option')[$index] ).attr('data-flitter-parm');
        //     console.log( filter );
        //     $.ajax({
        //         type: 'POST',
        //         url: '/admin/payment',
        //         data: {
        //             filter: filter,
        //         },
        //         success: function (data) {
        //             console.log(data);
        //         },
        //         error: function (data) {
        //
        //         }
        //     });
        // }

        $index = e.target.selectedIndex;
        if ($index !== 0) {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            if ($href !== '' || $href !== 'undefind') {
                window.location.href = $href;
            }
        }
    }
</script>

<!--модальное окно подтверждения действий админа-->
<div>
    <?php Modal::begin([
        'header' => '<h4 class="modal-head">Укажите причину изменений</h4>',
        'size' => 'modal-lg',
        'id' => 'admin-done',
        'options' => [
        ],
    ]);
    ?>

    <?php $form = ActiveForm::begin([
        'id' => 'comment-form',
        'action' => 'save',
//        'enableAjaxValidation' => true,
//        'validationUrl' => 'validate',
    ]) ?>

    <?= $form->field($commentModel, 'admin')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>
    <?= $form->field($commentModel, 'user')->hiddenInput(['value' => Yii::$app->session->get('targetUserId')])->label(false); ?>
    <?= $form->field($commentModel, 'description')->textarea()->label(false) ?>

    <?= Html::submitButton('Ок', ['class' => 'btn btn-success'/*, 'onclick' => 'sendAjax()'*/]) ?>
    <?= Html::a('Отмена', '/admin/', ['class' => 'btn btn-danger']) ?>

    <?php ActiveForm::end() ?>

    <?php Modal::end(); ?>
</div>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ВЫПЛАТЫ</p>
            </div>
            <!--<div class="col-lg-1 ml-auto">
                <div class="edit">
                    <img class="pencil-icon" src="/img/edit.png" alt="" onclick="pencil()">
                </div>
            </div>-->
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles-admin">
                                Сумма
                                <table class="sum-table">
                                    <tr>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('pay_rate') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('penalty') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('bonuse') ?></th>
                                        <th class="sum-column_title no-border">Итого</th>
                                    </tr>
                                </table>
                            </th>

                            <?php if (is_array($filter)) {
                                $filter = 'Статус платежа';
                            } ?>

                            <th class="titles-admin">
                                <div class="i-serrrlect-wrapper i-select-wrapper i-custom-select select-no-img">
                                    <select data-popup-open="popup-1" onchange="changeSelect( event )" name="q-2-8"
                                            id="q-2-2">
                                        <option data-hreff="#" datastelsoption value="Статус платежа"
                                                dataurl="/img/icons/1.png">Статус платежа
                                        </option>

                                        <option data-hreff="/admin/payment?filter=Оплачен" data-flitter-parm="Оплачен"
                                                selected datastelsoption value="<?= $filter ?>">Оплачен
                                        </option>

                                        <option data-hreff="/admin/payment?filter=Отклонен" data-flitter-parm="Отклонен"
                                                data-hreff="/admin/payment?filter=Отклонен" value="Отклонен"><span>Отклонен</span>
                                        </option>
                                        <option data-hreff="/admin/payment?filter=Ожидает выплату"
                                                data-flitter-parm="Ожидает выплату" value="Ожидает выплату"><a
                                                    class="btn" data-popup-open="popup-1">Ожидает выплату</a>
                                        <option data-hreff="/admin/payment?filter=Отправлен на оплату"
                                                data-flitter-parm="Отправлен на оплату" value="Отправлен на оплату">
                                            <span>Отправлен на оплату</span></option>
                                        <option data-hreff="/admin/payment?filter=Благотворительность"
                                                data-flitter-parm="Благотворительность" value="Благотворительность">
                                            <span>Благотворительность</span></option>
                                        <option data-hreff="/admin/payment"
                                                data-flitter-parm="Благотворительность" value="Все">
                                            <span>Все</span></option>
                                    </select>

                                </div>
                            </th>

                            <th class="titles-admin"><?= $dataProvider->sort->link('last_modified') ?></th>
                        </tr>

                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->visit_date)) ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->project ?></td>
                                <td class="table_data"><?= $item->location_city ?></td>
                                <td class="table_data">
                                    <table>
                                        <td class="sum_column_data"><?= $item->total_amount ?></td>
                                        <td class="sum_column_data"><?= $item->penalty ?></td>
                                        <td class="sum_column_data"><?= $item->bonuse ?></td>
                                        <td class="sum_column_data no-border"><?= $item->total_amount - $item->penalty + $item->bonuse ?></td>
                                    </table>
                                </td>
                                <?php if ($item->is_payed == 'Rejected') { ?>
                                    <td class='titles_items relative'>
                                        <div id='i-have-a-tooltip' data-description='Если Вам не ясна причина отмены платежа, обратитесь в службу поддержки ТП'>
                                            <span class='status_icons'>Отклонен<i class='refresh-button' data-id="<?= $item->id ?>"></i></span>
                                        </div>
                                    </td>
                                <?php } else { ?>
                                    <td class='titles_items'><?= $paymentStatus[$item->is_payed] ?? $item->is_payed ?></td>
                                <?php } ?>

                                <?php if ($item->is_payed == 'Charity') { ?>
                                    <button class="selector-p" data-id="<?= $item->id ?>">
                                        Отменить
                                    </button>
                                <?php } ?>

                                <!--                                <script>-->
                                <!--                                    if ($('.selector-p').text() != 'Благотворительность') {-->
                                <!--                                        $('.selector-p').css("display", "none");-->
                                <!--                                    } else {-->
                                <!--                                        $('.selector-p').css("display", "none");-->
                                <!--                                    }-->
                                <!--                                </script>-->

                                <!--<td class="table_data"><?/*= $item->payment_method */?></td>-->

                                <td class="titles_items"><?= date_create($item->last_modified)->Format('d.m.Y') ?></td>

                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                    </table>


                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <script>
            setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
        </script>

        <?php Pjax::end() ?>

    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buttons-wrapper">

                <?php $form = ActiveForm::begin(['options' => [
                    'id' => 'upform',
                    'enctype' => 'multipart/form-data',]
                ])
                ?>

                <?= $form->field($uploadModel, 'xlsFile')->fileInput(['style' => 'display: none'])->label(false) ?>

                <?= Html::Button('Сохранить', [
                    'id' => 'save-changes',
                    'class' => 'save-btn',
                ]) ?>

                <?php ActiveForm::end() ?>

                <?= Html::a('<button class="download-btn">Загрузить </button>',
                    ['excel/export-excel', 'type' => 'paym', 'userId' => Yii::$app->session->get('target-user-id')],
                    ['onclick' => 'uploadForm(event)'])
                ?>

            </div>
        </div>
    </div>
</div>

<!--комментарии администратора-->
<?php if ($adminComments) { ?>

    <section class="visits-table">
        <div class="container">
            <div class="row section-margin">
                <div class="col-lg-6">
                    <p class="title-size" style="text-transform: uppercase">Последние действия администратора</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-over-wrapp">
                        <table id="pjax-table" class="table-with-visits">
                            <tr class="lite-green">
                                <th class="titles-admin"><?= 'Дата' ?></th>
                                <th class="titles-admin"><?= 'Администратор' ?></th>
                                <th class="titles-admin"><?= 'Пользователь' ?></th>
                                <th class="titles-admin"><?= 'Комментарий' ?></th>
                            </tr>

                            <?php $i = 1 ?>

                            <?php foreach ($adminComments as $item) { ?>

                                <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                    <td class="table_data"><?= $item['date'] ?></td>
                                    <td class="table_data"><?= $users[$item['admin']]['username'] ?></td>
                                    <!--                                    <td class="table_data">-->
                                    <? //= $users[$item['user']]['username'] ?><!--</td>-->
                                    <td class="table_data"><?= $item['description'] ?></td>
                                </tr>
                                <?php $i++ ?>
                            <?php } ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } ?>

<script>
    // поведение при нажатии на карандаш
    function pencil() {
        const pencilStatus = $(".selector-p").css("display");
        if (pencilStatus === "block") {
            $(".selector-p").css("display", "none");
            $(".selector-status").css("display", "block");
        } else {
            $(".selector-p").css("display", "block");
            $(".selector-status").css("display", "none");
        }
    }

    // поведение при нажатии на карандаш
    // function pencil() {
    //     if ($("a.visitstatus").hasClass("disabled")) {
    //         $("a.visitstatus").removeClass('disabled').addClass('enabled');
    //         $("a.visitstatus").attr("onClick", "");
    //     } else {
    //         $("a.visitstatus").css("onclick", "true");
    //         $("a.visitstatus").removeClass('enabled').addClass('disabled');
    //         $("a.visitstatus").attr("onClick", "return false");
    //     }
    // }

    // $("a.visitstatus").attr("onClick", "return false");

    // при пустом поле user не производить экспорт данных
    function uploadForm(event) {
        let userField = document.getElementById('user-field');
        if (userField.innerHTML.length < 1) {
            event.preventDefault();
            alert('Для этого действия необходимо выбрать пользователя!');
            return false;
        }
    }

    //все действия для передачи в контроллер
    var actions = [];

    $(function () {
        let id;
        $(".refresh-button").on('click', function () {
            id = $(this).data('id');
            $(this).parent().parent().text('Ожидает выплату');

            // console.log($(this).parent().prop("classList"))
            //добюавили значение в массив действий
            actions.push([id]);
        })
    })

    // действие при нажатии на кнопку сохранения
    $("#save-changes").click(function () {
        // alert(actions);
        if (actions.length > 0) {
            $("#admin-done").modal("show");
        }

    })

    // document.getElementById('uploadform-xlsfile').onchange = function (event) {
    //
    //     const formData = new FormData($('#upform')[0]);
    //
    //     $.ajax({
    //         type: 'post',
    //         url: '/admin/payment',
    //         data: formData,
    //         cache: false,
    //         contentType: false,
    //         processData: false,
    //         success: function (data) {
    //             console.log('Proceed successfully');
    //         },
    //         error: function (data) {
    //             console.log(data, 'Proceed error');
    //         }
    //     });
    //
    //     return true;
    // }

    // отправка форм на сервер
    $('#comment-form').on('beforeSubmit', function () {
        var data = $(this).serialize();
        // отправляем данные на сервер
        $.ajax({
            type: 'POST',
            url: '/admin/save',
            data: data,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            }
        }).done(function (data) {
            // данные сохранены
            console.log('success ', data);

            //при успешном ответе отправить ajax с вариантами ответов
            sendAjax();

            //прячем модальное  окно
            $('#admin-done').modal('hide');

            //меняем отображение полей
            $(".selector-p").css("display", "block");
            $(".selector-status").css("display", "none");
        }).fail(function () {
            // не удалось выполнить запрос к серверу
            console.log('Запрос не принят');
        });

        return false;
    });

    //отправка собранных данных на сервер
    function sendAjax() {

        $.ajax({
            type: 'post',
            url: '/admin/payment',
            data: {
                actions
            },
            success: function (response) {
                $.pjax.reload({container: '#content', async: false});
                console.log('Proceed successfully' + response);
            },
            error: function (response) {
                console.log(response, 'Proceed error');
            }
        });
    }

    //
    $(function () {
        $('.selector-p').on('click', function (e) {
            // function cancelCharity() {
            var id = $(this).attr('data-id');
            console.log(id);
            $.ajax({
                type: 'post',
                url: '/admin/site/cancel-charity',
                data: {
                    id
                },
                success: function (response) {
                    $.pjax.reload({container: '#content', async: false});
                    console.log('Cancelling successfully' + response);
                },
                error: function (response) {
                    console.log(response, 'Proceed error');
                }
            });
        });
    });

</script>

