<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Visit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'visit_date')->textInput() ?>

    <?= $form->field($model, 'anketa_id')->textInput() ?>

    <?= $form->field($model, 'survey_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_state_region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pay_rate')->textInput() ?>

    <?= $form->field($model, 'penalty')->textInput() ?>

    <?= $form->field($model, 'bonuse')->textInput() ?>

    <?= $form->field($model, 'accumulated_bonuse')->textInput() ?>

    <?= $form->field($model, 'payment_method')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_payed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payed_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visit_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deposit_term')->textInput() ?>

    <?= $form->field($model, 'deposit_left')->textInput() ?>

    <?= $form->field($model, 'management')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_payment_status_changed')->textInput() ?>

    <?= $form->field($model, 'last_modified')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
