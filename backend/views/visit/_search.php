<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VisitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'visit_date') ?>

    <?= $form->field($model, 'anketa_id') ?>

    <?= $form->field($model, 'survey_title') ?>

    <?php // echo $form->field($model, 'client_name') ?>

    <?php // echo $form->field($model, 'location_name') ?>

    <?php // echo $form->field($model, 'location_state_region') ?>

    <?php // echo $form->field($model, 'location_city') ?>

    <?php // echo $form->field($model, 'location_address') ?>

    <?php // echo $form->field($model, 'pay_rate') ?>

    <?php // echo $form->field($model, 'penalty') ?>

    <?php // echo $form->field($model, 'payroll_currency') ?>

    <?php // echo $form->field($model, 'total_amount') ?>

    <?php // echo $form->field($model, 'payroll_items') ?>

    <?php // echo $form->field($model, 'bonuse') ?>

    <?php // echo $form->field($model, 'accumulated_bonuse') ?>

    <?php // echo $form->field($model, 'payment_method') ?>

    <?php // echo $form->field($model, 'is_payed') ?>

    <?php // echo $form->field($model, 'payed_amount') ?>

    <?php // echo $form->field($model, 'visit_status') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'is_prompt_payment') ?>

    <?php // echo $form->field($model, 'deposit_term') ?>

    <?php // echo $form->field($model, 'deposit_left') ?>

    <?php // echo $form->field($model, 'management') ?>

    <?php // echo $form->field($model, 'project') ?>

    <?php // echo $form->field($model, 'is_payment_status_changed') ?>

    <?php // echo $form->field($model, 'work_flow_status') ?>

    <?php // echo $form->field($model, 'survey_status') ?>

    <?php // echo $form->field($model, 'accumulation_start_date') ?>

    <?php // echo $form->field($model, 'last_modified') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'first_import_date') ?>

    <?php // echo $form->field($model, 'sent_wallet') ?>

    <?php // echo $form->field($model, 'sent_charity') ?>

    <?php // echo $form->field($model, 'sent_pay') ?>

    <?php // echo $form->field($model, 'delete_wallet') ?>

    <?php // echo $form->field($model, 'pat_payment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
