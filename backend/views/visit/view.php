<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Visit */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Visits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="visit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'visit_date',
            'anketa_id',
            'survey_title:ntext',
            'client_name',
            'location_name',
            'location_state_region',
            'location_city',
            'location_address:ntext',
            'pay_rate',
            'penalty',
            'payroll_currency',
            'total_amount',
            'payroll_items:ntext',
            'bonuse',
            'accumulated_bonuse',
            'payment_method',
            'is_payed',
            'payed_amount',
            'visit_status',
            'payment_status',
            'is_prompt_payment',
            'deposit_term',
            'deposit_left',
            'management',
            'project',
            'is_payment_status_changed',
            'work_flow_status',
            'survey_status',
            'accumulation_start_date',
            'last_modified',
            'created_at',
            'updated_at',
            'first_import_date',
            'sent_wallet',
            'sent_charity',
            'sent_pay',
            'delete_wallet',
            'pat_payment',
        ],
    ]) ?>

</div>
