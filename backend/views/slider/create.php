<?php

use yii\helpers\Html;

$this->title = 'Новый слайд';

?>
<style>
    .slider-create {
        padding: 20px 0px;
        min-height: 450px;
    }

    .btn {
        border-radius: 0px;
        padding: 12px;
        font-size: 14px;
    }
</style>

<div class="container">
<div class="slider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>