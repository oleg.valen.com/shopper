<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Управление слайдером';

?>

<style>
    .slider-index {
        padding: 20px 0px;
        min-height: 450px;
    }

    .btn {
        border-radius: 0px;
        padding: 12px;
        font-size: 14px;
    }
</style>

<div class="container">
    <div class="slider-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Добавить слайд', ['create'], ['class' => 'btn btn-success btn-xl', 'style' => 'margin-bottom: 20px']) ?>
        </p>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'filename',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img('/uploads/' . $data['filename'],
                            ['height' => '30px']);
                    },
                ],
                [
                    'attribute' => 'date',
                    'label' => 'Дата размещения',
                    'value' => function($data) {
                        return date('d-m-Y', strtotime($data['date']));
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->status ? 'Показывать' : 'Спрятать';
                    }
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Управление',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-screenshot"></span>',
                                $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('Удалить', $url);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('Изменить', $url);
                        },
                    ],
                    'template' => '{update} {delete}',
                ],
//                ['class' => 'yii\grid\ActionColumn',
//                    'header' => 'Управление',
////                    'template' => '{update} {delete}',
//                ],

            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>