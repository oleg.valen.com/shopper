<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = 'Редактировать изображение: ' . $model->id;

?>

<style>
    .slider-update {
        padding: 20px 0px;
        min-height: 450px;
    }

    .btn {
        border-radius: 0px;
        padding: 12px;
        font-size: 14px;
    }
</style>

<div class="container">
    <div class="slider-update">

        <h1><?= Html::encode($this->title) ?></h1>
        <?= Html::img('/uploads/' . $model->filename, [
            'height' => '150px',
            'width' => '1110px',
            'alt' => 'Slider Image'
        ]) ?>
        <?= $this->render('_form', ['model' => $model,]) ?>

    </div>
</div>