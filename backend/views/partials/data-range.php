<?php

use kartik\daterange\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="filters">

    <?php $form = ActiveForm::begin([
            'id' => 'date-selector',
        'action' => [$page],
        'method' => 'post',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <!--        блок выбора даты-->
    <div class="box">
        <?php

        $filterModel->date = $selectedDate;

        echo $form->field($filterModel, 'date', [
            'options' => ['class' => 'drp-container form-group']])
            ->widget(DateRangePicker::classname(), [
                'convertFormat' => true,
                'value' => $selectedDate,
                'presetDropdown' => false,
                'hideInput' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y',
                        'separator' => ' - ',
                    ],
                    'opens' => 'right',
                    'ranges' => [
                        'Сегодня' => ["moment().startOf('Today')", "moment().endOf('Today')"],
                        'Вчера' => ["moment().startOf('day').subtract(1, 'day')", "moment().subtract(1, 'day').endOf('day')"],
                        'Эта неделя' => ["moment().startOf('week')", "moment().endOf('week')"],
                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"],
                    ],
                ],

            ])
            ->label(false);
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script>
        $('input#select-date').change(function () {
            $("#date-selector").submit()
        });
    </script>
</div>
