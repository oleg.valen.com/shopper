<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Log */

$this->title = 'Редактировать вопрос/ответ №: ' . $model->id;
?>
<div class="container">
    <div class="log-update page-box">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
<style>
    .form-control{
        width: 100%;
        resize: none;
    }
    #faq-status{
        width: 30%;
        min-width: 290px;
        margin: 0;
    }
</style>
