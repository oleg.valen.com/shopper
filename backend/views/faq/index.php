<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы и ответы';
?>
<div class="container faq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Новый вопрос/ответ', ['create'], ['class' => 'btn btn-success', 'style' => 'margin: 15px 0px']) ?>
    </p>
    <div class="table-over-wrapp">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout'=>"{summary}\n{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'question:ntext',
                [
                    'attribute' => 'answer',
                    'value' => function ($data) {
                        return mb_substr($data->answer, 0, 180) . '...';
                    },
                    'format' => 'html'
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($data) {
                        return $data ? 'Показывать' : 'Скрыть';
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view1} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => 'View',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', $url, [
                                'title' => 'Update',
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', $url, [
                                'title' => 'Delete',
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => 'Вы уверены что хотите удалить этот вопрос?',
                                ]
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>

        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
            'hideOnSinglePage' => true,
            'prevPageLabel' => '&laquo; ',
            'nextPageLabel' => ' &raquo;',

            'linkOptions' => ['class' => 'page-number'],
            'activePageCssClass' => 'active-page',
        ]);
        ?>

    </div>

</div>
