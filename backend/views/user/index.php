<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление пользователями';

?>

<style>

    .table-bordered {
        border: 1px solid #ddd;
    }

    .user-page {
        padding: 20px 0px;
        min-height: 450px;
    }

    .btn {
        border-radius: 0px;
        padding: 12px;
        font-size: 14px;
    }
</style>

<div class="container">
    <div class="user-page">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= Html::a('Добавить нового пользователя', ['create'],
            ['class' => 'btn btn-success btn-xl', 'style' => 'margin-bottom: 20px']) ?>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered'
            ],
            'columns' => [

                'username',
                'email:email',
                [
                    'attribute' => 'role',
                    'value' => function ($data) {
                        if ($data->role == 'admin') {
                            $result = 'Администратор';
                        } else if ($data->role == 'user') {
                            $result = 'Пользователь';
                        }

                        return ($result);
                    },
                    'format' => 'html',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Управление',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-screenshot"></span>',
                                $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('Удалить', $url);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('Редактировать', $url);
                        },
                    ],
                    'template' => '{update} {delete}',
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>