<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Добавить пользователя';

?>
<div class="container">
<div class="user-create" style="padding: 20px">

    <h3><?= $this->title ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>