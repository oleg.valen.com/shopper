<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

$visitStatus = Yii::$app->params['visitStatus'];

?>

<style>
    .disabled {
        color: #000;
    }

    .enabled {
        color: blue;
    }

    textarea.form-control {
        height: 100%;
        width: 100%;
        min-height: 220px;
    }

    .btn {
        margin-right: 10px;
        width: 100px;
    }

    .selector-p {
        display: block;
    }

    .modal-head {
        position: absolute;
    }

    button .close {
        position: relative;
    }

    .submit-buttons {
        padding-bottom: 10px;
    }

    .modal-content {
        padding: 5px;
    }
</style>

<section class="calc section-margin">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/calculator') ?>

        </div>
    </div>
</section>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ТАБЛИЦА ВИЗИТОВ</p>
            </div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label(false); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
        </div>

        <script src="/js/libs/customSelect.js"></script>

        <?php Pjax::begin(['id' => 'content']); ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles-admin">
                                Сумма
                                <table class="sum-table">
                                    <tr>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('pay_rate') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('penalty') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('bonuse') ?></th>
                                        <th class="sum-column_title no-border"><?= $dataProvider->sort->link('total_amount') ?></th>
                                    </tr>
                                </table>
                            </th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('accumulated_bonuse') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_status') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('last_modified') ?></th>
                        </tr>

                        <?php $i = 1 ?>

                        <?php foreach ($visit as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->visit_date)) ?? '' ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="table_data"><?= $item->location_city . '<br>' . ($LocationStateRegion[$item->location_state_region] ?? '') . '<br>' . $item->location_address ?></td>
                                <td class="table_data">
                                    <table>
                                        <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                        <td class="sum_column_data"><?= $item->penalty ?></td>
                                        <td class="sum_column_data"><?= $item->bonuse + $item->wait_bonuse - $item->wait_bonuse_sm ?></td>
                                        <td class="sum_column_data no-border"><?= $item->total_amount + $item->wait_bonuse - $item->wait_bonuse_sm ?></td>
                                    </table>
                                </td>

                                <!--определение даты бонуса-->
                                <?php
                                $startDate = strtotime($item->visit_date);
                                $target_date = date('d.m.Y', strtotime("+{$item->deposit_term} day", $startDate));
                                ?>

                                <td class="table_data"><?= $item->accumulated_bonuse ?> RUB</td>
                                <td class="table_data" style="<?= $item->is_prompt_payment ? 'background-color: yellow' : '' ?>">
                                    <p class="selector-p">
                                        <?= $visitStatus[$item->visit_status] ?? $item->visit_status ?>
                                    </p>
                                    <select data-id="<?= $item->anketa_id ?>"
                                            class="selector-status"
                                            style="display: none">
                                        <option value="<?= $visitStatus[$item->visit_status] ?? '-'?>">
                                            <?= isset($visitStatus[$item->visit_status]) && $visitStatus[$item->visit_status] == "Validation"
                                                ? $visitStatus["Validation"]
                                                : $visitStatus["Completed"] ?>
                                        </option>
                                        <option value="<?= isset($visitStatus[$item->visit_status]) && $item->visit_status == "Validation" ? $visitStatus["Completed"] : $visitStatus["Validation"] ?>">
                                            <?= isset($visitStatus[$item->visit_status]) && $visitStatus[$item->visit_status] == "Validation" ? $visitStatus["Completed"] : $visitStatus["Validation"] ?>
                                        </option>
                                    </select>
                                </td>
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->last_modified)) ?></td>

                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                    </table>

                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <script>
            setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
        </script>

        <?php Pjax::end(); ?>

    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buttons-wrapper">

                <?= Html::a('<button class="download-btn">Выгрузить </button>',
                    ['excel/export-excel', 'type' => 'index', 'userId' => Yii::$app->session->get('target-user-id')],
                    ['onclick' => 'uploadForm(event)'])
                ?>

            </div>
        </div>
    </div>
</div>

<?php /*if ($adminComments) { */?><!--

    <section class="visits-table">
        <div class="container">
            <div class="row section-margin">
                <div class="col-lg-6">
                    <p class="title-size" style="text-transform: uppercase">Последние действия администратора</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-over-wrapp">
                        <table id="pjax-table" class="table-with-visits">
                            <tr class="lite-green">
                                <th class="titles-admin"><?/*= 'Дата' */?></th>
                                <th class="titles-admin"><?/*= 'Администратор' */?></th>
                                <th class="titles-admin"><?/*= 'Пользователь' */?></th>
                                <th class="titles-admin"><?/*= 'Комментарий' */?></th>
                            </tr>

                            <?php /*$i = 1 */?>
                            <?php /*foreach ($adminComments as $item) { */?>
                                <tr class="<?/*= $i % 2 == 0 ? 'light-grren' : '' */?>">
                                    <td class="table_data"><?/*= date('d-m-Y H:i:s', strtotime($item['date'])) */?></td>
                                    <td class="table_data"><?/*= $users[$item['admin']]['username'] ?? 'Admin' */?></td>
                                    <td class="table_data"><?/*= $users[$item['user']]['username'] */?></td>
                                    <td class="table_data"><?/*= $item['description'] */?></td>
                                </tr>
                                <?php /*$i++ */?>
                            <?php /*} */?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

--><?php /*} */?>

<script>
    Array.from(document.querySelectorAll('.table-with-visits tr td.summa')).map((v) => {
        v.addEventListener('mouseover', function (e) {
            var rect = this.getBoundingClientRect(),
                tooltip = document.querySelector('.tooltip-table-with-visits');

            tooltip.querySelector('.surcharge').innerHTML = this.getAttribute('data-surcharge');
            tooltip.querySelector('.persent').innerHTML = this.getAttribute('data-persent');

            tooltip.style.opacity = '1';
            tooltip.style.left = this.offsetLeft + rect.width / 2 + 'px';
            tooltip.style.top = this.offsetTop + rect.height / 2 + 'px';

        });
        v.addEventListener('mouseout', function (e) {
            var rect = this.getBoundingClientRect(),
                tooltip = document.querySelector('.tooltip-table-with-visits');

            tooltip.style.opacity = '0';
        });
    });
</script>

<script>
    // поведение при нажатии на карандаш
    function pencil() {
        const pencilStatus = $(".selector-p").css("display");
        if (pencilStatus === "block") {
            $(".selector-p").css("display", "none");
            $(".selector-status").css("display", "block");
        } else {
            $(".selector-p").css("display", "block");
            $(".selector-status").css("display", "none");
        }
    }

    //все действия для передачи в контроллер
    var actions = [];

    $(function () {

        let newValue, id;

        $(".selector-status").change(function () {

            id = $(this).data('id');
            newValue = $(this).val();

            //добюавили значение в массив действий
            actions.push([id, newValue]);
        })
    })

    // действие при нажатии на кнопку сохранения
    $("#save-changes").click(function () {
        // alert('actions');
        if (actions.length > 0) {
            $("#admin-done").modal("show");
        }

    })

    // проверка поля user

    // при пустом поле user не производить экспорт данных
    function uploadForm(event) {
        let userField = document.getElementById('user-field');
        if (userField.innerHTML.length < 1) {
            event.preventDefault();
            alert('Для этого действия необходимо выбрать пользователя!');
            return false;
        }
    }

    function showPopUp(e) {
        if (e.target.value === 'Вывести средства') {
            var targeted_popup_class = jQuery(e.target).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

            e.preventDefault();
        }
    }
</script>

<!--отправка файла на сервер-->
<script>
    document.getElementById('uploadform-xlsfile').onchange = function (event) {

        const formData = new FormData($('#upform')[0]);

        $.ajax({
            type: 'post',
            url: '/admin/index',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log('Proceed successfully');
            },
            error: function (data) {
                console.log(data, 'Proceed error');
            }
        });

        return true;
    }

    // отправка форм на сервер
    $('#comment-form').on('beforeSubmit', function () {
        var data = $(this).serialize();

        // отправляем данные на сервер
        $.ajax({
            type: 'POST',
            url: '/admin/site/save',
            data: data,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            }
        }).done(function (data) {

                // данные сохранены
                console.log('success ', data);

                //при успешном ответе отправить ajax с вариантами ответов
                sendAjax();

                //прячем модальное  окно
                $('#admin-done').modal('hide');

                //меняем отображение полей
                $(".selector-p").css("display", "block");
                $(".selector-status").css("display", "none");
            })
            .fail(function () {
                // не удалось выполнить запрос к серверу
                console.log('Запрос не принят');
            })

        return false;
    })

    function sendAjax() {

        $.ajax({
            type: 'post',
            url: '/admin/index',
            data: {
                actions
            },
            success: function (response) {
                $.pjax.reload({container: '#content', async: false});
                console.log('Proceed successfully' + response);
            },
            error: function (response) {
                console.log(response, 'Proceed error');
            }
        });
    }
</script>
