<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

$summaryData['period'] = $searchModel->dateSentToPay ?? '';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-md-3 "></div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label('Дата визита'); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?php if (!empty($searchModel->date)) { ?>
                    <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-monthly-report'], [
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                'data' => json_encode(isset($searchModel->date) ? $searchModel->date : []),
                            ],
                        ]
                    ]); ?>
                <?php } ?>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin">Всего подлежит оплате Шоппера за отчетный месяц</th>
                            <th class="titles-admin">Всего массовые гиперы</th>
                            <th class="titles-admin">Всего массовые классические</th>
                            <th class="titles-admin">Всего сумма по визитам, которая была в накоплении хотя бы 1 день
                            </th>
                            <th class="titles-admin">Сумма которая остается в накоплении на день формирования отчета
                            </th>
                            <th class="titles-admin">Сумма бонусов в накоплении</th>
                            <th class="titles-admin">Сумма бонусов, которые уже не накапливаются</th>
                            <th class="titles-admin">Сумма бонусов, которая выведена ТП</th>
                        </tr>
                        <?php if ($summaryData): ?>
                            <tr class="lite-green">
                                <td class="table_data"><?= number_format($summaryData['total_amount'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['sum_gipper'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['sum_ordinary'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['accumulated_bonuse'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['accumulated_sum_current'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['accumulated_bonuse_not_payed'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['bonuses_to_user_amount'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($summaryData['bonuse_out'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                            </tr>
                        <?php endif; ?>
                    </table>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]); ?>

                </div>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>
