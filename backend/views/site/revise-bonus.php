<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\models\VisitStatus;
use common\models\IsPayed;

$this->title = 'Административная панель';

$summaryData['period'] = $searchModel->dateSentToPay ?? '';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-revise-bonus'], [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'data' => '',
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin">Username</th>
                            <th class="titles-admin">User id</th>
                            <th class="titles-admin">Anketa id</th>
                            <th class="titles-admin">Accum. bonus</th>
                            <th class="titles-admin">Visit date</th>
                            <th class="titles-admin">Sent to pay</th>
                            <th class="titles-admin">Accum. start date</th>
                            <th class="titles-admin">Date diff</th>
                            <th class="titles-admin">Bonus persent</th>
                            <th class="titles-admin">Amount calc</th>
                            <th class="titles-admin">Amount diff</th>
                            <th class="titles-admin">Total amount</th>
                            <th class="titles-admin">Visit status</th>
                            <th class="titles-admin">Is payed</th>
                        </tr>
                        <?php foreach ($data as $item): ?>
                            <tr class="lite-green">
                                <td class="table_data"><?= $item['username'] ?></td>
                                <td class="table_data"><?= $item['user_id'] ?></td>
                                <td class="table_data"><?= $item['anketa_id'] ?></td>
                                <td class="table_data"><?= number_format($item['accumulated_bonuse'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= $item['visit_date'] ?></td>
                                <td class="table_data"><?= $item['sent_to_pay'] ?></td>
                                <td class="table_data"><?= $item['accumulation_start_date'] ?></td>
                                <td class="table_data"><?= $item['date_diff'] ?></td>
                                <td class="table_data"><?= number_format($item['bonuse_persent'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($item['amount_calc'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($item['amount_diff'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= number_format($item['total_amount'] ?? 0, 2, ',', ' ') ?? '' ?></td>
                                <td class="table_data"><?= VisitStatus::$values[$item['visit_status']] ?></td>
                                <td class="table_data"><?= IsPayed::$values[$item['is_payed']] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="lite-green">
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"><?= number_format($total['accumulatedBonuse'] ?? 0, 2, ',', ' ') ?? '' ?></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                            <th class="table_data"><?= number_format($total['amountCalc'] ?? 0, 2, ',', ' ') ?? '' ?></th>
                            <th class="table_data"><?= number_format($total['amountDiff'] ?? 0, 2, ',', ' ') ?? '' ?></th>
                            <th class="table_data"><?= number_format($total['totalAmount'] ?? 0, 2, ',', ' ') ?? '' ?></th>
                            <th class="table_data"></th>
                            <th class="table_data"></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>
