<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;

?>

<style>
    .site-error {
        min-height: 300px;
    }

    .error-message {
        margin-top: 100px;
        text-align: center;
        font-size: x-large;
    }

</style>

<div class="site-error">

    <h1 class="error-message"><?= 'Ошибка ' . $exception->statusCode . ' ' . nl2br(Html::encode($message)); ?></h1>

</div>
