<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-md-3 "></div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'dates', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->dates,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label(false); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-bonuse'], [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'dates' => json_encode(isset($searchModel->dates) ? $searchModel->dates : []),
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <?php $color = 1 ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('payment_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('login') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('amount') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('payment_anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketas_data') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('deadline_date') ?></th>
                        </tr>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $color % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= $item->id ?></td>
                                <td class="table_data"><?= date('d.m.Y H:i:s', $item->created_at + 3600*3) ?? '' ?></td>
                                <td class="table_data"><?= $item->login ?></td>
                                <td class="table_data"><?= $item->amount ?></td>
                                <td class="table_data"><?= $item->payment_anketa_id ?></td>
                                <td class="table_data"><?= $item->anketas_data ?></td>
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->deadline_date)) ?? '' ?></td>
                            </tr>
                            <?php $color++ ?>
                        <?php } ?>
                    </table>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]); ?>

                </div>
            </div>
        </div>
        <?php Pjax::end(); ?>

    </div>
</section>
