<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="table-over-wrapp">
                <table class="table-with-visits">
                    <tr class="lite-green">
                        <th class="titles-admin"><?= Html::a('Дата визита', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Номер анкеты', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Проект', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Адрес локации', ['admin/index']) ?></th>
                        <th class="titles-admin">
                            Сумма
                            <table class="sum-table">
                                <tr>
                                    <th class="sum-column_title"><?= Html::a('За визит', ['admin/index']) ?></th>
                                    <th class="sum-column_title"><?= Html::a('Штраф', ['admin/index']) ?></th>
                                    <th class="sum-column_title"><?= Html::a('Доплата', ['admin/index']) ?></th>
                                    <th class="sum-column_title no-border">Итого</th>
                                </tr>
                            </table>
                        </th>
                        <th class="titles-admin"><?= Html::a('Бонус', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Статус визита', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Управление  средствами', ['admin/index']) ?></th>
                        <th class="titles-admin"><?= Html::a('Дата смены визита', ['admin/index']) ?></th>
                    </tr>

                    <tr class="">
                        <td class="table_data"><?= date_create($model->visit_date)->Format('d.m.Y') ?></td>
                        <td class="table_data"><?= $model->anketa_id ?></td>
                        <td class="table_data"><?= $model->project ?></td>
                        <td class="table_data"><?= $model->location ?></td>
                        <td class="table_data">
                            <table>
                                <td class="sum_column_data"><?= $model->amount ?></td>
                                <td class="sum_column_data"><?= $model->penalty ?></td>
                                <td class="sum_column_data"><?= $model->extra_charge ?></td>
                                <td class="sum_column_data no-border"><?= $model->amount - $model->penalty + $model->extra_charge ?></td>
                            </table>
                        </td>

                        <!--определение даты бонуса-->
                        <?php
                        $startDate = strtotime($model->last_modified);
                        $target_date = date('d.m.Y', strtotime("+{$model->deposit_term} day", $startDate));
                        ?>

                        <td class="table_data"><?= $target_date ?> <p>сумма составит:</p>
                            <p class="price"><?= $model->amount + $model->bonuse ?> RUB</p></td>
                        <td class="table_data"><?= $model->visit_status ?></td>
                        <td class="table_data"><?= $model->payment_status ?></td>
                        <td class="table_data"><?= date_create($model->last_modified)->Format('d.m.Y') ?></td>

                    </tr>

                </table>
            </div>
        </div>
    </div>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'visit_status')->dropDownList([
        'Проверка' => 'Проверка',
        'Одобрено' => 'Одобрено',
    ]) ?>

    <!--    <? //= $form->field($model, 'payment_status')->dropDownList([
    //        '0' => '---',
    //        'Оплачен' => 'Оплачен',
    //        'Отправлен на оплату' => 'Отправлен на оплату',
    //        'Ожидает выплату' => 'Ожидает выплату',
    //        'Вывод средств' => 'Вывод средств',
    //        'Благотворительность' => 'Благотворительность',
    //        'Отклонен' => 'Отклонен',
    //        'Выплатить срочно' => 'Выплатить срочно',
    //    ]) ?> -->

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
