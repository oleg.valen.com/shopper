<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

$summaryData['period'] = $searchModel->dateSentToPay ?? '';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">

        <div class="row section-margin">
            <div class="col-md-3 "></div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'dateSentToPay', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->dateSentToPay,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label('Дата оплаты'); ?>
                    </div>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label('Дата виизита'); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-mass'], [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'data' => json_encode(isset($summaryData) ? $summaryData : []),
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp" style="min-height: 20px">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin">Период</th>
                            <th class="titles-admin">Сумма анкет, поданных на оплату</th>
                            <th class="titles-admin">Сумма анкет с признаком "гипер"</th>
                            <th class="titles-admin">Сумма анкет без признака "гипер"</th>
                            <th class="titles-admin">Количество ТП, подавших на оплату</th>
                            <th class="titles-admin">Количество анкет, поданных на оплату</th>
                            <th class="titles-admin">Сумма выведенных из «копилки» бонусов</th>
                            <th class="titles-admin">Сумма бонусов, отправленных в «копилку»</th>
                        </tr>
                        <tr class="">
                            <td class="table_data"><?= $searchModel->dateSentToPay ?? '' ?></td>
                            <td class="table_data"><?= number_format($summaryData['sum'] ?? 0, 2, ',', ' '); ?></td>
                            <td class="table_data"><?= number_format($summaryData['sum_gipper'] ?? 0, 2, ',', ' '); ?></td>
                            <td class="table_data"><?= number_format($summaryData['sum_ordinary'] ?? 0, 2, ',', ' '); ?></td>
                            <td class="table_data"><?= $summaryData['tp'] ?? 0 ?></td>
                            <td class="table_data"><?= $summaryData['anketa'] ?? 0 ?></td>
                            <td class="table_data"><?= $summaryData['payed_wallet'] ?? 0 ?></td>
                            <td class="table_data"><?= $summaryData['wallet_bonuses'] ?? 0 ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <?php $color = 1 ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('is_prompt_payment') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('sent_to_pay') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('total_amount') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('accumulated_bonuse') ?></th>
                        </tr>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $color % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->is_prompt_payment ? '+' : '' ?></td>
                                <td class="table_data"><?= !empty($item->sent_to_pay) ? date('d.m.Y H:i:s', strtotime($item->sent_to_pay)) : '-' ?></td>
                                <td class="table_data"><?= $item->total_amount ?></td>
                                <td class="table_data"><?= $item->accumulated_bonuse ?></td>
                            </tr>
                            <?php $color++ ?>
                        <?php } ?>
                    </table>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]); ?>

                </div>
            </div>
        </div>



        <?php Pjax::end(); ?>

    </div>
</section>
