<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

$summaryData['period'] = $searchModel->dateSentToPay ?? '';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-md-3 "></div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label('Дата визита'); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?php if (!empty($searchModel->date)) { ?>
                    <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-array'], [
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                'data' => json_encode(isset($searchModel->date) ? $searchModel->date : []),
                            ],
                        ]
                    ]); ?>
                <?php } ?>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <?php $color = 1 ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin">Период</th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('pay_rate') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('bonuse') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('penalty') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('total_amount') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('is_prompt_payment') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('is_payed') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('sent_to_pay') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('accumulated_bonuse') ?></th>
                        </tr>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $color % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= $searchModel->date ?? '' ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->project ?? '' ?></td>
                                <td class="table_data"><?= $item->pay_rate ?? '' ?></td>
                                <td class="table_data"><?= $item->bonuse ?? '' ?></td>
                                <td class="table_data"><?= $item->penalty ?? '' ?></td>
                                <td class="table_data"><?= $item->total_amount ?></td>
                                <td class="table_data"><?= $item->is_prompt_payment ? 'Гипер' : '' ?></td>
                                <td class="table_data"><?= $item->is_payed == 'Not_payed' ? 'Накапливается' : 'Выведена на оплату' ?></td>
                                <td class="table_data"><?= !empty($item->sent_to_pay) ? date('d.m.Y', strtotime($item->sent_to_pay)) : '' ?></td>
                                <td class="table_data"><?= !empty($item->accumulated_bonuse) ? $item->accumulated_bonuse : '' ?></td>
                            </tr>
                            <?php $color++ ?>
                        <?php } ?>
                    </table>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]); ?>

                </div>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>
