<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Административная панель';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">

        <div class="row section-margin">
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-bonuse-statistick'], [
                    'data' => [
                        'method' => 'post',
                    ]
                ]); ?>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('user_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('username') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('email') ?></th>
                            <th class="titles-admin">Бонусы в накоплении</th>
                            <th class="titles-admin">Бонусов в копилке</th>
                            <th class="titles-admin">Сумма анкет, которая находится в накоплении</th>
                        </tr>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $item->id % 2 == 0 ? 'light-grren' : '' ?>">
                                <td class="table_data"><?= $item->user_id ?></td>
                                <td class="table_data"><?= $item->username ?></td>
                                <td class="table_data"><?= $item->email ?></td>
                                <td class="table_data"><?= $item->totalBonuses ?></td>
                                <td class="table_data"><?= $item->walletBonuses ?></td>
                                <td class="table_data"><?= $item->totalAccumulation ?></td>
                            </tr>
                        <?php } ?>
                    </table>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]); ?>

                </div>
            </div>
        </div>
        <?php Pjax::end(); ?>

    </div>
</section>
