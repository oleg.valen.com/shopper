<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Накопления';

$visitStatus = [
    'Completed' => 'Проверен',
    'Validation' => 'На проверке',
    'On Hold' => 'В процессе',
    'No items' => 'Неопределен',
    'Approved_to_withdrawal' => 'Одобрен для вывода',
];

?>

<style>
    .disabled {
        color: #000;
    }

    .enabled {
        color: blue;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size-accumulation">НАКОПЛЕНИЯ</p>
            </div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label(false); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
        </div>

        <?php Pjax::begin(); ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table class="table-with-visits">
                        <tr class="light-grren">
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles-admin">
                                Сумма
                                <table class="sum-table">
                                    <tr>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('pay_rate') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('penalty') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('bonuse') ?></th>
                                        <th class="sum-column_title no-border"><?= $dataProvider->sort->link('total_amount') ?></th>
                                    </tr>
                                </table>
                            </th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_status') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('is_payed') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('accumulated_bonuse') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('last_modified') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('deposit_term') ?></th>
                        </tr>

                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">

                                <td class="table_data"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->survey_title . "<br>" . $item->client_name . "<br>" . $item->location_name ?></td>
                                <td class="table_data"><?= $item->location_city . '<br>' . ($LocationStateRegion[$item->location_state_region] ?? '') . '<br>' . $item->location_address ?></td>
                                <td class="table_data">
                                    <table>
                                        <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                        <td class="sum_column_data"><?= $item->penalty ?></td>
                                        <td class="sum_column_data"><?= $item->bonuse ?></td>
                                        <td class="sum_column_data no-border"><?= $item->total_amount ?></td>
                                    </table>
                                </td>
                                <td class="table_data"><?= $visitStatus[$item->visit_status]  ?></p></td>
                                <td class="table_data acumulation">Накопление</td>
                                <td class="table_data"><?= $item->accumulated_bonuse ?? 0 ?></td>
                                <td class="table_data"><?= date('d.m.Y', strtotime($item->first_import_date)) ?></td>

                                <?php
                                $startDate = strtotime($item->last_modified);
                                $target_date = (strtotime("+{$item->deposit_term} day", $startDate));
                                $now_date = time();
                                $datediff = $target_date - $now_date;
                                $left = floor($datediff / (60 * 60 * 24)) > 0 ? floor($datediff / (60 * 60 * 24)) : '0';
                                ?>

                                <td class="table_data"><?= $left ?> дней</td>

                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                    </table>

                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',
                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <?php Pjax::end(); ?>

    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buttons-wrapper">

                <?= Html::a('<button class="download-btn">Выгрузить </button>',
                    ['excel/export-excel', 'type' => 'accum', 'userId' => Yii::$app->session->get('target-user-id')],
                    ['onclick' => 'uploadForm(event)'])
                ?>

            </div>
        </div>
    </div>
</div>

<script>
    // поведение при нажатии на карандаш
    function pencil() {
        if ($("a.visitstatus").hasClass("disabled")) {
            $("a.visitstatus").removeClass('disabled').addClass('enabled');
            $("a.visitstatus").attr("onClick", "");
        } else {
            $("a.visitstatus").css("onclick", "true");
            $("a.visitstatus").removeClass('enabled').addClass('disabled');
            $("a.visitstatus").attr("onClick", "return false");
        }
    }

    $("a.visitstatus").attr("onClick", "return false");

    // при пустом поле user не производить экспорт данных
    function uploadForm(event) {
        let userField = document.getElementById('user-field');
        if (userField.innerHTML.length < 1) {
            event.preventDefault();
            alert('Для этого действия необходимо выбрать пользователя!');
            return false;
        }
    }

    document.getElementById('uploadform-xlsfile').onchange = function (event) {

        const formData = new FormData($('#upform')[0]);

        $.ajax({
            type: 'post',
            url: '/admin/site/accumulation',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log('Proceed successfully');
            },
            error: function (data) {
                console.log(data, 'Proceed error');
            }
        });

        return true;
    }
</script>
