<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'АНКЕТЫ В НАКОПЛЕНИИ';

$summaryData['period'] = $searchModel->dateSentToPay ?? '';

?>

<style>
    .table-over-wrapp .table-with-visits .titles-admin {
        padding: 10px 10px 10px 10px !important;
    }
</style>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-md-3 "></div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>

                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label('Дата визита'); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-3 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Загрузить</button>', ['excel/export-mass'], [
                    'data' => [
                        'method' => 'post',
                        'params' => [
                            'data' => json_encode(isset($summaryData) ? $summaryData : []),
                        ],
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin">Дата формирования отчёта</th>
                            <th class="titles-admin">Период</th>
                            <th class="titles-admin">Сумма начислений за все визиты периода</th>
                            <th class="titles-admin">Сумма в накоплении за период</th>
                            <th class="titles-admin">Сумма бонусов за период</th>
                        </tr>
                        <tr class="">
                            <td class="table_data"><?= date('d.m.Y H:i:s') ?></td>
                            <td class="table_data" style="width: 20%"><?= $searchModel->date ?? '' ?></td>
                            <td class="table_data"><?= number_format($summaryData['total_amount'] ?? 0, 2, ',', ' '); ?></td>
                            <td class="table_data"><?= number_format($summaryData['sum_accumulating'] ?? 0, 2, ',', ' '); ?></td>
                            <td class="table_data"><?= number_format($summaryData['sum_bonuses'] ?? 0, 2, ',', ' '); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
