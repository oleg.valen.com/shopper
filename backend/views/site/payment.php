<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

$this->title = 'Выплаты';

?>

<style>
    .disabled {
        color: #000;
    }

    .enabled {
        color: blue;
    }

    textarea.form-control {
        height: 100%;
        width: 100%;
        min-height: 220px;
    }

    .btn {
        margin-right: 10px;
        width: 100px;
    }

    .selector-p {
        display: none;
    }

    .modal-head {
        position: absolute;
    }

    button .close {
        position: relative;
    }

</style>

<script src="/js/libs/customSelect.js"></script>

<script>

    function changeSelect(e) {
        $index = e.target.selectedIndex;
        if ($index !== 0) {
            $href = $($(e.target).find('option')[$index]).attr('data-hreff');
            if ($href !== '' || $href !== 'undefind') {
                window.location.href = $href;
            }
        }
    }
</script>

<section class="visits-table">
    <div class="container">
        <div class="row section-margin">
            <div class="col-lg-3">
                <p class="title-size">ВЫПЛАТЫ</p>
            </div>
            <div class="col-md-3 ">
                <div class="filters">
                    <?php $form = ActiveForm::begin([
                        'id' => 'date-selector',
                        'method' => 'get',
                        'options' => [
                            'data-pjax' => 1
                        ]
                    ]); ?>
                    <div class="box">
                        <?= $form->field($searchModel, 'date', [
                            'options' => ['class' => 'drp-container form-group']])
                            ->widget(DateRangePicker::classname(), [
                                'id' => 'select-date',
                                'convertFormat' => true,
                                'value' => $searchModel->date,
                                'presetDropdown' => false,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd-m-Y',
                                        'separator' => ' - ',
                                    ],
                                    'opens' => 'right',
                                    'ranges' => [
                                        'Этот месяц' => ["moment().startOf('month')", "moment().endOf('month')"],
                                        'Прошлый месяц' => ["moment().startOf('month').subtract(1, 'month')", "moment().subtract(1, 'months').endOf('month')"],
                                        'Этот год' => ["moment().startOf('year')", "moment().endOf('year')"]
                                    ]
                                ]
                            ])->label(false); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Подобрать', ['class' => 'btn btn-primary', 'style' => 'visibility: hidden']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <script>
                        $('#date-selector').change(function () {
                            $("#date-selector").submit()
                        });
                    </script>
                </div>
            </div>
        </div>

        <?php Pjax::begin(['id' => 'content']); ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-over-wrapp">
                    <table id="pjax-table" class="table-with-visits">
                        <tr class="lite-green">
                            <th class="titles-admin"><?= $dataProvider->sort->link('visit_date') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('anketa_id') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('project') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('location_city') ?></th>
                            <th class="titles-admin">
                                Сумма
                                <table class="sum-table">
                                    <tr>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('pay_rate') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('penalty') ?></th>
                                        <th class="sum-column_title"><?= $dataProvider->sort->link('bonuse') ?></th>
                                        <th class="sum-column_title no-border"><?= $dataProvider->sort->link('total_amount') ?></th>
                                    </tr>
                                </table>
                            </th>

                            <?php $paymentStatus = Yii::$app->params['paymentStatus']; ?>

                            <th class="titles-admin"><?= $dataProvider->sort->link('is_payed') ?></th>
                            <th class="titles-admin"><?= $dataProvider->sort->link('last_modified') ?></th>
                        </tr>

                        <?php $i = 1 ?>
                        <?php foreach ($dataProvider->getModels() as $item) { ?>
                            <tr class="<?= $i % 2 == 0 ? 'light-grren' : '' ?>">

                                <td class="table_data"><?= date_create($item->visit_date)->Format('d.m.Y') ?></td>
                                <td class="table_data"><?= $item->anketa_id ?></td>
                                <td class="table_data"><?= $item->project ?></td>
                                <td class="table_data"><?= $item->location_city ?></td>
                                <td class="table_data">
                                    <table>
                                        <td class="sum_column_data"><?= $item->pay_rate ?></td>
                                        <td class="sum_column_data"><?= $item->penalty ?></td>
                                        <td class="sum_column_data"><?= $item->bonuse ?></td>
                                        <td class="sum_column_data no-border"><?= $item->total_amount ?></td>
                                    </table>
                                </td>


                                <?= ($item->is_payed == 'Rejected')
                                    ? "<td class='titles_items relative'>
                                          <div id='i-have-a-tooltip' data-description='Если Вам не ясна причина отмены платежа, обратитесь в службу поддержки ТП'>
                                               <span class='status_icons'>Отклонен<i class='refresh-button' data-id='$item->id'></i></span>
                                          </div>
                                       </td>"
                                    : "<td class='titles_items'>" . $paymentStatus[$item->is_payed] ?? $item->is_payed . "</td>";
                                ?>

                                <?php if ($item->is_payed == 'Charity') { ?>
                                    <button class="selector-p" data-id="<?= $item->id ?>">
                                        Отменить
                                    </button>
                                <?php } ?>

                                <td class="titles_items"><?= date_create($item->last_modified)->Format('d.m.Y') ?></td>

                            </tr>
                            <?php $i++ ?>
                        <?php } ?>

                    </table>


                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'hideOnSinglePage' => true,
                        'prevPageLabel' => '&laquo; ',
                        'nextPageLabel' => ' &raquo;',

                        'linkOptions' => ['class' => 'page-number'],
                        'activePageCssClass' => 'active-page',
                    ]);
                    ?>

                </div>
            </div>
        </div>

        <script>
            setSelct(document.querySelectorAll('#pjax-table .i-custom-select'));
        </script>

        <?php Pjax::end() ?>

    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buttons-wrapper">

                <?= Html::a('<button class="download-btn">Выгрузить </button>',
                    ['excel/export-excel', 'type' => 'paym', 'userId' => Yii::$app->session->get('target-user-id')],
                    ['onclick' => 'uploadForm(event)'])
                ?>

            </div>
        </div>
    </div>
</div>

<script>
    // поведение при нажатии на карандаш
    function pencil() {
        const pencilStatus = $(".selector-p").css("display");
        if (pencilStatus === "block") {
            $(".selector-p").css("display", "none");
            $(".selector-status").css("display", "block");
        } else {
            $(".selector-p").css("display", "block");
            $(".selector-status").css("display", "none");
        }
    }

    // при пустом поле user не производить экспорт данных
    function uploadForm(event) {
        let userField = document.getElementById('user-field');
        if (userField.innerHTML.length < 1) {
            event.preventDefault();
            alert('Для этого действия необходимо выбрать пользователя!');
            return false;
        }
    }

    //все действия для передачи в контроллер
    var actions = [];

    $(function () {
        let id;
        $(".refresh-button").on('click', function () {
            id = $(this).data('id');
            $(this).parent().parent().text('Ожидает выплату');

            // console.log($(this).parent().prop("classList"))
            //добюавили значение в массив действий
            actions.push([id]);
        })
    })

    // действие при нажатии на кнопку сохранения
    $("#save-changes").click(function () {
        // alert(actions);
        if (actions.length > 0) {
            $("#admin-done").modal("show");
        }

    })

    // отправка форм на сервер
    $('#comment-form').on('beforeSubmit', function () {
        var data = $(this).serialize();
        // отправляем данные на сервер
        $.ajax({
            type: 'POST',
            url: '/admin/save',
            data: data,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            }
        }).done(function (data) {
            // данные сохранены
            console.log('success ', data);

            //при успешном ответе отправить ajax с вариантами ответов
            sendAjax();

            //прячем модальное  окно
            $('#admin-done').modal('hide');

            //меняем отображение полей
            $(".selector-p").css("display", "block");
            $(".selector-status").css("display", "none");
        }).fail(function () {
            // не удалось выполнить запрос к серверу
            console.log('Запрос не принят');
        });

        return false;
    });

    //отправка собранных данных на сервер
    function sendAjax() {

        $.ajax({
            type: 'post',
            url: '/admin/payment',
            data: {
                actions
            },
            success: function (response) {
                $.pjax.reload({container: '#content', async: false});
                console.log('Proceed successfully' + response);
            },
            error: function (response) {
                console.log(response, 'Proceed error');
            }
        });
    }

    //
    $(function () {
        $('.selector-p').on('click', function (e) {
            // function cancelCharity() {
            var id = $(this).attr('data-id');
            console.log(id);
            $.ajax({
                type: 'post',
                url: '/admin/site/cancel-charity',
                data: {
                    id
                },
                success: function (response) {
                    $.pjax.reload({container: '#content', async: false});
                    console.log('Cancelling successfully' + response);
                },
                error: function (response) {
                    console.log(response, 'Proceed error');
                }
            });
        });
    });

</script>

