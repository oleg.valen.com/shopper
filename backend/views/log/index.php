<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логирование действий пользователя';

$startDate = date('d-m-Y', strtotime(Yii::$app->session->get('log-start-date')));
$endDate = date('d-m-Y', strtotime(Yii::$app->session->get('log-end-date')));
$selectedDate = $startDate . ' - ' . $endDate;

?>

<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->has('target-user-id')) { ?>
    <div class="row">
        <div class="col-md-6">
            <?= $this->render('/partials/data-range', ['page' => 'index', 'selectedDate' => $selectedDate ?? '2020-09-10', 'filterModel' => $filterModel ?? []]) ?>
        </div>
        <div class="col-md-6">
            <div class="col-lg-6 ml-auto reports"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: flex-end; ">
                <?= Html::a('<button class="download-file">Выгрузить лог</button>',
                    ['excel/export-log']) ?>
            </div>
        </div>
    </div>
    <div class="table-over-wrapp">
        <?php } ?>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout'=>"{summary}\n{items}",
//            'layout'=>"{sorter}\n{summary}\n{items}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'user_id',
                    'value' => function ($data) use ($userData) {
                        return $userData->username;
                    }
                ],
                'visit_id',
                'action:ntext',
                [
                    'attribute' => 'created_at',
                    'value' => function ($data) {
                        return date('d-m-Y H:i:s', strtotime($data->created_at));
                    }
                ],
                'user_ip',
                //'updated_at',

//            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
            'hideOnSinglePage' => true,
            'prevPageLabel' => '&laquo; ',
            'nextPageLabel' => ' &raquo;',

            'linkOptions' => ['class' => 'page-number'],
            'activePageCssClass' => 'active-page',
        ]);
        ?>

    </div>

</div>
