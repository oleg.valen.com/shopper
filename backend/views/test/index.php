<table class="table table-bordered">
    <thead>
    <tr>
        <td><?= 'SecurityObjectUserID' ?></td>
        <td><?= 'Login' ?></td>
        <td><?= 'Name' ?></td>
        <td><?= 'FamilyName' ?></td>
        <td><?= 'Country' ?></td>
        <td><?= 'ShopperBonusAnswer' ?></td>
        <td><?= 'ShopperBonusComment' ?></td>
        <td><?= 'SurveyInstanceID' ?></td>
        <td><?= 'SurveyAcceptance' ?></td>
        <td><?= 'WorkFlowStepID' ?></td>
        <td><?= 'WorkFlowStatus' ?></td>
        <td><?= 'SurveyTitle' ?></td>
        <td><?= 'ClientName' ?></td>
        <td><?= 'LocationStoreID' ?></td>
        <td><?= 'LocationName' ?></td>
        <td><?= 'LocationAddress1' ?></td>
        <td><?= 'SurveyInstanceStatusID' ?></td>
        <td><?= 'SurveyStatusName' ?></td>
        <td><?= 'Date' ?></td>
        <td><?= 'PayRate' ?></td>
        <td><?= 'PrecalcPayrollItemsSum' ?></td>
        <td><?= 'PrecalcPayrollItemsCount' ?></td>
        <td><?= 'PayrollCurrency' ?></td>
        <td><?= 'IsOkForPayroll' ?></td>
        <td><?= 'IsHoldPayroll' ?></td>
        <td><?= 'IsOkForExport' ?></td>
        <td><?= 'HoldExport' ?></td>
        <td><?= 'Completed' ?></td>
        <td><?= 'ClientAccessStatusID' ?></td>
        <td><?= 'ClientAccessDescription' ?></td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($XMLresponse as $itemsList) { ?>
        <?php foreach ($itemsList as $item) { ?>
            <tr>
                <td><?= $item->SecurityObjectUserID ?? '-' ?></td>
                <td><?= $item->Login ?? '-' ?></td>
                <td><?= $item->Name ?? '-' ?></td>
                <td><?= $item->FamilyName ?? '-' ?></td>
                <td><?= $item->Country ?? '-' ?></td>
                <td><?= $item->ShopperBonusAnswer ?? '-' ?></td>
                <td><?= $item->ShopperBonusComment ?? '-' ?></td>
                <td><?= $item->SurveyInstanceID ?? '-' ?></td>
                <td><?= $item->SurveyAcceptance ?? '-' ?></td>
                <td><?= $item->WorkFlowStepID ?? '-' ?></td>
                <td><?= $item->WorkFlowStatus ?? '-' ?></td>
                <td><?= $item->SurveyTitle ?? '-' ?></td>
                <td><?= $item->ClientName ?? '-' ?></td>
                <td><?= $item->LocationStoreID ?? '-' ?></td>
                <td><?= $item->LocationName ?? '-' ?></td>
                <td><?= $item->LocationAddress1 ?? '-' ?></td>
                <td><?= $item->SurveyInstanceStatusID ?? '-' ?></td>
                <td><?= $item->SurveyStatusName ?? '-' ?></td>
                <td><?= $item->Date ?? '-' ?></td>
                <td><?= $item->PayRate ?? '-' ?></td>
                <td><?= $item->PrecalcPayrollItemsSum ?? '-' ?></td>
                <td><?= $item->PrecalcPayrollItemsCount ?? '-' ?></td>
                <td><?= $item->PayrollCurrency ?? '-' ?></td>
                <td><?= $item->IsOkForPayroll ?? '-' ?></td>
                <td><?= $item->IsHoldPayroll ?? '-' ?></td>
                <td><?= $item->IsOkForExport ?? '-' ?></td>
                <td><?= $item->HoldExport ?? '-' ?></td>
                <td><?= $item->Completed ?? '-' ?></td>
                <td><?= $item->ClientAccessStatusID ?? '-' ?></td>
                <td><?= $item->ClientAccessDescription ?? '-' ?></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>


<?php //} ?>
