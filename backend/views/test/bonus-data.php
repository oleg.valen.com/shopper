<table class="table table-bordered">
    <thead>
    <tr>
        <td>Date</td>
        <td>InstanceID</td>
        <td>SurveyFormName</td>
        <td>ClientName</td>
        <td>LocationAddress</td>
        <td>PayRate</td>
        <td>SurveyStatus</td>
        <td>PayrollItems</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($XMLresponse as $itemsList) { ?>
        <?php foreach ($itemsList as $item) { ?>
            <tr>
                <td><?= $item->Date ?? '-' ?></td>
                <td><?= $item->InstanceID ?? '-' ?></td>
                <td><?= $item->SurveyFormName ?? '-' ?></td>
                <td><?= $item->ClientName ?? '-' ?></td>
                <td><?= $item->LocationAddress ?? '-' ?></td>
                <td><?= $item->PayRate ?? '-' ?></td>
                <td><?= $item->SurveyStatus ?? '-' ?></td>
                <td><?= $item->PayrollItems ?? '-' ?></td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>
