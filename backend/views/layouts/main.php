<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\Pjax;

$session = Yii::$app->session;
//$session->destroy();

$userId = $session->has('userId') ? $session->get('userId') : 1;
$userLogin = $session->has('userLogin') ? $session->get('userLogin') : '';

AppAsset::register($this);

$cookies = Yii::$app->request->cookies;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<?php if (!Yii::$app->user->isGuest) { ?>

    <header id="header" class="header">
        <div class="container">
            <div class="row above-menu">
                <div class="col-lg-3">
                    <a class="shopmetrics" href="https://4serviceru.shopmetrics.com" target="_blank"><img
                                class="return-arrow" src="/img/return-arrow.png" alt="">shopmetrics</a>
                </div>
                <div class="col-lg-3 ml-auto">
                    <a class="rules" href="<?= Url::to(['rules/rules-index']) ?>">Правила</a>
                    <?= Html::a(Html::img('/img/out.png', ['class' => 'out']),
                        ['/site/logout'],
                        [
                            'data' => ['method' => 'post'],
                            'class' => 'white',
//                            'linkOptions' => ['data-method' => 'post'],

                        ])
                    ?>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark">
                <a class="navbar-brand" href="<?= Url::to(['main/index']) ?>"><img class="logo" src="/img/logo.png"
                                                                                   alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= Url::to(['main/index']) ?>">ГЛАВНАЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['main/payment']) ?>">ВЫПЛАТЫ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['main/accumulation']) ?>">НАКОПЛЕНИЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['main/bonus']) ?>">БОНУСНЫЕ СТАВКИ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['main/charity']) ?>">БЛАГОТВОРИТЕЛЬНОСТЬ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['main/bonuse-statistick']) ?>">СТАТИСТИКА</a>
                        </li>
                    </ul>
                    <div class="d-flex icons ml-auto">
                        <div class="avatar">
                            <div class="avatar-pop-up i-pop-up">
                                <div class="name">
                                    <?= $session['user']['family_name'] . ' ' . $session['user']['name'] ?>
                                    <img src="/img/county-flags/ukraine.svg" alt="" class="flag">
                                </div>
                                <div class="any-fields">
                                    <p>SUPREME</p>
                                    <p>Ставка: 18%</p>
                                    <p>Бонусов: 4650 RUB</p>
                                    <p>Способ оплаты: Росбанк</p>
                                    <p>Реквизиты: 4444 4444 4444 4444</p>
                                    <p>super.puper.vitaliya@vitalua.com</p>
                                </div>
                                <div class="bottom-green-help-info">
                                    Изменить ЛЮБУЮ личную <br>
                                    информацию можно <br>
                                    в shopmetrics <br>
                                </div>
                            </div>
                        </div>

                        <?php Pjax::begin(['id' => 'pjax-wallet']); ?>

                        <div class="wallet">
                            <img src="/img/wallet.png" alt="wellet">
                            <div class="wallet-pop-up i-pop-up" id="wallet-items">

                                <?php if (isset($session['wallet'][0])) { ?>

                                    <table class="wallet-table-header">
                                        <thead>
                                        <tr>
                                            <th>Номер анкеты</th>
                                            <th>Сумма</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div class="walet-table-wrapper waller-yable-scroller">
                                        <table class="wallet-table-body">
                                            <tbody>

                                            <?php $summ = 0 ?>
                                            <?php foreach ($session['wallet'] as $item) { ?>
                                                <tr id="walet-<?= $item['id'] ?>">
                                                    <td><?= $item->anketa_id ?></td>
                                                    <td><?= $item->amount ?></td>
                                                    <td>
                                                        <a class="remove-item" data-id="<?= $item->id ?>"
                                                           href='<?= Url::to(["/main/delete/?id={$item->id}"]) ?>'>X</a>
                                                    </td>
                                                    <?php $summ += $item['amount'] ?>
                                                </tr>
                                            <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="bouns-info flex j-s-b a-c">
                                        <div>БОНУС ЗА ВЫВОД СРЕДСТВ</div>
                                        <div class="bonus-bar">
                                            <div class="bonus-bar-line" style="width: calc( 100 / 150 * 100% )"></div>
                                        </div>
                                    </div>
                                    <div class="bonus-bottom-info flex j-s-b">
                                        <div>ВСЕГО</div>
                                        <div><?= $summ ?> RUB</div>
                                    </div>
                                    <div class="bonus-bottom-btn flex j-c">
                                        <button class="i-btn">
                                            <a class="i-btn" href="<?= Url::to(['main/pay']) ?>">Оправить на оплату</a>
                                        </button>
                                    </div>

                                <?php } else { ?>
                                    <p><strong>Кошелек пуст</strong></p>
                                <?php } ?>

                            </div>
                        </div>

                        <?php Pjax::end(); ?>

                    </div>
                </div>
            </nav>
        </div>
    </header>

<?php } ?>

<div class="">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php if (!Yii::$app->user->isGuest) { ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 contacnt">
                    <p class="support">ПОДДЕРЖКА:</p>
                    <p class="contact-info">support@gmail.com</p>
                    <p class="contact-info">+380 65 889 90 90</p>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
