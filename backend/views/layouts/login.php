<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$session = Yii::$app->session;
$session->destroy();

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="main">
    <div class="main-container">
        <header id="header" class="header-login">
            <a href=""><img class="logo" src="/img/logo.png" alt="logo"></a>
        </header>
        <div class="login-form-wrapper">
            <div class="door">
            </div>
            <div class="login">
                <div class="title-wrapper">
                    <p>ВХОД</p>
                </div>
                <div class="form-wrapper">

                    <?= $content ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
