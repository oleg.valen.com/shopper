<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

$session = Yii::$app->session;

$userId = $session->has('userId') ? $session->get('userId') : 1;
$userLogin = $session->has('userLogin') ? $session->get('userLogin') : '';

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php if (!Yii::$app->user->isGuest) { ?>

    <header id="header" class="header">
        <div class="container">
            <div class="row above-menu">
                <div class="col-lg-3">
                    <a href="/admin/">
                        <button class="admin_button">ADMIN</button>
                    </a>
                    <p class="admin_login"><?= Yii::$app->user->identity->username ?></p>
                </div>
                <div class="col-lg-3 ml-auto">
                    <?= Html::a(Html::img('/img/out.png', ['class' => 'out admin-out']), ['/site/logout'],
                        [
                            'data' => ['method' => 'post'],
                            'class' => 'white',
                        ])
                    ?>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark" style="padding: .5rem 0">
                <a class="navbar-brand" href="<?= Url::to(['site/index']) ?>">
                    <img class="logo" src="/img/logo.png" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= Url::to(['site/index']) ?>">ГЛАВНАЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['site/accumulation']) ?>">НАКОПЛЕНИЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="<?= Url::to(['site/payment'], ['login' => 'admin']) ?>">ВЫПЛАТЫ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="<?= Url::to(['site/payed'], ['login' => 'admin']) ?>">ОПЛАЧЕННЫЕ</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ОТЧЕТНОСТЬ
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?= Url::to(['site/bonus'], ['login' => 'admin']) ?>">БОНУСЫ</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/bonuse-statistick']) ?>">СТАТИСТИКА</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/mass']) ?>">МАССОВЫЕ</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/accumulation-ankets']) ?>">АНКЕТЫ В НАКОПЛЕНИИ</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/ankets-array']) ?>">МАССИВ АНКЕТ</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/monthly-report']) ?>">ОТЧЕТ ПО МЕСЯЦАМ</a>
                                <a class="dropdown-item" href="<?= Url::to(['site/revise-bonus']) ?>">СВЕРКА БОНУСОВ</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                НАСТРОЙКИ
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?= Url::to(['faq/index']) ?>">F.A.Q.</a>
                                <a class="dropdown-item" href="<?= Url::to(['slider/index']) ?>">СЛАЙДЕР</a>
                            </div>
                        </li>
                        <!--                        <li class="nav-item">-->
                        <!--                            <a class="nav-link" href="-->
                        <? //= Url::to(['faq/index']) ?><!--">ВОПРОСЫ/ОТВЕТЫ</a>-->
                        <!--                        </li>-->

                        <style>
                            #form {
                                display: none;
                                padding-right: 0px;
                            }

                            .form-control {
                                display: block;
                                width: 151px;
                                margin: 0 auto;
                                height: calc(1.5em + .75rem + 2px);
                                padding: .375rem .75rem;
                                font-size: 10px;
                                font-weight: 400;
                                line-height: 1.5;
                                color: #495057;
                                background-color: #fff;
                                background-clip: padding-box;
                                border: 1px solid #ced4da;
                                border-radius: .25rem;
                                transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
                            }

                            .dropdown-menu {
                                left: auto;
                                right: 0;
                            }
                        </style>

                        <li class="nav-item">
                            <a class="nav-link" href="#" id="search">К ЮЗЕРУ</a>
                            <div id="form" class="dropdown-menu pull-right">

                                <form class="navbar-form navbar-right" action="/admin/site/finduser" method="get">
                                    <div class="form-group">
                                        <input type="text" name="login" class="form-control"
                                               placeholder="Введи логин пользователя">
                                    </div>
                                </form>

                            </div>
                        </li>

                        <div class="user-nav-icons" style="display: flex;">
                            <!--                            <div class="avatar">-->
                            <!--                                <div class="avatar-pop-up i-pop-up">-->
                            <!--                                    <div class="name">-->
                            <!--                                        <p style="text-transform: uppercase">Name</p>-->
                            <!--                                        <img src="/img/county-flags/russia.svg" alt="" class="flag">-->
                            <!--                                    </div>-->
                            <!--                                    <div class="any-fields">-->
                            <!--                                        <p>Ставка: 111 %</p>-->
                            <!--                                        <p>Бонусов: 111 RUB</p>-->
                            <!--                                        <p>Способ оплаты: test</p>-->
                            <!--                                    </div>-->
                            <!--                                    <div class="bottom-green-help-info">-->
                            <!--                                        <a href="https://4serviceru.shopmetrics.com/document.asp?alias=mystadministration.shopper.profile&MODE=SELECT&frmStep=3"-->
                            <!--                                           target="_blank">Редактировать профиль</a>-->
                            <!--                                        <a href="#" id="image-apply"><p>Сменить аватар</p></a>-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->

                            <?php if (Yii::$app->session->has('target-user-id')) { ?>

                                <?= \backend\components\HeaderWidget::widget() ?>

                            <?php } ?>
                        </div>

                        <script>
                            $(document).ready(function () {
                                var searchBlock = $('#form');
                                $(document).on('click', '#search', function () {
                                    searchBlock.slideToggle();
                                    return false;
                                });
                            });
                        </script>

                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <script>
        $('.dropdown-toggle').dropdown()
    </script>

    <div class="bonus-wrapper" style="">
        <section class="under-menu" style="position: relative;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="d-flex justify-content-between align-items-baseline"
                            style="margin-bottom: 0px; padding: 5px 0">

                            <?= \backend\components\BonusesWidget::widget() ?>

                            <div class="account">
                                <?php if (Yii::$app->session->has('target-user-id')) { ?>
                                    <a class="log-link"
                                       href="<?= Url::to(['log/index', 'user_id' => Yii::$app->session->get('target-user-id')]) ?>">ЛОГ
                                        ДЕЙСТВИЙ</a>
                                <?php } ?>
                                <div class="user-login">
                                    <span class="account_item">УЧЕТНАЯ ЗАПИСЬ:</span>
                                    <span class="account_item login">
                                        <?= Yii::$app->session->get('target-user-login', '') ?>
                                    </span>
                                </div>

                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php } ?>

<div class="">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php if (!Yii::$app->user->isGuest) { ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-between">
                    <div class="rules-wrapper">
                        <a class="rules" href="<?= Url::to(['faq/index']) ?>">FAQ. Вопрос/Ответ</a>
                    </div>
                    <div class="contacnt">
                        <p class="support">ПОДДЕРЖКА: <span class="support_mail">support@gmail.com</span></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php $this->endBody() ?>

<style>
    .header .admin-out {
        margin-right: 0;
    }
</style>
</body>
</html>
<?php $this->endPage() ?>
